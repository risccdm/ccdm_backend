package com.ccdm.script.model;

import java.util.List;

import com.ccdm.instance.service.helper.vo.InstanceHelperVO;

public class TriggerScriptRequest {

	private List<InstanceHelperVO> instanceHelperVOList;
	private String scriptCommandId;
	
	public List<InstanceHelperVO> getInstanceHelperVOList() {
		return instanceHelperVOList;
	}
	public void setInstanceHelperVOList(List<InstanceHelperVO> instanceHelperVOList) {
		this.instanceHelperVOList = instanceHelperVOList;
	}
	public String getScriptCommandId() {
		return scriptCommandId;
	}
	public void setScriptCommandId(String scriptCommandId) {
		this.scriptCommandId = scriptCommandId;
	}

}
