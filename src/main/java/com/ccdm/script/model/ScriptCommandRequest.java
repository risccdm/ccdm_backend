package com.ccdm.script.model;

import java.util.List;

public class ScriptCommandRequest {
	
	private String scriptId;
	private String scriptName;
	private List<String> executionCommands;

	public String getScriptId() {
		return scriptId;
	}

	public void setScriptId(String scriptId) {
		this.scriptId = scriptId;
	}

	public List<String> getExecutionCommands() {
		return executionCommands;
	}

	public void setExecutionCommands(List<String> executionCommands) {
		this.executionCommands = executionCommands;
	}

	public String getScriptName() {
		return scriptName;
	}

	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	
}
