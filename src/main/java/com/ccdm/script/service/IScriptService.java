package com.ccdm.script.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ccdm.agent.model.ExecutionScriptResponse;
import com.ccdm.elastic.vo.ExecutionScript;
import com.ccdm.elastic.vo.ScriptOutputLog;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.script.model.ScriptCommandRequest;
import com.ccdm.script.model.TriggerScriptRequest;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;

public interface IScriptService {

	void saveScripts(Integer userId, HttpServletRequest httpServletRequest) throws CCDMException;

	void saveScripts(Integer userId, ScriptCommandRequest scriptCommandRequest);

	List<ExecutionScript> getAllExecutionScripts(Integer userId);

	ScriptCommandRequest getScriptByScriptId(String scriptId) throws CCDMException;

	void deleteScriptByScriptId(String scriptId) throws CCDMException;

	void downLoadScriptCommands(String scriptId, HttpServletResponse response) throws CCDMException;

	ExecutionScript getScriptCommandByScriptId(String scriptId);

	void saveLogMessage(String scriptId, ExecutionScriptResponse executionScriptResponse, InstanceInfo instanaceInfo);

	List<ScriptOutputLog> getScriptOutputLogByInstanceId(String instanceId);
}
