package com.ccdm.script.service;

import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ccdm.agent.model.ExecutionScriptResponse;
import com.ccdm.common.enums.ScriptTypeEnum;
import com.ccdm.elastic.repository.executionscript.IExecutionScriptRepository;
import com.ccdm.elastic.repository.scriptlog.IScriptOutputLogRepository;
import com.ccdm.elastic.vo.ExecutionScript;
import com.ccdm.elastic.vo.ScriptOutputLog;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.script.model.ScriptCommandRequest;
import com.ccdm.vo.InstanceInfo;

@Component
@Transactional
public class ScriptServiceImpl implements IScriptService {

	private static final String MULTIPART_FORM_DATA = "multipart/form-data";
	private static final String SEMI_COLON_DELIMITER = ";";
	private static final String SCRIPT_NAME = "scriptName";

	private static final Logger LOGGER = LoggerFactory.getLogger(ScriptServiceImpl.class);

	@Autowired
	private IExecutionScriptRepository executionScriptRepository;
	@Autowired
	private IScriptOutputLogRepository scriptOutputLogRepository;
	
	@SuppressWarnings("rawtypes")
	@Override
	public void saveScripts(Integer userId, HttpServletRequest httpServletRequest) throws CCDMException {
		if(httpServletRequest == null) {
			LOGGER.error(ErrorCodes.FILE_UPLOAD_REQ_INVALID.getErrorMessage());
			throw new CCDMException(ErrorCodes.FILE_UPLOAD_REQ_INVALID);
		}
		ExecutionScript executionScript = new ExecutionScript();
		executionScript.setUserId(userId);
		executionScript.setScriptType(ScriptTypeEnum.FILE.getType());
		executionScript.setScriptName(httpServletRequest.getParameter(SCRIPT_NAME));
		if(httpServletRequest.getContentType().toLowerCase().indexOf(MULTIPART_FORM_DATA) > -1 )
		{
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) httpServletRequest;
			Set set = multipartHttpServletRequest.getFileMap().entrySet();
			Iterator itr = set.iterator();
			while(itr.hasNext())
			{
				Map.Entry entry = (Map.Entry) itr.next();
				MultipartFile file = (MultipartFile) entry.getValue();
				
				if(file != null)
				{
					try {
		            byte[] bytes = file.getBytes();
		            String commandsInFile = new String(bytes, "UTF-8");
		            executionScript.setExecutableCommands(commandsInFile);
		            executionScript.setFileName(file.getName());
		            executionScriptRepository.save(executionScript);
		            
		        } catch (Exception e) {
		        	LOGGER.error("You failed to upload  => " + e.getMessage());
		        	throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		        }
				}
				}
			}
		
	}

	@Override
	public void saveScripts(Integer userId, ScriptCommandRequest scriptCommandRequest) {
		if(scriptCommandRequest == null || scriptCommandRequest.getExecutionCommands() == null
				|| scriptCommandRequest.getExecutionCommands().isEmpty()) {
			LOGGER.error("Command To Execute is Empty");
		}
		ExecutionScript executionScript = getExecutionScriptObject(scriptCommandRequest.getScriptId());
		executionScript.setUserId(userId);
		executionScript.setScriptType(ScriptTypeEnum.COMMAND.getType());
		executionScript.setScriptName(scriptCommandRequest.getScriptName());
		
		StringBuffer executionCommand = new StringBuffer();
		for(String commands : scriptCommandRequest.getExecutionCommands()) {
			if(commands.trim().isEmpty()) {
				continue;
			}
			executionCommand.append(appendDelimitor(commands));
		}
		executionScript.setExecutableCommands(executionCommand.toString());
		executionScriptRepository.save(executionScript);
	}

	private ExecutionScript getExecutionScriptObject(String scriptId) {
		if(scriptId != null) {
			return executionScriptRepository.findOne(scriptId);
		}
		return new ExecutionScript();
	}

	private String appendDelimitor(String commands) {
		if(!(commands.substring(commands.length()-1)).equals(SEMI_COLON_DELIMITER)) {
			return commands + SEMI_COLON_DELIMITER;
		}
		return commands;
	}

	public void saveLogMessage(String scriptId, ExecutionScriptResponse executionScriptResponse, InstanceInfo instanaceInfo) {
		ScriptOutputLog outputLog = new ScriptOutputLog();
		outputLog = new ScriptOutputLog();
		outputLog.setInstanceId(instanaceInfo.getInstanceId());
		outputLog.setScriptId(scriptId);
		outputLog.setStatus(executionScriptResponse.getStatus().name());
		outputLog.setScriptOutput(executionScriptResponse.getScriptOutput());
		scriptOutputLogRepository.save(outputLog);
	}

	
	private void validateScriptCommandId(String scriptCommandId) throws CCDMException {
		if(scriptCommandId == null ||
				scriptCommandId.trim().isEmpty()) {
			LOGGER.error("TriggerScript Request ScriptCommandId is Empty");
			throw new CCDMException(ErrorCodes.TRIGGER_REQUEST_COMMANDID_EMPTY);
		}
	}

	@Override
	public List<ExecutionScript> getAllExecutionScripts(Integer userId) {
		List<ExecutionScript> executionScripts = executionScriptRepository.findAllByUserId(userId);
		return executionScripts;
	}

	@Override
	public ScriptCommandRequest getScriptByScriptId(String scriptId) throws CCDMException {
		validateScriptCommandId(scriptId);
		ExecutionScript executionScript = getScriptCommandByScriptId(scriptId);
		ScriptCommandRequest commandRequest = buildScriptCommandRequest(executionScript);
		return commandRequest;
	}

	public ExecutionScript getScriptCommandByScriptId(String scriptId) 
	{
		ExecutionScript executionScript = executionScriptRepository.findOne(scriptId);
		return executionScript;
	}

	private ScriptCommandRequest buildScriptCommandRequest(ExecutionScript executionScript) {
		ScriptCommandRequest commandRequest = new ScriptCommandRequest();
		commandRequest.setScriptName(executionScript.getScriptName());
		String[] splittedCommands = executionScript.getExecutableCommands().split(SEMI_COLON_DELIMITER);
		List<String> commandList = new ArrayList<String>();
		for(String command : splittedCommands) {
			commandList.add(command);
		}
		commandRequest.setExecutionCommands(commandList);
		commandRequest.setScriptId(executionScript.getId());
		return commandRequest;
	}

	@Override
	public void deleteScriptByScriptId(String scriptId) throws CCDMException {
		validateScriptCommandId(scriptId);
		executionScriptRepository.delete(scriptId);
	}

	@Override
	public void downLoadScriptCommands(String scriptId, HttpServletResponse httpServletResponse) throws CCDMException 
	{
		try
		{
			validateScriptCommandId(scriptId);
			ExecutionScript executionScript = getScriptCommandByScriptId(scriptId);
			
			String fileName = "ScriptFile.txt";
			String scriptCommands = "# No Commands";
			if(executionScript != null)
			{
				fileName = executionScript.getFileName() != null ? executionScript.getFileName() : fileName;
				scriptCommands = executionScript.getExecutableCommands();
			}
			
			OutputStream out = httpServletResponse.getOutputStream();
			httpServletResponse.setContentType("text/plain; charset=utf-8");
			httpServletResponse.addHeader("Content-Disposition","attachment; filename=\"" + fileName + "\"");
			out.write(scriptCommands.getBytes(Charset.forName("UTF-8")));
			out.flush();
			out.close();
		}
		catch(Exception e) {
			throw new CCDMException(ErrorCodes.ERROR_FILE_UPLOAD);
		}
	}

	@Override
	public List<ScriptOutputLog> getScriptOutputLogByInstanceId(String instanceId) {
		return scriptOutputLogRepository.findAllByInstanceId(instanceId);
	}
}
