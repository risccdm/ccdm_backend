package com.ccdm.script.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.agent.AgentService;
import com.ccdm.agent.model.ExecutionScriptRequest;
import com.ccdm.agent.model.ExecutionScriptResponse;
import com.ccdm.base.controller.BaseController;
import com.ccdm.elastic.vo.ExecutionScript;
import com.ccdm.elastic.vo.ScriptOutputLog;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.script.model.ScriptCommandRequest;
import com.ccdm.script.service.IScriptService;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;

@RestController
@RequestMapping(value = "/script")
public class ScriptController extends BaseController 
{
	@Autowired
	private AgentService agentService;

	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}

	@Autowired
	private IScriptService scriptService;
	
	@RequestMapping(value = "/file/upload", method = RequestMethod.POST)
	public @ResponseBody CCDMResponse saveScriptFile(HttpServletRequest httpServletRequest) throws CCDMException {
		AppUser appUser = getLoggedInUser();
        scriptService.saveScripts(appUser.getUserId(), httpServletRequest);
		return getCCDMResponse();
	}
	
	@RequestMapping(value = "/command/save", method = RequestMethod.POST)
	public @ResponseBody CCDMResponse saveScript(@RequestBody ScriptCommandRequest scriptCommandRequest) throws CCDMUnauthorizedException {
		AppUser appUser = getLoggedInUser();
		scriptService.saveScripts(appUser.getUserId(), scriptCommandRequest);
		return getCCDMResponse();
	}
	
	@RequestMapping(value = "/commands", method = RequestMethod.GET)
	public @ResponseBody CCDMResponse getAllExecutionScripts() throws CCDMUnauthorizedException {
		AppUser appUser = getLoggedInUser();
		List<ExecutionScript> executionScripts = scriptService.getAllExecutionScripts(appUser.getUserId());
		return getCCDMResponse(executionScripts);
	}
	
	@RequestMapping(value = "/{scriptId}", method = RequestMethod.GET)
	public @ResponseBody CCDMResponse getScriptByScriptId(@PathVariable(value = "scriptId") String scriptId) throws CCDMException {
		getLoggedInUserId();
		ScriptCommandRequest executionScript = scriptService.getScriptByScriptId(scriptId);
		return getCCDMResponse(executionScript);
	}
	
	@RequestMapping(value = "/{scriptId}", method = RequestMethod.DELETE)
	public @ResponseBody CCDMResponse deleteScriptByScriptId(@PathVariable(value = "scriptId") String scriptId) throws CCDMException {
		getLoggedInUserId();
		scriptService.deleteScriptByScriptId(scriptId);
		return getCCDMResponse();
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public void downLoadScriptCommands(@RequestParam(value = "scriptId") String scriptId, HttpServletResponse response) throws CCDMException, IOException 
	{
		getLoggedInUserId();
		scriptService.downLoadScriptCommands(scriptId, response);
	}
	
	public @ResponseBody CCDMResponse getScriptCommandByScriptId(String scriptId) throws CCDMException {
		getLoggedInUserId();
		ExecutionScript executionScript = scriptService.getScriptCommandByScriptId(scriptId);
		return getCCDMResponse(executionScript);
	}
	
	public ExecutionScriptResponse executeScriptOnInstance(ExecutionScriptRequest executionScriptRequest) throws CCDMException
	{
		ExecutionScriptResponse scriptResponse = agentService.executeScript(executionScriptRequest);
		return scriptResponse;
	}
	
	public void saveScriptExectionLog(String scriptId, ExecutionScriptResponse executionScriptResponse, InstanceInfo instanceInfo)
	{
		scriptService.saveLogMessage(scriptId, executionScriptResponse, instanceInfo);
	}
	
	public List<ScriptOutputLog> getScriptOutputLogByInstanceId(String instanceId) {
		return scriptService.getScriptOutputLogByInstanceId(instanceId);
	}
}
