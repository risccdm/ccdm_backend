package com.ccdm.solutions.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.solution.model.CustomerInfoModel;
import com.ccdm.solution.model.DeployModel;
import com.ccdm.solution.model.SolutionClientMapModel;
import com.ccdm.solution.model.SolutionListResponse;
import com.ccdm.solution.model.SolutionRequest;
import com.ccdm.solution.model.SolutionResponse;
import com.ccdm.solution.model.SolutionSlaRequest;
import com.ccdm.solutions.service.SolutionService;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.Solution;

@RestController
@RequestMapping(value = "/solution")
public class SolutionController extends BaseController {

	@Autowired
	private SolutionService solutionService;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}

	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public CCDMResponse getSolutions() throws CCDMException{
		AppUser appUser = getLoggedInUser();
		List<SolutionListResponse> listResponse = solutionService.getSolutions(appUser);
		return getCCDMResponse(listResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/vmTemplates", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getVMTemplates() throws CCDMUnauthorizedException{
		getLoggedInUser();
		return getResponseEntity(solutionService.getVMTemplates());
	}
	
	@RequestMapping(value = "/loadBals", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getLBTemplates() throws CCDMUnauthorizedException{
		getLoggedInUser();
		return getResponseEntity(solutionService.getLBTemplates());
	}
	
	@RequestMapping(value = "/scripts", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getScripts() throws CCDMUnauthorizedException{
		getLoggedInUser();
		return getResponseEntity(solutionService.getScripts());
	}
	
	@RequestMapping(value = "/{solutionId}", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getSolutionBySolutionId(@PathVariable(value = "solutionId")Integer solutionId) throws CCDMException{
		AppUser appUser = getLoggedInUser();
		SolutionResponse solutionResponse = solutionService.getSolutionResponseBySolutionId(solutionId, appUser);
		return getResponseEntity(solutionResponse);
	}
	
	@RequestMapping(value = "/deploy", method = RequestMethod.POST)
	public CCDMResponse deploySolution(@RequestBody DeployModel deployModel) throws CCDMException 
	{
		AppUser appUser = getLoggedInUser();
		solutionService.deploySolution(deployModel, appUser);
		return getCCDMResponse("SUCCESS", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public CCDMResponse  save(@RequestBody SolutionRequest solutionRequest) throws CCDMException{
		Solution solution = saveOrUpdateSolution(solutionRequest);
		return getCCDMResponse(solution.getSolutionId());
	}

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public CCDMResponse updateSolution(@RequestBody SolutionRequest solutionRequest)throws CCDMException{
		Solution solution = saveOrUpdateSolution(solutionRequest);
		return getCCDMResponse(solution.getSolutionId());
	}
	
	@RequestMapping(value = "/sla", method = RequestMethod.POST)
	public CCDMResponse updateSlaInSolution(@RequestBody SolutionSlaRequest solutionSlaRequest) throws CCDMException
	{
		solutionService.saveSlaBySolutionId(solutionSlaRequest, getLoggedInUser());
		return getCCDMResponse(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/solutionClientMap", method = RequestMethod.GET) 
	public CCDMResponse getSolutionClientMap() throws CCDMException
	{
		SolutionClientMapModel solutionClientMapModel  = solutionService.getSolutionClientMapByUserId(getLoggedInUserId());
		return getCCDMResponse(solutionClientMapModel, HttpStatus.OK);
	}

	/* ******************* Non Mapping Methods ****************** */
	public Solution getSolutionById(Integer solutionId)
	{
		return solutionService.getsolutionById(solutionId);
	}
	
	private Solution saveOrUpdateSolution(SolutionRequest solutionRequest)
			throws CCDMUnauthorizedException, CCDMException {
		AppUser appUser = getLoggedInUser();
		Solution solution = solutionService.saveSolution(solutionRequest, appUser);
		return solution;
	}	
	
	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public CCDMResponse getCustomerList() throws CCDMException {
		AppUser appUser = getLoggedInUser();
		List<CustomerInfoModel> customerInfoModelList = solutionService.getCustomerList(appUser);
		return getCCDMResponse(customerInfoModelList);
	}
	public void saveSolution(Solution solution) {
		solutionService.saveOrUpdateSolution(solution);
	}
}
