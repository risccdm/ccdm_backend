package com.ccdm.solutions.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.solution.model.CustomerInfoModel;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.CustomerInfo;
import com.ccdm.vo.Solution;
import com.ccdm.vo.SolutionClientMap;
import com.ccdm.vo.SolutionVmMap;
import com.ccdm.vo.WebServerProvider;

@Repository
public class SolutionDaoImpl extends BaseDAOImpl implements SolutionDao 
{
	@SuppressWarnings("unchecked")
	@Override
	public List<Solution> getAllSolutionByClientId(AppUser appUser) {
		List<Solution> solutions = getSession().createCriteria(Solution.class)
								   .setFetchMode("notification", FetchMode.JOIN)
								   .setFetchMode("slaType", FetchMode.JOIN)
								   .add(Restrictions.eq("clientId", appUser.getClientId())).list();
		return solutions;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SolutionVmMap> getSolutionVMMapBySolutionId(Integer solutionId) {
		List<SolutionVmMap> vmMaps = getSession().createCriteria(SolutionVmMap.class)
									 .setFetchMode("viewModel", FetchMode.JOIN)
									 .add(Restrictions.eq("solution.solutionId", solutionId))
									 .add(Restrictions.eq("isDeleted", false)).list();
		return vmMaps;
	}
	
	@Override
	public Solution saveSolution(Solution solution)
	{
		super.saveOrUpdate(solution);
		return solution;
	}
	
	public SolutionClientMap saveSolutionClientMap(SolutionClientMap clientMap) {
		super.saveOrUpdate(clientMap);
		return clientMap;
	}
	
	@Override
	public void saveSolutionVmMap(SolutionVmMap solutionVmMap){
		super.saveOrUpdate(solutionVmMap);
	}

	@Override
	public Solution getSolutionBySolutionId(Integer solutionId, AppUser appUser){
		Solution solution = (Solution) getSession().createCriteria(Solution.class)
									.setFetchMode("slaType", FetchMode.JOIN)
									.add(Restrictions.eq("clientId", appUser.getClientId()))
									.add(Restrictions.idEq(solutionId)).uniqueResult();
		return solution;
	}

	@Override
	public Solution getSolutionById(Integer solutionId) {
		Solution solution = (Solution) getSession().createCriteria(Solution.class)
							.setFetchMode("notification", FetchMode.JOIN)
							.setFetchMode("slaType", FetchMode.JOIN)
							.add(Restrictions.idEq(solutionId)).uniqueResult();
		return solution;
	}

	@Override
	public void commitTransaction() 
	{
		super.commitTraction();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SolutionClientMap> getSolutionClientMapByUserId(int loggedInUserId) 
	{
		return getSession().createCriteria(SolutionClientMap.class)
					.setFetchMode("solution", FetchMode.JOIN)
					.createAlias("solution", "sol")
					.setFetchMode("customerInfo", FetchMode.JOIN).list();
//					.add(Restrictions.eq("sol.appUser.userId", loggedInUserId)).list();
	}
	
	@Override
	public CustomerInfo getCustomerInfoById(Integer customerId) {
		CustomerInfo customerInfo =(CustomerInfo) getSession().createCriteria(CustomerInfo.class)
									.add(Restrictions.idEq(customerId)).uniqueResult();
		return customerInfo;
	}

	@Override
	public CustomerInfo saveCustomerInfo(CustomerInfo customerInfo) {
		super.save(customerInfo);
		return customerInfo;
	}

	@Override
	public WebServerProvider getProviderById(Integer providerId) {
		WebServerProvider provider = (WebServerProvider) getSession().createCriteria(WebServerProvider.class)
										.add(Restrictions.idEq(providerId)).uniqueResult();
		return provider;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CustomerInfoModel> getCustomerInfoModel(AppUser appUser) {
		Criteria criteria = getSession().createCriteria(CustomerInfo.class)
								.setProjection(Projections.projectionList()
								.add(Projections.property("id"), "customerId")
								.add(Projections.property("customerName"), "customerName"))
								.setResultTransformer(Transformers.aliasToBean(CustomerInfoModel.class));
		return criteria.list();
	}
}
