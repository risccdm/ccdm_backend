package com.ccdm.solutions.dao;

import java.util.List;

import com.ccdm.solution.model.CustomerInfoModel;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.CustomerInfo;
import com.ccdm.vo.Solution;
import com.ccdm.vo.SolutionClientMap;
import com.ccdm.vo.SolutionVmMap;
import com.ccdm.vo.WebServerProvider;

public interface SolutionDao 
{
	List<Solution> getAllSolutionByClientId(AppUser appUser);

	List<SolutionVmMap> getSolutionVMMapBySolutionId(Integer solutionId);

	Solution saveSolution(Solution solution);

	void saveSolutionVmMap(SolutionVmMap solutionVmMap);

	SolutionClientMap saveSolutionClientMap(SolutionClientMap clientMap);

	Solution getSolutionById(Integer solutionId);

	Solution getSolutionBySolutionId(Integer solutionId, AppUser appUser);
	
	void commitTransaction();

	List<SolutionClientMap> getSolutionClientMapByUserId(int loggedInUserId);

	CustomerInfo getCustomerInfoById(Integer customerId);

	CustomerInfo saveCustomerInfo(CustomerInfo customerInfo);

	WebServerProvider getProviderById(Integer providerId);

	List<CustomerInfoModel> getCustomerInfoModel(AppUser appUser);
}

