package com.ccdm.solutions.service;

import java.util.List;
import java.util.Map;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.solution.model.CustomerInfoModel;
import com.ccdm.solution.model.DeployModel;
import com.ccdm.solution.model.ScriptModel;
import com.ccdm.solution.model.SolutionClientMapModel;
import com.ccdm.solution.model.SolutionListResponse;
import com.ccdm.solution.model.SolutionRequest;
import com.ccdm.solution.model.SolutionResponse;
import com.ccdm.solution.model.SolutionSlaRequest;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.Solution;

public interface SolutionService 

{
	List<Map<String, String>> getVMTemplates() throws CCDMUnauthorizedException;
	
	List<Map<String, String>> getLBTemplates() throws CCDMUnauthorizedException;			
	
	List<ScriptModel> getScripts() throws CCDMUnauthorizedException;

	List<SolutionListResponse> getSolutions(AppUser appUser) throws CCDMException;

	Solution saveSolution(SolutionRequest solutionRequest, AppUser appUser) throws CCDMException;
	
	Solution getSolutionBySolutionId(Integer solutionId, AppUser appUser) throws CCDMException;

	SolutionResponse getSolutionResponseBySolutionId(Integer solutionId, AppUser appUser) throws CCDMException;
	
	Solution getsolutionById(Integer solutionId);

	Solution saveOrUpdateSolution(Solution solution);

	void saveSlaBySolutionId(SolutionSlaRequest solutionSlaRequest, AppUser appUser) throws CCDMException;

	void deploySolution(DeployModel deployModel, AppUser appUser) throws CCDMException;

	SolutionClientMapModel getSolutionClientMapByUserId(int loggedInUserId) throws CCDMException;

	List<CustomerInfoModel> getCustomerList(AppUser appUser);

}
