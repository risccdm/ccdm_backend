package com.ccdm.solutions.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.LoadBal.controller.LBCreation;
import com.ccdm.LoadBal.controller.LoadBalController;
import com.ccdm.common.constants.SequenceCounterConstants;
import com.ccdm.common.validator.CommonValidator;
import com.ccdm.elastic.repository.executionscript.IExecutionScriptRepository;
import com.ccdm.elastic.repository.sequencecounter.ISequenceCounterRepoCustom;
import com.ccdm.elastic.repository.solutionhistory.ISolutionHistoryRepository;
import com.ccdm.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.elastic.solutionhistory.model.EInstanceTask;
import com.ccdm.elastic.solutionhistory.model.ELbState;
import com.ccdm.elastic.solutionhistory.model.ESolutionState;
import com.ccdm.elastic.solutionhistory.model.ScriptRequestVo;
import com.ccdm.elastic.vo.ExecutionScript;
import com.ccdm.elastic.vo.LbHistory;
import com.ccdm.elastic.vo.SolutionHistory;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.icinga.service.IcingaConstants;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.instance.constants.InstanceConstant;
import com.ccdm.platformreferece.controller.ProviderPlatformReferenceController;
import com.ccdm.script.controller.ScriptController;
import com.ccdm.script.model.ScriptCommandRequest;
import com.ccdm.sla.service.SlaService;
import com.ccdm.solution.model.CustomerInfoModel;
import com.ccdm.solution.model.DeployModel;
import com.ccdm.solution.model.NotifyModel;
import com.ccdm.solution.model.ScriptModel;
import com.ccdm.solution.model.SlaModel;
import com.ccdm.solution.model.SolutionClientMapModel;
import com.ccdm.solution.model.SolutionListResponse;
import com.ccdm.solution.model.SolutionModel;
import com.ccdm.solution.model.SolutionRequest;
import com.ccdm.solution.model.SolutionResponse;
import com.ccdm.solution.model.SolutionSlaRequest;
import com.ccdm.solution.model.VmDetails;
import com.ccdm.solution.model.VmModel;
import com.ccdm.solution.model.VmScriptModel;
import com.ccdm.solution.request.InstanceConfig;
import com.ccdm.solution.request.OsVo;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.solution.request.ViewModelVo;
import com.ccdm.solution.request.VirtualMachineVo;
import com.ccdm.solutions.dao.SolutionDao;
import com.ccdm.validators.SolutionRequestValidator;
import com.ccdm.viewModel.controller.ViewModelController;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.CustomerInfo;
import com.ccdm.vo.Notification;
import com.ccdm.vo.ProviderPlatformReference;
import com.ccdm.vo.SlaType;
import com.ccdm.vo.Solution;
import com.ccdm.vo.SolutionClientMap;
import com.ccdm.vo.SolutionVmMap;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.LoadBal;
import com.ccdm.vo.WebServerProvider;

@Service
@Transactional(readOnly = false)
public class SolutionServiceImpl implements SolutionService 
{
	private static final Logger logger = LoggerFactory.getLogger(SolutionServiceImpl.class);
	
	@Autowired
	private SolutionDao solutionDAO;
	@Autowired
	private IExecutionScriptRepository executionScriptRepository;
	@Autowired
	private ScriptController scriptController;
	@Autowired
	private ViewModelController viewModelController;
	@Autowired
	private LoadBalController loadBalController;
	@Autowired
	private SolutionRequestValidator solutionRequestValidator;
	@Autowired
	private SlaService slaService;
	@Autowired
	private ISolutionHistoryRepository solutionHistoryRepository;
	@Autowired
	private ProviderPlatformReferenceController providerPlatformReferenceController;
	@Autowired
	private ISequenceCounterRepoCustom iSequenceCounterRepoCustom;
	
	@Override
	public List<Map<String, String>> getVMTemplates() throws CCDMUnauthorizedException{
		CCDMResponse res = viewModelController.gettemplateNames().getBody();
		List<Map<String, String>> models = res.getContent();
		return models;
	}
	@Override
	public List<Map<String, String>> getLBTemplates() throws CCDMUnauthorizedException{
		CCDMResponse res = loadBalController.gettemplateNames().getBody();
		List<Map<String, String>> models = res.getContent();
		return models;
	}
	@Override
	public List<ScriptModel> getScripts() throws CCDMUnauthorizedException{
		CCDMResponse res = scriptController.getAllExecutionScripts();
		List<ExecutionScript> scripts = res.getContent();
		List<ScriptModel> scriptModels = getScriptModel(scripts);
		return scriptModels;
	}

	@Override
	public List<SolutionListResponse> getSolutions(AppUser appUser) throws CCDMException {
		List<SolutionListResponse> listResponses = new ArrayList<SolutionListResponse>();
		List<Solution> solutionsList = solutionDAO.getAllSolutionByClientId(appUser); 
		for(Solution solution : solutionsList) {
			listResponses.add(buildsolutionResponse(solution, appUser.getUserId()));
		}
		return listResponses;
	}

	private SolutionListResponse buildsolutionResponse(Solution solution, Integer appuserId) throws CCDMException {
		return new SolutionListResponse(buildSolutionModel(solution), 
										buildVmDetails(solution.getSolutionId(), appuserId), 
													buildSlaModel(solution.getSlaType()), 
															buildNotifyModel(solution.getNotification()));
	}

	private List<VmDetails> buildVmDetails(Integer solutionId, Integer appuserId) throws CCDMException {
		List<VmDetails> vmDetailsList = new ArrayList<VmDetails>(); 
		List<SolutionVmMap> solutionVmMaps = solutionDAO.getSolutionVMMapBySolutionId(solutionId);
		for(SolutionVmMap solutionVmMap : solutionVmMaps) {
			vmDetailsList.add(new VmDetails(buildVmModel(solutionVmMap.getViewModel()), buildScriptModel(solutionVmMap.getScriptId())));
		}
		return vmDetailsList;
	}

	private VmModel buildVmModel(ViewModel viewModel) throws CCDMException {
		if(viewModel == null) {
			logger.error("slaType Is Empty");
			throw new CCDMException(ErrorCodes.VIEW_MODEL_EMPTY);
		}
		return new VmModel(viewModel.getVmId(), viewModel.getName());
	}

	private ScriptModel buildScriptModel(String scriptId) throws CCDMException {
		if(scriptId==null || scriptId.trim().isEmpty()){
			return new ScriptModel();
		}
		ExecutionScript executionScript = executionScriptRepository.findOne(scriptId);
		if(executionScript == null) {
			return new ScriptModel();
		}
		return new ScriptModel(executionScript.getId(), executionScript.getScriptName());
	}

	private SlaModel buildSlaModel(SlaType slaType) throws CCDMException {
		if(slaType == null) {
			return new SlaModel();
		}
		return new SlaModel(slaType.getSlaTypeId(), slaType.getSlaTypeName());
	}

	private NotifyModel buildNotifyModel(Notification notification) throws CCDMException {
		if(notification == null) {
			return new NotifyModel();
		}
		return new NotifyModel(notification.getId(), notification.getNotificationType());
	}

	private SolutionModel buildSolutionModel(Solution solution) throws CCDMException {
		if(solution == null) {
			logger.error("Solution Is Empty");
			throw new CCDMException(ErrorCodes.SOLUTION_EMPTY);
		}
		return new SolutionModel(solution.getSolutionId(), solution.getSolutionName(), solution.getCreationDate());
	}

	private List<ScriptModel> getScriptModel(List<ExecutionScript> scripts){
		List<ScriptModel> scriptModels = new ArrayList<ScriptModel>();
		for (ExecutionScript script : scripts) {
			ScriptModel scriptModel = new ScriptModel();
			scriptModel.setScriptId(script.getId());
			scriptModel.setScriptName(script.getScriptName());
			
			scriptModels.add(scriptModel);
		}
		return scriptModels;
	}

	public void deploySolution(DeployModel deployModel, AppUser appUser ) throws CCDMException 
	{
		validateDeployModel(deployModel);
		
		List<SolutionVmMap> vmMapsList = solutionDAO.getSolutionVMMapBySolutionId(deployModel.getSolutionId());
		if(vmMapsList ==null || vmMapsList.size() == 0) {
			return;
		}
		
		WebServerProvider  webServerProvider = solutionDAO.getProviderById(deployModel.getProviderId());
		Solution solution = vmMapsList.get(0).getSolution();
		SolutionClientMap clientMap = buildsolutionClientVMMap(solution, deployModel);
		clientMap = solutionDAO.saveSolutionClientMap(clientMap);
		String customerName = clientMap.getCustomerInfo().getCustomerName();
		Date deployForDate = new Date();
		LoadBal lb = new LoadBal();
		String deployUniqueId = UUID.randomUUID().toString().replaceAll("-", "");
		
		for(SolutionVmMap solutionVmMap : vmMapsList) 
		{
			Integer seqNo = 0;
			String instanceName = buildTagNames(webServerProvider);
			
			ProviderPlatformReference platformReference = getProviderPlatformReferenceByProviderIdVmIdOsId(webServerProvider.getId(), solutionVmMap.getViewModel().getVirtualMachine().getId(), solutionVmMap.getViewModel().getOperatingSystem().getId()); 
			ServerCredentialVo serverCredentialVo = buildServerCredentialVo(platformReference, webServerProvider);
			
			//Instance Creation Object Building					// Step : 1
			SolutionHistory instanceCreationHistoryReq = createCommonInstanceSolutionHistory(solutionVmMap, appUser, deployModel, seqNo+=1, EInstanceTask.CREATE_INSTANCE, customerName, deployForDate, deployUniqueId, webServerProvider);
			CreateInstanceRequestVo createInstanceRequestVo = getInstanceRequestVo(solutionVmMap.getViewModel(), webServerProvider, customerName, instanceName, platformReference);
			createInstanceRequestVo.setSolutionClientMapId(clientMap.getId());
			createInstanceRequestVo.setSolutionVmMapId(solutionVmMap.getSolutionVmMapId());
			createInstanceRequestVo.setInstanceName(instanceName);
			createInstanceRequestVo.setServerCredentialVo(serverCredentialVo);
			createInstanceRequestVo.setInstanceConfig(getInstanceConfig(platformReference, serverCredentialVo));
			instanceCreationHistoryReq.setRequestObject(createInstanceRequestVo);
			instanceCreationHistoryReq.setInstanceName(instanceName);
			saveOrUpdateSolutionHistory(instanceCreationHistoryReq, ESolutionState.WAITING.getSolutionState());
			instanceCreationHistoryReq.setInstanceUniqueId(instanceCreationHistoryReq.getId());
			saveOrUpdateSolutionHistory(instanceCreationHistoryReq, ESolutionState.WAITING.getSolutionState());
			
			//AgentMasterConfigRequest Build Place				//Step : 2
			SolutionHistory agentMasterConfigHistoryReq = createCommonInstanceSolutionHistory(solutionVmMap, appUser, deployModel, seqNo+=1, EInstanceTask.MASTER_AGENT_CONFIG, customerName, deployForDate, deployUniqueId, webServerProvider);
			agentMasterConfigHistoryReq.setInstanceUniqueId(instanceCreationHistoryReq.getId());
			agentMasterConfigHistoryReq.setRefId(instanceCreationHistoryReq.getId());
			agentMasterConfigHistoryReq.setInstanceName(instanceName);
			agentMasterConfigHistoryReq.setRequestObject(getMasterConfigRequest());
			saveOrUpdateSolutionHistory(agentMasterConfigHistoryReq, ESolutionState.WAITING.getSolutionState());
			
			//AgentClientInstallRequest Build Place				//Step : 3
			SolutionHistory agentClientInstallHistoryReq = createCommonInstanceSolutionHistory(solutionVmMap, appUser, deployModel, seqNo+=1, EInstanceTask.CLIENT_AGENT_INSTALL, customerName, deployForDate, deployUniqueId, webServerProvider);
			agentClientInstallHistoryReq.setInstanceUniqueId(instanceCreationHistoryReq.getId());
			agentClientInstallHistoryReq.setRefId(agentMasterConfigHistoryReq.getId());
			agentClientInstallHistoryReq.setInstanceName(instanceName);
			AgentClientRequestVo agentClientRequestVo = getAgentClientRequest(webServerProvider);
			agentClientInstallHistoryReq.setRequestObject(agentClientRequestVo);
			agentClientRequestVo.setServerCredentialVo(serverCredentialVo);
			saveOrUpdateSolutionHistory(agentClientInstallHistoryReq, ESolutionState.WAITING.getSolutionState());
			
			//ScriptRequestVo									//Step : 4
			SolutionHistory scriptHistoryReq = createCommonInstanceSolutionHistory(solutionVmMap, appUser, deployModel, seqNo+=1, EInstanceTask.SCRIPT_EXECUTION, customerName, deployForDate, deployUniqueId, webServerProvider);
			scriptHistoryReq.setInstanceUniqueId(instanceCreationHistoryReq.getId());
			scriptHistoryReq.setRefId(instanceCreationHistoryReq.getId());
			scriptHistoryReq.setInstanceName(instanceName);
			ScriptRequestVo requestVo = getScriptRequestVo(solutionVmMap.getScriptId(), webServerProvider);
			scriptHistoryReq.setRequestObject(requestVo);
			requestVo.setServerCredentialVo(serverCredentialVo);
			saveOrUpdateSolutionHistory(scriptHistoryReq, ESolutionState.WAITING.getSolutionState());
			
			//lbtemplate                 								//Step : 5
			SolutionHistory lbHistoryReq = createCommonInstanceSolutionHistory(solutionVmMap, appUser, deployModel, seqNo+=1, EInstanceTask.SCRIPT_EXECUTION, customerName, deployForDate, deployUniqueId, webServerProvider);
		    lbHistoryReq.setInstanceUniqueId(instanceCreationHistoryReq.getId());
			lbHistoryReq.setRefId(instanceCreationHistoryReq.getId());
			lbHistoryReq.setInstanceName(instanceName);
			ScriptRequestVo requestVo1 = getScriptRequestVo(solutionVmMap.getScriptId(), webServerProvider);
			lbHistoryReq.setRequestObject(requestVo1);
			requestVo1.setServerCredentialVo(serverCredentialVo);
			saveOrUpdateSolutionHistory(scriptHistoryReq, ESolutionState.WAITING.getSolutionState());
		
		}
	}
	private AgentClientRequestVo getAgentClientRequest(WebServerProvider webServerProvider) throws CCDMException {
		AgentClientRequestVo agentClientRequestVo = new AgentClientRequestVo();
		agentClientRequestVo.setUploadFilePath(IcingaConstants.ICINGA_CLIENT_CONFIG_FILE_PATH_IN_LOCAL);
		return agentClientRequestVo;
	}

	private ScriptRequestVo getScriptRequestVo(String scriptId, WebServerProvider webServerProvider) throws CCDMException {
		ExecutionScript executionScript = executionScriptRepository.findOne(scriptId);
		ScriptRequestVo  requestVo = new ScriptRequestVo();
		requestVo.setScriptName(executionScript.getScriptName());
		requestVo.setScriptCommand(executionScript.getExecutableCommands());
		requestVo.setScriptId(executionScript.getId());
		return requestVo;
	}

	private AgentMasterConfigRequestVo getMasterConfigRequest() {
		return new AgentMasterConfigRequestVo(null, new ClientInstanceVo(IcingaConstants.MASTER_PUBLIC_DNS, 
												IcingaConstants.MASTER_KEYNAME, InstanceConstant.INSTANCE_DEFAULT_PORT_NUMBER, IcingaConstants.MASTER_INSTANCE_ID, 
												IcingaConstants.MASTER_IPV4, IcingaConstants.MASTER_TAGNAME) , getMasterServerCredentialVo());
	}

	private ServerCredentialVo getMasterServerCredentialVo() {
		ServerCredentialVo credentialVo = new ServerCredentialVo();
		credentialVo.setUser(IcingaConstants.MASTER_USER_NAME);
		credentialVo.setPemKey(IcingaConstants.MASTER_KEYNAME);
		return credentialVo;
	}

	private SolutionHistory saveOrUpdateSolutionHistory(SolutionHistory solutionHistory, String status) {
		solutionHistory.setStatus(status);
		solutionHistoryRepository.save(solutionHistory);
		return solutionHistory;
	}
	
	private CreateInstanceRequestVo getInstanceRequestVo(ViewModel viewModel, WebServerProvider webServerProvider, String customerName, String instanceName, ProviderPlatformReference platformReference) throws CCDMException {
		CreateInstanceRequestVo requestVo = new CreateInstanceRequestVo();
		requestVo.setInstanceName(instanceName);
		requestVo.setViewModelVo(getViewModelVO(viewModel));
		requestVo.setProviderId(webServerProvider.getId());
		requestVo.setProviderName(webServerProvider.getAliasName());
		requestVo.setCreatedFor(customerName);
		return requestVo;
	}

	private ViewModelVo getViewModelVO(ViewModel viewModel) {
		return new ViewModelVo(viewModel.getName(), getOsVo(viewModel), getVirtualMachineVo(viewModel));
	}

	private VirtualMachineVo getVirtualMachineVo(ViewModel viewModel) {
		return new VirtualMachineVo(viewModel.getVirtualMachine().getModel(), viewModel.getVirtualMachine().getVCpu()+"", viewModel.getVirtualMachine().getMemory()+"", viewModel.getVirtualMachine().getStorage());
	}

	private OsVo getOsVo(ViewModel viewModel) {
		return new OsVo(viewModel.getOperatingSystem().getName(),  viewModel.getOperatingSystem().getType(), viewModel.getOperatingSystem().getOsVersion());
	}

	private InstanceConfig getInstanceConfig(ProviderPlatformReference platformReference, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		return new InstanceConfig(platformReference.getMachineType(), platformReference.getImageId(), serverCredentialVo.getPemKey(), InstanceConstant.SECURITY_GROUPS, platformReference.getImageProject(), platformReference.getId());
	}
	
	private ProviderPlatformReference getProviderPlatformReferenceByProviderIdVmIdOsId(Integer providerId, Integer virtuaMachineId, Integer osId) throws CCDMException 
	{
		ProviderPlatformReference platformReference = providerPlatformReferenceController.getPlatformReference(providerId, virtuaMachineId, osId);
		if(platformReference == null) {
			throw new CCDMException(ErrorCodes.INVALID_PROVIDER_PLATFORM_REFERENCE);
		}
		return platformReference;
	}

	private ServerCredentialVo buildServerCredentialVo(ProviderPlatformReference platformReference, WebServerProvider webServerProvider) throws CCDMException 
	{
		if(platformReference.getServerCredential() == null) {
			throw new CCDMException(ErrorCodes.INVALID_SERVER_CREDENTIAL);
		}
		ServerCredentialVo serverCredentialVo = new ServerCredentialVo();
		serverCredentialVo.setAccessId(platformReference.getServerCredential().getAccessId());
		serverCredentialVo.setAccessKey(platformReference.getServerCredential().getAccessKey());
		serverCredentialVo.setPemKey(platformReference.getServerCredential().getPemKey());
		serverCredentialVo.setProviderName(webServerProvider.getAliasName());
		serverCredentialVo.setUser(platformReference.getServerCredential().getUser());
		return serverCredentialVo;
	}

	private String buildTagNames(WebServerProvider webServerProvider) throws CCDMException 
	{
		return webServerProvider.getAliasName()+"-"+String.valueOf(iSequenceCounterRepoCustom.getSequenceCounterValueById(SequenceCounterConstants.INSTANCE_NAME));
	}
	
	private SolutionHistory createCommonInstanceSolutionHistory(SolutionVmMap solutionVmMap, AppUser appUser, DeployModel deployModel, Integer seqNo, EInstanceTask eInstanceTask, String customerName, Date deployDate, String deployUniqueId, WebServerProvider webServerProvider) throws CCDMException {
		SolutionHistory solutionHistory = new SolutionHistory();
		solutionHistory.setSolutionId(solutionVmMap.getSolution().getSolutionId());
		solutionHistory.setSolutionName(solutionVmMap.getSolution().getSolutionName());
		solutionHistory.setCreatedFor(customerName);
		solutionHistory.setInstanceTaskName(eInstanceTask.getTaskName());
		solutionHistory.setServerProvider(webServerProvider.getAliasName());
		solutionHistory.setSeqNo(seqNo);
		solutionHistory.setDeployDate(deployDate);
		solutionHistory.setDeployUniqueId(deployUniqueId);
		return solutionHistory;
	}


	
	private SolutionClientMap buildsolutionClientVMMap(Solution solution, DeployModel deployModel) throws CCDMException {
		CustomerInfo customerInfo = null;
		if(deployModel.getCustomerId() != null  && deployModel.getCustomerId() > 0) {
			customerInfo = solutionDAO.getCustomerInfoById(deployModel.getCustomerId());
		} 
		return new SolutionClientMap(solution, customerInfo);
	}

	private void validateDeployModel(DeployModel deployModel) throws CCDMException {
		if(deployModel == null) {
			logger.error("Deploy Model Object Is Empty");
			throw new CCDMException(ErrorCodes.INVALID_DEPLOY_MODEL_OBJECT);
		}
		if(deployModel.getSolutionId() == null || deployModel.getSolutionId() < 1) {
			logger.error("Invalid Solution Id ");
			throw new CCDMException(ErrorCodes.INVALID_SOLUTION_ID);
		}
		if( (deployModel.getCustomerId() == null  || deployModel.getCustomerId() < 1) ) {
			throw new CCDMException(ErrorCodes.INVALID_CUSTOMER_INFO);
		}
		if(deployModel.getProviderId() == null || deployModel.getProviderId() < 1) {
			throw new CCDMException(ErrorCodes.WEB_SERVER_PROVIDER_INVALID);
		}
	}
	
	@Override
	public Solution saveSolution(SolutionRequest solutionRequest, AppUser appUser) throws CCDMException
	{
		solutionRequestValidator.validateSolutionRequest(solutionRequest);
		Solution solution = buildSolution(solutionRequest, appUser);
		Solution solutionWithId = saveOrUpdateSolution(solution);
		List<SolutionVmMap> existingSolutionVmMaps = solutionDAO.getSolutionVMMapBySolutionId(solutionWithId.getSolutionId());
		updateSolutionVmMap(existingSolutionVmMaps, true);
		List<SolutionVmMap> newSolutionVmMaps = buildSolutionVmMapList(solutionWithId, solutionRequest);
		updateSolutionVmMap(newSolutionVmMaps, false);
		return solutionWithId;
	}
	
	private void updateSolutionVmMap(List<SolutionVmMap> solutionVmMaps, Boolean isDeleted){
		for (SolutionVmMap solutionVmMap : solutionVmMaps) {
			solutionVmMap.setIsDeleted(isDeleted);
			solutionDAO.saveSolutionVmMap(solutionVmMap);
		}
	}
	
	private Solution buildSolution(SolutionRequest solutionRequest, AppUser appUser){
		Solution solution = checkSolutionAlreadyExist(solutionRequest, appUser);
		solution.setAppUser(appUser);
		solution.setSolutionName(solutionRequest.getSolutionName());
		solution.setDescription(solutionRequest.getDescription());
		solution.setModificationDate(new Date());
		solution.setClientId(appUser.getClientId());
		return solution;
	}
	
	private Solution checkSolutionAlreadyExist(SolutionRequest solutionRequest, AppUser appUser){
		if(solutionRequest.getSolutionId() == null){
			return new Solution();
		}
		Solution solution = solutionDAO.getSolutionBySolutionId(solutionRequest.getSolutionId(), appUser);
		return solution;
	}
	
	private List<SolutionVmMap> buildSolutionVmMapList(Solution solution, SolutionRequest solutionRequest) throws CCDMUnauthorizedException{
		List<SolutionVmMap> solutionVmMaps = new ArrayList<SolutionVmMap>();
		List<VmScriptModel> vmScriptModels = solutionRequest.getVmScriptModel();
		
		for (VmScriptModel vmScriptModel : vmScriptModels) {
			SolutionVmMap solutionVmMap = new SolutionVmMap();
			ViewModel viewModel = viewModelController.getViewModelById(vmScriptModel.getVmModel().getVmId()).getContent();
			if(viewModel != null){
				solutionVmMap.setSolution(solution);
				solutionVmMap.setScriptId(vmScriptModel.getScriptModel().getScriptId());
				solutionVmMap.setViewModel(viewModel);
				
				solutionVmMaps.add(solutionVmMap);
			}
		}
		return solutionVmMaps;
	}

	@Override
	public Solution getsolutionById(Integer solutionId) {
		return solutionDAO.getSolutionById(solutionId);
	}

	@Override
	public Solution saveOrUpdateSolution(Solution solution) {
		return solutionDAO.saveSolution(solution);
	}
	public Solution getSolutionBySolutionId(Integer solutionId, AppUser appUser) throws CCDMException 
	{
		CommonValidator.validateId(solutionId, ErrorCodes.INVALID_SOLUTION_ID);
		return solutionDAO.getSolutionBySolutionId(solutionId, appUser);
	}

	@Override
	public SolutionResponse getSolutionResponseBySolutionId(Integer solutionId, AppUser appUser) throws CCDMException 
	{
		Solution solution = solutionDAO.getSolutionBySolutionId(solutionId, appUser);
		SolutionResponse solutionResponse = new SolutionResponse();
		solutionValidation(solution);
		
		solutionResponse.setSolutionId(solution.getSolutionId());
		solutionResponse.setSolutionName(solution.getSolutionName());
		solutionResponse.setDescription(solution.getDescription());
		
		List<SolutionVmMap> solutionVmMaps = solutionDAO.getSolutionVMMapBySolutionId(solutionId);
		
		List<VmScriptModel> vmScriptModels = buildVmSolutionMapList(solutionVmMaps);
		
		solutionResponse.setVmScriptModel(vmScriptModels);
		
		return solutionResponse;
	}

	private List<VmScriptModel> buildVmSolutionMapList(List<SolutionVmMap> solutionVmMaps) throws CCDMException {
		List<VmScriptModel> vmScriptModels = new ArrayList<VmScriptModel>();
		
		for (SolutionVmMap solutionVmMap : solutionVmMaps) {
			
			VmScriptModel vmScriptModel = new VmScriptModel();
			
			if(solutionVmMap.getViewModel() != null){
				
				ViewModel viewModel = solutionVmMap.getViewModel();
				VmModel vmModel = new VmModel();
				vmModel.setVmId(viewModel.getVmId());
				vmModel.setVmTemplateName(viewModel.getName());
				vmScriptModel.setVmModel(vmModel);
				
			}
			
			if(solutionVmMap.getScriptId() != null || solutionVmMap.getScriptId() != ""){
				
				ScriptCommandRequest executionScript = scriptController.getScriptByScriptId(solutionVmMap.getScriptId()).getContent();
				
				if(executionScript != null){
					
					ScriptModel scriptModel = new ScriptModel();
					scriptModel.setScriptId(executionScript.getScriptId());
					scriptModel.setScriptName(executionScript.getScriptName());
					vmScriptModel.setScriptModel(scriptModel);
					
				}
			}
			vmScriptModels.add(vmScriptModel);
		}
		return vmScriptModels;
	}

	private void solutionValidation(Solution solution) throws CCDMException {
		if(solution == null){
			logger.error("Requested Solution Not Available");
			throw new CCDMException(ErrorCodes.REQUESTED_SOLUTION_NOT_AVAILABLE);
		}
		if(solution.getSolutionName() == null || solution.getSolutionName() == ""){
			logger.error("Invalid Solution Name");
			throw new CCDMException(ErrorCodes.INVALID_SOLUTION_NAME);
		}
		if(solution.getSolutionId() == null){
			logger.error("Invalid Solution Id");
			throw new CCDMException(ErrorCodes.INVALID_OR_NULL_SOLUTIONID);
		}
	}
	
	@Override
	public void saveSlaBySolutionId(SolutionSlaRequest solutionSlaRequest, AppUser appUser) throws CCDMException 
	{
		CommonValidator.validateObject(solutionSlaRequest, ErrorCodes.INVALID_SOLUTION_REQ);
		CommonValidator.validateId(solutionSlaRequest.getSolutionId(), ErrorCodes.INVALID_SOLUTION_ID);
		CommonValidator.validateId(solutionSlaRequest.getSlaTypeId(), ErrorCodes.INVALID_SLA_TYPE_ID);
		
		Solution solution = solutionDAO.getSolutionBySolutionId(solutionSlaRequest.getSolutionId(), appUser);
		SlaType slaType = slaService.getSlaTypeBySlaTypeId(solutionSlaRequest.getSlaTypeId());
		
		CommonValidator.validateObject(solution, ErrorCodes.INVALID_SOLUTION_OBJECT);
		CommonValidator.validateObject(slaType, ErrorCodes.INVALID_SLA_TYPE);
		
		solution.setSlaType(slaType);
		solutionDAO.saveSolution(solution);
	}

	@Override
	public SolutionClientMapModel getSolutionClientMapByUserId(int loggedInUserId) throws CCDMException 
	{
		List<SolutionClientMap> solutionClientMapList = solutionDAO.getSolutionClientMapByUserId(loggedInUserId);
		
		SolutionClientMapModel solutionClientMapModel = new SolutionClientMapModel();
		if(solutionClientMapList == null || solutionClientMapList.size() < 1)
		{
			return solutionClientMapModel;
		}
		
		Map<Integer, String> solutionModelMap = new HashMap<Integer, String>();
		Map<Integer, String> customerInfoModelMap = new HashMap<Integer, String>();
		for(SolutionClientMap solutionClient : solutionClientMapList)
		{
			solutionModelMap.put(solutionClient.getSolution().getSolutionId(), solutionClient.getSolution().getSolutionName());
			customerInfoModelMap.put(solutionClient.getCustomerInfo().getId(), solutionClient.getCustomerInfo().getCustomerName());
		}
		
		solutionClientMapModel = new SolutionClientMapModel(getSolutionModelList(solutionModelMap), getCustomerInfoModelList(customerInfoModelMap));
		return solutionClientMapModel;
	}

	private List<CustomerInfoModel> getCustomerInfoModelList(Map<Integer, String> customerInfoModelMap) 
	{
		List<CustomerInfoModel> customerInfoModelList = new ArrayList<>();
		if(customerInfoModelMap == null || customerInfoModelMap.size() < 1)
		{
			return customerInfoModelList;
		}
		for(Map.Entry<Integer, String> map : customerInfoModelMap.entrySet())
		{
			customerInfoModelList.add(new CustomerInfoModel(map.getKey(), map.getValue()));
		}
		return customerInfoModelList;
	}

	private List<SolutionModel> getSolutionModelList(Map<Integer, String> solutionModelMap) 
	{
		List<SolutionModel> solutionModelList = new ArrayList<>();
		if(solutionModelMap == null || solutionModelMap.size() < 1)
		{
			return solutionModelList;
		}
		for(Map.Entry<Integer, String> map : solutionModelMap.entrySet())
		{
			solutionModelList.add(new SolutionModel(map.getKey(), map.getValue()));
		}
		return solutionModelList;
	}

	@Override
	public List<CustomerInfoModel> getCustomerList(AppUser appUser) {
		List<CustomerInfoModel> customerInfoModels = solutionDAO.getCustomerInfoModel(appUser);
		return customerInfoModels;
	}
	
	
}
