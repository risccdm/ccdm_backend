package com.ccdm.communication.constants;

public class CommunicationConstants {

	public static String EMAIL_SMTP_HOST     = "mail.smtp.host";

	public static String EMAIL_SMTP_AUTH     = "mail.smtp.auth";

	public static String EMAIL_SMTP_PORT     = "mail.smtp.port";
	
	public static String SMS_FROM_PHONE_NUM  = "From";
	
	public static String SMS_TO_PHONE_NUM	 = "To";
	
	public static String SMS_BODY 		     = "Body";
	
	public static String SMS_PARAMS		     = "params";
	
	public static String SMS_MESSAGEFACTORY  = "messageFactory"; 
}
