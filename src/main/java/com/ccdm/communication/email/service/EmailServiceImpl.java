package com.ccdm.communication.email.service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.communication.config.EmailConfig;
import com.ccdm.communication.property.EmailConfigProperty;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;

@Service
@Transactional(readOnly = false)
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	private EmailConfig emailConfig;
	
	@Autowired
	private EmailConfigProperty emailConfigProperty;

	private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	
	private static final String TEXT_HTML_CHARSET_UTF_8 = "text/html; charset=utf-8";
	
	@Override
	public Boolean sendEmail(String toAddress, String mailContent, String mailSubject) throws AddressException, MessagingException, CCDMException{
		
		CommonValidator.validateEmail(toAddress, ErrorCodes.INVALID_TO_ADDRESS);
		CommonValidator.validateString(mailContent, ErrorCodes.INVALID_MAIL_CONTENT);
		CommonValidator.validateString(mailSubject, ErrorCodes.INVALID_MAIL_SUBJECT);
		try {
			
			Message message = new MimeMessage(emailConfig.getEmailSession());
			message.setFrom(new InternetAddress(emailConfigProperty.getFromAddress()));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toAddress));
			message.setSubject(mailSubject);
			message.setContent(mailContent, TEXT_HTML_CHARSET_UTF_8);
			
			sendMail(message);
		}
		catch(Exception e)
		{
			logger.error("Error sending Email to {} : Error : {}", toAddress, e);
			return false;
		}
		return true;
	}
	
	private void sendMail(Message message) throws MessagingException{
		Transport.send(message);
	}
}
