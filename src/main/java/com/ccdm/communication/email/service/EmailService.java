package com.ccdm.communication.email.service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.ccdm.exceptions.CCDMException;

public interface EmailService {

	Boolean sendEmail(String toAddress, String mailContent, String mailSubject) throws AddressException, MessagingException, CCDMException;

}
