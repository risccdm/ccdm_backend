package com.ccdm.communication.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.communication.constants.CommunicationConstants;
import com.ccdm.communication.property.SmsConfigProperty;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Account;

@Component
public class SmsConfig {

	@Autowired
	private SmsConfigProperty smsConfigProperty;
	
	public MessageFactory getMessageFactory(){
		//username --> authSid, password --> authTocken
		TwilioRestClient restClient = new TwilioRestClient(smsConfigProperty.getAuthSid(), smsConfigProperty.getAuthTocken());
		Account account = restClient.getAccount();
		MessageFactory messageFactory = account.getMessageFactory();
		return messageFactory;
	}
	
	public Properties getProperties()
	{    
		/**
		 * This will be handy while we need to set more than one property
		 * */
		 Properties properties = new Properties();
		 properties.put(CommunicationConstants.SMS_FROM_PHONE_NUM, smsConfigProperty.getFromNumber());
		 
		return properties;
	}
	
	public List<NameValuePair> getMessageInputParams(String toPhoneNum,
			   String smsBodyContent, String fromPhoneNumber)
	{
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair( CommunicationConstants.SMS_FROM_PHONE_NUM, fromPhoneNumber )); 
		params.add(new BasicNameValuePair( CommunicationConstants.SMS_TO_PHONE_NUM, toPhoneNum )); 
		params.add(new BasicNameValuePair( CommunicationConstants.SMS_BODY, smsBodyContent ));
		return params;
	}
	
}
