package com.ccdm.communication.config;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.communication.constants.CommunicationConstants;
import com.ccdm.communication.property.EmailConfigProperty;

@Component
public class EmailConfig {
	
	@Autowired
	private EmailConfigProperty emailConfigProperty;

	public Properties getEmailProperties(){
		Properties mailProperties = new Properties();
		mailProperties.put(CommunicationConstants.EMAIL_SMTP_AUTH, emailConfigProperty.getSmtpAuth());
		mailProperties.put(CommunicationConstants.EMAIL_SMTP_HOST, emailConfigProperty.getSmtpHost());
		mailProperties.put(CommunicationConstants.EMAIL_SMTP_PORT, emailConfigProperty.getSmtpPort());
		
		return mailProperties;
	}
	
	public Authenticator getAuthenticator(){
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailConfigProperty.getUsername(), emailConfigProperty.getPassword());
			}
		};
		
		return authenticator;
	}
	
	public Session getEmailSession(){
		Session emailSession = Session.getDefaultInstance(getEmailProperties(), getAuthenticator());
		
		return emailSession;
	}
}
