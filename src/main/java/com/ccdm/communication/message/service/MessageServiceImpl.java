package com.ccdm.communication.message.service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.NameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.communication.config.SmsConfig;
import com.ccdm.communication.constants.CommunicationConstants;
import com.ccdm.communication.email.service.EmailServiceImpl;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;

@Service
@Transactional(readOnly = false)
public class MessageServiceImpl implements MessageService{
	
	@Autowired
	private SmsConfig smsConfig;

	private static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);
	
	private static final String TEXT_HTML_CHARSET_UTF_8 = "text/html; charset=utf-8";
	
	@Override
	public Boolean sendMessage(String toPhNumber, String smsContent) throws CCDMException, TwilioRestException{
		
		CommonValidator.validateString(smsContent, ErrorCodes.INVALID_SMS_BODY_CONTENT);
		CommonValidator.validatePhoneNum(toPhNumber, ErrorCodes.INVALID_TO_PHONE_NUMBER);
		
		Map<String, String> smsInputParams = new HashMap<String, String>();
		smsInputParams.put(CommunicationConstants.SMS_TO_PHONE_NUM, toPhNumber);
		smsInputParams.put(CommunicationConstants.SMS_BODY, smsContent);
		
		try{
			
			Properties properties = smsConfig.getProperties();
			String fromPhNumber = properties.getProperty(CommunicationConstants.SMS_FROM_PHONE_NUM);
			
			List<NameValuePair> params = smsConfig.getMessageInputParams(toPhNumber, smsContent, fromPhNumber);
			
			MessageFactory messageFactory = smsConfig.getMessageFactory();
			
			messageFactory.create(params);
			
		}catch(Exception e){
			logger.error("Error sending sms to {} : Error : {}", toPhNumber, e);
			return false;
		}
		
		return true;
	}
}
