package com.ccdm.communication.message.service;

import com.ccdm.exceptions.CCDMException;
import com.twilio.sdk.TwilioRestException;

public interface MessageService {

	Boolean sendMessage(String toPhNumber, String smsContent) throws CCDMException, TwilioRestException;

}
