package com.ccdm.communication.controller;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.communication.email.service.EmailService;
import com.ccdm.communication.message.service.MessageService;
import com.ccdm.exceptions.CCDMException;
import com.twilio.sdk.TwilioRestException;

@RestController
@RequestMapping(value = "/communication")
public class CommunicationController extends BaseController {

	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private MessageService messageService;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	@RequestMapping("/sendMail")
	public void sendMail(String toAddress, String mailContent, String mailSubject) throws AddressException, MessagingException, CCDMException{
		emailService.sendEmail(toAddress, mailContent, mailSubject);
	}
	
	@RequestMapping("/sendMessage")
	public void sendMessage(String toPhoneNum, String smsBodyContent) throws CCDMException, TwilioRestException{
		messageService.sendMessage(toPhoneNum, smsBodyContent);
	}

}
