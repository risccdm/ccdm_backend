package com.ccdm.communication.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "communication.sms")
public class SmsConfigProperty {

	private String authSid;
	private String authTocken;
	private String fromNumber;
	
	public String getAuthSid() {
		return authSid;
	}
	public void setAuthSid(String authSid) {
		this.authSid = authSid;
	}
	public String getAuthTocken() {
		return authTocken;
	}
	public void setAuthTocken(String authTocken) {
		this.authTocken = authTocken;
	}
	public String getFromNumber() {
		return fromNumber;
	}
	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}
	
}
