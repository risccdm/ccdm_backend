package com.ccdm.LoadBal.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.validators.TemplateValidator;
import com.ccdm.viewModel.controller.ViewModelController;
import com.ccdm.LoadBal.controller.LoadBalRequest;
import com.ccdm.LoadBal.dao.LoadBalDAO;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.LoadBal;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

@Service
@Transactional(readOnly = false)
public class LoadBalServiceImpl implements LoadBalService {
	
	@Autowired
	private LoadBalDAO templateDao;
	
	@Autowired
	private TemplateValidator templateValidator;
	
	@Autowired
	private ViewModelController viewModelController;
	
	@Override
	public List<ViewModel> getTemplates(){
		System.out.println("inside get templates---->");
	 //List<ViewModel> str = templateDao.getTemplates();
	// System.out.println("Aji"+str.toString());
		return templateDao.getTemplates();
	}
	
	@Override
	public List<VirtualMachine> getVirualMachineSizes(){
		return templateDao.getVirtualMachineSizes();
	}
	
	@Override
	public List<LoadBal> getAllTemplates(Integer userId){
		System.out.println("getie");
		return templateDao.getAllTemplates(userId);
	}
	
	@Override
	public List<WebServerProvider> getWebServerProviders(){
		return templateDao.getWebServerProviders();
	}
	
	@Override
	public Boolean isNamePresent(String name, Integer clientId){
		LoadBal template = templateDao.getTemplate(name, clientId);
		
		if(template == null){
			return false;
		}
		return true;
	}
	
	@Override
	public void save(LoadBal template) throws CCDMException 
	{
		
		templateDao.saveOrUpdate(template);
		System.out.println(template);
		System.out.println("t10");
	}

	@Override
	public LoadBal buildTemplate(LoadBalRequest templateRequest, AppUser appUser)
	{
		LoadBal loadBal = new LoadBal();
		loadBal.setAppUser(appUser);
		loadBal.setlbname(templateRequest.getlbname());
		loadBal.setlbdescription(templateRequest.getlbdescription());
		loadBal.setlbtype(templateRequest.getlbtype());
		loadBal.setTemplates(templateRequest.getTemplates());
		loadBal.setClientId(appUser.getClientId());
		return loadBal;
	}

	@Override
	public List<Map<String, String>> getTemplateNames(Integer appUserId) 
	{
		
		List<Map<String, String>> templateNames = new ArrayList<>();
		
		List<LoadBal> templates = templateDao.getTemplateNames(appUserId);
		
		for(LoadBal template : templates)
		{
			Map<String, String> templateName = new HashMap<>();
			templateName.put("id", String.valueOf((int)template.getlbID()));
			templateName.put("name", template.getlbname());
			templateNames.add(templateName);
		}
		return templateNames;
	}

	/*@Override
	public Object testEalstic()
	{
		return templateDao.testEalstic();
	}*/

	@Override
	public LoadBal getTemplateById(Integer templateId) 
	{
		return templateDao.getTemplateById(templateId);
	}
}
