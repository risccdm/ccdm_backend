package com.ccdm.LoadBal.service;

import java.util.List;
import java.util.Map;

import com.ccdm.LoadBal.controller.LoadBalRequest;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.LoadBal;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.WebServerProvider;

public interface LoadBalService 
{
	
	List<ViewModel> getTemplates();	

	List<VirtualMachine> getVirualMachineSizes();

	List<WebServerProvider> getWebServerProviders();

	void save(LoadBal template) throws CCDMException;

	Boolean isNamePresent(String name, Integer clientId);

	List<Map<String, String>> getTemplateNames(Integer appUserId);
	
	List<LoadBal> getAllTemplates(Integer userId);
	
	LoadBal buildTemplate(LoadBalRequest templateRequest, AppUser appUser);

	LoadBal getTemplateById(Integer templateId);

}
