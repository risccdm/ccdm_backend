package com.ccdm.LoadBal.dao;

import java.util.List;

import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.LoadBal;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

public interface LoadBalDAO{

	List<ViewModel> getTemplates();

	List<VirtualMachine> getVirtualMachineSizes();

	List<WebServerProvider> getWebServerProviders();
	
	void saveOrUpdate(LoadBal solution);

	LoadBal getTemplate(String name, Integer userId);

	List<LoadBal> getTemplateNames(Integer appUserId);
	List<LoadBal> getAllTemplates(Integer userId);

	/*Object testEalstic();*/

	LoadBal getTemplateById(Integer templateId);

}
