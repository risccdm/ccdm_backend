package com.ccdm.LoadBal.dao;


import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ccdm.LoadBal.controller.LBCreation;
import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.infrastructure.config.ElasticsearchConfiguration;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.LoadBal;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

@Repository
public class LoadBalDAOImpl extends BaseDAOImpl implements LoadBalDAO
{
	//public String name2;
	@Autowired
	ElasticsearchConfiguration elasticsearchConfiguration;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViewModel> getTemplates(){
		List<ViewModel> Vmtemplates = getSession().createCriteria(ViewModel.class).list();
		System.out.println("HEREEEEE--------"+Vmtemplates.get(0).getName());
		return Vmtemplates;	
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VirtualMachine> getVirtualMachineSizes(){
		List<VirtualMachine> virtualMachines = getSession().createCriteria(VirtualMachine.class).list();
		return virtualMachines;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LoadBal> getAllTemplates(Integer userId){
		List<LoadBal> loadbal = (List<LoadBal>) getSession().createCriteria(LoadBal.class).
				add(Restrictions.eq("appUser.userId", userId)).
				//addOrder(Order.desc("id")).
				addOrder(Order.desc("id")).list();
		System.out.println("this"+ loadbal);
		return loadbal;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<WebServerProvider> getWebServerProviders(){
		List<WebServerProvider> webServerProviders = getSession().createCriteria(WebServerProvider.class).list();
		return webServerProviders;
	}
	
	@Override
	public void saveOrUpdate(LoadBal template) {
		super.saveOrUpdate(template);
		
		String lbname = template.getlbname();
		LBCreation lb = new LBCreation();
		lb.lbcall(lbname);
		//System.out.println(template);
	  // String name = template.getlbname();	   
		
	
	}
	
	@Override
	public LoadBal getTemplate(String name, Integer clientId) {
		LoadBal template = (LoadBal) getSession().createCriteria(LoadBal.class).
				add(Restrictions.eq("appUser.userId", clientId)).
				add(Restrictions.eq("name", name)).uniqueResult();
		
		return template;
	}

	@Override
	public List<LoadBal> getTemplateNames(Integer appUserId) 
	{
		
     @SuppressWarnings("unchecked")
	List<LoadBal> templates = (List<LoadBal>) getSession().createCriteria(LoadBal.class)
    		 			                 .add(Restrictions.eq("appUser.userId", appUserId))
    		 			                 .list();
		return templates;
	}

	/*@Override
	public Object testEalstic()
	{
		
		ElasticTeset elasticTeset  = new ElasticTeset();
		elasticTeset.setId(121);
		elasticTeset.setAge(16);
		elasticTeset.setName("Arya");
		
		IndexQuery indexQuery =  new IndexQuery();  
		indexQuery.setObject(elasticTeset);
		indexQuery.setIndexName("ccdmtestindex");
		
		elasticsearchConfiguration.elasticsearchTemplate().index(indexQuery);
		
		return null;
	}*/

	@Override
	public LoadBal getTemplateById(Integer templateId) 
	{
		return (LoadBal) getSession().createCriteria(LoadBal.class)
						   .add(Restrictions.idEq(templateId)).uniqueResult();
	}
}
