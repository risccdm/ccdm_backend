package com.ccdm.LoadBal.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.LoadBal.service.LoadBalService;
import com.ccdm.admin.service.UserService;
import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
//import com.ccdm.viewModel.service.ViewModelService;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.LoadBal;
//import com.ccdm.vo.ViewModel;


@RestController
@RequestMapping(value = "/lbtemplate")
public class LoadBalController extends BaseController
{
	
	@Autowired
	private LoadBalService templateService;
	@Autowired
	private UserService userService;
	
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}

	@RequestMapping(value = "/VmTemplates", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getTemplates() {
		System.out.println("testie" + templateService.getTemplates());	
		return getResponseEntity(templateService.getTemplates());
		
}
	
	@RequestMapping(value = "/vmSizes", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getVMSizes() {
		return getResponseEntity(templateService.getVirualMachineSizes());
	}
	
	@RequestMapping(value = "/webServerProviders", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getWebServerProviders() {
		return getResponseEntity(templateService.getWebServerProviders());
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<CCDMResponse>  save(@RequestBody LoadBalRequest templateRequest) throws Exception 
	{
		System.out.println("t1");
		AppUser appUser = getLoggedInUser();
		System.out.println("t3");
		LoadBal template = templateService.buildTemplate(templateRequest, appUser);
		System.out.println("t4");
		templateService.save(template);
		System.out.println("t2");
		return getResponseEntity(template.getlbID());
		
		   
	}
	
	@RequestMapping(value = "/isPresent", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> isNamePresent(@RequestParam String name) throws CCDMException {
		
		AppUser appUser = userService.getLoggedInUser();
		Boolean isNameAvailable = !templateService.isNamePresent(name,appUser.getClientId());
		return getResponseEntity(isNameAvailable);
	}
	
	@RequestMapping(value = "/names", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> gettemplateNames() throws CCDMUnauthorizedException
	{
		Integer appUserId = userService.getLoggedInUser().getUserId();
		
		return getResponseEntity(templateService.getTemplateNames(appUserId));
	}
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  gettemplates() throws CCDMUnauthorizedException {
		System.out.println("get");
		AppUser appUser = userService.getLoggedInUser();
		System.out.println("thiss" + templateService.getAllTemplates(appUser.getUserId()));
		System.out.println("get------>"+ appUser.getUserName());
		return getResponseEntity(templateService.getAllTemplates(appUser.getUserId()));
	}

	
}
