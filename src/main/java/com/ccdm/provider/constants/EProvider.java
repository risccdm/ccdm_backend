package com.ccdm.provider.constants;

public enum EProvider 
{
	AMAZON("amazon"),GOOGLE("google");
	
	private String providerName;
	
	private EProvider(String providerName) {
		this.providerName = providerName;
	}
	
	public String getProviderName() {
		return this.providerName;
	}
}
