package com.ccdm.platformreferece.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.platformreferece.service.ProviderPlatformReferenceService;
import com.ccdm.vo.ProviderPlatformReference;

@RestController
@RequestMapping(value = "/providerplatformreference")
public class ProviderPlatformReferenceController extends BaseController  
{
	@Override
	protected Class<?> getClassName()
	{
		return this.getClass();
	}
	
	@Autowired
	private ProviderPlatformReferenceService platformReferenceService;

	public ProviderPlatformReference getPlatformReference(Integer providerId, Integer vmId, Integer osId) {
		return platformReferenceService.getPlatformReference(providerId, vmId, osId);
	}
	
	public ProviderPlatformReference getProviderPlatformReferenceById(Integer providerRefId)
	{
		return platformReferenceService.getProviderPlatformReferenceById(providerRefId);
	}
}
