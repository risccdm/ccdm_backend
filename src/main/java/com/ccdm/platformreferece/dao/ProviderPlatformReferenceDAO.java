package com.ccdm.platformreferece.dao;

import com.ccdm.vo.ProviderPlatformReference;

public interface ProviderPlatformReferenceDAO 
{
	ProviderPlatformReference getPlatformReferenceByProviderIdAndVmIdAndOsId(Integer providerId, Integer vmId, Integer osId);

	ProviderPlatformReference getProviderPlatformReferenceById(Integer providerRefId);
}
