package com.ccdm.platformreferece.dao;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.ProviderPlatformReference;

@Repository
public class ProviderPlatformReferenceDaoImpl extends BaseDAOImpl implements ProviderPlatformReferenceDAO
{

	@Override
	public ProviderPlatformReference getPlatformReferenceByProviderIdAndVmIdAndOsId(Integer providerId, Integer vmId, Integer osId) {
		ProviderPlatformReference reference = (ProviderPlatformReference) getSession().createCriteria(ProviderPlatformReference.class)
				.setFetchMode("serverCredential", FetchMode.JOIN)
				.add(Restrictions.eq("webServerProvider.id", providerId))
				.add(Restrictions.eq("virtualMachine.id", vmId))
				.add(Restrictions.eq("operatingSystem.id", osId))
				.uniqueResult();
		return reference;
	}

	@Override
	public ProviderPlatformReference getProviderPlatformReferenceById(Integer providerRefId) 
	{
		ProviderPlatformReference reference = (ProviderPlatformReference) getSession().createCriteria(ProviderPlatformReference.class)
														  .setFetchMode("operatingSystem", FetchMode.JOIN)
														  .setFetchMode("virtualMachine", FetchMode.JOIN)
														  .setFetchMode("webServerProvider", FetchMode.JOIN)
														  .setFetchMode("serverCredential", FetchMode.JOIN)
														  .add(Restrictions.idEq(providerRefId)).uniqueResult();
		return reference;
	}
}
 