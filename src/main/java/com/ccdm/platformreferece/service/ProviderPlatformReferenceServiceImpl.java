package com.ccdm.platformreferece.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.platformreferece.dao.ProviderPlatformReferenceDAO;
import com.ccdm.vo.ProviderPlatformReference;

@Service
@Transactional(readOnly = false)
public class ProviderPlatformReferenceServiceImpl implements  ProviderPlatformReferenceService
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderPlatformReferenceServiceImpl.class);

	@Autowired
	private ProviderPlatformReferenceDAO providerPlatformReferenceDao;
	
	@Override
	public ProviderPlatformReference getPlatformReference(Integer providerId, Integer vmId, Integer osId) {
		return providerPlatformReferenceDao.getPlatformReferenceByProviderIdAndVmIdAndOsId(providerId, vmId, osId);
	}

	@Override
	public ProviderPlatformReference getProviderPlatformReferenceById(Integer providerRefId) 
	{
		return providerPlatformReferenceDao.getProviderPlatformReferenceById(providerRefId);
	}

	
}
