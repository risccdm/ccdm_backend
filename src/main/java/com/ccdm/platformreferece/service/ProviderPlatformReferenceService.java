package com.ccdm.platformreferece.service;

import com.ccdm.vo.ProviderPlatformReference;

public interface ProviderPlatformReferenceService 
{
	ProviderPlatformReference getPlatformReference(Integer providerId, Integer vmId, Integer osId);

	ProviderPlatformReference getProviderPlatformReferenceById(Integer providerRefId);
}
