package com.ccdm.vo;
// Generated Dec 16, 2016 4:32:17 PM by Hibernate Tools 5.1.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * NotificationAspectReference generated by hbm2java
 */
@Entity
@Table(name = "notification_aspect_reference", catalog = "ccdmdb")
public class NotificationAspectReference implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5197253492101389153L;
	private Integer id;
	private String methodName;
	private String notificationAuthorization;

	public NotificationAspectReference() {
	}

	public NotificationAspectReference(String methodName, String notificationAuthorization) {
		this.methodName = methodName;
		this.notificationAuthorization = notificationAuthorization;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "METHOD_NAME", nullable = false, length = 500)
	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	@Column(name = "NOTIFICATION_AUTHORIZATION", nullable = false, length = 100)
	public String getNotificationAuthorization() {
		return this.notificationAuthorization;
	}

	public void setNotificationAuthorization(String notificationAuthorization) {
		this.notificationAuthorization = notificationAuthorization;
	}

}
