package com.ccdm.vo;
// Generated 5 Dec, 2016 12:20:11 PM by Hibernate Tools 5.2.0.Beta1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * UserRole generated by hbm2java
 */
@Entity
@Table(name = "user_role", catalog = "ccdmdb")
public class UserRole implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8240505595390909255L;
	private Integer roleId;
	private String roleName;
	private Set<RolePageMap> rolePageMaps = new HashSet<RolePageMap>(0);
	private Set<UserRoleMap> userRoleMaps = new HashSet<UserRoleMap>(0);

	public UserRole() {
	}

	public UserRole(String roleName) {
		this.roleName = roleName;
	}

	public UserRole(String roleName, Set<RolePageMap> rolePageMaps, Set<UserRoleMap> userRoleMaps) {
		this.roleName = roleName;
		this.rolePageMaps = rolePageMaps;
		this.userRoleMaps = userRoleMaps;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ROLE_ID", unique = true, nullable = false)
	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	@Column(name = "ROLE_NAME", nullable = false, length = 45)
	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userRole")
	public Set<RolePageMap> getRolePageMaps() {
		return this.rolePageMaps;
	}

	public void setRolePageMaps(Set<RolePageMap> rolePageMaps) {
		this.rolePageMaps = rolePageMaps;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "userRole")
	public Set<UserRoleMap> getUserRoleMaps() {
		return this.userRoleMaps;
	}

	public void setUserRoleMaps(Set<UserRoleMap> userRoleMaps) {
		this.userRoleMaps = userRoleMaps;
	}

}
