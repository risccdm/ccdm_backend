package com.ccdm.vo;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * InstanceCostReference generated by hbm2java
 */
@Entity
@Table(name = "instance_cost_reference", catalog = "ccdmdb")
public class InstanceCostReference implements java.io.Serializable {

	private static final long serialVersionUID = -5905553635603264056L;
	private Integer id;
	private CostDetails costDetails;
	private InstanceInfo instanceInfo;
	private Date creationDate = new Date();

	public InstanceCostReference() {
	}

	public InstanceCostReference(CostDetails costDetails, InstanceInfo instanceInfo, Date creationDate) {
		this.costDetails = costDetails;
		this.instanceInfo = instanceInfo;
		this.creationDate = creationDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COST_DETAIL_ID", nullable = false)
	public CostDetails getCostDetails() {
		return this.costDetails;
	}

	public void setCostDetails(CostDetails costDetails) {
		this.costDetails = costDetails;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "INSTANCE_INFO_ID", nullable = false)
	public InstanceInfo getInstanceInfo() {
		return this.instanceInfo;
	}

	public void setInstanceInfo(InstanceInfo instanceInfo) {
		this.instanceInfo = instanceInfo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATION_DATE", nullable = false, length = 19)
	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

}
