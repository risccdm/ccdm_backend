package com.ccdm.vo;
// Generated 5 Dec, 2016 12:20:11 PM by Hibernate Tools 5.2.0.Beta1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * InstanceInfo generated by hbm2java
 */
@Entity
@Table(name = "instance_info", catalog = "ccdmdb")
public class InstanceInfo implements java.io.Serializable {

	private static final long serialVersionUID = -7919935294380552447L;
	private Integer id;
	@JsonIgnore
	private ProviderPlatformReference providerPlatformReference;
	private SolutionClientMap solutionClientMap;
	private SolutionVmMap solutionVmMap;
	private String tagName;
	private String instanceId;
	private String instanceType;
	private String availabilityZone;
	private String instanceState;
	private String keyName;
	@JsonFormat(pattern = "E MMM dd HH:mm:ss z yyyy")
	private Date launchTime;
	private String securityGroup;
	private String publicDns;
	private Integer port;
	private String publicIpv4;
	@JsonIgnore
	private Set<InstanceCostReference> instanceCostReferences = new HashSet<InstanceCostReference>(0);

	public InstanceInfo() {
	}

	public InstanceInfo(ProviderPlatformReference providerPlatformReference, String publicDns) {
		this.providerPlatformReference = providerPlatformReference;
		this.publicDns = publicDns;
	}

	public InstanceInfo(ProviderPlatformReference providerPlatformReference, SolutionClientMap solutionClientMap,
			SolutionVmMap solutionVmMap, String tagName, String instanceId, String instanceType,
			String availabilityZone, String instanceState, String keyName, Date launchTime, String securityGroup,
			String publicDns, Integer port, String publicIpv4, Set<InstanceCostReference> instanceCostReferences) {
		this.providerPlatformReference = providerPlatformReference;
		this.solutionClientMap = solutionClientMap;
		this.solutionVmMap = solutionVmMap;
		this.tagName = tagName;
		this.instanceId = instanceId;
		this.instanceType = instanceType;
		this.availabilityZone = availabilityZone;
		this.instanceState = instanceState;
		this.keyName = keyName;
		this.launchTime = launchTime;
		this.securityGroup = securityGroup;
		this.publicDns = publicDns;
		this.port = port;
		this.publicIpv4 = publicIpv4;
		this.instanceCostReferences = instanceCostReferences;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PROVIDER_PLATFORM_REF_ID", nullable = false)
	public ProviderPlatformReference getProviderPlatformReference() {
		return this.providerPlatformReference;
	}

	public void setProviderPlatformReference(ProviderPlatformReference providerPlatformReference) {
		this.providerPlatformReference = providerPlatformReference;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SOLUTION_CIENT_MAP_ID")
	public SolutionClientMap getSolutionClientMap() {
		return this.solutionClientMap;
	}

	public void setSolutionClientMap(SolutionClientMap solutionClientMap) {
		this.solutionClientMap = solutionClientMap;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SOLUTION_VM_MAP_ID")
	public SolutionVmMap getSolutionVmMap() {
		return this.solutionVmMap;
	}

	public void setSolutionVmMap(SolutionVmMap solutionVmMap) {
		this.solutionVmMap = solutionVmMap;
	}

	@Column(name = "TAG_NAME", length = 50)
	public String getTagName() {
		return this.tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Column(name = "INSTANCE_ID", length = 100)
	public String getInstanceId() {
		return this.instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	@Column(name = "INSTANCE_TYPE", length = 100)
	public String getInstanceType() {
		return this.instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	@Column(name = "AVAILABILITY_ZONE", length = 50)
	public String getAvailabilityZone() {
		return this.availabilityZone;
	}

	public void setAvailabilityZone(String availabilityZone) {
		this.availabilityZone = availabilityZone;
	}

	@Column(name = "INSTANCE_STATE", length = 45)
	public String getInstanceState() {
		return this.instanceState;
	}

	public void setInstanceState(String instanceState) {
		this.instanceState = instanceState;
	}

	@Column(name = "KEY_NAME", length = 100)
	public String getKeyName() {
		return this.keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAUNCH_TIME", length = 19)
	public Date getLaunchTime() {
		return this.launchTime;
	}

	public void setLaunchTime(Date launchTime) {
		this.launchTime = launchTime;
	}

	@Column(name = "SECURITY_GROUP", length = 100)
	public String getSecurityGroup() {
		return this.securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	@Column(name = "PUBLIC_DNS", nullable = false, length = 100)
	public String getPublicDns() {
		return this.publicDns;
	}

	public void setPublicDns(String publicDns) {
		this.publicDns = publicDns;
	}

	@Column(name = "PORT")
	public Integer getPort() {
		return this.port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	@Column(name = "PUBLIC_IPv4", length = 100)
	public String getPublicIpv4() {
		return this.publicIpv4;
	}

	public void setPublicIpv4(String publicIpv4) {
		this.publicIpv4 = publicIpv4;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "instanceInfo")
	public Set<InstanceCostReference> getInstanceCostReferences() {
		return this.instanceCostReferences;
	}

	public void setInstanceCostReferences(Set<InstanceCostReference> instanceCostReferences) {
		this.instanceCostReferences = instanceCostReferences;
	}
}
