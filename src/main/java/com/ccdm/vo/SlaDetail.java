package com.ccdm.vo;
// Generated 7 Dec, 2016 11:24:55 AM by Hibernate Tools 5.2.0.Beta1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * SlaDetail generated by hbm2java
 */
@Entity
@Table(name = "sla_detail", catalog = "ccdmdb")
public class SlaDetail implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3847283646273130798L;
	private Integer slaDetailId;
	@JsonIgnore
	private SlaType slaType;
	private String priorityType;
	private Integer responseTime;
	private Integer uptime;
	private Integer workingHours;
	private Integer credits;

	public SlaDetail() {
	}

	public SlaDetail(SlaType slaType) {
		this.slaType = slaType;
	}

	public SlaDetail(SlaType slaType, String priorityType, Integer responseTime, Integer uptime, Integer workingHours,
			Integer credits) {
		this.slaType = slaType;
		this.priorityType = priorityType;
		this.responseTime = responseTime;
		this.uptime = uptime;
		this.workingHours = workingHours;
		this.credits = credits;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "SLA_DETAIL_ID", unique = true, nullable = false)
	public Integer getSlaDetailId() {
		return this.slaDetailId;
	}

	public void setSlaDetailId(Integer slaDetailId) {
		this.slaDetailId = slaDetailId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SLA_TYPE", nullable = false)
	public SlaType getSlaType() {
		return this.slaType;
	}

	public void setSlaType(SlaType slaType) {
		this.slaType = slaType;
	}

	@Column(name = "PRIORITY_TYPE", length = 45)
	public String getPriorityType() {
		return this.priorityType;
	}

	public void setPriorityType(String priorityType) {
		this.priorityType = priorityType;
	}

	@Column(name = "RESPONSE_TIME")
	public Integer getResponseTime() {
		return this.responseTime;
	}

	public void setResponseTime(Integer responseTime) {
		this.responseTime = responseTime;
	}

	@Column(name = "UPTIME")
	public Integer getUptime() {
		return this.uptime;
	}

	public void setUptime(Integer uptime) {
		this.uptime = uptime;
	}

	@Column(name = "WORKING_HOURS")
	public Integer getWorkingHours() {
		return this.workingHours;
	}

	public void setWorkingHours(Integer workingHours) {
		this.workingHours = workingHours;
	}

	@Column(name = "CREDITS")
	public Integer getCredits() {
		return this.credits;
	}

	public void setCredits(Integer credits) {
		this.credits = credits;
	}

}
