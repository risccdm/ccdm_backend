package com.ccdm.viewModel.dao;

import java.util.List;

import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

public interface ViewModelDAO {

	List<OperatingSystem> getOperatingSystems();

	List<VirtualMachine> getVirtualMachineSizes();

	List<WebServerProvider> getWebServerProviders();

	void saveOrUpdate(ViewModel solution);

	ViewModel getTemplate(String name, Integer userId);

	List<ViewModel> getTemplateNames(Integer appUserId);
	List<ViewModel> getTemplates(Integer userId);

	/*Object testEalstic();*/

	ViewModel getTemplateById(Integer templateId);

}
