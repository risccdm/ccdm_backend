package com.ccdm.viewModel.dao;

import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.infrastructure.config.ElasticsearchConfiguration;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

@Repository
public class ViewModelDAOImpl extends BaseDAOImpl implements ViewModelDAO {
	
	@Autowired
	ElasticsearchConfiguration elasticsearchConfiguration;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OperatingSystem> getOperatingSystems(){
		List<OperatingSystem> operatingSystems = getSession().createCriteria(OperatingSystem.class).list();
		System.out.println("vm1"+ operatingSystems);
		return operatingSystems;	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VirtualMachine> getVirtualMachineSizes(){
		List<VirtualMachine> virtualMachines = getSession().createCriteria(VirtualMachine.class).list();
		return virtualMachines;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViewModel> getTemplates(Integer userId){
		List<ViewModel> templates = (List<ViewModel>) getSession().createCriteria(ViewModel.class).
				add(Restrictions.eq("appUser.userId", userId)).
				setFetchMode("operatingSystem", FetchMode.JOIN).
				setFetchMode("virtualMachine", FetchMode.JOIN).
				setFetchMode("webServerProvider", FetchMode.JOIN).addOrder(Order.desc("id")).list();
				System.out.println("this"+ templates);
		return templates;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<WebServerProvider> getWebServerProviders(){
		List<WebServerProvider> webServerProviders = getSession().createCriteria(WebServerProvider.class).list();
		return webServerProviders;
	}
	
	@Override
	public void saveOrUpdate(ViewModel template) {
		super.saveOrUpdate(template);
	}
	
	@Override
	public ViewModel getTemplate(String name, Integer clientId) {
		ViewModel template = (ViewModel) getSession().createCriteria(ViewModel.class).
				add(Restrictions.eq("appUser.userId", clientId)).
				add(Restrictions.eq("name", name)).uniqueResult();
		
		return template;
	}

	@Override
	public List<ViewModel> getTemplateNames(Integer appUserId) 
	{
		
     @SuppressWarnings("unchecked")
	List<ViewModel> templates = (List<ViewModel>) getSession().createCriteria(ViewModel.class)
    		 			                 .add(Restrictions.eq("appUser.userId", appUserId))
    		 			                 .list();
		return templates;
	}

	/*@Override
	public Object testEalstic()
	{
		
		ElasticTeset elasticTeset  = new ElasticTeset();
		elasticTeset.setId(121);
		elasticTeset.setAge(16);
		elasticTeset.setName("Arya");
		
		IndexQuery indexQuery =  new IndexQuery();  
		indexQuery.setObject(elasticTeset);
		indexQuery.setIndexName("ccdmtestindex");
		
		elasticsearchConfiguration.elasticsearchTemplate().index(indexQuery);
		
		return null;
	}*/

	@Override
	public ViewModel getTemplateById(Integer templateId) 
	{
		return (ViewModel) getSession().createCriteria(ViewModel.class)
						   .add(Restrictions.idEq(templateId)).uniqueResult();
	}
}
