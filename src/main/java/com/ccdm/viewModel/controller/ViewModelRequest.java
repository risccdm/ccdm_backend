package com.ccdm.viewModel.controller;

import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.VirtualMachine;

public class ViewModelRequest implements java.io.Serializable {
	
	private String templateName;
	private OperatingSystem operatingSystem;
	private VirtualMachine virtualMachine;
	
	public OperatingSystem getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(OperatingSystem operatingSystem) {
		this.operatingSystem = operatingSystem;
		System.out.println(operatingSystem);
		System.out.println("my5");
	}
	public VirtualMachine getVirtualMachine() {
		return virtualMachine;
	}
	public void setVirtualMachine(VirtualMachine virtualMachine) {
		this.virtualMachine = virtualMachine;
		System.out.println(virtualMachine);
		System.out.println("my6");
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
		System.out.println(templateName);
		System.out.println("my7");
	}
	
}
