package com.ccdm.viewModel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.admin.service.UserService;
import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.viewModel.service.ViewModelService;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.ViewModel;

@RestController
@RequestMapping(value = "/template")
public class ViewModelController extends BaseController 
{
	@Autowired
	private ViewModelService templateService;
	@Autowired
	private UserService userService;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}

	@RequestMapping(value = "/operatingSystems", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getOperatingSystems() {
		System.out.println("vm4" + templateService.getOperatingSystems());
		return getResponseEntity(templateService.getOperatingSystems());
	}
	
	@RequestMapping(value = "/vmSizes", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getVMSizes() {
		return getResponseEntity(templateService.getVirualMachineSizes());
	}
	
	@RequestMapping(value = "/webServerProviders", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getWebServerProviders() {
		return getResponseEntity(templateService.getWebServerProviders());
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ResponseEntity<CCDMResponse>  save(@RequestBody ViewModelRequest templateRequest) throws Exception 
	{
		System.out.println("my1");
		AppUser appUser = getLoggedInUser();
		System.out.println("my2");
		ViewModel template = templateService.buildTemplate(templateRequest, appUser);
		System.out.println("my3");
		templateService.save(template);
		System.out.println("my4");
		return getResponseEntity(template.getVmId());
	}
	
	@RequestMapping(value = "/isPresent", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> isNamePresent(@RequestParam String name) throws CCDMException {
		
		AppUser appUser = userService.getLoggedInUser();
		Boolean isNameAvailable = !templateService.isNamePresent(name,appUser.getClientId());
		return getResponseEntity(isNameAvailable);
	}
	
	@RequestMapping(value = "/names", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> gettemplateNames() throws CCDMUnauthorizedException
	{
		Integer appUserId = userService.getLoggedInUser().getUserId();
		
		return getResponseEntity(templateService.getTemplateNames(appUserId));
	}
	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  gettemplates() throws CCDMUnauthorizedException {
		AppUser appUser = userService.getLoggedInUser();
		return getResponseEntity(templateService.getTemplates(appUser.getUserId()));
	}

	@RequestMapping(value = "/{viewModelId}", method = RequestMethod.GET)
	public CCDMResponse getViewModelById(@PathVariable(value = "viewModelId") Integer viewModelId) throws CCDMUnauthorizedException{
		return getCCDMResponse(templateService.getTemplateById(viewModelId));
	}
}
