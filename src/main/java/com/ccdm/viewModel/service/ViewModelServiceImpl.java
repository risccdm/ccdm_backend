package com.ccdm.viewModel.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.validators.TemplateValidator;
import com.ccdm.viewModel.controller.ViewModelRequest;
import com.ccdm.viewModel.dao.ViewModelDAO;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

@Service
@Transactional(readOnly = false)
public class ViewModelServiceImpl implements ViewModelService {
	
	@Autowired
	private ViewModelDAO templateDao;
	
	@Autowired
	private TemplateValidator templateValidator;
	
	@Override
	public List<OperatingSystem> getOperatingSystems(){
		System.out.println("vm3" + templateDao.getOperatingSystems());
		return templateDao.getOperatingSystems();
	}
	
	@Override
	public List<VirtualMachine> getVirualMachineSizes(){
		return templateDao.getVirtualMachineSizes();
	}
	
	@Override
	public List<ViewModel> getTemplates(Integer userId){
		return templateDao.getTemplates(userId);
	}
	
	@Override
	public List<WebServerProvider> getWebServerProviders(){
		return templateDao.getWebServerProviders();
	}
	
	@Override
	public Boolean isNamePresent(String name, Integer clientId){
		ViewModel template = templateDao.getTemplate(name, clientId);
		
		if(template == null){
			return false;
		}
		return true;
	}
	
	@Override
	public void save(ViewModel template) throws CCDMException 
	{
		templateValidator.validateTemplates(template);
		templateDao.saveOrUpdate(template);
	}

	@Override
	public ViewModel buildTemplate(ViewModelRequest templateRequest, AppUser appUser)
	{
		ViewModel viewModel = new ViewModel();
		viewModel.setAppUser(appUser);
		viewModel.setName(templateRequest.getTemplateName());
		viewModel.setOperatingSystem(templateRequest.getOperatingSystem());
		viewModel.setVirtualMachine(templateRequest.getVirtualMachine());
		viewModel.setClientId(appUser.getClientId());
		return viewModel;
	}

	@Override
	public List<Map<String, String>> getTemplateNames(Integer appUserId) 
	{
		
		List<Map<String, String>> templateNames = new ArrayList<>();
		
		List<ViewModel> templates = templateDao.getTemplateNames(appUserId);
		
		for(ViewModel template : templates)
		{
			Map<String, String> templateName = new HashMap<>();
			templateName.put("id", String.valueOf((int)template.getVmId()));
			templateName.put("name", template.getName());
			templateNames.add(templateName);
		}
		return templateNames;
	}

	/*@Override
	public Object testEalstic()
	{
		return templateDao.testEalstic();
	}*/

	@Override
	public ViewModel getTemplateById(Integer templateId) 
	{
		return templateDao.getTemplateById(templateId);
	}
}
