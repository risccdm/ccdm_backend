package com.ccdm.viewModel.service;

import java.util.List;
import java.util.Map;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.viewModel.controller.ViewModelRequest;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

public interface ViewModelService 
{
	List<OperatingSystem> getOperatingSystems();

	List<VirtualMachine> getVirualMachineSizes();

	List<WebServerProvider> getWebServerProviders();

	void save(ViewModel template) throws CCDMException;

	Boolean isNamePresent(String name, Integer clientId);

	List<Map<String, String>> getTemplateNames(Integer appUserId);
	List<ViewModel> getTemplates(Integer userId);

	/*Object testEalstic();*/
	
	ViewModel buildTemplate(ViewModelRequest solutionRequest, AppUser appUser);

	ViewModel getTemplateById(Integer templateId);

}
