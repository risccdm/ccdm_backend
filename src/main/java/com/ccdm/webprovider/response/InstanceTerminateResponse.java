package com.ccdm.webprovider.response;

public class InstanceTerminateResponse {

	private String currentState;
	private String instanceId;
	
	public InstanceTerminateResponse() {}
	
	public InstanceTerminateResponse(String currentState, String instanceId) {
		this.currentState = currentState;
		this.instanceId = instanceId;
	}
	
	public String getCurrnetState() {
		return currentState;
	}
	public void setCurrnetState(String currnetState) {
		this.currentState = currnetState;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
}
