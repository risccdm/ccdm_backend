package com.ccdm.webprovider.response;

public class InstanceRebootResponse {

	private String currentState;
	private String instanceId;

	public InstanceRebootResponse() {}
	
	public InstanceRebootResponse(String currentState, String instanceId) {
		this.currentState = currentState;
		this.instanceId = instanceId;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
	
}
