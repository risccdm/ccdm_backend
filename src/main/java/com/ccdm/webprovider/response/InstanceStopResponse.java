package com.ccdm.webprovider.response;

public class InstanceStopResponse {

	private String currnetState;
	private String instanceId;
	
	public InstanceStopResponse() {}
	
	public InstanceStopResponse(String currnetState, String instanceId) {
		this.currnetState = currnetState;
		this.instanceId = instanceId;
	}
	
	public String getCurrnetState() {
		return currnetState;
	}
	public void setCurrnetState(String currnetState) {
		this.currnetState = currnetState;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
}
