package com.ccdm.webprovider.response;

import com.ccdm.vo.InstanceInfo;

public class InstanceStatusResponse 
{
	private InstanceInfo instanceInfo;
	
	public InstanceStatusResponse() {}
	
	public InstanceStatusResponse(InstanceInfo instanceInfo) {
		this.setInstanceInfo(instanceInfo);
	}

	public InstanceInfo getInstanceInfo() {
		return instanceInfo;
	}

	public void setInstanceInfo(InstanceInfo instanceInfo) {
		this.instanceInfo = instanceInfo;
	}
}
