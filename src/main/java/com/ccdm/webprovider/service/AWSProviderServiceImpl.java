package com.ccdm.webprovider.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.RebootInstancesRequest;
import com.amazonaws.services.ec2.model.RebootInstancesResult;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;
import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.constants.EInstanceSystemState;
import com.ccdm.instance.factory.AmazonEC2ClientFactory;
import com.ccdm.instance.thread.InstanceStateUpdateThreads;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.utils.AwsUtils;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.webprovider.helper.AWSInstanceHelper;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;

@Component("awsProvider")
public class AWSProviderServiceImpl implements IProviderService 
{
	private static final Logger logger = LoggerFactory.getLogger(AWSProviderServiceImpl.class);
	
	@Autowired
	private AmazonEC2ClientFactory amazonEC2ClientFactory;
	@Autowired
	private AWSInstanceHelper awsInstanceHelper;
	@Autowired
	private AwsUtils awsUtils;
	@Autowired
	private InstanceStateUpdateThreads instanceStateUpdateThread;
	
	@Override
	public InstanceStartResponse start(InstanceInfo instanceInfo) throws CCDMException 
	{
		ServerCredentialVo serverCredentialVo = awsInstanceHelper.getServerCredentialVo(instanceInfo.getProviderPlatformReference());
		StartInstancesRequest startInstancesRequest = new StartInstancesRequest();
		startInstancesRequest.setInstanceIds(awsUtils.getInstanceIdsAsList(instanceInfo.getInstanceId()));
		logger.info("Starting instance Requested to AWS");
		StartInstancesResult startInstancesResult = getAmazonEC2(serverCredentialVo).startInstances(startInstancesRequest);
		InstanceStartResponse instanceStartResponse = awsInstanceHelper.getStartResponse(startInstancesResult);
		awsInstanceHelper.instanceStateUpdateByThread(instanceStartResponse.getCurrnetState(), instanceInfo, serverCredentialVo);
		instanceStateUpdateByThread(instanceInfo, serverCredentialVo);
		return instanceStartResponse;
	}
	
	@Override
	public InstanceStopResponse stop(InstanceInfo instanceInfo) throws CCDMException 
	{
		ServerCredentialVo serverCredentialVo = awsInstanceHelper.getServerCredentialVo(instanceInfo.getProviderPlatformReference());
		StopInstancesRequest stopInstancesRequest = new StopInstancesRequest();
		stopInstancesRequest.setInstanceIds(awsUtils.getInstanceIdsAsList(instanceInfo.getInstanceId()));
		logger.info("Stopping instance Requested to AWS");
		StopInstancesResult stopInstancesResult = getAmazonEC2(serverCredentialVo).stopInstances(stopInstancesRequest);
		InstanceStopResponse stopResponse = awsInstanceHelper.getStopResponse(stopInstancesResult);
		awsInstanceHelper.instanceStateUpdateByThread(stopResponse.getCurrnetState(), instanceInfo, serverCredentialVo);
		instanceStateUpdateByThread(instanceInfo, serverCredentialVo);
		return stopResponse;
	}

	@Override
	public InstanceRebootResponse reboot(InstanceInfo instanceInfo) throws CCDMException 
	{
		ServerCredentialVo serverCredentialVo = awsInstanceHelper.getServerCredentialVo(instanceInfo.getProviderPlatformReference());
		RebootInstancesRequest rebootInstancesRequest = new RebootInstancesRequest();
		rebootInstancesRequest.setInstanceIds(awsUtils.getInstanceIdsAsList(instanceInfo.getInstanceId()));
		logger.info("Rebooting instance Requested to AWS");
		RebootInstancesResult rebootInstancesResult = getAmazonEC2(serverCredentialVo).rebootInstances(rebootInstancesRequest);
		InstanceRebootResponse rebootResponse = awsInstanceHelper.getRebootResponse(rebootInstancesResult, instanceInfo.getInstanceId());
		awsInstanceHelper.instanceStateUpdateByThread(rebootResponse.getCurrentState(), instanceInfo, serverCredentialVo);
		instanceStateUpdateByThread(instanceInfo, serverCredentialVo);
		return rebootResponse;
	}

	@Override
	public InstanceTerminateResponse terminate(InstanceInfo instanceInfo) throws CCDMException 
	{
		ServerCredentialVo serverCredentialVo = awsInstanceHelper.getServerCredentialVo(instanceInfo.getProviderPlatformReference());
		TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest();
		terminateInstancesRequest.setInstanceIds(awsUtils.getInstanceIdsAsList(instanceInfo.getInstanceId()));
		logger.info("Terminating instance Requested to AWS");
		TerminateInstancesResult terminateInstancesResult = getAmazonEC2(serverCredentialVo).terminateInstances(terminateInstancesRequest);
		InstanceTerminateResponse terminateResponse = awsInstanceHelper.getInsanceTerminateResponse(terminateInstancesResult);
		awsInstanceHelper.instanceStateUpdateByThread(terminateResponse.getCurrnetState(), instanceInfo, serverCredentialVo);
		instanceStateUpdateByThread(instanceInfo, serverCredentialVo);
		return terminateResponse;
	}
	
	@Override
	public InstanceStatusResponse describe(InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		DescribeInstancesRequest request = awsInstanceHelper.getDescribeInstancesRequest(instanceInfo.getInstanceId());
		logger.info("Describing instance Requested to AWS");
		DescribeInstancesResult describeInstancesResult = getAmazonEC2(serverCredentialVo).describeInstances(request);
		InstanceStatusResponse response = awsInstanceHelper.getInstanceStatusResponse(describeInstancesResult);
		return response;
	}
	
	@Override
	 public List<InstanceStatusResponse> describeAllInstance(InstanceInfo instanceInfo) throws CCDMException 
	 {
		  logger.info("describing All State of instance(s)...");
		  DescribeInstancesResult descResult = getAmazonEC2(awsInstanceHelper.getServerCredentialVo(instanceInfo.getProviderPlatformReference())).describeInstances();
		  List<InstanceStatusResponse> response = awsInstanceHelper.getAllInstanceStatus(descResult);
		  logger.info("All State of instance(s) described...");
		  return response;
	 }
	
	/* *********************************** Private Methods *********************************** */
	private AmazonEC2 getAmazonEC2(ServerCredentialVo serverCredentialVo) 
	{
		return amazonEC2ClientFactory.getAmazonEC2(serverCredentialVo.getAccessId(), serverCredentialVo.getAccessKey());
	}
	
	private void instanceStateUpdateByThread(InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		CommonValidator.validateString(instanceInfo.getInstanceId(), ErrorCodes.INVALID_INSTANCE_ID);
		CommonValidator.validateObject(serverCredentialVo, ErrorCodes.INVALID_SERVER_CREDENTIAL);
		if(instanceInfo != null)
		{
			if( instanceInfo.getInstanceState().equalsIgnoreCase(EInstanceSystemState.PENDING.getInstanceState())
					|| instanceInfo.getInstanceState().equalsIgnoreCase(EInstanceSystemState.REBOOTING.getInstanceState())
					|| instanceInfo.getInstanceState().equalsIgnoreCase(EInstanceSystemState.STOPPING.getInstanceState())
					|| instanceInfo.getInstanceState().equalsIgnoreCase(EInstanceSystemState.SHUTTING_DOWN.getInstanceState()))
			{
				instanceStateUpdateThread.initiateInstancStateUpdateThread(instanceInfo, serverCredentialVo);
			}
		}
	}
}
