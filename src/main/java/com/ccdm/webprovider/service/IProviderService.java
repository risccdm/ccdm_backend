package com.ccdm.webprovider.service;

import java.util.List;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;

public interface IProviderService {
	
	InstanceStopResponse stop(InstanceInfo instanceInfo) throws CCDMException;

	InstanceStartResponse start(InstanceInfo instanceInfo) throws CCDMException;

	InstanceRebootResponse reboot(InstanceInfo instanceInfo) throws CCDMException;

	InstanceTerminateResponse terminate(InstanceInfo instanceInfo) throws CCDMException;
	
	InstanceStatusResponse describe(InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) throws CCDMException;
	
	List<InstanceStatusResponse> describeAllInstance(InstanceInfo instanceInfo) throws CCDMException;
}
