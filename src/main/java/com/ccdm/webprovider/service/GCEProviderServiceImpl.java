package com.ccdm.webprovider.service;

import java.util.List;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ccdm.common.enums.EGCEImageProject;
import com.ccdm.common.provider.constant.GCEConstants;
import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.GCEException;
import com.ccdm.infrastructure.property.AppCommonProperties;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.webprovider.helper.IGCEInstanceHelper;
import com.ccdm.webprovider.response.InstanceCreateResponse;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;
import com.ccdm.workout.GoogleServerInstance.EGCERegions;
import com.google.cloud.compute.Address;
import com.google.cloud.compute.AddressInfo;
import com.google.cloud.compute.AttachedDisk;
import com.google.cloud.compute.AttachedDisk.PersistentDiskConfiguration;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.DiskId;
import com.google.cloud.compute.DiskInfo;
import com.google.cloud.compute.ImageDiskConfiguration;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.MachineTypeId;
import com.google.cloud.compute.NetworkId;
import com.google.cloud.compute.NetworkInterface;
import com.google.cloud.compute.NetworkInterface.AccessConfig;
import com.google.cloud.compute.Operation;
import com.google.cloud.compute.RegionAddressId;

@Component("gceProvider")
public class GCEProviderServiceImpl implements IProviderService 
{
	private static final Logger logger = LoggerFactory.getLogger(GCEProviderServiceImpl.class);
	
	private static final Integer MOCKVALUE = 1;
	private static final Integer REALVALUE = 2;
	
	@Autowired
	@Qualifier("gceInstanceHelper")
	private IGCEInstanceHelper gceInstanceHelper;
	
	@Autowired
	@Qualifier("gceInstanceMockHelper")
	private IGCEInstanceHelper gceInstanceMockHelper;
	
	@Autowired
	private AppCommonProperties appCommonProperties;
	
	/*@Override
	public InstanceCreateResponse create(CreateInstanceRequestVo creatInstanceRequest) throws CCDMException {
		if(MOCKVALUE.equals(GceRelaOrMock())) {
			return buildInstanceCreateResponse(creatInstanceRequest);
		}
		InstanceInfo info = buildGceInstanceInfo(getInstanceHelper().getCompute(), creatInstanceRequest.getInstanceName(),
				creatInstanceRequest.getInstanceConfig().getInstanceType(), creatInstanceRequest.getInstanceConfig().getImageId());
		return getInstanceHelper().create(info);
	}*/

	@Override
	public InstanceStopResponse stop(InstanceInfo instanceInfo) throws CCDMException {
		InstanceId gceInstanceId = buildGCEInstanceId(instanceInfo.getTagName());
		return getInstanceHelper().stop(gceInstanceId);
	}

	@Override
	public InstanceStartResponse start(InstanceInfo instanceInfo) throws CCDMException {
		InstanceId gceInstanceId = buildGCEInstanceId(instanceInfo.getTagName());
		return getInstanceHelper().start(gceInstanceId);
	}

	@Override
	public InstanceRebootResponse reboot(InstanceInfo instanceInfo) throws CCDMException {
		InstanceId gceInstanceId = buildGCEInstanceId(instanceInfo.getTagName());
		return getInstanceHelper().reBoot(gceInstanceId);
	}

	@Override
	public InstanceStatusResponse describe(InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) throws CCDMException {
		InstanceId gceInstanceId = buildGCEInstanceId(instanceInfo.getTagName());
		return getInstanceHelper().describe(gceInstanceId);
	}

	@Override
	public InstanceTerminateResponse terminate(InstanceInfo instanceInfo) throws CCDMException {
		InstanceId gceInstanceId = buildGCEInstanceId(instanceInfo.getTagName());
		return getInstanceHelper().terminate(gceInstanceId);
	}

	private DiskId getImageId(Compute compute, String imageFamily, String instanceName) throws GCEException {
		ImageId imageId = ImageId.of(EGCEImageProject.UBUNTU_OS_CLOUD.getImageProjectName(), imageFamily);
		DiskId diskId = DiskId.of(GCEConstants.GCE_US_CENTRAL1_A, GCEConstants.GCE_DISK + instanceName);
		ImageDiskConfiguration diskConfiguration = ImageDiskConfiguration.of(imageId);
		DiskInfo disk = DiskInfo.of(diskId, diskConfiguration);
		com.google.cloud.compute.Operation operation = compute.create(disk);
		// Wait for operation to complete
		try {
			operation = operation.waitFor();
		} catch (InterruptedException | TimeoutException e) {
			throw new GCEException("GCE Disk creation failed");
		}
		if (operation.getErrors() == null) {
		  logger.info("Disk " + diskId + " was successfully created");
		  return diskId;
		} else {
		  throw new GCEException("GCE Disk creation failed");
		}
	}
	
	private RegionAddressId createRegion(Compute compute, String instanceName) throws GCEException  {
		RegionAddressId addressId = RegionAddressId.of(EGCERegions.USCENTRAL1.getGCERegionName(), GCEConstants.GCE_REGION_ADDRESS + instanceName);
		Operation operation = compute.create(AddressInfo.of(addressId));
		// Wait for operation to complete
		try {
			operation = operation.waitFor();
		} catch (InterruptedException | TimeoutException e) {
			throw new GCEException("GCE Address creation failed");
		}
		if (operation.getErrors() == null) {
		  logger.info("Address " + addressId + " was successfully created");
		  return addressId;
		} else {
		  throw new GCEException("GCE Address creation failed");
		}
	}
	
	private com.google.cloud.compute.InstanceInfo buildGceInstanceInfo(Compute compute, String instanceName, String machineType, String imageFamily) throws CCDMException {
		Address externalIp = compute.getAddress(createRegion(compute, instanceName));
		InstanceId instanceId = InstanceId.of(GCEConstants.GCE_US_CENTRAL1_A, instanceName);
		NetworkId networkId = NetworkId.of(GCEConstants.GCE_DEFAULT_NERWORK);
		PersistentDiskConfiguration attachConfiguration =
		    PersistentDiskConfiguration.newBuilder(getImageId(compute, imageFamily, instanceName)).setBoot(true).build();
		AttachedDisk attachedDisk = AttachedDisk.of("dev_"+instanceName, attachConfiguration);
		NetworkInterface networkInterface = NetworkInterface.newBuilder(networkId) .setAccessConfigurations(AccessConfig.of(externalIp.getAddress())).build();
		MachineTypeId machineTypeId = MachineTypeId.of(GCEConstants.GCE_US_CENTRAL1_A, machineType);
		com.google.cloud.compute.InstanceInfo instanceInfo = com.google.cloud.compute.InstanceInfo.of(instanceId, machineTypeId, attachedDisk, networkInterface);
		return instanceInfo;
	}
	
	public InstanceId buildGCEInstanceId(String instanceId) {
		InstanceId instance = InstanceId.of(GCEConstants.GCE_US_CENTRAL1_A, instanceId);
		return instance;
	}
	
	// 1 ==>> Mock & 2 ==>> Real
	private IGCEInstanceHelper getInstanceHelper() {
			if(MOCKVALUE.equals(GceRelaOrMock())) {
				return gceInstanceMockHelper;
			}
			if(REALVALUE.equals(GceRelaOrMock())) {
				return gceInstanceHelper;
			}
			return gceInstanceHelper;
	}
	
	private Integer GceRelaOrMock() {
		try {
			Integer mockOrReal = Integer.valueOf(appCommonProperties.getGceInstanceRealOrMock());
			return mockOrReal;
		} catch (Exception e) {
			return REALVALUE;
		}
	}
	
	private InstanceCreateResponse buildInstanceCreateResponse(CreateInstanceRequestVo creatInstanceRequest){
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(creatInstanceRequest.getInstanceName());
		clientInstanceVo.setInstanceState(Operation.Status.DONE.name());
		clientInstanceVo.setInstanceName(creatInstanceRequest.getInstanceName());
		clientInstanceVo.setPublicDns(creatInstanceRequest.getInstanceName());
		clientInstanceVo.setPublicIpv4(creatInstanceRequest.getInstanceName());
		return new InstanceCreateResponse(clientInstanceVo);
	}
	
	@Override
	public List<InstanceStatusResponse> describeAllInstance(InstanceInfo instanceInfo) throws CCDMException {
		return null;
	}
	
}
