package com.ccdm.webprovider.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.factory.GoogleCloudGCEClientFactory;
import com.ccdm.webprovider.response.InstanceCreateResponse;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.Operation;

@Component("gceInstanceMockHelper")
public class GCEInstanceMockHelperImpl implements IGCEInstanceHelper {

	@Autowired
	private GoogleCloudGCEClientFactory googleCloudGCEClientFactory;
	
	@Override
	public InstanceCreateResponse create(InstanceInfo instanceInfo) throws CCDMException {
		return buildInstanceCreateResponse(instanceInfo);
	}

	@Override
	public InstanceStopResponse stop(InstanceId instanceId) throws CCDMException {
		return new InstanceStopResponse(Operation.Status.DONE.name(), instanceId.getInstance());
	}

	@Override
	public InstanceStartResponse start(InstanceId instanceId) throws CCDMException {
		return new InstanceStartResponse(Operation.Status.DONE.name(), instanceId.getInstance());
	}

	@Override
	public InstanceRebootResponse reBoot(InstanceId instanceId) throws CCDMException {
		return new InstanceRebootResponse(Operation.Status.DONE.name(), instanceId.getInstance());
	}

	@Override
	public InstanceStatusResponse describe(InstanceId instanceId) throws CCDMException {
		return new InstanceStatusResponse(new com.ccdm.vo.InstanceInfo());
	}

	@Override
	public InstanceTerminateResponse terminate(InstanceId instanceId) throws CCDMException {
		return new InstanceTerminateResponse(Operation.Status.DONE.name(), instanceId.getInstance());
	}

	@Override
	public Compute getCompute() throws CCDMException {
		return googleCloudGCEClientFactory.getGoogleCloudGCECompute();
	}
	
	private InstanceCreateResponse buildInstanceCreateResponse(InstanceInfo instanceInfo){
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(instanceInfo.getInstanceId().getInstance());
		clientInstanceVo.setInstanceState(instanceInfo.getStatus().name());
		clientInstanceVo.setInstanceName(instanceInfo.getTags().getValues().get(0));
		clientInstanceVo.setPublicDns(instanceInfo.getInstanceId().getSelfLink());
		clientInstanceVo.setPublicIpv4(instanceInfo.getNetworkInterfaces().get(0).getNetwork().getNetwork());
		return new InstanceCreateResponse(clientInstanceVo);
	}
}
