package com.ccdm.webprovider.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.RebootInstancesResult;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TerminateInstancesResult;
import com.ccdm.common.validator.CommonValidator;
import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.elastic.solutionhistory.model.CreateInstanceRequestVo;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.constants.EInstanceSystemState;
import com.ccdm.instance.constants.InstanceConstant;
import com.ccdm.instance.thread.InstanceStateUpdateThreads;
import com.ccdm.instance.validator.InstanceValidator;
import com.ccdm.platformreferece.controller.ProviderPlatformReferenceController;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.vo.ProviderPlatformReference;
import com.ccdm.vo.ServerCredential;
import com.ccdm.webprovider.response.InstanceCreateResponse;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;

@Component
public class AWSInstanceHelper
{
	@Autowired
	private InstanceStateUpdateThreads instanceStateUpdateThread;
	@Autowired
	private ProviderPlatformReferenceController providerPlatformReferenceController;
	
	public RunInstancesRequest getCreateRunInstanceRequest(CreateInstanceRequestVo request) throws CCDMException 
	{
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest()
    	        .withInstanceType(request.getInstanceConfig().getInstanceType())
    	        .withImageId(request.getInstanceConfig().getImageId())
    	        .withMinCount(1)
    	        .withMaxCount(1)
    	        .withKeyName(request.getInstanceConfig().getPemKey());
		runInstancesRequest.setSecurityGroups(getSecurityGroups(request.getInstanceConfig().getSecurityGroup()));
		return runInstancesRequest;
	}

	public InstanceCreateResponse getCreateResponse(InstanceInfo instanceInfo) throws CCDMException 
	{
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(instanceInfo.getInstanceId());
		clientInstanceVo.setInstanceState(instanceInfo.getInstanceState());
		clientInstanceVo.setKeyName(instanceInfo.getKeyName());
		clientInstanceVo.setLaunchTime(instanceInfo.getLaunchTime());
		clientInstanceVo.setPort(instanceInfo.getPort());
		clientInstanceVo.setPublicDns(instanceInfo.getPublicDns());
		clientInstanceVo.setPublicIpv4(instanceInfo.getPublicIpv4());
		clientInstanceVo.setInstanceName(instanceInfo.getTagName());
		
		InstanceCreateResponse response = new InstanceCreateResponse();
		response.setClientInstanceVo(clientInstanceVo);
		return response;
	}

	public CreateTagsRequest getCreateTagRequest(String instanceId, String tagName) throws CCDMException 
	{
		return new CreateTagsRequest().withResources(instanceId)
		 					   		  .withTags(new Tag(InstanceConstant.TAG_NAME_KEY, tagName));
	}

	public DescribeInstancesRequest getDescribeInstancesRequest(String instanceId) throws CCDMException 
	{
		DescribeInstancesRequest describeInstancesRequest = new DescribeInstancesRequest();
		describeInstancesRequest.setInstanceIds(getInstanceIds(instanceId));
		return describeInstancesRequest;
	}
	
	public Instance getInstanceFromReservation(Reservation reservation)
	{
		// it will return only one insance if we made one instance create request
		List<Instance> instances = reservation.getInstances();
		return instances.get(0);
	}
	
	public InstanceInfo buildInstanceInfo(Instance instance) 
	{
		InstanceInfo instanceInfo = new InstanceInfo();
		instanceInfo.setInstanceId(instance.getInstanceId());
		instanceInfo.setInstanceState(instance.getState().getName());
		instanceInfo.setInstanceType(instance.getInstanceType());
		instanceInfo.setKeyName(instance.getKeyName());
		instanceInfo.setLaunchTime(instance.getLaunchTime());
		instanceInfo.setPort(InstanceConstant.INSTANCE_DEFAULT_PORT_NUMBER);
		instanceInfo.setPublicDns(instance.getPublicDnsName());
		instanceInfo.setPublicIpv4(instance.getPublicIpAddress());
		instanceInfo.setSecurityGroup(instance.getSecurityGroups().get(0).getGroupName());
		return instanceInfo;
	}
	
	public void instanceStateUpdateByThread(String newInstanceState, InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		try
		{
			CommonValidator.validateObject(instanceInfo, ErrorCodes.INVALID_INSTANCE_ID);
			InstanceValidator.validateServerCredentialVo(serverCredentialVo);
			if(newInstanceState != null && !newInstanceState.isEmpty())
			{
				if( newInstanceState.equalsIgnoreCase(EInstanceSystemState.PENDING.getInstanceState())
						|| newInstanceState.equalsIgnoreCase(EInstanceSystemState.REBOOTING.getInstanceState())
						|| newInstanceState.equalsIgnoreCase(EInstanceSystemState.STOPPING.getInstanceState())
						|| newInstanceState.equalsIgnoreCase(EInstanceSystemState.SHUTTING_DOWN.getInstanceState()))
				{
					instanceStateUpdateThread.initiateInstancStateUpdateThread(instanceInfo, serverCredentialVo);
				}
			}
		}
		catch(Exception e) {
			throw new CCDMException(ErrorCodes.CANNOT_UPDATE_INSTANCE_STATE);
		}
	}
	
	/* *********************************** Private Methods *********************************** */
	private Collection<String> getInstanceIds(String instanceId) 
	{
		List<String> instanceIds = new ArrayList<>();
		instanceIds.add(instanceId);
		return instanceIds;
	}
	
	private Collection<String> getSecurityGroups(String securityGroup) 
	{
		List<String> securityGroups = new ArrayList<>();
		securityGroups.add(securityGroup);
		return securityGroups;
	}

	public InstanceStatusResponse getInstanceStatusResponse(DescribeInstancesResult describeInstancesResult) 
	{
		Instance instance = getInstanceFromReservation(describeInstancesResult.getReservations().get(0));
		InstanceStatusResponse response = new InstanceStatusResponse(buildInstanceInfoFromInstance(instance));
		return response;
	}
	
	private InstanceInfo buildInstanceInfoFromInstance(Instance instance)
	{
		InstanceInfo instanceInfo = new InstanceInfo();
		instanceInfo.setInstanceId(instance.getInstanceId());
		instanceInfo.setInstanceState(instance.getState().getName());
		instanceInfo.setInstanceType(instance.getInstanceType());
		instanceInfo.setKeyName(instance.getKeyName());
		instanceInfo.setLaunchTime(instance.getLaunchTime());
		instanceInfo.setPublicDns(instance.getPublicDnsName());
		instanceInfo.setPublicIpv4(instance.getPublicIpAddress());
		if(instance.getSecurityGroups() != null && instance.getSecurityGroups().size() > 0)
		{
			instanceInfo.setSecurityGroup(instance.getSecurityGroups().get(0).getGroupName());
		}
		if(instance.getTags() != null && instance.getTags().size() > 0)
		{
			instanceInfo.setTagName(instance.getTags().get(0).getValue());
		}
		return instanceInfo;
	}

	public InstanceStartResponse getStartResponse(StartInstancesResult startInstancesResult) {
		return new InstanceStartResponse(startInstancesResult.getStartingInstances().get(0).getCurrentState().getName(), startInstancesResult.getStartingInstances().get(0).getInstanceId());
	}

	public InstanceStopResponse getStopResponse(StopInstancesResult stopInstancesResult) {
		return new InstanceStopResponse(stopInstancesResult.getStoppingInstances().get(0).getCurrentState().getName(), stopInstancesResult.getStoppingInstances().get(0).getInstanceId());
	}

	public InstanceRebootResponse getRebootResponse(RebootInstancesResult rebootInstancesResult, String isntanceId) {
		return new InstanceRebootResponse(EInstanceSystemState.REBOOTING.getInstanceState(), isntanceId);
	}

	public InstanceTerminateResponse getInsanceTerminateResponse(TerminateInstancesResult terminateInstancesResult) {
		return new InstanceTerminateResponse(terminateInstancesResult.getTerminatingInstances().get(0).getCurrentState().getName(), terminateInstancesResult.getTerminatingInstances().get(0).getInstanceId());
	}

	public List<InstanceStatusResponse> getAllInstanceStatus(DescribeInstancesResult descResult) 
	{
		List<InstanceStatusResponse> list = new ArrayList<>();
		for(Reservation reservation : descResult.getReservations())
		{
			for(Instance instance : reservation.getInstances())
			{
				InstanceStatusResponse response = new InstanceStatusResponse(buildInstanceInfoFromInstance(instance));
				list.add(response);
			}
		}
		return list;
	}
	
	public List<String> getInstanceIds(List<InstanceInfo> instanceInfoList) 
	{
		List<String> instanceIds = new ArrayList<>();
		if(instanceInfoList == null || instanceInfoList.size() <= 0)
		{
			return instanceIds;
		}
		
		for(InstanceInfo instanceInfo : instanceInfoList)
		{
			instanceIds.add(instanceInfo.getInstanceId());
		}
		
		return instanceIds;
	}
	
	public ServerCredentialVo getServerCredentialVo(ProviderPlatformReference providerPlatformReference)  throws CCDMException 
	{
		ProviderPlatformReference providerPlatformRef = providerPlatformReferenceController.getProviderPlatformReferenceById(providerPlatformReference.getId());
		ServerCredentialVo credentialVo =  buildServerCredentialVo(providerPlatformRef.getServerCredential());
		return credentialVo;
	}
	
	private ServerCredentialVo buildServerCredentialVo(ServerCredential serverCredential) throws CCDMException 
	{
		if(serverCredential == null)
		{
			throw new CCDMException(ErrorCodes.INVALID_SERVER_CREDENTIAL);
		}
		ServerCredentialVo serverCredentialVo = new ServerCredentialVo();
		serverCredentialVo.setAccessId(serverCredential.getAccessId());
		serverCredentialVo.setAccessKey(serverCredential.getAccessKey());
		serverCredentialVo.setPemKey(serverCredential.getPemKey());
		serverCredentialVo.setUser(serverCredential.getUser());
		return serverCredentialVo;
	}
}
