package com.ccdm.webprovider.helper;

import java.util.concurrent.TimeoutException;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.GCEException;
import com.ccdm.webprovider.response.InstanceCreateResponse;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.Operation;

public interface IGCEInstanceHelper {

	InstanceCreateResponse create(InstanceInfo instanceInfo) throws CCDMException;

	InstanceStopResponse stop(InstanceId instanceId) throws CCDMException;

	InstanceStartResponse start(InstanceId instanceId) throws CCDMException;

	InstanceRebootResponse reBoot(InstanceId instanceId) throws CCDMException;

	InstanceStatusResponse describe(InstanceId instanceId) throws CCDMException;

	InstanceTerminateResponse terminate(InstanceId instanceId) throws CCDMException;

	Compute getCompute() throws CCDMException;
	
	default Operation waitfor(Operation operation) throws GCEException {
		try {
			operation = operation.waitFor();
			if (operation.getErrors() == null) {
				return operation;
			} else {
				throw new GCEException(ErrorCodes.GCE_OPERATION_WAITFOR_EXCEPTION);
			}
		} catch (InterruptedException | TimeoutException e) {
			throw new GCEException(ErrorCodes.GCE_OPERATION_WAITFOR_EXCEPTION);
		}
	}
	
}
