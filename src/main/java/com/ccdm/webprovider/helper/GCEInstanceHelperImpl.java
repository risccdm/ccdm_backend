package com.ccdm.webprovider.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.biz.GCEInstanceResponseAdapter;
import com.ccdm.instance.factory.GoogleCloudGCEClientFactory;
import com.ccdm.webprovider.helper.IGCEInstanceHelper;
import com.ccdm.webprovider.response.InstanceCreateResponse;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.Operation;

@Component("gceInstanceHelper")
public class GCEInstanceHelperImpl implements IGCEInstanceHelper {

	@Autowired
	private GCEInstanceResponseAdapter gceInstanceResponseAdapter;
	
	@Autowired
	private GoogleCloudGCEClientFactory googleCloudGCEClientFactory;
	
	@Override
	public InstanceStartResponse start(InstanceId instanceId) throws CCDMException {
		Operation operation = waitfor(getCompute().start(instanceId)) ;
		return (InstanceStartResponse) gceInstanceResponseAdapter.buildResponse(operation, InstanceStartResponse.class);
	}

	@Override
	public InstanceStopResponse stop(InstanceId instanceId) throws CCDMException {
		Operation operation =  waitfor(getCompute().stop(instanceId));
		return (InstanceStopResponse) gceInstanceResponseAdapter.buildResponse(operation, InstanceStopResponse.class);
	}

	@Override
	public InstanceRebootResponse reBoot(InstanceId instanceId) throws CCDMException {
		Operation operation = waitfor(getCompute().reset(instanceId));
		return (InstanceRebootResponse) gceInstanceResponseAdapter.buildResponse(operation, InstanceRebootResponse.class);
	}

	@Override
	public InstanceCreateResponse create(InstanceInfo instanceInfo) throws CCDMException {
		Operation operation = waitfor(getCompute().create(instanceInfo));
		return (InstanceCreateResponse) gceInstanceResponseAdapter.buildResponse(instanceInfo, InstanceCreateResponse.class);
	}

	@Override
	public InstanceStatusResponse describe(InstanceId instanceId) throws CCDMException {
		Instance instance = getCompute().getInstance(instanceId);
		return (InstanceStatusResponse) gceInstanceResponseAdapter.buildResponse(instance, InstanceStatusResponse.class);
	}

	@Override
	public InstanceTerminateResponse terminate(InstanceId instanceId) throws CCDMException {
		Operation operation = waitfor(getCompute().deleteInstance(instanceId));
		return (InstanceTerminateResponse) gceInstanceResponseAdapter.buildResponse(operation, InstanceTerminateResponse.class);
	}

	@Override
	public Compute getCompute() throws CCDMException {
		return googleCloudGCEClientFactory.getGoogleCloudGCECompute();
	}
	
}
