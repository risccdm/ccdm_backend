package com.ccdm.webprovider.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ccdm.provider.constants.EProvider;
import com.ccdm.webprovider.service.AWSProviderServiceImpl;
import com.ccdm.webprovider.service.GCEProviderServiceImpl;
import com.ccdm.webprovider.service.IProviderService;

@Component
public class WebServerProviderFactory 
{
	@Autowired
	@Qualifier("awsProvider")
	private AWSProviderServiceImpl awsProviderService;
	@Autowired
	@Qualifier("gceProvider")
	private GCEProviderServiceImpl gceProviderService;
	
	public IProviderService getWebProviderByProviderName(String providerName)
	{
		if(providerName.equalsIgnoreCase(EProvider.AMAZON.getProviderName()))
		{
			return awsProviderService;
		}
		else if(providerName.equalsIgnoreCase(EProvider.GOOGLE.getProviderName()))
		{
			return gceProviderService;
		}
		return null;
	}
}
