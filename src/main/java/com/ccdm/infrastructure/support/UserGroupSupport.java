package com.ccdm.infrastructure.support;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.errors.ErrorId;
import com.ccdm.errors.ErrorId.Severity;
import com.ccdm.exceptions.CCDMUnauthorizedException;

public abstract class UserGroupSupport 
{
	public String getLoggedInUserName() throws CCDMUnauthorizedException 
	{
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (obj != null && obj instanceof User) 
		{
			User user = (User) obj;
			return user.getUsername();
		}
		throw new CCDMUnauthorizedException(new ErrorId(ErrorCodes.INVALID_USER.getErrorCode(),
				ErrorCodes.INVALID_USER.getErrorMessage(), Severity.FATAL), HttpStatus.UNAUTHORIZED);
	}
}
