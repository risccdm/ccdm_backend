package com.ccdm.infrastructure.support;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class HibernateDAOSupport extends UserGroupSupport
{
	private static final Logger logger = LoggerFactory.getLogger(HibernateDAOSupport.class);
	
	public abstract SessionFactory getSessionFactory();
	
	public Session getSession() 
	{
		Session currentSession = null;
		try
		{
			currentSession = getSessionFactory().getCurrentSession();
		}
		catch(Exception e)
		{
			logger.error("Error while getting current session.!");
		}
		return currentSession;
	}
	
	public void save(Object model) {
		getSession().save(model);
	}

	public void saveOrUpdate(Object model) {
		getSession().saveOrUpdate(model);
	}
	
	public void delete(Object model) {
		getSession().delete(model);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> List<T> criteria(Criteria criteria) { 
	    return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getById(Class<?> clazz, Long id) {
		return  (T) getSession().createCriteria(clazz).add(Restrictions.eq("id", id)).uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getByField(Class<?> clazz, String field, Object id) {
		return  (T) getSession().createCriteria(clazz).add(Restrictions.eq(field, id)).uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> getListOfRecordsByField(Class<?> clazz, String field, Object id) {
		return  getSession().createCriteria(clazz).add(Restrictions.eq(field, id)).list();
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> getAll(Class<?> clazz) {
		return getSession().createCriteria(clazz).list();
	}
}
