package com.ccdm.infrastructure.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import com.ccdm.property.EnvironmentProperties;

@Configuration
public class LdapConfiguration 
{
	@Autowired
	EnvironmentProperties envProp;
	
	@Bean
	public LdapContextSource contextSource()
	{
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl("localhost");
		/*contextSource.setBase(base);
		contextSource.setUserDn(userDn);
		contextSource.setPassword(password);*/
		return contextSource;
	}
	
	@Bean
	public LdapTemplate ldapTemplate()
	{
		return new LdapTemplate(contextSource());
	}
}
