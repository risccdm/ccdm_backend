package com.ccdm.infrastructure.config;

import java.beans.PropertyVetoException;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.ccdm.infrastructure.property.HibernateProperty;
import com.ccdm.property.DatabaseProperties;
import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@ComponentScan("com.ccdm")
@EnableTransactionManagement
@EnableJpaRepositories("com.ccdm")
public class ApplicationContextConfig 
{
	private static final Logger logger = LoggerFactory.getLogger(ApplicationContextConfig.class);
	
	@Autowired
	private HibernateProperty hibernateProperty;
	
	@Autowired
	private DatabaseProperties databaseProperties;
	
	@Autowired
	@Bean(name = "dataSource")
	public DataSource getDataSource()
	{
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		try
		{
			dataSource.setDriverClass(databaseProperties.getJdbcDriver());
	    	dataSource.setJdbcUrl(databaseProperties.getJdbcUrl());
			dataSource.setUser(databaseProperties.getJdbcUser());
			dataSource.setPassword(databaseProperties.getJdbcPassword());
		}
		catch(PropertyVetoException e)
		{
			e.printStackTrace();
		}
		return dataSource;
	}
	
	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource)
	{
		LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
		sessionBuilder.addProperties(hibernateProperty.getHibernateProperties());
		sessionBuilder.scanPackages("com.ccdm.vo");
		
		logger.info("SessionFactory Created..!");
		return sessionBuilder.buildSessionFactory();
	}
	
	@Autowired
	@Bean(name = "transactionManager")
	@Primary
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory)
	{
		HibernateTransactionManager hibernateTransactionManager = new HibernateTransactionManager(sessionFactory);
		hibernateTransactionManager.setNestedTransactionAllowed(true);
		logger.info("Hibernate Transaction Manager Created..!");
		return hibernateTransactionManager;
	}
	
	@Autowired
	@Bean
	public HibernateTemplate hibernateTemplate(SessionFactory sessionFactory)
	{
		logger.info("Hibernate Template Created..!");
		return new HibernateTemplate(sessionFactory);
	}
	
	@Bean(name = "entityManagerFactory")
	@Autowired
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory()
	{
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(getDataSource());
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan("com.ccdm.vo");
		entityManagerFactoryBean.setJpaProperties(hibernateProperty.getHibernateProperties());
		return entityManagerFactoryBean;
	}
	
	@Autowired
	@Bean(name = "jpaTransactionManager")
	public JpaTransactionManager jpaTransactionManager()
	{
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
		return jpaTransactionManager;
	}
}
