package com.ccdm.infrastructure.config; 

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import com.ccdm.property.EnvironmentProperties;
import com.ccdm.property.PropertyConstants;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.ccdm")
public class ElasticsearchConfiguration 
{
    @Autowired
	private EnvironmentProperties environmentProperties; 
	
	private static final Logger logger = LoggerFactory.getLogger(ElasticsearchConfiguration.class);

	@Bean
	public Client client() 
	{
		TransportClient client = null;
		try 
		{
			Settings settings = Settings.settingsBuilder()
	                .put(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_PING_TIME_OUT, 
	                		environmentProperties.getElasticClientTransportPingTimeOut())
	                .put(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_SNIFF, 
	                		environmentProperties.getElasticClientTransportSniff())
	                .put(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_NODES_SAMPLER_INTERVAL, 
	                		environmentProperties.getElasticClientTransportNodesSmaplerInterval())
	                .put(PropertyConstants.ELASTIC_CLUSTER_NAME, environmentProperties.getElasticClusterName()).build();
	
			client = TransportClient.builder().settings(settings).build();
			
			InetAddress inetAddress = InetAddress.getByName(environmentProperties.getElasticClientTransportIp());
			TransportAddress address = new InetSocketTransportAddress(inetAddress, Integer.valueOf(environmentProperties.getElasticClientTransportPort()));
			client.addTransportAddress(address);
		}
		catch (UnknownHostException e)
	    {
		   logger.error("Connecting elasticsearch client");
		}
		return client;
	}

	@Bean
	public ElasticsearchOperations elasticsearchTemplate()
	{
	   return new ElasticsearchTemplate(client());
	}

	@Bean
	public SearchRequestBuilder getSolutionhHistoryQueryBuilder()
	{
		SearchRequestBuilder searchReqBuilder = client().prepareSearch(environmentProperties.getElasticClusterName());
		searchReqBuilder.setTypes(environmentProperties.getElasticClusterTypeSolutionHistory());
		return searchReqBuilder;
	}
}