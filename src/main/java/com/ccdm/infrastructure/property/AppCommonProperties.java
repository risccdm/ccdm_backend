package com.ccdm.infrastructure.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="mainApp")
public class AppCommonProperties 
{
	private String hibernateMySQLDialect;
	private String gceInstanceRealOrMock;

	public void setHibernateMySQLDialect(String hibernateMySQLDialect) {
		this.hibernateMySQLDialect = hibernateMySQLDialect;
	}
	
	public String getHibernateMySQLDialect() {
		return hibernateMySQLDialect;
	}

	public String getGceInstanceRealOrMock() {
		return gceInstanceRealOrMock;
	}

	public void setGceInstanceRealOrMock(String gceInstanceRealOrMock) {
		this.gceInstanceRealOrMock = gceInstanceRealOrMock;
	}
	
}
