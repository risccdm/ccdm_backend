package com.ccdm.infrastructure.property;

import java.util.Properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mainApp.hibernate")
public class HibernateProperty 
{
	private String showSql;
	private String mySqlDialect;
	private String connectionProvider;
	private int c3p0MinSize;
	private int c3p0MaxSize;
	private int acquireIncrement;
	private int c3p0IdleTestPeriod;
	private int c3p0MaxStatements;
	private int c3p0TimeOut;
	private boolean orderUpdates;
	private boolean orderInserts;
	private String hibernateCurrentSessionContextClass;
	
	public String getShowSql() {
		return showSql;
	}
	public void setShowSql(String showSql) {
		this.showSql = showSql;
	}
	public String getMySqlDialect() {
		return mySqlDialect;
	}
	public void setMySqlDialect(String mySqlDialect) {
		this.mySqlDialect = mySqlDialect;
	}
	public String getConnectionProvider() {
		return connectionProvider;
	}
	public void setConnectionProvider(String connectionProvider) {
		this.connectionProvider = connectionProvider;
	}
	public int getC3p0MinSize() {
		return c3p0MinSize;
	}
	public void setC3p0MinSize(int c3p0MinSize) {
		this.c3p0MinSize = c3p0MinSize;
	}
	public int getC3p0MaxSize() {
		return c3p0MaxSize;
	}
	public void setC3p0MaxSize(int c3p0MaxSize) {
		this.c3p0MaxSize = c3p0MaxSize;
	}
	public int getAcquireIncrement() {
		return acquireIncrement;
	}
	public void setAcquireIncrement(int acquireIncrement) {
		this.acquireIncrement = acquireIncrement;
	}
	public int getC3p0IdleTestPeriod() {
		return c3p0IdleTestPeriod;
	}
	public void setC3p0IdleTestPeriod(int c3p0IdleTestPeriod) {
		this.c3p0IdleTestPeriod = c3p0IdleTestPeriod;
	}
	public int getC3p0MaxStatements() {
		return c3p0MaxStatements;
	}
	public void setC3p0MaxStatements(int c3p0MaxStatements) {
		this.c3p0MaxStatements = c3p0MaxStatements;
	}
	public int getC3p0TimeOut() {
		return c3p0TimeOut;
	}
	public void setC3p0TimeOut(int c3p0TimeOut) {
		this.c3p0TimeOut = c3p0TimeOut;
	}
	public boolean isOrderUpdates() {
		return orderUpdates;
	}
	public void setOrderUpdates(boolean orderUpdates) {
		this.orderUpdates = orderUpdates;
	}
	public boolean isOrderInserts() {
		return orderInserts;
	}
	public void setOrderInserts(boolean orderInserts) {
		this.orderInserts = orderInserts;
	}
	public String getHibernateCurrentSessionContextClass() {
		return hibernateCurrentSessionContextClass;
	}
	public void setHibernateCurrentSessionContextClass(String hibernateCurrentSessionContextClass) {
		this.hibernateCurrentSessionContextClass = hibernateCurrentSessionContextClass;
	}
	
	public Properties getHibernateProperties()
	{
		Properties properties = new Properties();
		properties.put("hibernate.show_sql", showSql);
    	properties.put("hibernate.dialect", mySqlDialect);
    	properties.put("connection.provider_class", connectionProvider);
    	properties.put("hibernate.c3p0.min_size", c3p0MinSize);
    	properties.put("hibernate.c3p0.max_size", c3p0MaxSize);
    	properties.put("hibernate.c3p0.acquire_increment", acquireIncrement);
    	properties.put("hibernate.c3p0.idle_test_period", c3p0IdleTestPeriod);
    	properties.put("hibernate.c3p0.max_statements", c3p0MaxStatements);
    	properties.put("hibernate.c3p0.timeout", c3p0TimeOut);
    	properties.put("hibernate.order_updates", orderUpdates);
    	properties.put("hibernate.order_inserts", orderInserts);
    	//properties.put("hibernate.current_session_context_class", hibernateCurrentSessionContextClass);
    	
    	return properties;
	}
}
