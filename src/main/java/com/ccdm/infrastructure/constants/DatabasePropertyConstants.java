package com.ccdm.infrastructure.constants;

public class DatabasePropertyConstants 
{
	public static final String MYSQL_DATASOURCE_URL = "mysqlDataSourceURL";
	public static final String MYSQL_USERNAME = "mysqlUserName";
	public static final String MYSQL_PASSWORD = "mysqlPassword";
	public static final String MYSQL_DRIVER = "mysqlDriver";
}
