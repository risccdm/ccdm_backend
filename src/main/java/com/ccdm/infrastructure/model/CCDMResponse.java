package com.ccdm.infrastructure.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class CCDMResponse 
{
	private final String CONTENT = "content";
	
	private final String EMPTY = "";
	
	private final String STATUS = "status";
	
	private final String SUCCESS = "Success!";
	
	private Map<String, Object> res;
	
	{
		res = new HashMap<String, Object>();
		res.put(CONTENT, EMPTY);
		res.put(STATUS, SUCCESS);
	}
	
	public <T> void setContent(T obj) {
		res.put(CONTENT, obj);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getContent() {
		System.out.println("hi"+res.get(CONTENT));
		return (T) res.get(CONTENT);
	}
	
	public <T> void setStatus(T obj) {
		
		res.put(STATUS, obj);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getStatus() {
		return (T) res.get(STATUS);
	}
}
