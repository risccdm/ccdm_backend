package com.ccdm.jdbc.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.property.DatabaseProperties;
import com.ccdm.property.EnvironmentProperties;

@Component
public class JdbcConnnectionProvider {

	@Autowired
	private DatabaseProperties  databaseProperties;
	@Autowired
	private EnvironmentProperties environmentProperties;
	
	private static final Logger logger = LoggerFactory.getLogger(JdbcConnnectionProvider.class);
	
	public Connection getJdbcConnection() {
		Connection connection = null;
		try {
			Class.forName(databaseProperties.getJdbcDriver());
			connection = (Connection) DriverManager.getConnection(databaseProperties.getJdbcUrl(), environmentProperties.getBatchMySqlUsername(), environmentProperties.getBatchMySqlPassword());
			logger.info("JDBC Connection Created Successfully!!!");
		} catch (ClassNotFoundException | SQLException ex) {
			logger.error("Could not get JDBC Connection ");
			return connection;
		} 
		return connection;
	}
	
}
