package com.ccdm.errors;

public interface ErrorCodes 
{
	ErrorId INTERNAL_ERROR = new ErrorId("C1001", "Something went wrong. Please try again...");
	ErrorId INVALID_USER = new ErrorId("C1002", "Please login to continue..");
	ErrorId INVALID_PASSWORD = new ErrorId("C1003","Password cannot be null");
	ErrorId REQUEST_OBJECT_CAST_ERROR = new ErrorId("C1004", "Request Object cast error.");
	ErrorId RESPONSE_OBJECT_CAST_ERROR = new ErrorId("C1005", "Response Object cast error.");
	
	// Instance Exception
	ErrorId INSTANCES_NOT_AVAILABLE_TO_TAG = new ErrorId("C1101","Instance Not Available to Set Tag Names.");
	ErrorId MISMATCH_INSTANCE_TAG_NAMES = new ErrorId("C1102","Tag Names must be less than or equal to Instance List.");
	ErrorId INSTANCE_CANNOT_CREATED_ON_SERVER = new ErrorId("C1103", "Problem creating Instance on Server");
	ErrorId CANNOT_CHECK_INSTANCE_STATUS = new ErrorId("C1104", "Problem on Checking Instance Status");
	ErrorId CANNOT_STOP_INSTANCE = new ErrorId("C1105", "Problem with Stopping Instance.");
	ErrorId CANNOT_START_INSTANCE = new ErrorId("C1106", "Problem with Starting Instance.");
	ErrorId CANNOT_REBOOT_INSTANCE = new ErrorId("C1107", "Problem with Rebooting Instance.");
	ErrorId CANNOT_TERMINATE_INSTANCE = new ErrorId("C1108", "Problem with Terminating Instance.");
	ErrorId KEY_ALREADY_EXISTS = new ErrorId("C1107", "Cannot Create Key. KeyName May Already Exists. Try difference name.");
	ErrorId CANNOT_CREATE_KEY = new ErrorId("C1108", "Cannot Create Key.");
	ErrorId ERROR_CLIENT_CONFIG_IN_MASTER_ICINGA = new ErrorId("C1109", "Cannot Config Client Instance in Master Icinga");
	ErrorId ERROR_CLIENT_ICINGA_CONFIG = new ErrorId("C1110", "Cannot Config Client Icinga in Instance.");
	ErrorId ERROR_ICINGA_INSTALLATION = new ErrorId("C1111", "Error in Icinga Installation.");
	ErrorId INVALID_TICKET_SALT = new ErrorId("C1113", "Ticket Salt Require to Config Icinga Client.");
	ErrorId INVALID_ICINGA_CLIENT_SCRIPT_FILE = new ErrorId("C1114", "Invalid Script File Path to Config Icinga.");
	ErrorId INVALID_STATE_MOVE = new ErrorId("C1115", "Invalid State Move.");
	ErrorId CANNOT_UPDATE_INSTANCE_STATE = new ErrorId("C1116", "Cannot Update Instance State.");
	ErrorId CANNOT_SET_NAME_TO_INSTANCE = new ErrorId("C1117", "Cannot Set Name for Instance.");
	
	ErrorId THREAD_SLEEP_FAILED = new ErrorId("C20000", "Thread Sleep Failed.");
	ErrorId CREATE_INSTANCE_TASK_FAILED = new ErrorId("C20001", "Create Instance - Task Failed.");
	ErrorId MASTER_AGENT_CONFIG_FAILED = new ErrorId("C20002", "Master Agent Configuration - Task Failed.");
	ErrorId CLIENT_AGENT_CONFIG_FAILED = new ErrorId("C20003", "Client Agent Installation - Task Failed.");
	ErrorId SCRIPT_EXECUTION__FAILED = new ErrorId("C20004", "Script Execution - Task Failed.");
	
	ErrorId AWS_PROVIDER_SERVICE_REQUEST_FAILED = new ErrorId("C30001", "Failure in AWS Service Request");
	
	ErrorId INVALID_CREATE_INSTACE_REQUEST = new ErrorId("C1201", "Invalid Create Instace Request.");
	ErrorId INVALID_SOLUTION_REQUEST = new ErrorId("C1202", "Invalid Solution Request.");
	ErrorId INVALID_IMAGE_ID = new ErrorId("C1203", "Invalid Image Id.");
	ErrorId INVALID_INSTANCE_TYPE = new ErrorId("C1204", "Invalid Instance Type");
	ErrorId INVALID_KEY_NAME = new ErrorId("C1205", "Invalid Key Name.");
	ErrorId INVALID_TAG_NAME = new ErrorId("C1206", "Invalid Tag Name.");
	ErrorId INVALID_SECURITY_GROUPS = new ErrorId("C1207", "Invalid Security Group(s).");
	ErrorId INVALID_RUN_INSTANCE_RESULT = new ErrorId("C1208", "Invalid Create Instance Result.");
	ErrorId CANNOT_SERVER_INSTANCE_INFO = new ErrorId("C1209", "Cannot Save Created Instance Information.");
	ErrorId INVALID_APPUSER_ID = new ErrorId("C1210", "Invalid AppUserId. Cannot get Server Credential Info.");
	ErrorId INVALID_SERVER_CREDENTIAL = new ErrorId("C1211", "Invalid Credential By UserId");
	ErrorId INSTANCE_LIST_CANNOT_BE_EMPTY = new ErrorId("C1212", "Instance List Cannot Be Empty to Install Icinga.");
	ErrorId SECURITY_GROUP_EMPTY = new ErrorId("C1213", "Security Group Cannot be empty.");
	ErrorId CREATED_FOR_NAME_EMPTY = new ErrorId("C1214", "Created For Name Cannot be empty.");
	ErrorId INVALID_CLIENT_NAME = new ErrorId("C1215", "Invalid Client Name.");
	ErrorId INVALID_INSTANCE_STATUS_REQUEST = new ErrorId("C1216", "Invalid Instance Status Request.");
	ErrorId INVALID_INSTANCE_LIST = new ErrorId("C1217", "Instance List is Empty.");
	
	ErrorId INVALID_MASTER_AGENT_REQ = new ErrorId("C15011", "Master Agent Request Cannot be null.");
	ErrorId INVALID_CLIENT_AGENT_REQ = new ErrorId("C15012", "Client Agent Request Cannot be null.");
	
	// Server Credential Error Codes
	ErrorId INVALID_SERVER_CREDENTIAL_OBJECT = new ErrorId("C13001", "Server Credential Object is Null");
	ErrorId INVALID_ACCESS_ID = new ErrorId("C13002", "Invalid Access Id.");
	ErrorId INVALID_ACCESS_KEY = new ErrorId("C13003", "Invalid Access Key.");
	ErrorId INVALID_PRIVATE_KEY = new ErrorId("C13004", "Invalid Private Key.");
	ErrorId INVALID_CREDENTIAL_USER_NAME = new ErrorId("C13005", "Invalid Credential User Name");
	ErrorId INVALID_INSTANCE = new ErrorId("C13006", "Invalid Instance");
	ErrorId INVALID_INSTANCE_TAG_NAME = new ErrorId("C13007", "Invalid Instance Tag Name");
	ErrorId INVALID_INSTANCE_ID = new ErrorId("C13008", "Invalid Instance Id");
	ErrorId INVALID_INSTANCE_KEY = new ErrorId("C13009", "Invalid Instance Key");
	ErrorId INVALID_INSTANCE_PUBLIC_DNS = new ErrorId("C13010", "Invalid Instance Public DNS");
	ErrorId INVALID_INSTANCE_PUBLIC_IP4 = new ErrorId("C13011", "Invalid Instance Public IP4");
	
	// Webserver Provider
	ErrorId INVALID_WEB_SERVER_PROVIDER = new ErrorId("C21001", "Invalid Web Server Provider");
	
	// JSCH Exception
	ErrorId JSCH_INTERNAL_ERROR = new ErrorId("ccdm1000", "Internal Error.");
	ErrorId CANNOT_CONNECT_TO_SERVER = new ErrorId("ccdm1001","Cannot connect to Server for Session.");
	ErrorId CANNOT_OPEN_CHANNEL = new ErrorId("ccdm1002","Cannot open channel on server.");
	ErrorId CANNOT_LOG_CHANNEL_OUTPUT = new ErrorId("ccdm1002", "Cannot Log Channel Output.");
	ErrorId CANNOT_UPLOAD_FILE = new ErrorId("ccdm1003", "Cannot upload Script File in Instance.");
	ErrorId INVALID_UPLOAD_FILE_PATH = new ErrorId("ccdm1004", "Invalid Upload File's Path");
	
	//Template Exception
	ErrorId SOLUTION_NAME_INVALID = new ErrorId("ccdm2001", "Template Name Cannot Be null");
	ErrorId OPERATING_SYSTEM_INVALID = new ErrorId("ccdm2002", "Operating System Cannot Be null");
	ErrorId WEB_SERVER_PROVIDER_INVALID = new ErrorId("ccdm2003", "Web Server Provider Cannot Be null");
	ErrorId VIRTUAL_MACHINE_INVALID = new ErrorId("ccdm2004", "Virtual Machine Cannot Be null");
	ErrorId INVALID_TEMPLATE_ID = new ErrorId("ccdm2005", "Invalid Template Id.");
	ErrorId INVALID_SOLUTION_ID = new ErrorId("ccdm2006", "Invalid Solution Id");
	ErrorId INVALID_DEPLOY_NAME = new ErrorId("ccdm2007", "Invalid Deploy Name");
	ErrorId INVALID_DEPLOY_MODEL_OBJECT = new ErrorId("ccdm2008", "Deploy Model Object Is Empty!");
	
	ErrorId INVALID_TRIGGER_SCRIPT_REQUEST = new ErrorId("ccdm3001", "Trigger Script Request Is Empty!");
	ErrorId TRIGGER_REQUEST_COMMANDID_EMPTY = new ErrorId("ccdm3002", "Trigger Script Request ScriptCommandId Invalid!");
	ErrorId EXECUTION_SCRIPT_EMPTY = new ErrorId("ccdm3003", "Execution Script Request Empty!");
	ErrorId FILE_UPLOAD_REQ_INVALID = new ErrorId("ccdm3004", "Invalid File Upload Request");
	ErrorId ERROR_FILE_UPLOAD = new ErrorId("ccdm3005", "Cannot Download File.");
	ErrorId INVALID_SCRIPT = new ErrorId("ccdm3006", "Invalid Script.");
	
	ErrorId PROVIDER_COST_UNAVAILABLE = new ErrorId("ccdm4001", "Provider cost unavailable for your request");
	ErrorId SOLUTION_EMPTY = new ErrorId("ccdm4002", "Solution Is Empty!!");
	ErrorId NOTIFICATION_EMPTY = new ErrorId("ccdm4003", "Notification Is Empty!!");
	ErrorId SLA_EMPTY = new ErrorId("ccdm4004", "Sla Object Is Empty!!");
	ErrorId VIEW_MODEL_EMPTY = new ErrorId("ccdm4005", "view model Object Is Empty!!");
	ErrorId INVALID_NOTIFICATION_AUTHORIZATION = new ErrorId("ccdm4006","Invalid Notification Authorization Type");
	ErrorId INVALID_NOTIFICATION_ID = new ErrorId("ccdm4007", "Invalid Notification Id");
	ErrorId INVALID_NOTIFICATION_REQUEST = new ErrorId("ccdm4008", "Notification Request Object Is Empty!");
	ErrorId INVALID_NOTIFICATION_TYPE = new ErrorId("ccdm4009", "Notification Type Is Empty!!");
	ErrorId ENABLE_ATLEAST_ONE_NOTIFICATION = new ErrorId("ccdm40010", "Please Enable Atleast One Notification Setting!!!");

	
	//Create Solution Exception
	ErrorId INVALID_SOLUTION_NAME = new ErrorId("ccdm5001", "Solution Name Invalid");
	ErrorId VM_SCRIPT_LIST_NULL_OR_EMPTY = new ErrorId("ccdm5002", "VM Templates and Scripts for the solution cannot be null");
	ErrorId VM_TEMPLATE_ID_NULL = new ErrorId("ccdm5003", "VM Template ID cannot be null");
	ErrorId REQUESTED_SOLUTION_NOT_AVAILABLE = new ErrorId("ccdm5004", "Requested Solution not available");
	ErrorId INVALID_OR_NULL_SOLUTIONID = new ErrorId("ccdm5005", "SolutionId Null or  Invalid");
	ErrorId INVALID_SOLUTION_REQ = new ErrorId("ccdm5004", "Invalid Solution Req Object while Updating SLA Type");
	ErrorId INVALID_SOLUTION_OBJECT = new ErrorId("ccdm5005", "Invalid Solution Req Object while Updating SLA Type");
	ErrorId CANNOT_DEPLOY_SOlUTION = new ErrorId("ccdm5006", "Cannot Deploy Solution. Please Try again Later.");
	
	// SLA
	ErrorId INVALID_SLA_TYPE_ID = new ErrorId("ccdm6001", "Invalid Sla Type id.");
	ErrorId INVALID_SLA_TYPE_REQUEST = new ErrorId("ccdm6002", "Invalid Sla Type Request.");
	ErrorId INVALID_SLA_TYPE_NAME = new ErrorId("ccdm6003", "Invalid Sla Type Name.");
	ErrorId INVALID_SLA_DETAILS = new ErrorId("ccdm6004", "Invalid Sla Details.");
	ErrorId INVALID_SLA_DETAIL = new ErrorId("ccdm6005", "Invalid Sla Detail.");
	ErrorId INVALID_SLA_PRIORITY_TYPE = new ErrorId("ccdm6006", "Invalid Sla Priority Type.");
	ErrorId INVALID_SLA_RESPONSE_TIME = new ErrorId("ccdm6007", "Invalid Sla Response Time.");
	ErrorId INVALID_SLA_UP_TIME = new ErrorId("ccdm6008", "Invalid Sla Up Time.");
	ErrorId INVALID_SLA_WORKING_HOURS = new ErrorId("ccdm6009", "Invalid Sla Working Hours.");
	ErrorId INVALID_SLA_CREDITS = new ErrorId("ccdm6010", "Invalid Sla Credits.");
	ErrorId INVALID_SLA_TYPE = new ErrorId("ccdm6011", "Invalid Sla Type.");
	
	ErrorId INVALID_OBJECT_TYPE = new ErrorId("ccdm6012", "Invalid Object Type.");
	ErrorId INVALID_SOLUTION_HISTORY_ID = new ErrorId("ccdm6013", "Invalid SolutionHistory Id.");
	ErrorId SOLUTION_HISTORY_OBJECT_NULL = new ErrorId("ccdm6014", "SolutionHistory Object Empty");
	
	//Communication
	ErrorId INVALID_TO_ADDRESS = new ErrorId("ccdm7001", "Invalid To Address.");
	ErrorId INVALID_MAIL_CONTENT = new ErrorId("ccdm7002", "Invalid Mail Content.");
	ErrorId INVALID_MAIL_SUBJECT = new ErrorId("ccdm7003", "Invalid Mail Subject.");
	ErrorId INVALID_TO_PHONE_NUMBER = new ErrorId("ccdm7004", "Invalid To: Phone Number.");
	ErrorId INVALID_SMS_BODY_CONTENT = new ErrorId("ccdm7005", "Invalid Sms Body Content.");
	
	//Notification Aspect
	ErrorId INVALID_NOTIFICATION_AUTH_TYPE = new ErrorId("ccdm8001", "Notification Authorization type cannot be null or empty.");
	ErrorId INVALID_CUSTOMER_INFO = new ErrorId("ccdm8002","InValid Customer Information");
	
	// Customer_info
	ErrorId INVALID_CUSTOMER_ID = new ErrorId("ccdm9001","InValid Customer Id");
	ErrorId INVALID_COST_DETIALS_ID = new ErrorId("ccdm9002","InValid Cost Details Id");
	ErrorId INVALID_ISNTANCE_INFO_ID = new ErrorId("ccdm9003","Invalid InstanceInfo Id");
	ErrorId INVALID_PROVIDER_PLATFORM_REFERENCE = new ErrorId("ccdm9004", "Provider Platform Reference Not Available!!!");
	
	//GCE(Google Cloud) Error Codes
	ErrorId INVALID_INSTANCE_RESPONSE_CLASS = new ErrorId("ccdm10001","Requested response is Not a part of Instance Response type");
	ErrorId INVALID_GCE_INSTANCE_REQUEST_CLASS = new ErrorId("ccdm10002","Instance Request is Invalid ");
	ErrorId GCE_OPERATION_WAITFOR_EXCEPTION = new ErrorId("ccdm10003","Could Not Perform Operation.Waitfor() Process");
	
	// SequenceCounter Error Codes
	ErrorId INVALID_SEQUENCE_COUNTER_ID = new ErrorId("ccdm30001","Invalid Sequence Counter Id");
	
	// Cost Details
	ErrorId COST_CALCULATION_FAILED = new ErrorId("ccdm40001","Cost Calculation Failed.");
	
}
