package com.ccdm.server.credential.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.server.credential.service.ServerCredentialService;
import com.ccdm.vo.ServerCredential;

@RestController
@RequestMapping(value = "/credential")
public class ServerCredentialController extends BaseController 
{

	@Autowired
	private ServerCredentialService credentialService;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	public ServerCredential getServerCredential(Integer providerId) throws CCDMUnauthorizedException
	{
		return credentialService.getServerCredentialByClientId(getLoggedInClientId(), providerId);
	}
}
