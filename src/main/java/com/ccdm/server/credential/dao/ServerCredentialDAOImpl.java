package com.ccdm.server.credential.dao;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.ServerCredential;

@Repository
public class ServerCredentialDAOImpl extends BaseDAOImpl implements ServerCredentialDAO 
{
	@Override
	public ServerCredential getServerCredentialByClientId(Integer clientId, Integer providerId) 
	{
		return (ServerCredential) getSession().createCriteria(ServerCredential.class)
											  .add(Restrictions.eq("appUser.userId", clientId))
											  .add(Restrictions.eq("webServerProvider.id", providerId))
											  .uniqueResult();
	}
}
