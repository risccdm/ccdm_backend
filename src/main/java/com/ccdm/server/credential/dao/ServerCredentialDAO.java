package com.ccdm.server.credential.dao;

import com.ccdm.vo.ServerCredential;

public interface ServerCredentialDAO 
{
	ServerCredential getServerCredentialByClientId(Integer userId, Integer providerId);
}
