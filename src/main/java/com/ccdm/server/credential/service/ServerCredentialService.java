package com.ccdm.server.credential.service;

import com.ccdm.vo.ServerCredential;

public interface ServerCredentialService 
{
	ServerCredential getServerCredentialByClientId(Integer clientId, Integer providerId);
}
