package com.ccdm.server.credential.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.server.credential.dao.ServerCredentialDAO;
import com.ccdm.vo.ServerCredential;

@Service
@Transactional(readOnly = false)
public class ServerCredentialServiceImpl implements ServerCredentialService
{
	@Autowired
	private ServerCredentialDAO serverCredentialDao;

	@Override
	public ServerCredential getServerCredentialByClientId(Integer clientId, Integer providerId) 
	{
		return serverCredentialDao.getServerCredentialByClientId(clientId, providerId);
	}
}
