package com.ccdm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CCDMBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CCDMBackendApplication.class, args);
	}
}
