package com.ccdm.property;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/environment.properties")
@PropertySource(value = "file:///${CCDM_ENVIRONMENT_PROPERTIES}/environment.properties", ignoreResourceNotFound = true)
public class EnvironmentProperties 
{
	@Autowired
	private Environment environment;
	
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
	
	public String getMainAppDomain() {
		return environment.getProperty(PropertyConstants.MAIN_APP_DOMAIN_NAME);
	}
	
	public String getOpenServiceDomain() {
		return environment.getProperty(PropertyConstants.OPEN_SERVICE_DOMAIN_NAME);
	}

	public String getMainAppXAuthToken() {
		return environment.getProperty(PropertyConstants.MAIN_APP_XAUTH_TOKEN);
	}
	
	public String getBatchMySqlIp() {
		return environment.getProperty(PropertyConstants.MYSQL_IP_ADDRESS);
	}
	
	public String getBatchMySqlPort() {
		return environment.getProperty(PropertyConstants.MYSQL_PORT_NUMBER);
	}
	
	public String getBatchMySqlUsername() {
		return environment.getProperty(PropertyConstants.MYSQL_USER_NAME);
	}
	
	public String getBatchMySqlPassword() {
		return environment.getProperty(PropertyConstants.MYSQL_PASSWORD);
	}
	
	public String getEazycomAppOwner() {
		return environment.getProperty(PropertyConstants.EAZYCOM_APP_OWNER);
	}
	
	public String getEazycomAppName() {
		return environment.getProperty(PropertyConstants.EAZYCOM_APP_NAME);
	}
	public String getElasticClusterName()
	{
		return environment.getProperty(PropertyConstants.ELASTIC_CLUSTER_NAME);
	}
	public String getElasticClusterTypeSolutionHistory()
	{
		return environment.getProperty(PropertyConstants.ELASTIC_CLUSTER_TYPE_SOLUTION_HISTORY);
	}
	public String getElasticClientTransportPingTimeOut() 
	{
		return environment.getProperty(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_PING_TIME_OUT);
	}
	public String getElasticClientTransportSniff() 
    {
		return environment.getProperty(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_SNIFF);
	}
	public String getElasticClientTransportNodesSmaplerInterval() 
	{
		return environment.getProperty(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_NODES_SAMPLER_INTERVAL);
	}
	
	public String getElasticClientTransportIp() 
	{
		return environment.getProperty(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_IP);
	}
	
	public String getElasticClientTransportPort() 
	{
		return environment.getProperty(PropertyConstants.ELASTIC_CLIENT_TRANSPORT_PORT);
	}
	
}
