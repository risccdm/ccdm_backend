package com.ccdm.property;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/database.properties")
public class DatabaseProperties 
{
	@Autowired
	private Environment environment;
	
	@Autowired
	private EnvironmentProperties environmentProperties;
	
	public String getJdbcDriver() {
		return environment.getProperty(PropertyConstants.MYSQL_DRIVER_NAME);
	}
	
	public String getJdbcUrl() 
	{
		String url = environment.getProperty(PropertyConstants.MYSQL_DATASOURCE_URL);
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_IP, environmentProperties.getBatchMySqlIp());
		url = url.replace(PropertyConstants.MAIN_APP_MYSQL_PORT, environmentProperties.getBatchMySqlPort());
		return url;
	}
	
	public String getJdbcUser() {
		return environmentProperties.getBatchMySqlUsername();
	}
	
	public String getJdbcPassword() {
		return environmentProperties.getBatchMySqlPassword();
	}
	
	public String getBatchDatabaseName() {
		return environment.getProperty(PropertyConstants.MYSQL_DATABASE_NAME);
	}
}
