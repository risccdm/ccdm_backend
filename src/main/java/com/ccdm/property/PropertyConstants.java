package com.ccdm.property;

public class PropertyConstants 
{
	public static final String MAIN_APP_MYSQL_IP 	= "[MAIN_APP_MYSQL_IP]";
	public static final String MAIN_APP_MYSQL_PORT 	= "[MAIN_APP_MYSQL_PORT]";
	
	//MySql properties Name
	public static final String MYSQL_DATASOURCE_URL = "mainApp.mysql.DataSource.URL";
	public static final String MYSQL_DATABASE_NAME  = "mainApp.mysql.databaseName";
	public static final String MYSQL_DRIVER_NAME  	= "mainApp.mysqlDriver";
	public static final String MYSQL_IP_ADDRESS 	= "mainApp.mysql.ip";
	public static final String MYSQL_PORT_NUMBER 	= "mainApp.mysql.port";
	public static final String MYSQL_USER_NAME 		= "mainApp.mysql.username";
	public static final String MYSQL_PASSWORD 		= "mainApp.mysql.password";
	
	//Domain Configuration
	public static final String MAIN_APP_DOMAIN_NAME  	= "mainApp.domain";
	public static final String OPEN_SERVICE_DOMAIN_NAME = "openService.domain";
	
	// Domain Auth Configuration
	public static final String MAIN_APP_XAUTH_TOKEN  	= "mainApp.xAuthToken";
	
	public static final String EAZYCOM_APP_OWNER 	= "ccdm.app.owner";
	public static final String EAZYCOM_APP_NAME 	= "ccdm.app.name";
	
	public static final String ELASTIC_CLIENT_TRANSPORT_PING_TIME_OUT   = "client.transport.ping_timeout";
	public static final String ELASTIC_CLIENT_TRANSPORT_SNIFF           = "client.transport.sniff";
	public static final String ELASTIC_CLIENT_TRANSPORT_NODES_SAMPLER_INTERVAL   = "client.transport.nodes_sampler_interval";
	
	public static final String ELASTIC_CLIENT_TRANSPORT_IP   = "client.transport.ip";
	public static final String ELASTIC_CLIENT_TRANSPORT_PORT   = "client.transport.port";

	public static final String ELASTIC_CLUSTER_NAME                     = "cluster.name";
	public static final String ELASTIC_CLUSTER_TYPE_SOLUTION_HISTORY 	= "cluster.type.solutionhistory";
}
