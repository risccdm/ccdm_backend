package com.ccdm.security.matcher;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.util.matcher.RequestMatcher;

import com.ccdm.utils.CCDMUtils;

public class CustomCsrfRequestMatcher implements RequestMatcher
{
	// Default HTTP methods to skip CSRF check
	private final Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
	
	@Override
	public boolean matches(HttpServletRequest request) 
	{
		// To skip CSRF check if API token header is present i request
		if(CCDMUtils.isAPIAuthTokenPresent(request)) {
			return false;
		}
		return !allowedMethods.matcher(request.getMethod()).matches();
	}

}
