package com.ccdm.security;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.vo.RolePageMap;

@Component
public class UserRoleManager 
{
	@Autowired
	private UserRoleDAO userRoleDao;
	
	public Map<String, String> getRolePageMap()
	{
		Map<String, String> rolePageMap= new HashMap<String, String>();
		
		List<RolePageMap> rolePageMapList = userRoleDao.getRolePageMapList();
		
		for(RolePageMap pageMap : rolePageMapList)
		{
			if(rolePageMap.get(pageMap.getAccessablePages().getPageValue()) == null)
			{
				rolePageMap.put(pageMap.getAccessablePages().getPageValue(), pageMap.getUserRole().getRoleName());
			}
			
			else
			{
				rolePageMap.put(pageMap.getAccessablePages().getPageValue(), 
								rolePageMap.get(pageMap.getAccessablePages().getPageValue()) + "," + pageMap.getUserRole().getRoleName());
			}
		}
		
		return rolePageMap;
	}
}
