package com.ccdm.security.constants;

public class SecurityConstants 
{
	// Token authentication header token
		public static final String AUTH_HEADER_NAME = "X-AUTH-TOKEN";
		
		// API Url for matcher to authenticate by Token
		public static final String API_URL_FOR_TOKEN_AUTH_FILTER = "/**";
		
		// Filters to skip while Token based authentication happens
		public static final String SPRING_SECURITY_CONTEXT_PERSISTENCE_FILTER_APPLIED = "__spring_security_scpf_applied";
		public static final String SPRING_SECURRITY_SESSION_MGMT_FILTER_APPLIED = "__spring_security_session_mgmt_filter_applied";
		
		// CSRF token name in Cookie
		public static final String CSRF_TOKEN_NAME_IN_COOKIE = "XSRF-TOKEN";
		
		// CSRF Header token name
		public static final String CSRF_TOKEN_NAME_IN_HEADER = "X-XSRF-TOKEN";
		
		// Constants to generate http authentication roles
		public static final String BRACES_WITH_QUOTES = "')";
		public static final String HAS_ROLE_WITH_QUOTES = "hasRole('";
		//public static final String HAS_ROLE_WITH_QUOTES = "hasAuthority('";
		public static final String OR_WITH_SPACE = " or ";
		
		// Constants in http security config
		public static final String LOGIN_PAGE_URL = "/login";
		public static final String LOGOUT_SUCCESSFUL_URL = "/login";
		public static final String ACCESS_DENIED_PAGE = "/successMessage.html";
		
		public static final String USERNAME = "username";
		public static final String PASSWORD = "password";
		
		// Query for username password authentication
		public static final String GET_USER_CREDENTIALS_QUERY = "SELECT USER_NAME,PASSWORD, true from ccdmdb.app_user where USER_NAME=?";
		public static final String GET_USER_AUTHORITY_QUERY = "SELECT A.USER_NAME, UR.ROLE_NAME FROM app_user A,user_role UR,user_role_map URM where A.USER_ID=URM.USER_ID AND UR.ROLE_ID=URM.ROLE_ID AND IS_ENABLED=TRUE AND A.USER_NAME=?";
}
