package com.ccdm.security;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.OncePerRequestFilter;

import com.ccdm.security.constants.SecurityConstants;
import com.ccdm.security.filters.CsrfCookieFilter;
import com.ccdm.security.filters.SpringSecuritySessionDeletingFilter;
import com.ccdm.security.matcher.CustomCsrfRequestMatcher;
import com.ccdm.websecurity.RestAuthenticationEntryPoint;

@Configuration
@RestController
@EnableWebSecurity
public class WebSecurityConfig 
{
	@Configuration
	protected static class SecurityConfiguration extends WebSecurityConfigurerAdapter
	{
		@Autowired
		private RolePageManager rolePageManager;
		
		@Autowired
		public DataSource dataSource;
		
		@Autowired
		private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
		
		@Bean
		public PasswordEncoder passwordEncoder() 
		{
			PasswordEncoder encoder = new BCryptPasswordEncoder();
			return encoder;
		}
		
		private String getUserQuery() {
			return SecurityConstants.GET_USER_CREDENTIALS_QUERY;
		}
		
		private String getAuthoritiesQuery() {
			return SecurityConstants.GET_USER_AUTHORITY_QUERY;
		}
		
		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception
		{
			// UserName password authentication with password encoder
			authenticationManagerBuilder.jdbcAuthentication()
										.dataSource(dataSource)
										.passwordEncoder(passwordEncoder())
										.usersByUsernameQuery(getUserQuery())
										.authoritiesByUsernameQuery(getAuthoritiesQuery());
		}
		
		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception
		{
			Map<String, List<String>> rolePageMap = rolePageManager.getRolePageMap();
			
			// Prepare HTTP URL matchers with access roles
			for (Map.Entry<String, List<String>> rolePageMapItr : rolePageMap.entrySet()) 
			{
				List<String> roles = rolePageMapItr.getValue();
				StringBuilder userRole = new StringBuilder();
				for (int index = 0; index < roles.size(); index++) 
				{
					userRole.append(SecurityConstants.HAS_ROLE_WITH_QUOTES);
					userRole.append(roles.get(index));
					userRole.append(SecurityConstants.BRACES_WITH_QUOTES);
					if (index != roles.size() - 1) 
					{
						userRole.append(SecurityConstants.OR_WITH_SPACE);
					}
				}
				httpSecurity.authorizeRequests().antMatchers(rolePageMapItr.getKey()).access(userRole.toString());
			}
             
			// HTTP configuration for request
			httpSecurity.authorizeRequests().and().formLogin()
						.loginPage(SecurityConstants.LOGIN_PAGE_URL)
						.usernameParameter(SecurityConstants.USERNAME).passwordParameter(SecurityConstants.PASSWORD)
						.and().logout().logoutSuccessUrl(SecurityConstants.LOGOUT_SUCCESSFUL_URL)
						.and().exceptionHandling().accessDeniedPage(SecurityConstants.ACCESS_DENIED_PAGE)
						.authenticationEntryPoint(restAuthenticationEntryPoint)
						.and().csrf().requireCsrfProtectionMatcher(getCsrfRequestMatcher())
						.csrfTokenRepository(csrfTokenRepository()).and()
						.addFilterBefore(getSpringSecuritySessionDeletingFilter(), SecurityContextPersistenceFilter.class)
						.addFilterAfter(getCsrfCookieFilter(), CsrfFilter.class);
		}
		
		private CsrfTokenRepository csrfTokenRepository() 
		{
			HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
			repository.setHeaderName(SecurityConstants.CSRF_TOKEN_NAME_IN_HEADER);
			return repository;
		}
		
		// CSRF request matcher to skip CSRF check
		private RequestMatcher getCsrfRequestMatcher() {
			return new CustomCsrfRequestMatcher();
		}
		
		// Filter to set CSRF token in response's cookie
		private OncePerRequestFilter getCsrfCookieFilter() {
			return new CsrfCookieFilter();
		}
		
		// Filter to skip the session creation filters for token based authentication
		private SpringSecuritySessionDeletingFilter getSpringSecuritySessionDeletingFilter() {
			return new SpringSecuritySessionDeletingFilter();
		}
	}
}
