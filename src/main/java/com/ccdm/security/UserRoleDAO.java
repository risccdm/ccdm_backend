package com.ccdm.security;

import java.util.List;

import org.hibernate.FetchMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.vo.RolePageMap;

@Component
public class UserRoleDAO 
{
	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	@SuppressWarnings("unchecked")
	@Transactional(value = "transactionManager", readOnly = false)
	public List<RolePageMap> getRolePageMapList() 
	{
		 return (List<RolePageMap>) hibernateTemplate.getSessionFactory()
				 									 .getCurrentSession().createCriteria(RolePageMap.class)
				 									 .setFetchMode("accessablePages", FetchMode.JOIN)
				 									 .setFetchMode("userRole", FetchMode.JOIN).list();
	}
}
