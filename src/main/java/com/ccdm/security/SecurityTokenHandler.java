package com.ccdm.security;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public final class SecurityTokenHandler 
{
	public String createTokenForUser(UserSecurityToken user) 
	{
		byte[] userBytes = toJSON(user);
		String token = toBase64(userBytes);
		return token;
	}
	
	public UserSecurityToken parseToken(String token) 
	{
		byte[] userBytes = fromBase64(token);
		UserSecurityToken user = fromJSON(userBytes);
		return user;
	}

	private byte[] toJSON(UserSecurityToken user) 
	{
		try {
			return new ObjectMapper().writeValueAsBytes(user);
		} 
		catch (JsonProcessingException e) {
			throw new IllegalStateException(e);
		}
	}

	private String toBase64(byte[] content) {
		return DatatypeConverter.printBase64Binary(content);
	}
	
	private byte[] fromBase64(String content) {
		return DatatypeConverter.parseBase64Binary(content);
	}
	
	private UserSecurityToken fromJSON(final byte[] userBytes) 
	{
		try {
			return new ObjectMapper().readValue(new ByteArrayInputStream(userBytes), UserSecurityToken.class);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
	
	// Testing Purpose
	/*public static void main(String args[]) 
	{
//		UserSecurityToken user = new UserSecurityToken("admin", "e41a4abf-1575-4c40-ac6d-9058b78e057b");
//		UserSecurityToken user = new UserSecurityToken("admin", UUID.randomUUID().toString());
		UserSecurityToken user = new UserSecurityToken("mani", UUID.randomUUID().toString());
		SecurityTokenHandler tokenHandler = new SecurityTokenHandler();
		String token = tokenHandler.createTokenForUser(user);
		System.out.println(token);
	}*/
}
