package com.ccdm.security.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.WebUtils;

import com.ccdm.security.constants.SecurityConstants;

public class CsrfCookieFilter extends OncePerRequestFilter
{
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException 
	{
		CsrfToken csrf = (CsrfToken) request.getAttribute(CsrfToken.class.getName());
		
		// No need to set CSRF token in Cookie, if API token header present in request.
		final String keyFromTokenAuthHeader = request.getHeader(SecurityConstants.AUTH_HEADER_NAME);
		
		if (csrf != null && keyFromTokenAuthHeader == null) {
			Cookie cookie = WebUtils.getCookie(request, SecurityConstants.CSRF_TOKEN_NAME_IN_COOKIE);
			request.removeAttribute(CsrfToken.class.getName());
			String csrfToken = csrf.getToken();
			if (cookie == null || csrfToken != null && !csrfToken.equals(cookie.getValue())) 
			{
				cookie = new Cookie(SecurityConstants.CSRF_TOKEN_NAME_IN_COOKIE, csrfToken);
				cookie.setPath("/");
				response.addCookie(cookie);
			}
		}
		filterChain.doFilter(request, response);
	}
}
