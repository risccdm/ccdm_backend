package com.ccdm.security.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.ccdm.security.constants.SecurityConstants;
import com.ccdm.utils.CCDMUtils;

public class SpringSecuritySessionDeletingFilter extends GenericFilterBean implements Filter 
{
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
			throws IOException, ServletException 
	{
		final HttpServletRequest servletRequest = (HttpServletRequest) request;
		
		// Skip session related process if API token header is present in request
		if (CCDMUtils.isAPIAuthTokenPresent(servletRequest)) {
			servletRequest.setAttribute(SecurityConstants.SPRING_SECURITY_CONTEXT_PERSISTENCE_FILTER_APPLIED, Boolean.TRUE);
			servletRequest.setAttribute(SecurityConstants.SPRING_SECURRITY_SESSION_MGMT_FILTER_APPLIED, Boolean.TRUE);
		}
		filterChain.doFilter(request, response);
	}

}
