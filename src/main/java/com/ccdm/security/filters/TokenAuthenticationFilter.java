package com.ccdm.security.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.ccdm.exceptions.CCDMAuthenticationException;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.security.SecurityTokenHandler;
import com.ccdm.security.UserSecurityToken;
import com.ccdm.security.constants.SecurityConstants;
import com.ccdm.utils.CCDMUtils;

public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter 
{
	private SecurityTokenHandler tokenHandler;
	
	public void setTokenHandler(SecurityTokenHandler tokenHandler) {
		this.tokenHandler = tokenHandler;
	}
	
	public TokenAuthenticationFilter() {
		super(new AntPathRequestMatcher(SecurityConstants.API_URL_FOR_TOKEN_AUTH_FILTER));
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException 
	{
		String token = request.getHeader(SecurityConstants.AUTH_HEADER_NAME);
		if (CCDMUtils.isBlank(token)) {
			return null;
		}
		
		UsernamePasswordAuthenticationToken authRequest = null;
		try {
			authRequest = (UsernamePasswordAuthenticationToken) authUserByToken(token);
			authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
		} catch (CCDMException e) {
			try {
				throw new CCDMAuthenticationException(e.getMessage());
			} catch (CCDMAuthenticationException e1) {
				logger.error("An error occured while Authentication!");
				e1.printStackTrace();
			}
		}

		return this.getAuthenticationManager().authenticate(authRequest);
	}
	
	// Prepare and return authentication class by parsing token
	private AbstractAuthenticationToken authUserByToken(String token) throws CCDMException {
		String username = null;
		try 
		{
			// logic to extract username from token
			UserSecurityToken userFromToken = tokenHandler.parseToken(token);
			username = userFromToken.getUsername();
		} catch (Exception e) {
			logger.error("An error occured while parsing data from token!");
			throw new CCDMException("Invalid Token!");
		}

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		/* Empty authorities list for authentication. Authorization will happen
		 by the JDBC authentication provider.*/
		User principal = new User(username, token, authorities);
		AbstractAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(principal, token, principal.getAuthorities());
		return authToken;
	}
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException 
	{
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		// Skip this filterif the API token header is not present
		if (!CCDMUtils.isAPIAuthTokenPresent(request)) {
			chain.doFilter(req, res);
			return;
		}

		Authentication authResult;
		try 
		{
			authResult = attemptAuthentication(request, response);
			if (authResult == null) {
				/* return immediately as subclass has indicated that it hasn't
				completed authentication */
				return;
			}
		} catch (InternalAuthenticationServiceException failed) {
			logger.error("An internal error occurred while trying to authenticate the user.", failed);
			unsuccessfulAuthentication(request, response, failed);
			return;
		} catch (AuthenticationException failed) {
			// Authentication failed
			unsuccessfulAuthentication(request, response, failed);
			return;
		}

		this.setAuthenticationSuccessHandler(new AuthenticationSuccessHandler() 
		{
			@Override
			public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
					throws IOException, ServletException {
				chain.doFilter(request, response);
			}
		});

		successfulAuthentication(request, response, chain, authResult);
	}
}
