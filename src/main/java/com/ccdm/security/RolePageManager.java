package com.ccdm.security;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.vo.RolePageMap;

@Component
public class RolePageManager 
{
	@Autowired
	private RolePageDAO rolePageDao;
	
	public Map<String, List<String>> getRolePageMap()
	{
		Map<String, List<String>> rolePageMap= new HashMap<String, List<String>>();
		
		List<RolePageMap> rolePageMapList = rolePageDao.getRolePageMapList();
		
		for(RolePageMap pageMap : rolePageMapList)
		{
			if(rolePageMap.get(pageMap.getAccessablePages().getPageValue()) == null)
			{
				List<String> roleList = new ArrayList<String>();
				roleList.add(pageMap.getUserRole().getRoleName());
				rolePageMap.put(pageMap.getAccessablePages().getPageValue(), roleList);
			}
			else
			{
				List<String> roles = rolePageMap.get(pageMap.getAccessablePages().getPageValue());
				roles.add(pageMap.getUserRole().getRoleName());
				rolePageMap.put(pageMap.getAccessablePages().getPageValue(), roles);
			}
		}
		return rolePageMap;
	}
}
