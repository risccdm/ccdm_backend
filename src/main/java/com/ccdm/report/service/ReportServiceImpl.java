package com.ccdm.report.service;

import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.common.constants.SqlConstants;
import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.controller.InstanceController;
import com.ccdm.instance.service.helper.vo.ExtendedCostDetail;
import com.ccdm.instance.service.helper.vo.ProviderCostDetail;
import com.ccdm.report.dao.ReportDao;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceCostReference;
import com.ccdm.vo.InstanceInfo;

@Service
@Transactional(readOnly = false)
public class ReportServiceImpl implements ReportService
{
	
	private static final int TOTAL_NO_MONTHS = 12;

	private static final int TOTAL_NO_HOURS = 24;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class); 

	private static final String DATE_YEAR_FORMAT = "MMM-yyyy";
	
	@Autowired
	private InstanceController instanceController;
	
	@Autowired
	private ReportDao reportDao;
	
	@Override
	public List<ProviderCostDetail> getProviderCostDetais(AppUser appUser, Integer customerId, Integer solutionId, Integer pageNumber, Integer pageLimit) throws CCDMException 
	{
		try
		{
			pageNumber = pageNumber == null ? SqlConstants.PAGE_INDEX_STARTS_FROM : pageNumber;
			pageLimit  = pageLimit == null ? SqlConstants.DEFAULT_PAGE_LIMIT : pageLimit;
			List<InstanceInfo> instanceInfoList = instanceController.getInstanceInfoResult(appUser, customerId, solutionId, calculateFetchIndex(pageNumber, pageLimit), pageLimit);
			return buildProviderCostDetails(instanceInfoList);
		}
		catch(CCDMException e)
		{
			throw new CCDMException();
		}
		catch(Exception e)
		{
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
	}
	
	@Override
	public List<ExtendedCostDetail> getDetailedCostValues(AppUser appUser, Integer instanceId) throws CCDMException {
		CommonValidator.validateId(instanceId, ErrorCodes.INVALID_INSTANCE_ID);
		List<ExtendedCostDetail> extentedCostDetailsList  = new ArrayList<ExtendedCostDetail>();
		List<InstanceCostReference> costReferenceList = reportDao.getInstanceCostReferenceByInstanceId(instanceId);
		for(InstanceCostReference costReference : costReferenceList) {
			extentedCostDetailsList.addAll(buildExtendedCostDetail(costReference));
		}
		return extentedCostDetailsList;
	}
	
	// ************************************* Private Methods *********************************************** //
	
	private List<ExtendedCostDetail> buildExtendedCostDetail (InstanceCostReference costReference) {
		List<ExtendedCostDetail> extentedCostDetailsList  = new ArrayList<ExtendedCostDetail>();
		Date instanceStartDate = costReference.getInstanceInfo().getLaunchTime();
		Date endDate = new Date();
		ArrayList<Calendar> calendarsList = buildMonthList(instanceStartDate, endDate);
		for(Calendar calendar : calendarsList) {
			
			//For End Month
			if (isDateCurrentMonthAndYear(endDate, calendar.getTime(), DATE_YEAR_FORMAT)) {
				Date EndMonthStartDate = getStartOfDay(buildCalendarDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendarsList.size()==1 ? calendar.get(Calendar.DAY_OF_MONTH) :  calendar.getActualMinimum(Calendar.DAY_OF_MONTH)));
				Long hours = getHourDifference(EndMonthStartDate, endDate) ;
				String monthAndYear  = convertDate2String(calendar.getTime(), DATE_YEAR_FORMAT);
				Integer days = (int) ( ((int) (hours % TOTAL_NO_HOURS) == 0) ? (hours / TOTAL_NO_HOURS) : ((hours / TOTAL_NO_HOURS)+1) ) ;
				Double costValue = calculateProviderCost(hours, costReference.getCostDetails().getCostValue());
				extentedCostDetailsList.add(adaptExtendedCostDetail(monthAndYear, hours.intValue(), costValue, days));
				continue;
			}
			//For Started Month
			if(isDateCurrentMonthAndYear(instanceStartDate, calendar.getTime(), DATE_YEAR_FORMAT)) {
				Date startMonthEndDate = getEndOfDay(buildCalendarDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.getActualMaximum(Calendar.DAY_OF_MONTH)));
				String monthAndYear  = convertDate2String(calendar.getTime(), DATE_YEAR_FORMAT);
//				long test2 = addStartDateRemaindHours(instanceStartDate);
				Long hours = getHourDifference(instanceStartDate, startMonthEndDate) ;
				Integer days = (int) ( ((int) (hours % TOTAL_NO_HOURS) == 0) ? (hours / TOTAL_NO_HOURS) : ((hours / TOTAL_NO_HOURS)+1) ) ;
				Double costValue = calculateProviderCost(hours, costReference.getCostDetails().getCostValue());
				extentedCostDetailsList.add(adaptExtendedCostDetail(monthAndYear, hours.intValue(), costValue, days));
				continue;
			}
			
			
			String monthAndYear  = convertDate2String(calendar.getTime(), DATE_YEAR_FORMAT);
			Integer days = calculateDaysInMonth(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));
			Integer hours = days * TOTAL_NO_HOURS;
			Double costValue = calculateProviderCost(hours, costReference.getCostDetails().getCostValue());
			extentedCostDetailsList.add(adaptExtendedCostDetail(monthAndYear, hours, costValue, days));
			
		}
		return extentedCostDetailsList;
	}
	
	private Date buildCalendarDate(int year, int month, int date) {
		Calendar fromDateCal = new GregorianCalendar();
		fromDateCal.set(year, month, date);
		return fromDateCal.getTime();
	}
	
	private Date getEndOfDay(Date date) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	    return calendar.getTime();
	}

	private Date getStartOfDay(Date date) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
	
	private ExtendedCostDetail adaptExtendedCostDetail(String monthAndYear, Integer hours, Double costValue, Integer days) {
		return new ExtendedCostDetail(monthAndYear, costValue, hours, days);
	}
	
	private boolean isDateCurrentMonthAndYear(Date reqDate, Date checkingDate, String format) {
		return convertDate2String(reqDate, format).equals(convertDate2String(checkingDate, format));
	}
	
	private String convertDate2String(Date date, String format) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}
	
	private int calculateDaysInMonth(int year, int month) {
		
		//In Below version Java 8
		/*int iYear = 1999;
		int iMonth = Calendar.FEBRUARY; // 1 (months begin with 0)
		int iDay = 1;
		// Create a calendar object and set year and month
		Calendar mycal = new GregorianCalendar(iYear, iMonth, iDay);
		// Get the number of days in that month
		int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28 */		
		
		//In Java version Eight
		java.time.YearMonth yearMonthObject = YearMonth.of(year, month);
		int daysInMonth = yearMonthObject.lengthOfMonth(); //28
		return daysInMonth;
	}
	
	private int calculateMonthDifference(Date startDate, Date endDate) {
		Calendar startCalendar = new GregorianCalendar();
		startCalendar.setTime(startDate);
		Calendar endCalendar = new GregorianCalendar();
		endCalendar.setTime(endDate);
		int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
		int diffMonth = diffYear * TOTAL_NO_MONTHS + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
		return diffMonth;
	}
	
	private ArrayList<Calendar> buildMonthList(Date fromDate, Date toDate) {
		ArrayList<Calendar> monthList = new ArrayList<Calendar>();

		Date tempFromDate = getFirstDateOfMonth(fromDate);
		/**
		 * Is tempFromDate is before toDate..?
		 * */
		int noOfMonths = calculateMonthDifference(fromDate, toDate) + 1;
		for(int i=0; i<noOfMonths; i++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(tempFromDate);
			monthList.add(cal);
			cal = setNextMonth(cal);
			tempFromDate = cal.getTime();
		}
		return monthList;
	}
	
	private static Calendar setNextMonth(Calendar cal)
	{
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(cal.getTime());
		calendar.add(Calendar.MONTH, 1);
		return calendar;
	}
	
	private Date getFirstDateOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.getTime();
	}
	
	private Integer calculateFetchIndex(Integer pageNumber, Integer pageLimit)
	{
		return pageNumber * pageLimit;
	}
	
	private List<ProviderCostDetail> buildProviderCostDetails(List<InstanceInfo> instanceInfoList) throws CCDMException {
		List<ProviderCostDetail> costDetailsList = new ArrayList<ProviderCostDetail>();
		for(InstanceInfo info : instanceInfoList) {
			costDetailsList.add(getCostDetailInfo(info));
		}
		return costDetailsList;
	}
	
	private ProviderCostDetail getCostDetailInfo(InstanceInfo instanceInfo) throws CCDMException 
	{
		try
		{
			List<InstanceCostReference> costReferenceList = reportDao.getInstanceCostReferenceByInstanceId(instanceInfo.getId());
			Double costValue = 0d;
			for(InstanceCostReference costReference : costReferenceList) {
				String costTypeName = costReference.getCostDetails().getCostType();
				switch (costTypeName) {
				case "BASIC":
					costValue = calculateProviderCost(getHourDifference(instanceInfo.getLaunchTime(), new Date()), costReference.getCostDetails().getCostValue());
					break;
				case "ACCESSORIES":
					//TODO: Calcuate Cost Value for Additonal Accessories Configuration.
					break;
				default:
					LOGGER.warn("Cost Calculation Not Matched with any of the Cost Type === >>>> "+ costTypeName);
					break;
				}
				
			}
			return costReferenceList.isEmpty() ? null : new ProviderCostDetail(instanceInfo.getId(), instanceInfo.getTagName(), 
																getHourDifference(instanceInfo.getLaunchTime(), new Date()), getProviderName(costReferenceList), instanceInfo.getLaunchTime(), costValue);
		}
		catch(Exception e)
		{
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
		
	}
	
	private String getProviderName(List<InstanceCostReference> costReferenceList) {
		if(costReferenceList != null && !costReferenceList.isEmpty()) {
			return costReferenceList.get(0).getCostDetails().getWebServerProvider().getAliasName();
		}
		return "";
	}

	private double calculateProviderCost(long hours, double providerCost)
	{
		if(hours == 0)
		{
			return 50;
		}
        return hours * providerCost;
	}

	private long getHourDifference(Date fromDate, Date toDateTime)
	{
		long diff =  toDateTime.getTime() - fromDate.getTime() ;
		return diff / (60 * 60 * 1000);
	}

}
