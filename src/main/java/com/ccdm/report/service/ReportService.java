package com.ccdm.report.service;

import java.util.List;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.service.helper.vo.ExtendedCostDetail;
import com.ccdm.instance.service.helper.vo.ProviderCostDetail;
import com.ccdm.vo.AppUser;

public interface ReportService 
{
	List<ProviderCostDetail> getProviderCostDetais(AppUser appUser, Integer customerId, Integer solutionId, Integer pageNumber, Integer pageLimit) throws CCDMException;

	List<ExtendedCostDetail> getDetailedCostValues(AppUser appUser, Integer instanceId) throws CCDMException;
}
