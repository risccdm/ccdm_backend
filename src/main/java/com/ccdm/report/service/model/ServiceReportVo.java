package com.ccdm.report.service.model;

import java.util.Date;

public class ServiceReportVo {

	
	private String hostName;
	private String serviceName;
	private String serviceState;
	private Date lastStateChange;
	private String serviceOutput;
	
	
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceState() {
		return serviceState;
	}
	public void setServiceState(String serviceState) {
		this.serviceState = serviceState;
	}
	public Date getLastStateChange() {
		return lastStateChange;
	}
	public void setLastStateChange(Date lastStateChange) {
		this.lastStateChange = lastStateChange;
	}
	public String getServiceOutput() {
		return serviceOutput;
	}
	public void setServiceOutput(String serviceOutput) {
		this.serviceOutput = serviceOutput;
	}
	
	
	
}
