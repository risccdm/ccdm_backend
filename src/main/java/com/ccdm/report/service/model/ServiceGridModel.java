package com.ccdm.report.service.model;

public class ServiceGridModel {

	private String hostName;
	private String serviceName;
	private String serviceState;
	private String serviceHandled;
	private String serviceOutput;
	
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getServiceState() {
		return serviceState;
	}
	public void setServiceState(String serviceState) {
		this.serviceState = serviceState;
	}
	public String getServiceHandled() {
		return serviceHandled;
	}
	public void setServiceHandled(String serviceHandled) {
		this.serviceHandled = serviceHandled;
	}
	public String getServiceOutput() {
		return serviceOutput;
	}
	public void setServiceOutput(String serviceOutput) {
		this.serviceOutput = serviceOutput;
	}
	
}
