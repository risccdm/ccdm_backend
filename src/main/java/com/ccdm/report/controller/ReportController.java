package com.ccdm.report.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.instance.service.helper.vo.ExtendedCostDetail;
import com.ccdm.instance.service.helper.vo.ProviderCostDetail;
import com.ccdm.report.service.ReportService;
import com.ccdm.vo.AppUser;

@RestController
@RequestMapping(value = "/report")
public class ReportController extends BaseController
{

	@Autowired
	private ReportService reportService;
	
	@Override
	protected Class<?> getClassName() 
	{
		return this.getClass();
	}
	
	@RequestMapping(value = "/instance/cost", method = RequestMethod.GET)
	public CCDMResponse getProviderCostDetais(@RequestParam("customerId") Integer customerId, @RequestParam("solutionId") Integer solutionId,
												@RequestParam("pageNumber") Integer pageNumber,
												@RequestParam("pageLimit") Integer pageLimit) throws CCDMException {
		AppUser appUser = getLoggedInUser();
		List<ProviderCostDetail> providerCostDetailsList = reportService.getProviderCostDetais(appUser, customerId, solutionId, pageNumber, pageLimit);
		return getCCDMResponse(providerCostDetailsList);
	}
	
	@RequestMapping(value = "/instance/cost/detail" , method = RequestMethod.GET)
	public CCDMResponse getDetailedCostValues(@RequestParam("instanceInfoId") Integer instanceInfoId) throws CCDMException {
		AppUser appUser = getLoggedInUser();
		List<ExtendedCostDetail> extendedCostDetailsList = reportService.getDetailedCostValues(appUser, instanceInfoId);
		return getCCDMResponse(extendedCostDetailsList);
	}
	
}
