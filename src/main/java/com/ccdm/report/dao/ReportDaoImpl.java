package com.ccdm.report.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.InstanceCostReference;

@Repository
public class ReportDaoImpl extends BaseDAOImpl implements ReportDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<InstanceCostReference> getInstanceCostReferenceByInstanceId(Integer instanceInfoId) {
		Criteria criteria = (Criteria) getSession().createCriteria(InstanceCostReference.class)
							.setFetchMode("costDetails", FetchMode.JOIN)
							.setFetchMode("instanceInfo", FetchMode.JOIN)
							.setFetchMode("costDetails.webServerProvider", FetchMode.JOIN)
							.add(Restrictions.eq("instanceInfo.id", instanceInfoId));
		return criteria.list();
	}

	
	
}
