package com.ccdm.report.dao;

import java.util.List;

import com.ccdm.vo.InstanceCostReference;

public interface ReportDao {

	List<InstanceCostReference> getInstanceCostReferenceByInstanceId(Integer id);

}
