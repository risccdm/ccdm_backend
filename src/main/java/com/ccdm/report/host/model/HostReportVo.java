package com.ccdm.report.host.model;

import java.util.Date;

public class HostReportVo {

	
	private String hostName;
	private String hostState;
	private Date lastStateChange;
	private String hostOutput;
	
	
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getHostState() {
		return hostState;
	}
	public void setHostState(String hostState) {
		this.hostState = hostState;
	}
	public Date getLastStateChange() {
		return lastStateChange;
	}
	public void setLastStateChange(Date lastCheckDate) {
		this.lastStateChange = lastCheckDate;
	}
	public String getHostOutput() {
		return hostOutput;
	}
	public void setHostOutput(String hostOutput) {
		this.hostOutput = hostOutput;
	}
	
	
	
}
