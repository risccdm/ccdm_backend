package com.ccdm.exceptions;

import javax.naming.AuthenticationException;

public class CCDMAuthenticationException extends AuthenticationException 
{
	private static final long serialVersionUID = 24392470256713771L;
	
	public CCDMAuthenticationException(String msg)
	{
		super(msg);
	}
}
