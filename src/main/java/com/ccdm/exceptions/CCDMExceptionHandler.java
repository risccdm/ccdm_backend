package com.ccdm.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ccdm.errors.ErrorCodes;

@ControllerAdvice
public class CCDMExceptionHandler 
{
	private static final Logger logger = LoggerFactory.getLogger(CCDMExceptionHandler.class);
	
	@ExceptionHandler(CCDMException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody CCDMException handleCCDMException(CCDMException ex)
	{
		logger.error("CCDMException =======> ");
		return ex;
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody CCDMException handleException(Exception ex)
	{
		logger.error("Exception =======> ");
		ex.printStackTrace();
		return new CCDMException(ErrorCodes.INTERNAL_ERROR);
	}
}
