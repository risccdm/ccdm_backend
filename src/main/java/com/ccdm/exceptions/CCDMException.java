package com.ccdm.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ccdm.errors.ErrorId;
import com.ccdm.errors.ErrorIdList;
import com.fasterxml.jackson.annotation.JsonIgnore;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CCDMException extends Exception
{
	private static final long serialVersionUID = -6296902058318468545L;
	
	public CCDMException()
	{
		super();
	}
	
	@JsonIgnore
	@Override
	public StackTraceElement[] getStackTrace() {
		return super.getStackTrace();
	}
	
	public CCDMException(String message){
		super(message);
	}
	
	public CCDMException(ErrorIdList errorIdList){
		super(errorIdList.convertToJsonString());
	}
	
	public CCDMException(ErrorId errorId){
		super(new ErrorIdList(errorId).convertToJsonString());
	}
}
