package com.ccdm.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.ccdm.errors.ErrorId;
import com.ccdm.errors.ErrorIdList;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class CCDMUnauthorizedException extends CCDMException
{
	private static final long serialVersionUID = 5215952340477915756L;
	
	private HttpStatus httpStatus;
	
	public CCDMUnauthorizedException()
	{
		super();
	}
	
	public CCDMUnauthorizedException(ErrorId errorId, HttpStatus httpStatus)
	{
		super(new ErrorIdList(errorId).convertToJsonString());
		this.httpStatus = httpStatus;
	}
	
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}
