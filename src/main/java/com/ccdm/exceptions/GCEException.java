package com.ccdm.exceptions;

import com.ccdm.errors.ErrorId;
import com.ccdm.errors.ErrorIdList;

public class GCEException extends CCDMException {

	private static final long serialVersionUID = -8450636353532517941L;

	public GCEException(String message) {
		super(message);
	}

	public GCEException(ErrorIdList errorIdList) {
		super(errorIdList.convertToJsonString());
	}

	public GCEException(ErrorId errorId) {
		super(new ErrorIdList(errorId).convertToJsonString());
	}

}
