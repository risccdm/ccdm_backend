package com.ccdm.base.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;

import com.ccdm.infrastructure.support.HibernateDAOSupport;

public abstract class BaseDAOImpl extends HibernateDAOSupport 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	public void remove(Class<?> clazz, Long id) {
		delete(getById(clazz, id));
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> getValueOfSingleColumn(Class clazz, String propertyName)
	{
		Criteria criteria = getSession().createCriteria(clazz);
		criteria.setProjection(Projections.property(propertyName));
		List<String> categories = criteria.list();
		return categories;
	}
	
	public void commitTraction()
	{
		sessionFactory.getCurrentSession().getTransaction().commit();
	}
}	
