package com.ccdm.base.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.ccdm.admin.service.UserService;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.vo.AppUser;

@Component
public abstract class BaseController 
{
	protected Logger logger = LoggerFactory.getLogger(getClassName());
	
	@Autowired
	private UserService userService;
	
	protected abstract Class<?> getClassName();
	
	private Map<String,String> jsonObject = new HashMap<String, String>();
	
	private final String SUCCESS = "Success!";
	
	public ResponseEntity<CCDMResponse> getDefaultResponseEntity() {
		return getResponseEntity(jsonObject, SUCCESS, HttpStatus.OK);
	}

	public ResponseEntity<CCDMResponse> getResponseEntity(String status) {
		return getResponseEntity(jsonObject, status, HttpStatus.OK);
	}
	
	public ResponseEntity<CCDMResponse> getResponseEntity(Object content) {
		return getResponseEntity(content, SUCCESS, HttpStatus.OK);
	}
	
	public ResponseEntity<CCDMResponse> getResponseEntity(String status, HttpStatus httpStatus) {
		return getResponseEntity(jsonObject, status, httpStatus);
	}
	
	public ResponseEntity<CCDMResponse> getResponseEntity(Object content, String status) {
		return getResponseEntity(content, status, HttpStatus.OK);
	}
	
	public ResponseEntity<CCDMResponse> getResponseEntity(Object content, HttpStatus httpStatus) {
		return getResponseEntity(content, SUCCESS, httpStatus);
	}
	
	public ResponseEntity<CCDMResponse> getResponseEntity(Object content, String status, HttpStatus httpStatus) {
		CCDMResponse ecRes = new CCDMResponse();
		ecRes.setContent(content);
		if(StringUtils.isNotBlank(status)) {
			ecRes.setStatus(status);
		}
		return new ResponseEntity<CCDMResponse>(ecRes, httpStatus);
	}
	
	public CCDMResponse getCCDMResponse(Object content) {
		CCDMResponse ecRes = new CCDMResponse();
		if(content != null) {
			ecRes.setContent(content);
		}
		return ecRes;
	}
	
	public CCDMResponse getCCDMResponse(Object content, HttpStatus httpStatus) {
		CCDMResponse ecRes = new CCDMResponse();
		if(content != null) {
			ecRes.setContent(content);
			ecRes.setStatus(HttpStatus.OK);
		}
		return ecRes;
	}
	
	public CCDMResponse getCCDMResponse() {
		CCDMResponse ecRes = new CCDMResponse();
		ecRes.setContent(SUCCESS);
		ecRes.setStatus(HttpStatus.OK);
		return ecRes;
	}
	
	public CCDMResponse getCCDMResponse(HttpStatus httpStatus) {
		CCDMResponse ecRes = new CCDMResponse();
		ecRes.setStatus(HttpStatus.OK);
		return ecRes;
	}
	
	public int getLoggedInUserId() throws CCDMUnauthorizedException {
		AppUser appUser = userService.getLoggedInUser();
		return appUser.getUserId();
	}
	
	public int getLoggedInClientId() throws CCDMUnauthorizedException {
		AppUser appUser = userService.getLoggedInUser();
		return appUser.getClientId();
	}
	
	public AppUser getLoggedInUser() throws CCDMUnauthorizedException {
		AppUser appUser = userService.getLoggedInUser();
		return appUser;
	}

	public void setUserManager(UserService userService) {
		this.userService = userService;
	}
}
