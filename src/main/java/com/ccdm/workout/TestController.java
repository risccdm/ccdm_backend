package com.ccdm.workout;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.common.constants.SequenceCounterConstants;
import com.ccdm.elastic.repository.sequencecounter.ISequenceCounterRepoCustom;
import com.ccdm.elastic.repository.sequencecounter.ISequenceCounterRepository;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.infrastructure.config.ElasticsearchConfiguration;
import com.fasterxml.jackson.core.JsonProcessingException;

@RestController
public class TestController 
{
	@Autowired
	private ElasticsearchOperations esOperation;
	@Autowired
	ElasticsearchConfiguration esConfiguration;
	@Autowired
	private ISequenceCounterRepository iSequenceCounterRepo;
	@Autowired
	private ISequenceCounterRepoCustom iSequenceCounterRepoCustom;
	
	@RequestMapping(value = "/testElasticGroupBy")
	public void elasticGroupBy()
	{
		SearchRequestBuilder searchReq = esConfiguration.getSolutionhHistoryQueryBuilder();
		searchReq.setQuery(QueryBuilders.matchQuery("status", "Waiting"))
				 .addAggregation(AggregationBuilders.terms("group_by_deployUniqueId").field("deployUniqueId").order(Order.aggregation("max_depoyDate", true))
									.subAggregation(AggregationBuilders.max("max_depoyDate").field("deployDate")));

		SearchResponse searchRes = searchReq.execute().actionGet();
		
		Terms fieldATerms = searchRes.getAggregations().get("group_by_deployUniqueId");
		
		for(Terms.Bucket filedBucket : fieldATerms.getBuckets())
		{
			String fieldAValue = filedBucket.getKeyAsString();
			System.out.println(fieldAValue);
		}
		
	}
	
	@RequestMapping(value = "/elasticSeqCounterTest")
	public long elasticSequenceCounterTest() throws JsonProcessingException, CCDMException
	{
		/*SequenceCounter seqCounter = iSequenceCounterRepo.findOne(SequenceCounterConstants.INSTANCE_NAME);
		
		if(seqCounter != null)
		{
			System.out.println(new ObjectMapper().writeValueAsString(seqCounter));
			long newSequenceId = seqCounter.getSequenceId() + 1;
			seqCounter.setSequenceId(newSequenceId);
			iSequenceCounterRepo.save(seqCounter);
		}
		else
		{
			long newSequenceId = 1;
			SequenceCounter seqCounterCreate = new SequenceCounter(SequenceCounterConstants.INSTANCE_NAME, newSequenceId);
			iSequenceCounterRepo.save(seqCounterCreate);
		}*/
		
		return iSequenceCounterRepoCustom.getSequenceCounterValueById(SequenceCounterConstants.INSTANCE_NAME);
	}
}
