package com.ccdm.workout;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.model.Address;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.Operation;
import com.google.cloud.compute.AddressInfo;
import com.google.cloud.compute.AttachedDisk;
import com.google.cloud.compute.AttachedDisk.PersistentDiskConfiguration;
import com.google.cloud.compute.Compute.ImageListOption;
import com.google.cloud.compute.ComputeOptions;
import com.google.cloud.compute.DiskId;
import com.google.cloud.compute.DiskInfo;
import com.google.cloud.compute.ImageDiskConfiguration;
import com.google.cloud.compute.ImageId;
import com.google.cloud.compute.InstanceId;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.MachineTypeId;
import com.google.cloud.compute.NetworkId;
import com.google.cloud.compute.NetworkInterface.AccessConfig;
import com.google.cloud.compute.Region;
import com.google.cloud.compute.RegionAddressId;

public class GoogleServerInstance {

	
	
	
	/*private static void createInstance(String projectId,Compute compute) throws IOException {
        Instance instance = new Instance();
        instance.setFactory(JacksonFactory.getDefaultInstance());

        // Select a machine type.
        String machine = "https://www.googleapis.com/compute/v1/projects/hindproj/global/machineTypes/n1-standard-1";
        instance.setMachineType(machine);

        // Get a name from the user.
        String name = "v1";
        instance.setName(name);
        // Use the default network.  Could select here if needed.
        List<NetworkInterface> networkInterfaces = new ArrayList<NetworkInterface>();
        NetworkInterface iface = new NetworkInterface();
        iface.setFactory(JacksonFactory.getDefaultInstance());
        iface.setName("eth0");
        iface.setNetwork("https://www.googleapis.com/compute/v1/projects/hindproj/global/networks/default");//( COMPUTE_API + "/projects/" + projectId + "/networks/default");
        networkInterfaces.add(iface);
        instance.setNetworkInterfaces(networkInterfaces);

        // Select a zone.
        String zone = "https://www.googleapis.com/compute/v1/projects/hindproj/zones/us-central1-b";
        instance.setZone(zone);
        Compute.Instances.Insert ins = compute.instances().insert(projectId,"us-west1-a", instance);

        // Finally, let's run it.
        Operation op = ins.execute();
        System.out.println(op.toPrettyString());
        System.out.println(instance.toPrettyString());
      }
	*/
	private void createInstance() throws GeneralSecurityException, IOException {
		// Authentication is provided by the 'gcloud' tool when running locally
	    // and by built-in service accounts when running on GAE, GCE, or GKE.
//		DatastoreOptions.newBuilder().setProjectId("core-ridge-146607").build().getService();
		/*StorageOptions.newBuilder().setProjectId("core-ridge-146607")
	    .setCredentials(ServiceAccountCredentials.fromStream(new FileInputStream("D:\\PemFile\\My First Project-8351ac0121b3.json")))
	    .build().getService();*/
	    GoogleCredential credential = GoogleCredential.getApplicationDefault();

	    // The createScopedRequired method returns true when running on GAE or a local developer
	    // machine. In that case, the desired scopes must be passed in manually. When the code is
	    // running in GCE, GKE or a Managed VM, the scopes are pulled from the GCE metadata server.
	    // For more information, see
	    // https://developers.google.com/identity/protocols/application-default-credentials
	    if (credential.createScopedRequired()) {
	      credential =
	          credential.createScoped(
	              Collections.singletonList("https://www.googleapis.com/auth/cloud-platform"));
	    }
		
	    HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
	    JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
	    Compute computeService =
	        new Compute.Builder(httpTransport, jsonFactory, credential)
	            .setApplicationName("Google Cloud Platform Sample")
	            .build();

	    // TODO: Change placeholders below to appropriate parameter values for the 'insert' method:

	    // * Project ID for this request.
	    String project = "core-ridge-146607";

	    // * The name of the zone for this request.
	    String zone = "us-west1-a";

	    Instance instance = new Instance();
	    // TODO: Add code here to assign values to desired fields of the 'content' object

	    Compute.Instances.Insert request = computeService.instances().insert(project, zone, instance);
	    Operation response = request.execute();
	    System.out.println(response.getStatus());
	    System.out.println(response);
	    
	    // TODO: Add code here to process the 'response' object

	}
	
	private void gitHubExample() throws InterruptedException, TimeoutException {

		com.google.cloud.compute.Compute compute = getGooleCloudCompute();
		com.google.cloud.compute.Address externalIp = compute.getAddress(createRegion(compute));
		InstanceId instanceId = InstanceId.of("us-central1-a", "test-instance5");
		NetworkId networkId = NetworkId.of("default");
		PersistentDiskConfiguration attachConfiguration =
		    PersistentDiskConfiguration.newBuilder(getImageId(compute)).setBoot(true).build();
		AttachedDisk attachedDisk = AttachedDisk.of("dev0", attachConfiguration);
		com.google.cloud.compute.NetworkInterface networkInterface = com.google.cloud.compute.NetworkInterface.newBuilder(networkId)
		    .setAccessConfigurations(AccessConfig.of(externalIp.getAddress()))
		    .build();
		MachineTypeId machineTypeId = MachineTypeId.of("us-central1-a", "n1-standard-1");
		InstanceInfo instance =
		    InstanceInfo.of(instanceId, machineTypeId, attachedDisk, networkInterface);
		com.google.cloud.compute.Operation operation = compute.create(instance);
		com.google.cloud.compute.Instance instance2 = compute.getInstance(instanceId);
		operation = operation.waitFor();
		if (operation.getErrors() == null) {
		  System.out.println("Instance " + instanceId + " was successfully created");
		  System.out.println(instance);
		  System.out.println(instance2);
		} else {
		  // inspect operation.getErrors()
		  throw new RuntimeException("Instance creation failed");
		}		
	}
	
	private com.google.cloud.compute.Compute getGooleCloudCompute() {
		com.google.cloud.compute.Compute compute = ComputeOptions.getDefaultInstance().getService();
		return compute;
	}
	
	private RegionAddressId createRegion(com.google.cloud.compute.Compute compute) throws InterruptedException, TimeoutException {
		RegionAddressId addressId = RegionAddressId.of("us-central1", "test-address");
		com.google.cloud.compute.Operation operation = compute.create(AddressInfo.of(addressId));
		// Wait for operation to complete
		operation = operation.waitFor();
		if (operation.getErrors() == null) {
		  System.out.println("Address " + addressId + " was successfully created");
		  return addressId;
		} else {
		  throw new RuntimeException("Address creation failed");
		}
	}
	
	private DiskId getImageId(com.google.cloud.compute.Compute compute) throws InterruptedException, TimeoutException {
		ImageId imageId = ImageId.of("ubuntu-os-cloud", "ubuntu-1604-xenial-v20161221");
		DiskId diskId = DiskId.of("us-central1-a", "test-disk6");
		ImageDiskConfiguration diskConfiguration = ImageDiskConfiguration.of(imageId);
		DiskInfo disk = DiskInfo.of(diskId, diskConfiguration);
		com.google.cloud.compute.Operation operation = compute.create(disk);
		// Wait for operation to complete
		operation = operation.waitFor();
		if (operation.getErrors() == null) {
		  System.out.println("Disk " + diskId + " was successfully created");
		  return diskId;
		} else {
		  throw new RuntimeException("Disk creation failed");
		}
	}
	
	public enum EGCERegions {
		ASIAEAST1("asia-east1"), EUROPEWEST1("europe-west1"), USCENTRAL1("us-central1");
		private String regionName;
		private EGCERegions(String regionName) {
			this.regionName = regionName;
		}
		public String getGCERegionName() {
			return this.regionName;
		}
	}
	
	public enum EGCEZones {
		
	}
	
	public void stopInstance() {
		com.google.cloud.compute.Compute compute = getGooleCloudCompute();
		InstanceId instanceId = InstanceId.of("us-central1-a", "google-1");
		com.google.cloud.compute.Operation operation = compute.stop(instanceId);
		System.out.println(operation.getClass());
		System.out.println(com.google.cloud.compute.Operation.class);
		System.out.println(operation instanceof com.google.cloud.compute.Operation);
		if (operation.getErrors() == null) {
			  System.out.println(operation);
			} else {
			  throw new RuntimeException("Address creation failed");
			}
	}
	
	public void deleteInstance() {
		com.google.cloud.compute.Compute compute = getGooleCloudCompute();
		InstanceId instanceId = InstanceId.of("us-central1-a", "test-instance3");
		com.google.cloud.compute.Operation operation = compute.deleteInstance(instanceId);
		if (operation.getErrors() == null) {
			  System.out.println(operation);
			} else {
			  throw new RuntimeException("Address creation failed");
			}
	}
	
	public void rebootInstance() {
		com.google.cloud.compute.Compute compute = getGooleCloudCompute();
		InstanceId instanceId = InstanceId.of("us-central1-a", "test-instance3");
		com.google.cloud.compute.Operation operation = compute.reset(instanceId);
		if (operation.getErrors() == null) {
			  System.out.println(operation);
			} else {
			  throw new RuntimeException("Address creation failed");
			}
	}
	
	public void startInstance() {
		com.google.cloud.compute.Compute compute = getGooleCloudCompute();
		InstanceId instanceId = InstanceId.of("us-central1-a", "google-1");
		com.google.cloud.compute.Operation operation = compute.start(instanceId);
		Class<?> obj = InstanceStopResponse.class;
		Object k = obj;
		System.out.println(k.getClass() == InstanceStopResponse.class);
		System.out.println(k.getClass() == InstanceStartResponse.class);
		System.out.println(obj == InstanceStopResponse.class);
		System.out.println(obj == InstanceStartResponse.class);
		
		if(operation.getClass().isInstance(com.google.cloud.compute.Operation.class)) {
			System.out.println("test");
		}
		if (operation.getErrors() == null) {
			  System.out.println(operation);
			} else {
			  throw new RuntimeException("Address creation failed");
			}
	}
	
	public void getInstance() {
		com.google.cloud.compute.Compute compute = getGooleCloudCompute();
		InstanceId instanceId = InstanceId.of("us-central1-a", "test-instance3");
		com.google.cloud.compute.Instance ins = compute.getInstance(instanceId);
		System.out.println(ins);
	}
	
	public static void main(String a[]) throws GeneralSecurityException, IOException, InterruptedException, TimeoutException {
//		new GoogleServerInstance().gitHubExample();
//		new GoogleServerInstance().stopInstance();
		new GoogleServerInstance().startInstance();
//		new GoogleServerInstance().getInstance();
//		patternCheck();
	}
	
	private static void patternCheck() {
		Pattern pattern = Pattern.compile("(?:[a-z](?:[-a-z0-9]{0,61}[a-z0-9])?)");
		String test = "Region-VmMedium32-16-Rohit-7347".toLowerCase();
		Matcher matcher = pattern.matcher(test);
		System.out.println(test + " $$ " +matcher.matches());
	}
}
