package com.ccdm.workout;

import org.springframework.stereotype.Component;

@Component
public class TestObject 
{
	private String projectName;

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
/*	public static void main(String[] args) throws IOException
	{
		TestObject obj = new TestObject();
		obj.test();
		 
	}

	private void test() throws IOException 
	{
		Resource resource = resourceLoader.getResource("classpath:test.pem");
		File file = resource.getFile();
	}*/
}
