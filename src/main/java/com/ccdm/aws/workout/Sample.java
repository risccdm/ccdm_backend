package com.ccdm.aws.workout;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.CreateTagsResult;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusResult;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.DescribeKeyPairsResult;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;

public abstract class Sample implements AmazonEC2 
{
	
	@Override
	public CreateTagsResult createTags(CreateTagsRequest arg0) {
		
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public RunInstancesResult runInstances(RunInstancesRequest runInstancesRequest) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public DescribeKeyPairsResult describeKeyPairs() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public StartInstancesResult startInstances(StartInstancesRequest startInstancesRequest) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public DescribeInstanceStatusResult describeInstanceStatus(
			DescribeInstanceStatusRequest describeInstanceStatusRequest) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public DescribeInstancesResult describeInstances() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
