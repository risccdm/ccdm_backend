package com.ccdm.NetSet.controller;


import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.IpPermission;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by mshrek on 1/5/17.
 */
public class CreateSecurityGroups {

	private CreateSecurityGroupRequest myCSGR;
	private IpPermission ipPermission;
	private AuthorizeSecurityGroupIngressRequest authorizeSecurityGroupIngressRequest;
	private CreateSecurityGroupResult createSecurityGroupResult;

	public CreateSecurityGroupRequest getMyCSGR() {
		return myCSGR;
	}

	public IpPermission getIpPermission() {
		return ipPermission;
	}

	// Create security group with the given security group name and description
	// You must use US-ASCII characters for the security group name and
	// description.
	public CreateSecurityGroups(final String securityGroupName, final String description) {
		this.myCSGR = new CreateSecurityGroupRequest();
		this.myCSGR.withGroupName(securityGroupName).withDescription(description);
	}

	public CreateSecurityGroups createSecurityGroupResult(final CreateSecurityGroupRequest myCSGR) {
		createSecurityGroupResult = new CreateSecurityGroupResult();
		createSecurityGroupResult = CreateEC2InstanceClient.getMyEC2InstanceClient().createSecurityGroup(myCSGR);
		return this;
	}

	// This will configure the rules for incoming requests
	public CreateSecurityGroups configureIngressRules(int i, ArrayList<String> iprange, ArrayList<String> protocal,
			ArrayList<Integer> fromport, ArrayList<Integer> toport) {
		ipPermission = new IpPermission();

		ipPermission.withIpRanges(iprange.get(i)).withIpProtocol(protocal.get(i)).withFromPort(fromport.get(i))
				.withToPort(toport.get(i));
		return this;
	}

	public CreateSecurityGroups configureIngressRules1(int i, ArrayList<String> iprange, ArrayList<String> protocal,
			ArrayList<Integer> fromport, ArrayList<Integer> toport) {
		ipPermission = new IpPermission();

		ipPermission.withIpRanges(iprange.get(i)).withIpProtocol(protocal.get(i)).withFromPort(fromport.get(i))
				.withToPort(toport.get(i));
		return this;
	}

	public CreateSecurityGroups configureOutgressRules() {
		return this;
	}

	// This will actually set the rules for incoming requests to the instance
	public void authorizeSecurityGroupIngressRequest(final IpPermission ipPermission) {
		authorizeSecurityGroupIngressRequest = new AuthorizeSecurityGroupIngressRequest();
		authorizeSecurityGroupIngressRequest.withGroupName(myCSGR.getGroupName()).withIpPermissions(ipPermission);
		CreateEC2InstanceClient.getMyEC2InstanceClient()
				.authorizeSecurityGroupIngress(authorizeSecurityGroupIngressRequest);

	}

	// Comment main method when other tests use this class
	public void securitycall(String name, String des, ArrayList<String> protocal, ArrayList<String> iprange, ArrayList<Integer> fromport, ArrayList<Integer> toport) {

		System.out.println("Creating new security group ...");
		final String securityGroupName = name;
		final String securityGroupDescription = des;
		int check = 0;
		int i;
		// creating security group
		for (i = 0; i < iprange.size(); i++) {
			if (check==0) {
				System.out.println("if");
				CreateSecurityGroups mySecurityGroup = new CreateSecurityGroups(securityGroupName,securityGroupDescription);
				mySecurityGroup.createSecurityGroupResult(mySecurityGroup.getMyCSGR()).configureIngressRules(i, iprange, protocal, fromport, toport)
						.authorizeSecurityGroupIngressRequest(mySecurityGroup.getIpPermission());
				check =1;
			} else {
				System.out.println("else");
				CreateSecurityGroups mySecurityGroup = new CreateSecurityGroups(securityGroupName,
						securityGroupDescription);

				mySecurityGroup.configureIngressRules1(i, iprange, protocal, fromport, toport)
						.authorizeSecurityGroupIngressRequest(mySecurityGroup.getIpPermission());

			}
		}
		System.out.println("New security group created successfully !");

	}

}
