package com.ccdm.NetSet.controller;

import java.util.ArrayList;
import java.util.List;

import com.ccdm.NetSet.model.Netset;
import com.ccdm.NetSet.model.Networkmap;
import com.ccdm.solution.model.VmScriptModel;
import com.ccdm.vo.Networkrules;

public class NetSetRequest implements java.io.Serializable {
	
	private String grpname;
	private String grpdescription;
	private List<String> netmap;
	private List<Netset> testArray;
	
	
	 public void setTestArray(List<Netset> testArray) {
	     this.testArray = testArray;
	 }
	 public List<Netset> getTestArray() {
	     return testArray;
	 }
	 
	 public void setNetworkmap(List<String> netmap) {
			System.out.println("t7" + netmap);
			this.netmap = netmap;
		}
	 public List<String> getNetworkmap() {
			return netmap;
		}
	
	 
	public String getgrpname() {
		return grpname;
	}
	public void setgrpname(String grpname) {
		this.grpname = grpname;
		System.out.println(grpname);
		System.out.println("t6");
	}
	
	
	public String getgrpdescription() {
		return grpdescription;
	}
	public void setgrpdescription(String grpdescription) {
		this.grpdescription = grpdescription;
		System.out.println(grpdescription);
		System.out.println("t7");
	}
}
