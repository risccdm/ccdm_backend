package com.ccdm.NetSet.model;

public class Netset {

	
	private String rltype;
	private int rlprtcl;
	private int rlprtrng;
	private String rlsrce;
	private Integer rlstatus;
	
   public Netset() {}
	
	public Netset(String rltype, int rlprtcl, int rlprtrng, String rlsrce, Integer rlstatus ) {
		this.rltype = rltype;
		this.rlprtcl = rlprtcl;
		this.rlprtrng = rlprtrng;
		this.rlsrce = rlsrce;
		this.rlstatus = rlstatus;
	}
	public String getrltype() {
		return rltype;
	}

	public void setrltype(String rltype) {
		this.rltype = rltype;
	}
	
	public int getrlprtcl() {
		return rlprtcl;
	}

	public void setrlprtcl(int rlprtcl) {
		this.rlprtcl = rlprtcl;
	}
	
	
	public int getrlprtrng() {
		return rlprtrng;
	}

	public void setrlprtrng(int rlprtrng) {
		this.rlprtrng = rlprtrng;
	}
	
	public String getrlsrce() {
		return rlsrce;
	}

	public void setrlsrce(String rlsrce) {
		this.rlsrce = rlsrce;
	}
	public Integer getrlstatus() {
		return rlstatus;
	}

	public void setrlstatus(Integer rlstatus) {
		this.rlstatus = rlstatus;
	}
	

	
}
