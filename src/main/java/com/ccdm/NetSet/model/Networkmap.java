package com.ccdm.NetSet.model;

public class Networkmap {

	
	private Rltype rltype;
	private String rlprtcl;
	private String rlprtrng;
	private String rlsrce;
	private Integer rlstatus;

	public Rltype getrltype() {
		return rltype;
	}

	public void setrltype(Rltype rltype) {
		this.rltype = rltype;
	}
	public String getrlprtcl() {
		return rlprtcl;
	}

	public void setrlprtcl(String rlprtcl) {
		this.rlprtcl = rlprtcl;
	}
	
	public String getrlprtrng() {
		return rlprtrng;
	}

	public void setrlprtrng(String rlprtrng) {
		this.rlprtrng = rlprtrng;
	}
	
	public String getrlsrce() {
		return rlsrce;
	}

	public void setrlsrce(String rlsrce) {
		this.rlsrce = rlsrce;
	}

	public Integer getrlstatus() {
		return rlstatus;
	}

	public void setrlstatus(Integer rlstatus) {
		this.rlstatus = rlstatus;
	}

	
}
