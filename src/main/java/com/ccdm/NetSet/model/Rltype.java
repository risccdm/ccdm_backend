package com.ccdm.NetSet.model;

public class Rltype {
	
	private String rltype;
	
	public Rltype() {}
	
	public Rltype(String rltype) {
		this.rltype = rltype;
	}
	
	public String getVmTemplateName() {
		return rltype;
	}
	public void setVmTemplateName(String rltype) {
		this.rltype = rltype;
	}
	
}
