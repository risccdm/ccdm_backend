package com.ccdm.NetSet.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.solution.model.VmScriptModel;
import com.ccdm.validators.TemplateValidator;
import com.ccdm.viewModel.controller.ViewModelController;
import com.ccdm.NetSet.controller.CreateSecurityGroups;
import com.ccdm.NetSet.controller.NetSetRequest;
import com.ccdm.NetSet.dao.NetSetDAO;
import com.ccdm.NetSet.dao.NetSetDAOImpl;
import com.ccdm.NetSet.model.Netset;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.SolutionVmMap;
import com.ccdm.vo.NetSet;
import com.ccdm.vo.Networkrules;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

@Service
@Transactional(readOnly = false)
public class NetSetServiceImpl implements NetSetService {
	
	static String name;
	static String des;
	static int fromport;
	static int toport;
	static String type;
	static String iprange;
	    ArrayList<String> netiprange = new ArrayList<String>();
		ArrayList<String> netprotocal = new ArrayList<String>();
		ArrayList<Integer> netfromport = new ArrayList<Integer>();
		ArrayList<Integer> nettoport = new ArrayList<Integer>();
	
	@Autowired
	private NetSetDAO templateDao;
	
	@SuppressWarnings("unused")
	@Autowired
	private TemplateValidator templateValidator;
	
	@Autowired
	private ViewModelController viewModelController;
	
	@Override
	public List<ViewModel> getTemplates(){
		System.out.println("inside get templates---->");
	 //List<ViewModel> str = templateDao.getTemplates();
	// System.out.println("Aji"+str.toString());
		return templateDao.getTemplates();
	}
	
	@Override
	public List<VirtualMachine> getVirualMachineSizes(){
		return templateDao.getVirtualMachineSizes();
	}
	
	@Override
	public List<NetSet> getAllTemplates(Integer userId){
		System.out.println("getie");
		return templateDao.getAllTemplates(userId);
	}
	
	@Override
	public List<WebServerProvider> getWebServerProviders(){
		return templateDao.getWebServerProviders();
	}
	
	@Override
	public Boolean isNamePresent(String name, Integer clientId){
		NetSet template = templateDao.getTemplate(name, clientId);
		
		if(template == null){
			return false;
		}
		return true;
	}
	
	@Override
	public void save(NetSet template) throws CCDMException 
	{
		
		templateDao.saveOrUpdate(template);
		System.out.println(template);
		System.out.println("t10");
	}

	@Override
	public NetSet buildTemplate(NetSetRequest templateRequest, AppUser appUser)
	{
		NetSet netset = new NetSet();
		netset.setAppUser(appUser);
		netset.setgrpname(templateRequest.getgrpname());
		netset.setgrpdescription(templateRequest.getgrpdescription());
		netset.setClientId(appUser.getClientId());
		return netset;
	}
	
//	@Override
//	public List<Networkrules> buildNetworkmap(NetSetRequest templateRequest) throws CCDMUnauthorizedException
//	{
//		 List<Networkrules> netsets = new ArrayList<Networkrules>();
//		 List<Networkrules> networkmaps = templateRequest.getNetworkmap();
//		 for (String networkmap : networkmaps) {
//			 Networkrules netset = new Networkrules();
//				
//				
//					netset.setrltype(networkmap.getrltype());
//					netset.setrlprtcl(networkmap.getrlprtcl());
//					netset.setrlprtrng(networkmap.getrlprtrng());
//					netset.setrlsrce(networkmap.getrlsrce());
//					netset.setrlstatus(networkmap.getrlstatus());
//					
//					
//					netsets.add(netset);
//				
//			}
//			return netsets;
//	}

	@Override
	public List<Map<String, String>> getTemplateNames(Integer appUserId) 
	{
		
		List<Map<String, String>> templateNames = new ArrayList<>();
		
		List<NetSet> templates = templateDao.getTemplateNames(appUserId);
		
		for(NetSet template : templates)
		{
			Map<String, String> templateName = new HashMap<>();
			templateName.put("id", String.valueOf((int)template.getnwID()));
			templateName.put("name", template.getgrpname());
			templateNames.add(templateName);
			
			 name = template.getgrpname();
			 des = template.getgrpdescription();
		}
		return templateNames;
	}

	/*@Override
	public Object testEalstic()
	{
		return templateDao.testEalstic();
	}*/

	@Override
	public NetSet getTemplateById(Integer templateId) 
	{
		return templateDao.getTemplateById(templateId);
	}

	@Override
	public List<Networkrules> buildNetworkmap(NetSetRequest templateRequest, Integer nwID) throws CCDMUnauthorizedException {
		List<Networkrules> netsets = new ArrayList<Networkrules>();
		List<Netset> networkmaps = templateRequest.getTestArray();
		 for (Netset networkmap : networkmaps) {
			 Networkrules netset = new Networkrules();
			 		netset.setrltype(networkmap.getrltype());
					netset.setrlprtcl(networkmap.getrlprtcl());
					netset.setrlprtrng(networkmap.getrlprtrng());
					netset.setrlsrce(networkmap.getrlsrce());
					netset.setrlstatus(networkmap.getrlstatus());
					netset.setnwID(nwID);
					netsets.add(netset);
					 fromport = networkmap.getrlprtcl();
					 iprange = networkmap.getrlsrce();
					 toport = networkmap.getrlprtrng();
					 type = networkmap.getrltype();
					netprotocal.add(type);
					netiprange.add(iprange);
					netfromport.add(fromport);
					nettoport.add(toport);
			}
		 
		 
		 try{
		 NetSetDAOImpl call = new NetSetDAOImpl();
		 call.networkcall(netprotocal, netiprange, netfromport, nettoport);
		 } catch (ArithmeticException e) {
			netiprange.clear();
			netprotocal.clear();
			netfromport.clear();
			nettoport.clear();
			Logger.global.warning("give correct credentials");
		 }
			return netsets;
	}

	@Override
	public void updateNetworkrules(List<Networkrules> ntemplate) {
		for (Networkrules template : ntemplate) {
			templateDao.saveNetworkrules(template);
		}
		
	}
}
