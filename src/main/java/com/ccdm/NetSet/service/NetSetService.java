package com.ccdm.NetSet.service;

import java.util.List;
import java.util.Map;

import com.ccdm.NetSet.controller.NetSetRequest;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.NetSet;
import com.ccdm.vo.Networkrules;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.WebServerProvider;

public interface NetSetService 
{
	
	List<ViewModel> getTemplates();	

	List<VirtualMachine> getVirualMachineSizes();

	List<WebServerProvider> getWebServerProviders();

	void save(NetSet template) throws CCDMException;

	Boolean isNamePresent(String name, Integer clientId);

	List<Map<String, String>> getTemplateNames(Integer appUserId);
	
	List<NetSet> getAllTemplates(Integer userId);
	
	NetSet buildTemplate(NetSetRequest templateRequest, AppUser appUser);

	NetSet getTemplateById(Integer templateId);

	List<Networkrules> buildNetworkmap(NetSetRequest templateRequest, Integer nwID) throws CCDMUnauthorizedException;

	void updateNetworkrules(List<Networkrules> ntemplate);

}
