package com.ccdm.NetSet.dao;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ccdm.NetSet.controller.CreateSecurityGroups;
import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.infrastructure.config.ElasticsearchConfiguration;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.NetSet;
import com.ccdm.vo.Networkrules;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

@Repository
public class NetSetDAOImpl extends BaseDAOImpl implements NetSetDAO
{
	static String name;
	static String des;
	@Autowired
	ElasticsearchConfiguration elasticsearchConfiguration;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ViewModel> getTemplates(){
		List<ViewModel> Vmtemplates = getSession().createCriteria(ViewModel.class).list();
		System.out.println("HEREEEEE--------"+Vmtemplates.get(0).getName());
		return Vmtemplates;	
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VirtualMachine> getVirtualMachineSizes(){
		List<VirtualMachine> virtualMachines = getSession().createCriteria(VirtualMachine.class).list();
		return virtualMachines;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<NetSet> getAllTemplates(Integer userId){
		List<NetSet> loadbal = (List<NetSet>) getSession().createCriteria(NetSet.class).
				add(Restrictions.eq("appUser.userId", userId)).
				//addOrder(Order.desc("id")).
				addOrder(Order.desc("id")).list();
		System.out.println("this"+ loadbal);
		return loadbal;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<WebServerProvider> getWebServerProviders(){
		List<WebServerProvider> webServerProviders = getSession().createCriteria(WebServerProvider.class).list();
		return webServerProviders;
	}
	
	@Override
	public void saveOrUpdate(NetSet template) {
		super.saveOrUpdate(template);
		name = template.getgrpname();
		des = template.getgrpdescription();
		 
		System.out.println(template);
		System.out.println(template.getgrpname());
		//CreateSecurityGroups sg = new CreateSecurityGroups("dljkfk", "test");
		//sg.securitycall(name,des,"tcp","117.241.159.225/32",80,95);
		
	}
	
	@Override
	public void saveOrUpdate(Networkrules ntemplate) {
		super.saveOrUpdate(ntemplate);
		
	}
	
	@Override
	public NetSet getTemplate(String name, Integer clientId) {
		NetSet template = (NetSet) getSession().createCriteria(NetSet.class).
				add(Restrictions.eq("appUser.userId", clientId)).
				add(Restrictions.eq("name", name)).uniqueResult();
		
		return template;
	}

	@Override
	public List<NetSet> getTemplateNames(Integer appUserId) 
	{
		
     @SuppressWarnings("unchecked")
	List<NetSet> templates = (List<NetSet>) getSession().createCriteria(NetSet.class)
    		 			                 .add(Restrictions.eq("appUser.userId", appUserId))
    		 			                 .list();
		return templates;
	}

	/*@Override
	public Object testEalstic()
	{
		
		ElasticTeset elasticTeset  = new ElasticTeset();
		elasticTeset.setId(121);
		elasticTeset.setAge(16);
		elasticTeset.setName("Arya");
		
		IndexQuery indexQuery =  new IndexQuery();  
		indexQuery.setObject(elasticTeset);
		indexQuery.setIndexName("ccdmtestindex");
		
		elasticsearchConfiguration.elasticsearchTemplate().index(indexQuery);
		
		return null;
	}*/

	@Override
	public NetSet getTemplateById(Integer templateId) 
	{
		return (NetSet) getSession().createCriteria(NetSet.class)
						   .add(Restrictions.idEq(templateId)).uniqueResult();
	}

	@Override
	public void saveNetworkrules(Networkrules template) {
		super.saveOrUpdate(template);
		
	}
	public void networkcall(ArrayList<String> type, ArrayList<String> iprange, ArrayList<Integer> fromport, ArrayList<Integer> toport){
		
		 CreateSecurityGroups sg = new CreateSecurityGroups(null, null);
		 sg.securitycall(name,des,type,iprange,fromport,toport);
		 System.out.println(name);
	}
}
