package com.ccdm.NetSet.dao;

import java.util.List;

import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.NetSet;
import com.ccdm.vo.Networkrules;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;
import com.ccdm.vo.WebServerProvider;

public interface NetSetDAO{

	List<ViewModel> getTemplates();

	List<VirtualMachine> getVirtualMachineSizes();

	List<WebServerProvider> getWebServerProviders();
	
	void saveOrUpdate(NetSet solution);

	void saveOrUpdate(Networkrules solution);
	
	NetSet getTemplate(String name, Integer userId);

	List<NetSet> getTemplateNames(Integer appUserId);
	List<NetSet> getAllTemplates(Integer userId);

	/*Object testEalstic();*/

	NetSet getTemplateById(Integer templateId);

	void saveNetworkrules(Networkrules template);

}
