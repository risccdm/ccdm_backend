package com.ccdm.NetSet.constance;

import java.util.HashMap;
import java.util.Map;

public class SolutionState 
{
 
	 private Integer stateId; 
	 private String stateName; 
	 private String message;
	 private static Map<Integer, SolutionState> solutionState = new HashMap<Integer, SolutionState>();
	 
	 public static SolutionState INIT = new  SolutionState(100, "INIT", "Initializing Solution...");
	 public static SolutionState INSTANCE_CREATED = new  SolutionState(200, "INSTANCE_CREATED", "Instance created...");
	 public static SolutionState ICINGA_INSTALLED = new  SolutionState(300, "ICINGA_INSTALLED", "Icinga installed in instance...");
	 public static SolutionState ICINGA_CONFIGURED_WITH_MASTER = new  SolutionState(400, "ICINGA_MASTER_CONFIGURE", 
			 																				"Icinga configured with master..");
	 public static SolutionState SOLUTION_CREATED = new  SolutionState(500, "SOLUTION_CREATED", "Solution created...");
	 public static SolutionState SOLUTION_DEPLOYED = new  SolutionState(900, "SOLUTION_DEPLOYED", "Solution Deployed");
	 
	
	 public SolutionState(Integer stateId, String stateName, String message) 
	  {
		 this.stateId = stateId;
		 this.stateName = stateName;
		 this.stateName = message;
	  }
	 
	  public SolutionState getSolutionState(Integer stateId)
	  {
		  	return solutionState.get(stateId);
	  }
	 
	  static
	  {
		  solutionState.put(INIT.stateId, INIT);
		  solutionState.put(INSTANCE_CREATED.stateId, INSTANCE_CREATED);
		  solutionState.put(ICINGA_INSTALLED.stateId, ICINGA_INSTALLED);
		  solutionState.put(ICINGA_CONFIGURED_WITH_MASTER.stateId, ICINGA_CONFIGURED_WITH_MASTER);
		  solutionState.put(SOLUTION_CREATED.stateId, SOLUTION_CREATED);
		  
	  }

		public Integer getStateId() 
		{
			return stateId;
		}
	
		public String getStateName() 
		{
			return stateName;
		}
	
		public String getMessage() 
		{
			return message;
		}
}
