package com.ccdm.sla.service;

import java.util.List;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.sla.model.SlaTypeRequest;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.SlaDetail;
import com.ccdm.vo.SlaType;

public interface SlaService 
{
	List<SlaType> getSlaTypes(AppUser appUser) throws CCDMException;

	List<SlaDetail> getSlaDetailBySlaTypeId(Integer slaTypeId) throws CCDMException;

	SlaType getSlaTypeBySlaTypeId(Integer slaTypeId) throws CCDMException;

	void saveSal(SlaTypeRequest slaTypeRequest, AppUser appUser) throws CCDMException;
}
