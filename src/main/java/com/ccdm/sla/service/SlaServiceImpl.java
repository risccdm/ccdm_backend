package com.ccdm.sla.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.sla.dao.SlaDao;
import com.ccdm.sla.model.SlaTypeRequest;
import com.ccdm.validators.SlaValidator;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.SlaDetail;
import com.ccdm.vo.SlaType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Service
@Transactional(readOnly = false)
public class SlaServiceImpl implements SlaService 
{
	@Autowired
	private SlaDao slaDao;
	@Autowired
	private SlaValidator slaValidator;

	@Override
	public List<SlaType> getSlaTypes(AppUser appUser) throws CCDMException 
	{
		List<SlaType> slaTypes = slaDao.getSlaTypes(appUser);
		return slaTypes;
	}

	@Override
	public List<SlaDetail> getSlaDetailBySlaTypeId(Integer slaTypeId) throws CCDMException 
	{
		CommonValidator.validateId(slaTypeId, ErrorCodes.INVALID_SLA_TYPE_ID);
		List<SlaDetail> slaDetails = slaDao.getSlaDetailBySlaTypeId(slaTypeId);
		return slaDetails;
	}

	@Override
	public SlaType getSlaTypeBySlaTypeId(Integer slaTypeId) throws CCDMException 
	{
		CommonValidator.validateId(slaTypeId, ErrorCodes.INVALID_SLA_TYPE_ID);
		return slaDao.getSlaTypeBySlaTypeId(slaTypeId);
	}
	
	@Override
	public void saveSal(SlaTypeRequest slaTypeRequest, AppUser appUser) throws CCDMException 
	{
		slaValidator.validateSlaTypeRequest(slaTypeRequest);
		
		SlaType slaType = getSlaType(slaTypeRequest);
		slaType.setSlaTypeName(slaTypeRequest.getSlaTypeName());
		slaType.setAppUser(appUser);
		slaType.setClientId(appUser.getClientId());
		
		SlaType savedSlaType = slaDao.saveSlaType(slaType);
		
		for(SlaDetail slaDetail : slaTypeRequest.getSlaDetail())
		{
			// this will return existing detail if present in db else return new detail object
			SlaDetail existingSlaDetail = getSlaDetail(savedSlaType, slaDetail.getPriorityType());
			existingSlaDetail.setCredits(slaDetail.getCredits());
			existingSlaDetail.setPriorityType(slaDetail.getPriorityType());
			existingSlaDetail.setResponseTime(slaDetail.getResponseTime());
			existingSlaDetail.setUptime(slaDetail.getUptime());
			existingSlaDetail.setWorkingHours(slaDetail.getWorkingHours());
			slaDao.saveSlaDetail(existingSlaDetail);
		}
	}
	
	/* **************************************************** Private Methods **************************************************** */
	/* Sla Type Object Will be return based on slaTypeId - Edit mode or create Mode */
	private SlaType getSlaType(SlaTypeRequest slaTypeRequest) throws CCDMException 
	{
		if(slaTypeRequest.getSlaTypeId() == null || slaTypeRequest.getSlaTypeId() < 1)
		{
			SlaType slaType = new SlaType();
			return slaType;
		}
		else
		{
			CommonValidator.validateId(slaTypeRequest.getSlaTypeId(), ErrorCodes.INVALID_SLA_TYPE_ID);
			SlaType slaType = slaDao.getSlaTypeBySlaTypeId(slaTypeRequest.getSlaTypeId());
			return slaType;
		}
	}
	
	private SlaDetail getSlaDetail(SlaType savedSlaType, String priorityType) throws CCDMException 
	{
		CommonValidator.validateId(savedSlaType.getSlaTypeId(), ErrorCodes.INVALID_SLA_TYPE_ID);
		CommonValidator.validateString(priorityType, ErrorCodes.INVALID_SLA_PRIORITY_TYPE);
		
		SlaDetail slaDetail = slaDao.getSlaDetailBySlaTypeIdPriorityType(savedSlaType.getSlaTypeId(), priorityType);
		
		if(slaDetail == null)
		{
			slaDetail = new SlaDetail();
			slaDetail.setSlaType(savedSlaType);
		}
		return slaDetail;
	}
}
