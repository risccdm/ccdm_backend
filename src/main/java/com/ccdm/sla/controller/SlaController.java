package com.ccdm.sla.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.sla.model.SlaTypeRequest;
import com.ccdm.sla.service.SlaService;
import com.ccdm.solutions.service.SolutionService;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.SlaDetail;
import com.ccdm.vo.SlaType;
import com.ccdm.vo.Solution;

@RestController
@RequestMapping(value = "/sla")
public class SlaController  extends BaseController 
{
	@Autowired
	private SlaService slaService;
	@Autowired
	private SolutionService solutionService;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	@RequestMapping(value = "/types", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getSlaTypes() throws CCDMException
	{
		AppUser appUser = getLoggedInUser();
		List<SlaType> slaTypes = slaService.getSlaTypes(appUser);
		return getResponseEntity(slaTypes, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/detail/{slaTypeId}", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getSlaById(@PathVariable(value = "slaTypeId") Integer slaTypeId) throws CCDMException
	{
		List<SlaDetail> slaDetails = slaService.getSlaDetailBySlaTypeId(slaTypeId);
		return getResponseEntity(slaDetails, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/type/bySolution/{solutionId}")
	public ResponseEntity<CCDMResponse> getSlaBySolutionId(@PathVariable(value = "solutionId") Integer solutionId) throws CCDMException
	{
		SlaType slaType = null;
		Solution solution = solutionService.getSolutionBySolutionId(solutionId, getLoggedInUser());
		if(solution != null)
		{
			slaType = solution.getSlaType();
		}
		return getResponseEntity(slaType, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public CCDMResponse saveSlaDetail(@RequestBody SlaTypeRequest slaTypeRequest) throws CCDMException
	{
		AppUser user = getLoggedInUser();
		slaService.saveSal(slaTypeRequest, user);
		return getCCDMResponse(HttpStatus.OK);
	}
}
