package com.ccdm.sla.model;

import java.util.List;

import com.ccdm.vo.SlaDetail;

public class SlaTypeRequest 
{
	private Integer slaTypeId;
	private String slaTypeName;
	private List<SlaDetail> slaDetail;
	
	public String getSlaTypeName() {
		return slaTypeName;
	}
	public void setSlaTypeName(String slaTypeName) {
		this.slaTypeName = slaTypeName;
	}
	public List<SlaDetail> getSlaDetail() {
		return slaDetail;
	}
	public void setSlaDetail(List<SlaDetail> slaDetail) {
		this.slaDetail = slaDetail;
	}
	public Integer getSlaTypeId() {
		return slaTypeId;
	}
	public void setSlaTypeId(Integer slaTypeId) {
		this.slaTypeId = slaTypeId;
	}
}
