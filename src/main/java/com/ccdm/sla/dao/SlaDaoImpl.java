package com.ccdm.sla.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.SlaDetail;
import com.ccdm.vo.SlaType;

@Repository
public class SlaDaoImpl extends BaseDAOImpl implements SlaDao 
{
	@SuppressWarnings("unchecked")
	@Override
	public List<SlaType> getSlaTypes(AppUser appUser) 
	{
		List<SlaType> slaTypes = getSession().createCriteria(SlaType.class)
				.add(Restrictions.eq("clientId", appUser.getClientId())).list();
		return slaTypes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SlaDetail> getSlaDetailBySlaTypeId(Integer slaTypeId) 
	{
		List<SlaDetail> slaDetails = getSession().createCriteria(SlaDetail.class)
												.add(Restrictions.eq("slaType.slaTypeId", slaTypeId)).list();
		return slaDetails;
	}

	@Override
	public void saveSlaDetail(SlaDetail slaDetail) 
	{
		super.saveOrUpdate(slaDetail);
	}

	@Override
	public SlaType saveSlaType(SlaType slaType) 
	{
		super.save(slaType);
		return slaType;
	}

	@Override
	public SlaType getSlaTypeBySlaTypeId(Integer slaTypeId) 
	{
		SlaType slaType = (SlaType) getSession().createCriteria(SlaType.class)
									  .add(Restrictions.idEq(slaTypeId)).uniqueResult();
		return slaType;
	}

	@Override
	public SlaDetail getSlaDetailBySlaTypeIdPriorityType(Integer slaTypeId, String priorityType) 
	{
		SlaDetail slaDetail = (SlaDetail) getSession().createCriteria(SlaDetail.class)
										  .add(Restrictions.eq("priorityType", priorityType))
										  .add(Restrictions.eq("slaType.slaTypeId", slaTypeId))
										  .uniqueResult();
		return slaDetail;
	}
}
