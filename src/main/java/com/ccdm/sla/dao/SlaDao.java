package com.ccdm.sla.dao;

import java.util.List;

import com.ccdm.vo.AppUser;
import com.ccdm.vo.SlaDetail;
import com.ccdm.vo.SlaType;

public interface SlaDao 
{
	List<SlaType> getSlaTypes(AppUser appUser);

	List<SlaDetail> getSlaDetailBySlaTypeId(Integer slaTypeId);

	void saveSlaDetail(SlaDetail slaDetail);

	SlaType saveSlaType(SlaType slaType);

	SlaType getSlaTypeBySlaTypeId(Integer slaTypeId);

	SlaDetail getSlaDetailBySlaTypeIdPriorityType(Integer slaTypeId, String priorityType);
}
