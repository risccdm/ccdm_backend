package com.ccdm.instance.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;

@Repository
public class InstanceDAOImpl  extends BaseDAOImpl implements InstanceDAO 
{

	@Override
	public List<InstanceInfo> getInstances(Integer clientId) 
	{
		@SuppressWarnings("unchecked")
		List<InstanceInfo> instanceInfoList = (List<InstanceInfo>) getSession().createCriteria(InstanceInfo.class)
																.setFetchMode("providerPlatformReference", FetchMode.JOIN)
																.setFetchMode("providerPlatformReference.operatingSystem", FetchMode.JOIN)
																.setFetchMode("providerPlatformReference.virtualMachine", FetchMode.JOIN)
																.setFetchMode("providerPlatformReference.webServerProvider", FetchMode.JOIN)
																.setFetchMode("solutionClientMap", FetchMode.JOIN)
																.setFetchMode("solutionVmMap", FetchMode.JOIN)
																.setFetchMode("solutionClientMap.customerInfo", FetchMode.JOIN)
																.createAlias("solutionClientMap.solution", "sol_client_sol")
																.createAlias("solutionClientMap.solution.appUser", "sol_user")
																.add(Restrictions.eq("sol_user.clientId", clientId))
																.list();
		return instanceInfoList;
	}
	
	@Override
	public List<InstanceInfo> getInstanceInfoByInstanceIdList(List<String> instanceIdList) {
		@SuppressWarnings("unchecked")
		List<InstanceInfo> serverInstanaceInfoList = (List<InstanceInfo>) getSession().createCriteria(InstanceInfo.class)
															.add(Restrictions.in("instanceId", instanceIdList)).list();
		
		return serverInstanaceInfoList;
	}

	@Override
	public InstanceInfo getInstanceInfoByInstanceId(String instanceId) 
	{
		InstanceInfo serverInstanaceInfo = (InstanceInfo) getSession()
                .createCriteria(InstanceInfo.class)
				.add(Restrictions.eq("instanceId", instanceId))
				.setFetchMode("solutionClientMap", FetchMode.JOIN)
				.setFetchMode("solutionVmMap", FetchMode.JOIN)
				.uniqueResult();
		return serverInstanaceInfo;
	}
	
	@Override
	public void commitTraction() 
	{
		super.commitTraction();
	}

	@Override
	public InstanceInfo save(InstanceInfo serverInstanaceInfo) 
	{
		super.saveOrUpdate(serverInstanaceInfo);
		return serverInstanaceInfo;
	}

	@Override
	public List<InstanceInfo> getInstanceInfoByAllSolutionIdAndByAllCustomerId(Integer clientId) 
	{
		return getInstances(clientId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstanceInfo> getInstanceInfoBySolutionIdAndByCustomerId(Integer clientId, Integer solutionId, Integer customerId) 
	{
		return getSession().createCriteria(InstanceInfo.class)
							.setFetchMode("solutionClientMap", FetchMode.JOIN)
							.setFetchMode("solutionVmMap", FetchMode.JOIN)
							.createAlias("solutionClientMap.solution", "sol_client_sol")
							.createAlias("solutionClientMap.customerInfo", "sol_client_customer")
							.createAlias("solutionClientMap.solution.appUser", "sol_user")
							.add(Restrictions.eq("sol_client_sol.solutionId", solutionId))
							.add(Restrictions.eq("sol_client_customer.id", customerId))
							.add(Restrictions.eq("sol_user.clientId", clientId))
							.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstanceInfo> getInstanceInfoByAllSolutionIdAndByCustomerId(Integer clientId, Integer customerId) 
	{
		return (List<InstanceInfo>) getSession().createCriteria(InstanceInfo.class)
												.setFetchMode("solutionClientMap", FetchMode.JOIN)
												.setFetchMode("solutionVmMap", FetchMode.JOIN)
												.createAlias("solutionClientMap.customerInfo", "sol_client_customer")
												.createAlias("solutionClientMap.solution", "sol_client_sol")
												.createAlias("solutionClientMap.solution.appUser", "sol_user")
												.add(Restrictions.eq("sol_client_customer.id", customerId))
												.add(Restrictions.eq("sol_user.clientId", clientId))
												.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstanceInfo> getInstanceInfoBySolutionIdAndByAllCustomerId(Integer clientId, Integer solutionId) 
	{
		return (List<InstanceInfo>) getSession().createCriteria(InstanceInfo.class)
												.setFetchMode("solutionClientMap", FetchMode.JOIN)
												.setFetchMode("solutionVmMap", FetchMode.JOIN)
												.createAlias("solutionClientMap.solution", "sol_client_sol")
												.createAlias("solutionClientMap.solution.appUser", "sol_user")
												.add(Restrictions.eq("sol_client_sol.solutionId", solutionId))
												.setFetchMode("solutionClientMap.customerInfo", FetchMode.JOIN)
												.add(Restrictions.eq("sol_user.clientId", clientId))
												.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InstanceInfo> getInstanceInfo(AppUser appUser, Integer customerId, Integer solutionId,
			Integer pageNumber, Integer pageLimit) {
		
		Criteria criteria = getSession().createCriteria(InstanceInfo.class)
							.setFetchMode("solutionClientMap", FetchMode.JOIN)
							.setFetchMode("solutionClientMap.customerInfo", FetchMode.JOIN)
							.setFetchMode("solutionClientMap.solution", FetchMode.JOIN)
							.createAlias("solutionClientMap.customerInfo", "sol_client_customer")
							.createAlias("solutionClientMap.solution", "sol_client_sol");
		
		criteria.setFirstResult(pageNumber);
		criteria.setMaxResults(pageLimit);
		
		if(customerId > 0) {
			Criterion criterion = Restrictions.eq("sol_client_customer.id", customerId);
			criteria.add(criterion);
		}
		if(solutionId > 0) {
			Criterion criterion = Restrictions.eq("sol_client_sol.solutionId", solutionId);
			criteria.add(criterion);
		}
		
		return criteria.list();
	}

	@Override
	public InstanceInfo getInstanceInfoById(Integer instanceInfoId) {
		InstanceInfo serverInstanaceInfo = (InstanceInfo) getSession()
                .createCriteria(InstanceInfo.class)
				.add(Restrictions.idEq(instanceInfoId))
				.setFetchMode("providerPlatformReference.webServerProvider", FetchMode.JOIN)
				.setFetchMode("solutionClientMap", FetchMode.JOIN)
				.setFetchMode("solutionVmMap", FetchMode.JOIN)
				.setFetchMode("solutionVmMap.solution", FetchMode.JOIN)
				.uniqueResult();
		return serverInstanaceInfo;
	}
}
