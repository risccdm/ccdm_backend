package com.ccdm.instance.dao;

import java.util.List;

import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;

public interface InstanceDAO 
{
	List<InstanceInfo> getInstances(Integer userId);

	List<InstanceInfo> getInstanceInfoByInstanceIdList(List<String> instanceIdList);

	InstanceInfo getInstanceInfoByInstanceId(String instanceId);
	
	void commitTraction();

	InstanceInfo save(InstanceInfo serverInstanaceInfo);

	List<InstanceInfo> getInstanceInfoByAllSolutionIdAndByAllCustomerId(Integer clientId);

	List<InstanceInfo> getInstanceInfoBySolutionIdAndByCustomerId(Integer clientId, Integer solutionId, Integer customerName);

	List<InstanceInfo> getInstanceInfoByAllSolutionIdAndByCustomerId(Integer clientId, Integer customerName);

	List<InstanceInfo> getInstanceInfoBySolutionIdAndByAllCustomerId(Integer clientId, Integer solutionId);

	List<InstanceInfo> getInstanceInfo(AppUser appUser, Integer customerId, Integer solutionId, Integer pageNumber, Integer pageLimit);

	InstanceInfo getInstanceInfoById(Integer instanceInfoId);
}
