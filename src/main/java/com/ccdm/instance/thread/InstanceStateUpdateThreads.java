package com.ccdm.instance.thread;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.constants.EInstanceSystemState;
import com.ccdm.instance.constants.InstanceConstant;
import com.ccdm.instance.jdbcProcessor.InstanceJdbcProcessor;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.service.IProviderService;

@Component
public class InstanceStateUpdateThreads 
{
	private static final Logger logger = LoggerFactory.getLogger(InstanceStateUpdateThreads.class);
	
	@Autowired
	private InstanceJdbcProcessor instanceJdbcProcessor;
	@Autowired
	@Qualifier("awsProvider")
	private IProviderService providerService;
	
	public boolean updateInstanceState(InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) throws CCDMException
	{
		boolean updateFlag = false;
		try
		{
			logger.info("Updating Instance State started...");
			InstanceStatusResponse response =  providerService.describe(instanceInfo, serverCredentialVo);
			if(response.getInstanceInfo().getInstanceState().equalsIgnoreCase(EInstanceSystemState.STOPPED.getInstanceState()) 
					|| response.getInstanceInfo().getInstanceState().equalsIgnoreCase(EInstanceSystemState.TERMINATED.getInstanceState()) 
					|| response.getInstanceInfo().getInstanceState().equalsIgnoreCase(EInstanceSystemState.RUNNING.getInstanceState()))
			{
				instanceJdbcProcessor.updateInstanceState(response.getInstanceInfo());
				updateFlag = true;
				logger.info("Updating Instance State Thread Completed...");
			}
		}
		catch(Exception e) 
		{
			logger.info("Instance State Cannot be Update.");
			logger.error(e.getMessage());
			return true;
		}
		return updateFlag;
	}

	public void initiateInstancStateUpdateThread(InstanceInfo instanceInfo, ServerCredentialVo serverCredentialVo) 
	{
		 new Thread(new Runnable() 
		 {
				@Override
				public void run() 
				{
					try 
					{
						boolean updateFlag = true;
						long initialTimeout = Calendar.getInstance().getTimeInMillis();
						long timeDiff = 0;
						
						logger.info("Updating Instance State Thread Initiated...");
						while(updateFlag && (timeDiff < InstanceConstant.MAX_INSTANCE_UPDATE_TIMEOUT))
						{
							logger.info("Updating Instance State will start within 20 sec...");
							Thread.sleep(20000);
							updateFlag = !updateInstanceState(instanceInfo, serverCredentialVo);
							//saveOrUpdateSolutionHistory(lbtHistoryReq, ESolutionState.COMPLETED.getSolutionState());
							long currentTimeout = Calendar.getInstance().getTimeInMillis();
							timeDiff = (currentTimeout - initialTimeout);
						}
					} 
					catch (Exception e) {
						logger.error(e.getMessage());
					}
				}
		 }).start();
	}
}
