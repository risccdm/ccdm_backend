package com.ccdm.instance.jdbcProcessor;

import org.springframework.stereotype.Component;

@Component
public class InstanceJdbcQueries 
{
	public static final String INSTANCE_STATE_PARAM = "<INSTANCE_STATE_PARAM>";
	public static final String INSTANCE_ID_PARAM = "<INSTANCE_ID_PARAM>";
	public static final String PUBLIC_DNS_PARAM = "<PUBLIC_DNS_PARAM>";
	public static final String PUBLIC_IPV4 = "<PUBLIC_IPV4>";
	
	public final String UPDATE_INSTANCE_STATE_BY_INSTANCE_ID = "UPDATE instance_info SET INSTANCE_STATE = "+INSTANCE_STATE_PARAM+" "
																+ ", PUBLIC_DNS = "+PUBLIC_DNS_PARAM+" "
																+ ", PUBLIC_IPv4 = "+PUBLIC_IPV4+" "
																+ "WHERE INSTANCE_ID = "+INSTANCE_ID_PARAM;
}
