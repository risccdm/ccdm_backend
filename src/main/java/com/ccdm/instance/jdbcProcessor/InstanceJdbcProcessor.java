package com.ccdm.instance.jdbcProcessor;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.jdbc.connection.JdbcConnnectionProvider;
import com.ccdm.vo.InstanceInfo;

@Component
public class InstanceJdbcProcessor 
{
	private static final Logger logger = LoggerFactory.getLogger(InstanceJdbcProcessor.class);
	
	@Autowired
	private JdbcConnnectionProvider jdbcConnnectionProvider;
	@Autowired
	private InstanceJdbcQueries instanceJdbcQueries;
	
	public void updateInstanceState(InstanceInfo instanceInfo) throws CCDMException
	{
		Connection connection = null;
		Statement stmt = null;
		try
		{
			connection = jdbcConnnectionProvider.getJdbcConnection();
			stmt = connection.createStatement();

			// Let us update state of the Instance;
			int rows = stmt.executeUpdate(updateQueryForInstanceInfo(instanceInfo));
			if(rows > 0)
			{
				logger.info("Instance State Successfully Updated..!");
			}
			else
			{
				logger.info("No Update ..! InstanceId : "+instanceInfo.getTagName());
			}
		}
		catch(Exception e)
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
		finally
		{
			closeJdbcConnection(connection);
		}
	}

	private void closeJdbcConnection(Connection connection) throws CCDMException 
	{
		try 
		{
			if(!connection.isClosed()) 
			{
				connection.close();
				logger.info("JDBC connection Closed.!");
			}
		} 
		catch (SQLException e) 
		{
			logger.error("Exception Occur while Closing JDBC Connection After InstanceInfo Saved!!!");
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		}
	}

	private String updateQueryForInstanceInfo(InstanceInfo instanceInfo) 
	{
		String query = instanceJdbcQueries.UPDATE_INSTANCE_STATE_BY_INSTANCE_ID
						   .replaceAll(InstanceJdbcQueries.INSTANCE_STATE_PARAM, "'"+instanceInfo.getInstanceState()+"'")
						   .replaceAll(InstanceJdbcQueries.INSTANCE_ID_PARAM, "'"+instanceInfo.getInstanceId()+"'")
						   .replaceAll(InstanceJdbcQueries.PUBLIC_DNS_PARAM, instanceInfo.getPublicDns() == null ? "''" : "'"+instanceInfo.getPublicDns()+"'")
						   .replaceAll(InstanceJdbcQueries.PUBLIC_IPV4, instanceInfo.getPublicIpv4() == null ? "''" : "'"+instanceInfo.getPublicIpv4()+"'");
		logger.info("update query : "+query);
		return query;
	}
}
