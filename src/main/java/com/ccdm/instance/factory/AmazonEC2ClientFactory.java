package com.ccdm.instance.factory;

import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;

@Component
public class AmazonEC2ClientFactory
{
	private AmazonEC2 ec2;
	private AWSCredentials credentials = null;
	
	public AmazonEC2 getAmazonEC2(String accessId, String accessKey)
	{
		try
		{
			credentials = new BasicAWSCredentials(accessId, accessKey);
		}
		catch (Exception e) 
        {
            throw new AmazonClientException("Please make sure that your credentials is at the correct ", e);
        }
		
		ec2 = new AmazonEC2Client(credentials);
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		ec2.setRegion(usWest2);
		return ec2;
	}
}
