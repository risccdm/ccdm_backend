package com.ccdm.instance.factory;

import org.springframework.stereotype.Component;

import com.ccdm.exceptions.CCDMException;
import com.google.cloud.compute.Compute;
import com.google.cloud.compute.ComputeOptions;

@Component
public class GoogleCloudGCEClientFactory {
	
	//environment variable 'GOOGLE_APPLICATION_CREDENTIALS' to get the InstanceService 
	//export GOOGLE_APPLICATION_CREDENTIALS=/path/to/my/key.json
	public Compute getGoogleCloudGCECompute() throws CCDMException {
		try
		{	
			Compute compute = ComputeOptions.getDefaultInstance().getService();
			return compute;
		}
		catch (Exception e) 
        {
            throw new CCDMException("Please make sure that Google Cloud Credential is set to Environment Path");
        }
	}
	
}
