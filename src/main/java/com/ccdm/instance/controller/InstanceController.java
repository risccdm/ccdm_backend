package com.ccdm.instance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.instance.service.InstanceService;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;

@RestController
@RequestMapping(value = "/instance")
public class InstanceController extends BaseController 
{
	@Autowired
	private InstanceService instanceService;

	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public CCDMResponse getInstancesInfo(@RequestParam(value = "solutionId") Integer solutionId, @RequestParam(value = "customerId") Integer customerId) throws CCDMException
	{
		List<InstanceInfo> instanceInfoList =  instanceService.getInstanceList(getLoggedInClientId(), solutionId, customerId);
		return getCCDMResponse(instanceInfoList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/instanceInfo/{instanceId}")
	public CCDMResponse getInstanceInfoByInstanceId(@PathVariable(value = "instanceId") String instanceId) throws CCDMException
	{
		InstanceInfo instanceInfo = instanceService.getInstanceInfoByInstanceId(instanceId);
		return getCCDMResponse(instanceInfo, HttpStatus.OK);
	}
	
	public InstanceStatusResponse describeInstances(Integer instanceInfoId) throws CCDMException
	{
		InstanceStatusResponse instanceStatusResponse = instanceService.describeInstances(instanceInfoId);
		return instanceStatusResponse;
	}
	
	@RequestMapping(value = "/describe/update", method = RequestMethod.POST)
	public CCDMResponse describeAndUpdateAllInstanceState() throws CCDMException
	{
		instanceService.describeAndUpdateAllInstanceState(getLoggedInClientId());
		return getCCDMResponse(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/start/{instanceInfoId}", method = RequestMethod.POST)
	public CCDMResponse startInstance(@PathVariable(value = "instanceInfoId") Integer instanceInfoId) throws CCDMException
	{
		InstanceStartResponse instanceStartResponse = instanceService.startInstanceByInstanceId(instanceInfoId);
		return getCCDMResponse(instanceStartResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/stop/{instanceInfoId}", method = RequestMethod.POST)
	public CCDMResponse stopInstance(@PathVariable(value = "instanceInfoId") Integer instanceInfoId) throws CCDMException
	{
		InstanceStopResponse instanceStopResponse = instanceService.stopInstanceByInstanceId(instanceInfoId);
		return getCCDMResponse(instanceStopResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/reboot/{instanceInfoId}", method = RequestMethod.POST)
	public CCDMResponse rebootInstance(@PathVariable(value = "instanceInfoId") Integer instanceInfoId) throws CCDMException
	{
		InstanceRebootResponse instanceRebootResponse = instanceService.rebootInstanceByInstanceId(instanceInfoId);
		return getCCDMResponse(instanceRebootResponse, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/terminate/{instanceInfoId}", method = RequestMethod.POST)
	public CCDMResponse terminateInstance(@PathVariable(value = "instanceInfoId") Integer instanceInfoId) throws CCDMException
	{
		InstanceTerminateResponse instanceTerminateResponse = instanceService.terminateInstanceByInstanceId(instanceInfoId);
		return getCCDMResponse(instanceTerminateResponse, HttpStatus.OK);
	}
	
	public List<InstanceInfo> getInstanceInfoByInstanceIdList(List<String> instanceIdList) throws CCDMException
	{
		return instanceService.getInstanceInfoByInstanceIdList(instanceIdList);
	}

	public List<InstanceInfo> getInstanceInfoResult(AppUser appUser, Integer customerId, Integer solutionId,
			Integer pageNumber, Integer pageLimit) throws CCDMException {
		return instanceService.getInstanceInfo(appUser, customerId, solutionId, pageNumber, pageLimit);
	}
	
	public InstanceInfo getInstanceInfoById(Integer instanceInfoId) {
		return instanceService.getInstanceInfoById(instanceInfoId);
	}
}
