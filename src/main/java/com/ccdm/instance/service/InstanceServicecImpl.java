package com.ccdm.instance.service;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.constants.EInstanceSystemState;
import com.ccdm.instance.dao.InstanceDAO;
import com.ccdm.instance.service.helper.vo.InstanceHelperVO;
import com.ccdm.instance.thread.InstanceStateUpdateThreads;
import com.ccdm.server.credential.controller.ServerCredentialController;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.vo.ServerCredential;
import com.ccdm.vo.WebServerProvider;
import com.ccdm.webprovider.factory.WebServerProviderFactory;
import com.ccdm.webprovider.helper.AWSInstanceHelper;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;

@Service
@Transactional(readOnly = false)
public class InstanceServicecImpl implements InstanceService
{
	private static final Logger logger = LoggerFactory.getLogger(InstanceServicecImpl.class);

	@Autowired
	private InstanceDAO instanceDao;
	@Autowired
	private WebServerProviderFactory webServerProviderFactory;
	@Autowired
	private ServerCredentialController serverCredentialController;

	@Override
	public InstanceStatusResponse describeInstances(Integer instanceInfoId) throws CCDMException  
	{
		InstanceInfo instanceInfo = getInstanceInfoById(instanceInfoId, EInstanceSystemState.RUNNING.getInstanceState());
		InstanceStatusResponse instanceStatusResponse = webServerProviderFactory.getWebProviderByProviderName(instanceInfo.getProviderPlatformReference().getWebServerProvider().getAliasName())
																			.describe(instanceInfo, getServerCredentialVo(instanceInfo.getProviderPlatformReference().getWebServerProvider()));
		updateInstanceStatus(instanceStatusResponse.getInstanceInfo().getInstanceState(), instanceInfo);
		return instanceStatusResponse;
	}

	@Override
	public List<InstanceInfo> getInstanceInfoByInstanceIdList(List<String> instanceIdList)
	{
		return instanceDao.getInstanceInfoByInstanceIdList(instanceIdList);
	}

	@Override
	public List<InstanceStatusResponse> describeAndUpdateAllInstanceState(Integer clientId) throws CCDMException 
	{
		try
		{
			List<InstanceInfo> instanceInfoList = instanceDao.getInstances(clientId);
			
			InstanceInfo instanceInfo  = instanceInfoList.get(0);
			
			// By Default AWS as of now
			List<InstanceStatusResponse> instanceStatusResponseList = webServerProviderFactory.getWebProviderByProviderName(instanceInfoList.get(0).getProviderPlatformReference().getWebServerProvider()
																			.getAliasName()).describeAllInstance(instanceInfo);
			
			for(InstanceStatusResponse instanceStatusFromProvider : instanceStatusResponseList)
			{
				updateInstanceStatus(instanceStatusFromProvider);
			}
			updateRemainingInstanceStatus(instanceStatusResponseList, instanceInfoList);
			return instanceStatusResponseList;
		}
		catch(Exception e) 
		{
			logger.info(e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_CHECK_INSTANCE_STATUS);
		}
	}

	private void updateRemainingInstanceStatus(List<InstanceStatusResponse> instanceStatusResponseList, List<InstanceInfo> instanceInfoList) throws CCDMException 
	{
		if(instanceInfoList != null && instanceInfoList.size() > 0)
		{
			for(InstanceInfo instanceInfo : instanceInfoList)
			{
				if(!instanceStatusResponseList.stream().filter(o -> o.getInstanceInfo().getInstanceId().equalsIgnoreCase(instanceInfo.getInstanceId())).findFirst().isPresent())
				{
					InstanceStatusResponse instanceStatus = new InstanceStatusResponse();
					instanceInfo.setInstanceState(EInstanceSystemState.TERMINATED.getInstanceState());
					instanceInfo.setPublicDns("");
					instanceInfo.setPublicIpv4("");
					instanceStatus.setInstanceInfo(instanceInfo);
					updateInstanceStatus(instanceStatus);
				}
			}
		}
		logger.info("Some Instance Not available in Provider., There are marked as Terminated..!");
	}

	@Override
	public InstanceInfo getInstanceInfoByInstanceId(String instanceId) throws CCDMException 
	{
		CommonValidator.validateString(instanceId, ErrorCodes.INVALID_INSTANCE_ID);
		return instanceDao.getInstanceInfoByInstanceId(instanceId);
	}
	
	@Override
	public List<InstanceHelperVO> getInstances(Integer userId) 
	{
		List<InstanceInfo> serverInstanaceInfoList = instanceDao.getInstances(userId);
		List<InstanceHelperVO> instanceHelperVOList = buildInstanceHelperVoList(serverInstanaceInfoList);
		return instanceHelperVOList;
	}
	
	@Override
	public void updateInstanceStatus(InstanceStatusResponse instanceStatus) throws CCDMException 
	{
		try
		{
			if(instanceStatus != null)
			{
				InstanceInfo instanceInfo = instanceDao.getInstanceInfoByInstanceId(instanceStatus.getInstanceInfo().getInstanceId());
				if(instanceInfo != null)
				{
					instanceInfo.setInstanceState(instanceStatus.getInstanceInfo().getInstanceState());
					instanceDao.save(instanceInfo);
					logger.info(instanceInfo.getTagName()+" State Updated to "+instanceInfo.getInstanceState());
					logger.info("LoadBalancer Updated Sussfully");
				}
				else {
					logger.info("Instance(id = "+instanceStatus.getInstanceInfo().getInstanceId()+") Not Available to Update their State.!");
				}
			}
		}
		catch(Exception e) {
			throw new CCDMException(ErrorCodes.CANNOT_UPDATE_INSTANCE_STATE);
		}
	}
	
	@Override
	public List<InstanceInfo> getInstanceList(Integer clientId, Integer solutionId, Integer customerId) throws CCDMException 
	{
		try
		{
			if(solutionId == null || solutionId < 0)
			{
				throw new CCDMException(ErrorCodes.INVALID_SOLUTION_ID);
			}
			if(customerId == null || customerId < 0)
			{
				throw new CCDMException(ErrorCodes.INVALID_CUSTOMER_ID);
			}
			
			List<InstanceInfo> instanceInfoList = null;
			if(solutionId == 0 && customerId == 0)
			{
				instanceInfoList = instanceDao.getInstanceInfoByAllSolutionIdAndByAllCustomerId(clientId);
			}
			else if(solutionId > 0 && customerId > 0)	// both not option 'All'
			{
				instanceInfoList = instanceDao.getInstanceInfoBySolutionIdAndByCustomerId(clientId, solutionId, customerId);
			}
			else if(solutionId == 0 && customerId > 0)
			{
				instanceInfoList = instanceDao.getInstanceInfoByAllSolutionIdAndByCustomerId(clientId, customerId);
			}
			else if(solutionId != 0 && customerId == 0)
			{
				instanceInfoList = instanceDao.getInstanceInfoBySolutionIdAndByAllCustomerId(clientId, solutionId);
			}
			return instanceInfoList;
		}
		catch(Exception e) {
			throw new CCDMException(ErrorCodes.INVALID_INSTANCE_LIST);
		}
	}
	
	@Override
	public InstanceStartResponse startInstanceByInstanceId(Integer instanceInfoId) throws CCDMException 
	{
		try
		{
			logger.info("Starting Instance Initiated...");
			InstanceInfo instanceInfo = getInstanceInfoById(instanceInfoId, EInstanceSystemState.RUNNING.getInstanceState());
			InstanceStartResponse instanceStartResponse = webServerProviderFactory.getWebProviderByProviderName(instanceInfo.getProviderPlatformReference().getWebServerProvider().getAliasName())
																				.start(instanceInfo);
			updateInstanceStatus(instanceStartResponse.getCurrnetState(), instanceInfo);
			logger.info("Instance Started...");
			return instanceStartResponse;
		}
		catch(Exception e) 
		{
			logger.error("Failed to Start Instance -------> "+e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_START_INSTANCE);
		}
	}
	
	@Override
	public InstanceStopResponse stopInstanceByInstanceId(Integer instanceInfoId) throws CCDMException
	{
		try
		{
			logger.info("Stopping Instance Initiated...");
			InstanceInfo instanceInfo = getInstanceInfoById(instanceInfoId, EInstanceSystemState.STOPPING.getInstanceState());
			InstanceStopResponse instanceStopResponse = webServerProviderFactory.getWebProviderByProviderName(instanceInfo.getProviderPlatformReference().getWebServerProvider().getAliasName())
																				.stop(instanceInfo);
			updateInstanceStatus(instanceStopResponse.getCurrnetState(), instanceInfo);
			logger.info("Instance Stopped...");
			return instanceStopResponse;
		}
		catch(Exception e) 
		{
			logger.error("Failed to Stop Instance -------> "+e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_STOP_INSTANCE);
		}
	}
	
	@Override
	public InstanceRebootResponse rebootInstanceByInstanceId(Integer instanceInfoId) throws CCDMException
	{
		try
		{
			logger.info("Rebooting Instance Initiated...");
			InstanceInfo instanceInfo = getInstanceInfoById(instanceInfoId, EInstanceSystemState.REBOOTING.getInstanceState());
			InstanceRebootResponse rebootResponse = webServerProviderFactory.getWebProviderByProviderName(instanceInfo.getProviderPlatformReference().getWebServerProvider().getAliasName())
																		.reboot(instanceInfo);
			updateInstanceStatus(EInstanceSystemState.REBOOTING.getInstanceState(), instanceInfo);
			logger.info("Instance Restarted...");
			return rebootResponse;
		}
		catch(Exception e) 
		{
			logger.error("Failed to Reboot Instance -------> "+e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_REBOOT_INSTANCE);
		}
	}
	
	@Override
	public InstanceTerminateResponse terminateInstanceByInstanceId(Integer instanceId) throws CCDMException
	{
		try
		{
			logger.info("Terminating Instance Initiated...");
			InstanceInfo instanceInfo = getInstanceInfoById(instanceId, EInstanceSystemState.SHUTTING_DOWN.getInstanceState());
			InstanceTerminateResponse terminateResponse = webServerProviderFactory.getWebProviderByProviderName(instanceInfo.getProviderPlatformReference().getWebServerProvider().getAliasName())
																.terminate(instanceInfo);
			updateInstanceStatus(terminateResponse.getCurrnetState(), instanceInfo);
			logger.info("Instance Terminated...");
			return terminateResponse;
		}
		catch(Exception e) 
		{
			logger.error("Failed to Terminate Instance -------> "+e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_TERMINATE_INSTANCE);
		}
	}

	@Override
	public List<InstanceInfo> getInstanceInfo(AppUser appUser, Integer customerId, Integer solutionId,
			Integer pageNumber, Integer pageLimit) throws CCDMException {
		
		return instanceDao.getInstanceInfo(appUser, customerId, solutionId, pageNumber, pageLimit);
	}
	
	/* ************************ Private Methods ****************** */
	
	private ServerCredentialVo getServerCredentialVo(WebServerProvider webServerProvider)  throws CCDMException 
	{
		ServerCredential credential =  serverCredentialController.getServerCredential(webServerProvider.getId());
		return new ServerCredentialVo(credential.getUser(), credential.getPemKey(), credential.getAccessId(), credential.getAccessKey(), webServerProvider.getAliasName());
	}
	
	private List<InstanceHelperVO> buildInstanceHelperVoList(List<InstanceInfo> serverInstanaceInfoList)
	{
		List<InstanceHelperVO> instanceHelperVOList = new ArrayList<InstanceHelperVO>();
		
		for (InstanceInfo serverInstanaceInfo : serverInstanaceInfoList ) 
		{
			InstanceHelperVO instanceHelperVo = new InstanceHelperVO(); 
			instanceHelperVo.setId(serverInstanaceInfo.getId());
			instanceHelperVo.setName(serverInstanaceInfo.getTagName());
			instanceHelperVo.setInstanceId(serverInstanaceInfo.getInstanceId());
			instanceHelperVo.setPublicDns(serverInstanaceInfo.getPublicDns());
			instanceHelperVOList.add(instanceHelperVo);
		}
		return instanceHelperVOList;
	}

	private InstanceInfo updateInstanceStatus(String instanceState, InstanceInfo instanceInfo) throws CCDMException {
		instanceInfo.setInstanceState(instanceState);
		instanceDao.save(instanceInfo);
		return instanceInfo;
	}

	private InstanceInfo getInstanceInfoById(Integer instanceInfoId, String newState) throws CCDMException 
	{
		CommonValidator.validateId(instanceInfoId, ErrorCodes.INVALID_ISNTANCE_INFO_ID);
		InstanceInfo instaneInfo = instanceDao.getInstanceInfoById(instanceInfoId);
		if(instaneInfo == null)
		{
			throw new CCDMException(ErrorCodes.INVALID_INSTANCE);
		}
		validateInstanceByState(instaneInfo, newState);
		return instaneInfo;
	}
	
	private void validateInstanceByState(InstanceInfo instaneInfo, String newState) throws CCDMException 
	{
		String currentState = instaneInfo.getInstanceState();
		if(newState.equalsIgnoreCase(EInstanceSystemState.RUNNING.getInstanceState()))
		{
			if(!currentState.equalsIgnoreCase(EInstanceSystemState.STOPPED.getInstanceState()))
			{
				throw new CCDMException(ErrorCodes.INVALID_STATE_MOVE+" from "+currentState+" to "+newState);
			}
		}
		
		if(newState.equalsIgnoreCase(EInstanceSystemState.STOPPING.getInstanceState()))
		{
			if(!currentState.equalsIgnoreCase(EInstanceSystemState.RUNNING.getInstanceState()))
			{
				throw new CCDMException(ErrorCodes.INVALID_STATE_MOVE+" from "+currentState+" to "+newState);
			}
		}
		
		if(newState.equalsIgnoreCase(EInstanceSystemState.REBOOTING.getInstanceState()))
		{
			if(!currentState.equalsIgnoreCase(EInstanceSystemState.RUNNING.getInstanceState()))
			{
				throw new CCDMException(ErrorCodes.INVALID_STATE_MOVE+" from "+currentState+" to "+newState);
			}
		}
		
		if(newState.equalsIgnoreCase(EInstanceSystemState.SHUTTING_DOWN.getInstanceState()))
		{
			if(!currentState.equalsIgnoreCase(EInstanceSystemState.RUNNING.getInstanceState()) 
					&& !currentState.equalsIgnoreCase(EInstanceSystemState.STOPPED.getInstanceState()))
			{
				throw new CCDMException(ErrorCodes.INVALID_STATE_MOVE+" from "+currentState+" to "+newState);
			}
		}
	}

	@Override
	public InstanceInfo getInstanceInfoById(Integer instanceInfoId) {
		InstanceInfo instanceInfo = instanceDao.getInstanceInfoById(instanceInfoId);
		return instanceInfo;
	}
}
