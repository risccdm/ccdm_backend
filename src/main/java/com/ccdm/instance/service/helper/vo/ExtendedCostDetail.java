package com.ccdm.instance.service.helper.vo;

public class ExtendedCostDetail {

	private String monthAndYear;
	private Integer noOfHours;
	private Integer noOfDays;
	private Double costValue;
	
	public ExtendedCostDetail() {}
	
	public ExtendedCostDetail(String monthAndYear, Double costValue, Integer noOfHours, Integer noOfDays) {
		this.monthAndYear = monthAndYear;
		this.costValue = costValue;
		this.noOfDays = noOfDays;
		this.noOfHours = noOfHours;
	}
	
	public String getMonthAndYear() {
		return monthAndYear;
	}
	public void setMonthAndYear(String monthAndYear) {
		this.monthAndYear = monthAndYear;
	}
	public Double getCostValue() {
		return costValue;
	}
	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}

	public Integer getNoOfHours() {
		return noOfHours;
	}

	public void setNoOfHours(Integer noOfHours) {
		this.noOfHours = noOfHours;
	}

	public Integer getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}
	
}
