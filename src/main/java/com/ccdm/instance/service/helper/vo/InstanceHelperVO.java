package com.ccdm.instance.service.helper.vo;

public class InstanceHelperVO 
{
	private Integer id;
	private String name;
	private String instanceId;
	private String publicDns;
	  
    public Integer getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getPublicDns() {
		return publicDns;
	}
	public void setPublicDns(String publicDns) {
		this.publicDns = publicDns;
	}
   
}
