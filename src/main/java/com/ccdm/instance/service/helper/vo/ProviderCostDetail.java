package com.ccdm.instance.service.helper.vo;

import java.util.Date;

public class ProviderCostDetail {

	private String instanceName;
	private Integer instanceInfoId;
	private Long totalHours;
	private String providerName;
	private Date createdTime;
	private Double costValue;
	
	public ProviderCostDetail() {
	}
	
	public ProviderCostDetail(Integer instanceInfoId, String instanceName, Long totalHours, String providerName, Date createdTime, Double costValue) {
		this.instanceInfoId = instanceInfoId;
		this.instanceName = instanceName;
		this.totalHours = totalHours;
		this.providerName = providerName;
		this.createdTime = createdTime;
		this.costValue = costValue;
	}
	
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public Long getTotalHours() {
		return totalHours;
	}
	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public Date getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}
	public Double getCostValue() {
		return costValue;
	}
	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}

	public Integer getInstanceInfoId() {
		return instanceInfoId;
	}

	public void setInstanceInfoId(Integer instanceInfoId) {
		this.instanceInfoId = instanceInfoId;
	}
	
}
