package com.ccdm.instance.service.helper.vo;

import java.util.Date;

public class InstanceSearchResult
{
   private String instanceName;
   private String templateName;
   private Date launchDate;
   private double providerCost;
   
    public String getInstanceName() {
	return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Date getLaunchDate() {
		return launchDate;
	}
	public void setLaunchDate(Date launchDate) {
		this.launchDate = launchDate;
	}
	public double getProviderCost() {
		return providerCost;
	}
	public void setProviderCost(double providerCost) {
		this.providerCost = providerCost;
	}
   
}
