package com.ccdm.instance.service.helper.vo;

import java.util.List;

public class InstanceSearchResultWrapper 
{
    private List<InstanceSearchResult> instanceSearchResults;
    private double totCost;
  
    public List<InstanceSearchResult> getInstanceSearchResults() {
	return instanceSearchResults;
	}
	public void setInstanceSearchResults(List<InstanceSearchResult> instanceSearchResults) {
		this.instanceSearchResults = instanceSearchResults;
	}
	public double getTotCost() {
		return totCost;
	}
	public void setTotCost(double totCost) {
		this.totCost = totCost;
	}

}
