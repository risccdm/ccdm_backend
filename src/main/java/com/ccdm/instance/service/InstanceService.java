package com.ccdm.instance.service;

import java.util.List;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.service.helper.vo.InstanceHelperVO;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;

public interface InstanceService 
{
	InstanceStatusResponse describeInstances(Integer instanceInfoId) throws CCDMException;
	
	List<InstanceHelperVO> getInstances(Integer userId) throws CCDMException;
	
	List<InstanceInfo> getInstanceInfoByInstanceIdList(List<String> instanceIdList) throws CCDMException;

	List<InstanceInfo> getInstanceList(Integer clientId, Integer solutionId, Integer customerId) throws CCDMException;

	InstanceInfo getInstanceInfoByInstanceId(String instanceId) throws CCDMException;
	
	InstanceStartResponse startInstanceByInstanceId(Integer instanceInfoId) throws CCDMException;

	InstanceStopResponse stopInstanceByInstanceId(Integer instanceInfoId) throws CCDMException;

	InstanceRebootResponse rebootInstanceByInstanceId(Integer instanceInfoId) throws CCDMException;

	InstanceTerminateResponse terminateInstanceByInstanceId(Integer instanceInfoId) throws CCDMException;

	List<InstanceStatusResponse> describeAndUpdateAllInstanceState(Integer clientId) throws CCDMException;

	List<InstanceInfo> getInstanceInfo(AppUser appUser, Integer customerId, Integer solutionId, Integer pageNumber, Integer pageLimit) throws CCDMException;

	InstanceInfo getInstanceInfoById(Integer instanceInfoId);

	void updateInstanceStatus(InstanceStatusResponse instanceStatus) throws CCDMException;
}
