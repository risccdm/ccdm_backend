package com.ccdm.instance.constants;

public class InstanceConstant 
{
	public static final String PEM_FILE_PATH 					= "./src/pem/";
	public static final Integer INSTANCE_DEFAULT_PORT_NUMBER 	= 22;
	
	public static final String SECURITY_GROUPS 			= "launch-wizard-1";
	public static final String APPLICATION_INSTANCE_ID 	= "i-02319bea1b30d7328";	// Testing
	
	public static final String TAG_NAME_KEY = "Name";
	
	public static final long MAX_INSTANCE_UPDATE_TIMEOUT = 102000;
}
