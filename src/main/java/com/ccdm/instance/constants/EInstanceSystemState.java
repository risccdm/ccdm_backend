package com.ccdm.instance.constants;

public enum EInstanceSystemState 
{
	PENDING("pending"),RUNNING("running"),
	REBOOTING("rebooting"),
	STOPPING("stopping"),STOPPED("stopped"),
	SHUTTING_DOWN("shutting-down"),TERMINATED("terminated");
	
	private String instanceState;
	
	private EInstanceSystemState(String instanceState) {
		this.instanceState = instanceState;
	}
	
	public String getInstanceState() {
		return this.instanceState;
	}
}
