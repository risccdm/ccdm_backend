package com.ccdm.instance.validator;

import org.springframework.stereotype.Component;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.elastic.solutionhistory.model.AgentMasterConfigRequestVo;
import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.vo.ServerCredential;
import com.ccdm.vo.ViewModel;

@Component
public class InstanceValidator 
{
	public void validateViewModel(ViewModel viewModel) throws CCDMException 
	{
		CommonValidator.validateObject(viewModel, ErrorCodes.INVALID_SOLUTION_REQUEST);
	}

	public static void validateServerCredential(ServerCredential serverCredential) throws CCDMException 
	{
		CommonValidator.validateObject(serverCredential, ErrorCodes.INVALID_SERVER_CREDENTIAL_OBJECT);
		CommonValidator.validateString(serverCredential.getAccessId(), ErrorCodes.INVALID_ACCESS_ID);
		CommonValidator.validateString(serverCredential.getAccessKey(), ErrorCodes.INVALID_ACCESS_KEY);
		CommonValidator.validateString(serverCredential.getPemKey(), ErrorCodes.INVALID_PRIVATE_KEY);
		CommonValidator.validateString(serverCredential.getUser(), ErrorCodes.INVALID_CREDENTIAL_USER_NAME);
	}

	public static void validateInstance(ClientInstanceVo instanceInfoVo) throws CCDMException 
	{
		CommonValidator.validateObject(instanceInfoVo, ErrorCodes.INVALID_INSTANCE);
		CommonValidator.validateString(instanceInfoVo.getInstanceName(), ErrorCodes.INVALID_INSTANCE_TAG_NAME);
		CommonValidator.validateString(instanceInfoVo.getInstanceId(), ErrorCodes.INVALID_INSTANCE_ID);
		CommonValidator.validateString(instanceInfoVo.getKeyName(), ErrorCodes.INVALID_INSTANCE_KEY);
		CommonValidator.validateString(instanceInfoVo.getPublicDns(), ErrorCodes.INVALID_INSTANCE_PUBLIC_DNS);
		CommonValidator.validateString(instanceInfoVo.getPublicIpv4(), ErrorCodes.INVALID_INSTANCE_PUBLIC_IP4);
	}

	public static void validateServerCredentialVo(ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		CommonValidator.validateObject(serverCredentialVo, ErrorCodes.INVALID_SERVER_CREDENTIAL_OBJECT);
		CommonValidator.validateString(serverCredentialVo.getAccessId(), ErrorCodes.INVALID_ACCESS_ID);
		CommonValidator.validateString(serverCredentialVo.getAccessKey(), ErrorCodes.INVALID_ACCESS_KEY);
		CommonValidator.validateString(serverCredentialVo.getPemKey(), ErrorCodes.INVALID_PRIVATE_KEY);
		CommonValidator.validateString(serverCredentialVo.getUser(), ErrorCodes.INVALID_CREDENTIAL_USER_NAME);
	}

	public static void validateMasterAgentRequest(AgentMasterConfigRequestVo masterAgentConfigRequest) throws CCDMException 
	{
		CommonValidator.validateObject(masterAgentConfigRequest, ErrorCodes.INVALID_MASTER_AGENT_REQ);
		validateInstance(masterAgentConfigRequest.getClientInstanceVo());
		validateInstance(masterAgentConfigRequest.getMasterInstanceVo());
	}

	public static void validateClientAgentRequest(AgentClientRequestVo clientAgentRequest) throws CCDMException 
	{
		CommonValidator.validateObject(clientAgentRequest, ErrorCodes.INVALID_CLIENT_AGENT_REQ);
		validateInstance(clientAgentRequest.getClientInstanceVo());
		CommonValidator.validateString(clientAgentRequest.getTicketSalt(), ErrorCodes.INVALID_TICKET_SALT);
		CommonValidator.validateString(clientAgentRequest.getUploadFilePath(), ErrorCodes.INVALID_UPLOAD_FILE_PATH);
	}

}
