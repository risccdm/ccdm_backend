package com.ccdm.instance.biz;

import org.springframework.stereotype.Component;

import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.GCEException;
import com.ccdm.instance.constants.EInstanceSystemState;
import com.ccdm.webprovider.response.InstanceCreateResponse;
import com.ccdm.webprovider.response.InstanceRebootResponse;
import com.ccdm.webprovider.response.InstanceStartResponse;
import com.ccdm.webprovider.response.InstanceStatusResponse;
import com.ccdm.webprovider.response.InstanceStopResponse;
import com.ccdm.webprovider.response.InstanceTerminateResponse;
import com.google.cloud.compute.Instance;
import com.google.cloud.compute.InstanceInfo;
import com.google.cloud.compute.Operation;

@Component
public class GCEInstanceResponseAdapter {

	public Object buildResponse(Object actualResponse, Class<?> responseClass) throws GCEException {
		
		if(actualResponse instanceof  Operation) {
			Object response = convertToRequiredResponse((Operation) actualResponse, responseClass);
			if(response instanceof GCEException) {
				throw (GCEException) response;
			}
			return response;
		}
		
		if(actualResponse instanceof Instance) {
			return buildInstanceStatusResponse( (Instance) actualResponse);
		}
		
		if(actualResponse instanceof InstanceInfo) {
			return buildInstanceCreateResponse((InstanceInfo) actualResponse);
		}
		
		throw new GCEException(ErrorCodes.INVALID_GCE_INSTANCE_REQUEST_CLASS);
	}

	private Object convertToRequiredResponse(Operation operation, Class<?> responseClass) {
		
		if( responseClass == InstanceStartResponse.class ) {
			return buildInstanceStartResponse(operation);
		}
		if ( responseClass == InstanceStopResponse.class ) {
			return buildInstanceStopResponse(operation);
		}
		if (responseClass == InstanceTerminateResponse.class) {
			return buildInstanceTerminateResponse(operation);
		}
		if ( responseClass == InstanceRebootResponse.class) {
			return buildInstanceRebootResponse(operation);
		}
		
		return new GCEException(ErrorCodes.INVALID_INSTANCE_RESPONSE_CLASS);
	}
	
	private Object buildInstanceStartResponse(Operation operation){
		InstanceStartResponse instanceStartResponse = new InstanceStartResponse();
		String status = Operation.Status.DONE.name().equals(operation.getStatus().name()) ? EInstanceSystemState.RUNNING.getInstanceState() : EInstanceSystemState.PENDING.name(); 
		instanceStartResponse.setCurrnetState(status);
		instanceStartResponse.setInstanceId(operation.getTargetId());
		return instanceStartResponse;
	}
	
	private Object buildInstanceStopResponse(Operation operation){
		InstanceStopResponse instanceStopResponse = new InstanceStopResponse();
		String status = Operation.Status.DONE.name().equals(operation.getStatus().name()) ? EInstanceSystemState.STOPPED.getInstanceState() : EInstanceSystemState.STOPPING.name(); 
		instanceStopResponse.setCurrnetState(status);
		instanceStopResponse.setInstanceId(operation.getTargetId());
		return instanceStopResponse;
	}
	
	private Object buildInstanceStatusResponse(Instance instance){
		InstanceStatusResponse instanceStatusResponse = new InstanceStatusResponse();
		return instanceStatusResponse;
	}
	
	private Object buildInstanceTerminateResponse(Operation operation){
		InstanceTerminateResponse instanceTerminateResponse = new InstanceTerminateResponse();
		String status = Operation.Status.DONE.name().equals(operation.getStatus().name()) ? EInstanceSystemState.TERMINATED.getInstanceState() : EInstanceSystemState.SHUTTING_DOWN.getInstanceState();
		instanceTerminateResponse.setCurrnetState(status);
		instanceTerminateResponse.setInstanceId(operation.getTargetId());
		return instanceTerminateResponse;
	}
	
	private Object buildInstanceRebootResponse(Operation operation){
		InstanceRebootResponse instanceRebootResponse = new InstanceRebootResponse();
		String status = Operation.Status.DONE.name().equals(operation.getStatus().name())? EInstanceSystemState.RUNNING.getInstanceState() : EInstanceSystemState.RUNNING.getInstanceState();
		instanceRebootResponse.setCurrentState(status);
		instanceRebootResponse.setInstanceId(operation.getTargetId());
		return instanceRebootResponse;
	}
	
	private Object buildInstanceCreateResponse(InstanceInfo instanceInfo){
		ClientInstanceVo clientInstanceVo = new ClientInstanceVo();
		clientInstanceVo.setInstanceId(instanceInfo.getGeneratedId());
		clientInstanceVo.setInstanceState(instanceInfo.getStatus().name().toLowerCase());
		clientInstanceVo.setInstanceName(instanceInfo.getInstanceId().getInstance());
		clientInstanceVo.setPublicDns(instanceInfo.getNetworkInterfaces().get(0).getAccessConfigurations().get(0).getNatIp());
		clientInstanceVo.setPublicIpv4(instanceInfo.getNetworkInterfaces().get(0).getAccessConfigurations().get(0).getNatIp());
		return new InstanceCreateResponse(clientInstanceVo);
	}
	
	
	
}
