package com.ccdm.validators;

import org.springframework.stereotype.Component;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.solution.model.SolutionRequest;
import com.ccdm.solution.model.VmScriptModel;

@Component
public class SolutionRequestValidator {
	
	public void validateSolutionRequest(SolutionRequest solutionRequest) throws CCDMException{
		
		if(solutionRequest.getSolutionName() == null || solutionRequest.getSolutionName() == ""){
			throw new CCDMException(ErrorCodes.INVALID_SOLUTION_NAME);
		}
		
		if(solutionRequest.getVmScriptModel() == null || solutionRequest.getVmScriptModel().isEmpty()){
			throw new CCDMException(ErrorCodes.VM_SCRIPT_LIST_NULL_OR_EMPTY);
		}
		
		for (VmScriptModel vmScriptModel : solutionRequest.getVmScriptModel()) {
			
			if(vmScriptModel.getVmModel() == null || vmScriptModel.getVmModel().getVmId() == null){
				throw new CCDMException(ErrorCodes.VM_TEMPLATE_ID_NULL);
			}
		}
	}
	
	
}
