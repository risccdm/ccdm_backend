package com.ccdm.validators;

import java.util.List;

import org.springframework.stereotype.Component;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.sla.model.SlaTypeRequest;
import com.ccdm.vo.SlaDetail;

@Component
public class SlaValidator 
{
	public void validateSlaTypeRequest(SlaTypeRequest slaTypeRequest) throws CCDMException 
	{
		CommonValidator.validateObject(slaTypeRequest, ErrorCodes.INVALID_SLA_TYPE_REQUEST);
		CommonValidator.validateString(slaTypeRequest.getSlaTypeName(), ErrorCodes.INVALID_SLA_TYPE_NAME);
		validateSlaDetail(slaTypeRequest.getSlaDetail());
	}

	private void validateSlaDetail(List<SlaDetail> slaDetailList) throws CCDMException 
	{
		CommonValidator.validateList(slaDetailList, ErrorCodes.INVALID_SLA_DETAILS);
		for(SlaDetail slaDetail : slaDetailList)
		{
			CommonValidator.validateObject(slaDetail, ErrorCodes.INVALID_SLA_DETAIL);
			CommonValidator.validateString(slaDetail.getPriorityType(), ErrorCodes.INVALID_SLA_PRIORITY_TYPE);
			CommonValidator.validateId(slaDetail.getResponseTime(), ErrorCodes.INVALID_SLA_RESPONSE_TIME);
			CommonValidator.validateId(slaDetail.getUptime(), ErrorCodes.INVALID_SLA_UP_TIME);
			CommonValidator.validateId(slaDetail.getWorkingHours(), ErrorCodes.INVALID_SLA_WORKING_HOURS);
			CommonValidator.validateId(slaDetail.getCredits(), ErrorCodes.INVALID_SLA_CREDITS);
		}
	}
}
