package com.ccdm.validators;

import org.springframework.stereotype.Component;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.vo.LoadBal;
import com.ccdm.vo.OperatingSystem;
import com.ccdm.vo.ViewModel;
import com.ccdm.vo.VirtualMachine;


@Component
public class TemplateValidator 
{
	public void validateTemplates(ViewModel viewModel) throws CCDMException
	{
		validateSolutionName(viewModel.getName());
		validateOperatingSystem(viewModel.getOperatingSystem());
		validateVirtualMachine(viewModel.getVirtualMachine());
	}
	
	
	
	public void validateSolutionName(String SolutionName) throws CCDMException
	{
		if(SolutionName == null || SolutionName == ""){
			throw new CCDMException(ErrorCodes.SOLUTION_NAME_INVALID);
		}
	}
	
	public void validateOperatingSystem(OperatingSystem operatingSystem) throws CCDMException
	{
		if(operatingSystem == null){
			throw new CCDMException(ErrorCodes.OPERATING_SYSTEM_INVALID);
		}
	}
	
	public void validateVirtualMachine(VirtualMachine virtualMachine) throws CCDMException
	{
		if(virtualMachine == null){
			throw new CCDMException(ErrorCodes.VIRTUAL_MACHINE_INVALID);
		}
	}
}
