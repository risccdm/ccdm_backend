package com.ccdm.elastic.vo;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import com.ccdm.common.constants.ElasticConstants;

@Document(indexName = ElasticConstants.ELASTIC_DB_INDEX, type = ElasticConstants.SCRIPT_OUTPUT_LOG)
public class ScriptOutputLog {

	@Id
	private String id;
	private String status;
	private String scriptOutput;
	private String scriptId;
	private String instanceId;
	private Date executionDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getScriptOutput() {
		return scriptOutput;
	}
	public void setScriptOutput(String scriptOutput) {
		this.scriptOutput = scriptOutput;
	}
	public String getScriptId() {
		return scriptId;
	}
	public void setScriptId(String scriptId) {
		this.scriptId = scriptId;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public Date getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}
	
	
}
