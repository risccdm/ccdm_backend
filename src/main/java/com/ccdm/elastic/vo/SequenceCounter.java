package com.ccdm.elastic.vo;

import org.springframework.data.elasticsearch.annotations.Document;

import com.ccdm.common.constants.ElasticConstants;

@Document(indexName = ElasticConstants.ELASTIC_DB_INDEX, type = ElasticConstants.SEQUENCE_COUNTER)
public class SequenceCounter 
{
	private String id;
	private long sequenceId;
	
	public SequenceCounter() {}
	
	public SequenceCounter(String id, long sequenceId) 
	{
		this.id = id;
		this.sequenceId = sequenceId;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(long sequenceId) {
		this.sequenceId = sequenceId;
	}
}
