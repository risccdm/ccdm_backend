package com.ccdm.elastic.vo;

import java.util.Date;

import org.springframework.data.elasticsearch.annotations.Document;

import com.ccdm.common.constants.ElasticConstants;

@Document(indexName = ElasticConstants.ELASTIC_DB_INDEX, type = ElasticConstants.EXECUTIONSCRIPT)
public class ExecutionScript {

	private String id;
	private Integer userId;
	private Integer scriptType;
	private String executableCommands;
	private String scriptName;
	private Date creationDate = new Date();
	private String fileName;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public Integer getScriptType() {
		return scriptType;
	}
	
	public void setScriptType(Integer scriptType) {
		this.scriptType = scriptType;
	}
	
	public String getExecutableCommands() {
		return executableCommands;
	}
	
	public void setExecutableCommands(String executableCommands) {
		this.executableCommands = executableCommands;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getScriptName() {
		return scriptName;
	}

	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
