package com.ccdm.elastic.vo;

import java.util.Date;

import org.springframework.data.elasticsearch.annotations.Document;

import com.ccdm.common.constants.ElasticConstants;

@Document(indexName = ElasticConstants.ELASTIC_DB_INDEX, type = ElasticConstants.SOLUTION_HISTORY)
public class SolutionHistory {

	private String id;
	private Integer solutionId;
	private String solutionName;
	private String instanceName;
	private String createdFor;
	private String instanceTaskName;
	private Object requestObject;
	private String serverProvider;
	private Object responseObject;
	private String status;
	private String refId;
	private Integer seqNo;
	private String instanceUniqueId;
	private String deployUniqueId;
	private Date deployDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Integer getSolutionId() {
		return solutionId;
	}
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	public String getCreatedFor() {
		return createdFor;
	}
	public void setCreatedFor(String createdFor) {
		this.createdFor = createdFor;
	}
	public Object getRequestObject() {
		return requestObject;
	}
	public void setRequestObject(Object requestObject) {
		this.requestObject = requestObject;
	}
	public Object getResponseObject() {
		return responseObject;
	}
	public void setResponseObject(Object responseObject) {
		this.responseObject = responseObject;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public Integer getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(Integer seqNo) {
		this.seqNo = seqNo;
	}
	public String getInstanceTaskName() {
		return instanceTaskName;
	}
	public void setInstanceTaskName(String instanceTaskName) {
		this.instanceTaskName = instanceTaskName;
	}
	public String getServerProvider() {
		return serverProvider;
	}
	public void setServerProvider(String serverProvider) {
		this.serverProvider = serverProvider;
	}
	public String getSolutionName() {
		return solutionName;
	}
	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}
	public String getInstanceUniqueId() {
		return instanceUniqueId;
	}
	public void setInstanceUniqueId(String instanceUniqueId) {
		this.instanceUniqueId = instanceUniqueId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getDeployUniqueId() {
		return deployUniqueId;
	}
	public void setDeployUniqueId(String deployUniqueId) {
		this.deployUniqueId = deployUniqueId;
	}
	public Date getDeployDate() {
		return deployDate;
	}
	public void setDeployDate(Date deployDate) {
		this.deployDate = deployDate;
	}
}
