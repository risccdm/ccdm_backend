package com.ccdm.elastic.vo;

import java.util.Date;

import org.springframework.data.elasticsearch.annotations.Document;

import com.ccdm.common.constants.ElasticConstants;
import com.ccdm.vo.AppUser;

@Document(indexName = ElasticConstants.ELASTIC_DB_INDEX, type = ElasticConstants.LB_HISTORY)


public class LbHistory {
	
	
	private int lb_id;
	private String lb_name;
	private String lb_des;
	private String lb_type;
	private int vm_id;
	private int clint_id;
	private AppUser appUser;
	
	public String getLb_des() {
		return lb_des;
	}
	public void setLb_des(String lb_des) {
		this.lb_des = lb_des;
	}
	public int getLb_id() {
		return lb_id;
	}
	public void setLb_id(int lb_id) {
		this.lb_id = lb_id;
	}
	public String getLb_name() {
		return lb_name;
	}
	public void setLb_name(String lb_name) {
		this.lb_name = lb_name;
	}
	public String getLb_type() {
		return lb_type;
	}
	public void setLb_type(String lb_type) {
		this.lb_type = lb_type;
	}
	public int getVm_id() {
		return vm_id;
	}
	public void setVm_id(int vm_id) {
		this.vm_id = vm_id;
	}
	public int getClint_id() {
		return clint_id;
	}
	public void setClint_id(int clint_id) {
		this.clint_id = clint_id;
	}
	public AppUser getAppUser() {
		return appUser;
	}
	public void setAppUser(AppUser appUser) {
		this.appUser = appUser;
	}
	

}
