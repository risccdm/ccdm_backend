package com.ccdm.elastic.repository.executionscript;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.ccdm.elastic.vo.ExecutionScript;

public interface IExecutionScriptRepository extends ElasticsearchCrudRepository<ExecutionScript, String> {
	List<ExecutionScript> findAllByUserId(Integer userId);
}
