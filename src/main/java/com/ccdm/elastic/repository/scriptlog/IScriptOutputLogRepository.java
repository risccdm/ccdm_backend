package com.ccdm.elastic.repository.scriptlog;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.ccdm.elastic.vo.ScriptOutputLog;

public interface IScriptOutputLogRepository extends ElasticsearchCrudRepository<ScriptOutputLog, String> {

	List<ScriptOutputLog> findAllByInstanceId(String instanceId);
	
}
