package com.ccdm.elastic.repository.sequencecounter;

import com.ccdm.exceptions.CCDMException;

public interface ISequenceCounterRepoCustom 
{
	long getSequenceCounterValueById(String id) throws CCDMException;
}
