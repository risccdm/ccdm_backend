package com.ccdm.elastic.repository.sequencecounter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.elastic.vo.SequenceCounter;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;

@Repository
public class SequenceCounterRepoCustomImpl implements ISequenceCounterRepoCustom
{
	@Autowired
	private ISequenceCounterRepository iSequenceCounterRepo;
	
	@Override
	public long getSequenceCounterValueById(String seqCounterId) throws CCDMException 
	{
		CommonValidator.validateString(seqCounterId, ErrorCodes.INVALID_SEQUENCE_COUNTER_ID);
		SequenceCounter seqCounter = iSequenceCounterRepo.findOne(seqCounterId);
		long newSeqId = 1;
		if(seqCounter != null)
		{
			newSeqId = seqCounter.getSequenceId() + 1;
			seqCounter.setSequenceId(newSeqId);
		}
		else
		{
			seqCounter = new SequenceCounter(seqCounterId, newSeqId);
		}
		iSequenceCounterRepo.save(seqCounter);
		return newSeqId;
	}
}
