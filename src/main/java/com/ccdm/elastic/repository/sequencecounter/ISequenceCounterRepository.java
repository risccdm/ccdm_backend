package com.ccdm.elastic.repository.sequencecounter;

import org.springframework.data.repository.CrudRepository;

import com.ccdm.elastic.vo.SequenceCounter;

public interface ISequenceCounterRepository extends CrudRepository<SequenceCounter, String> 
{

}
