package com.ccdm.elastic.repository.solutionhistory;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.Criteria;
import org.springframework.data.elasticsearch.core.query.CriteriaQuery;
import org.springframework.stereotype.Repository;

import com.ccdm.elastic.vo.SolutionHistory;

@Repository
public class SolutionHistoryRepoCustomImpl implements ISolutionHistoryRepoCustom
{
	@Autowired
	private ElasticsearchOperations esOperation;

	@Override
	public List<SolutionHistory> findAllBySolutionIdAndCreatedForAndStatusAndOrderBySeqNo(Integer solutionId, String createdFor, String status) 
	{
		
		CriteriaQuery criteriaQuery = new CriteriaQuery(Criteria.where("solutionId").is(solutionId)
																.and("createdFor").is(createdFor)
																.and("status").is(status))
																.addSort(new Sort(Direction.ASC,"seqNo"));
		return esOperation.queryForList(criteriaQuery, SolutionHistory.class);
	}
}
