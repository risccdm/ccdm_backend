package com.ccdm.elastic.repository.solutionhistory;

import java.util.List;

import com.ccdm.elastic.vo.SolutionHistory;

public interface ISolutionHistoryRepoCustom 
{
	List<SolutionHistory> findAllBySolutionIdAndCreatedForAndStatusAndOrderBySeqNo(Integer solutionId, String createdFor, String status);
}
