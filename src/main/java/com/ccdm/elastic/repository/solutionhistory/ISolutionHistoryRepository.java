package com.ccdm.elastic.repository.solutionhistory;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ccdm.elastic.vo.SolutionHistory;

public interface ISolutionHistoryRepository extends CrudRepository<SolutionHistory, String> 
{
	List<SolutionHistory> findAllBySolutionIdAndCreatedFor(Integer solutionId, String createdFor);
	
	List<SolutionHistory> findAllByInstanceUniqueId(String instanceUniqueId);
	
	SolutionHistory findOneByInstanceTaskNameAndInstanceUniqueId(String instanceTaskName, String instanceUniqueId);
	
	List<SolutionHistory> findAllBySolutionIdAndCreatedForOrderBySeqNo(Integer solutionId, String createdFor);
	
	List<SolutionHistory> findAllBySolutionId(Integer solutionId);

	List<SolutionHistory> findAllByCreatedFor(String deployFor);
	
	List<SolutionHistory> findAllByOrderBySeqNoDesc();
}
