package com.ccdm.elastic.solutionhistory.model;

import com.ccdm.solution.request.ServerCredentialVo;

public class AgentMasterConfigRequestVo {

	private ClientInstanceVo clientInstanceVo;
	private ClientInstanceVo masterInstanceVo;
	private ServerCredentialVo serverCredentialVo;
	
	public AgentMasterConfigRequestVo() {}
	
	public AgentMasterConfigRequestVo(ClientInstanceVo clientInstanceVo, ClientInstanceVo masterInstanceVo, ServerCredentialVo serverCredentialVo) {
		this.clientInstanceVo = clientInstanceVo;
		this.masterInstanceVo = masterInstanceVo;
		this.serverCredentialVo = serverCredentialVo;
	}
	
	public ClientInstanceVo getMasterInstanceVo() {
		return masterInstanceVo;
	}

	public void setMasterInstanceVo(ClientInstanceVo masterInstanceVo) {
		this.masterInstanceVo = masterInstanceVo;
	}

	public ClientInstanceVo getClientInstanceVo() {
		return clientInstanceVo;
	}

	public void setClientInstanceVo(ClientInstanceVo clientInstanceVo) {
		this.clientInstanceVo = clientInstanceVo;
	}

	public ServerCredentialVo getServerCredentialVo() {
		return serverCredentialVo;
	}

	public void setServerCredentialVo(ServerCredentialVo serverCredentialVo) {
		this.serverCredentialVo = serverCredentialVo;
	}
	
}
