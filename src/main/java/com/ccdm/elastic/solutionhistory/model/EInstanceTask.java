package com.ccdm.elastic.solutionhistory.model;

public enum EInstanceTask {

	CREATE_INSTANCE("CreateInstance"),
	MASTER_AGENT_CONFIG("MasterAgentConfig"),
	CLIENT_AGENT_INSTALL("ClientAgentInstall"),
	SCRIPT_EXECUTION("ScriptExecution"),
	LOADBALANCER_CONFIG("LoadBalExecution");
	
	private String instanceTaskName;
	
	private EInstanceTask(String instanceTaskName) {
		this.instanceTaskName = instanceTaskName;
	}
	
	public String getTaskName() {
		return this.instanceTaskName;
	}
}
