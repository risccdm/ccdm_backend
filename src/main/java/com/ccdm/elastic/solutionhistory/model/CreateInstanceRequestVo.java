package com.ccdm.elastic.solutionhistory.model;

import com.ccdm.solution.request.InstanceConfig;
import com.ccdm.solution.request.ServerCredentialVo;
import com.ccdm.solution.request.ViewModelVo;

public class CreateInstanceRequestVo {

	private String instanceName;
	private InstanceConfig instanceConfig;
	private ViewModelVo viewModelVo;
	private Integer solutionClientMapId;
	private Integer solutionVmMapId;
	private Integer providerId;
	private String providerName;
	private ServerCredentialVo serverCredentialVo;
	private String createdFor;
	
	public ViewModelVo getViewModelVo() {
		return viewModelVo;
	}
	public void setViewModelVo(ViewModelVo viewModelVo) {
		this.viewModelVo = viewModelVo;
	}
	public InstanceConfig getInstanceConfig() {
		return instanceConfig;
	}
	public void setInstanceConfig(InstanceConfig instanceConfig) {
		this.instanceConfig = instanceConfig;
	}
	public Integer getSolutionClientMapId() {
		return solutionClientMapId;
	}
	public void setSolutionClientMapId(Integer solutionClientMapId) {
		this.solutionClientMapId = solutionClientMapId;
	}
	public Integer getSolutionVmMapId() {
		return solutionVmMapId;
	}
	public void setSolutionVmMapId(Integer solutionVmMapId) {
		this.solutionVmMapId = solutionVmMapId;
	}
	public Integer getProviderId() {
		return providerId;
	}
	public void setProviderId(Integer providerId) {
		this.providerId = providerId;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public ServerCredentialVo getServerCredentialVo() {
		return serverCredentialVo;
	}
	public void setServerCredentialVo(ServerCredentialVo serverCredentialVo) {
		this.serverCredentialVo = serverCredentialVo;
	}
	public String getCreatedFor() {
		return createdFor;
	}
	public void setCreatedFor(String createdFor) {
		this.createdFor = createdFor;
	}
	
}
