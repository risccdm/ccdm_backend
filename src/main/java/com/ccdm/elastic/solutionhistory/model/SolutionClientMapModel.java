package com.ccdm.elastic.solutionhistory.model;

public class SolutionClientMapModel {
	
	private Integer solutionId;
	private String solutionName;
	private String deployForName;
	public Integer getSolutionId() {
		return solutionId;
	}
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	public String getSolutionName() {
		return solutionName;
	}
	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}
	public String getDeployForName() {
		return deployForName;
	}
	public void setDeployForName(String deployForName) {
		this.deployForName = deployForName;
	}
	
}
