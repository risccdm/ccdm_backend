package com.ccdm.elastic.solutionhistory.model;

import java.util.ArrayList;
import java.util.List;

public class SolutionHistoryStatus {

	private Integer solutionId;
	private String solutionName;
	private String instanceName;
	private String createdFor;
	private String serverProvider;
	List<InstanceTaskStatus> instanceTaskStatus = new ArrayList<InstanceTaskStatus>();
	
	public SolutionHistoryStatus(Integer solutionId, String solutionName, String instanceName, String createdFor, String serverProvider, List<InstanceTaskStatus> instanceTaskStatus) {
		this.solutionId = solutionId;
		this.solutionName = solutionName;
		this.instanceName = instanceName;
		this.createdFor = createdFor;
		this.serverProvider = serverProvider;
		this.instanceTaskStatus = instanceTaskStatus;
	}
	
	public Integer getSolutionId() {
		return solutionId;
	}
	
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	public String getSolutionName() {
		return solutionName;
	}
	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}
	public String getCreatedFor() {
		return createdFor;
	}
	public void setCreatedFor(String createdFor) {
		this.createdFor = createdFor;
	}
	public String getServerProvider() {
		return serverProvider;
	}
	public void setServerProvider(String serverProvider) {
		this.serverProvider = serverProvider;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public List<InstanceTaskStatus> getInstanceTaskStatus() {
		return instanceTaskStatus;
	}

	public void setInstanceTaskStatus(List<InstanceTaskStatus> instanceTaskStatus) {
		this.instanceTaskStatus = instanceTaskStatus;
	}
	
}
