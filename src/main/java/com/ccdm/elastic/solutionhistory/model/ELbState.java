package com.ccdm.elastic.solutionhistory.model;

public enum ELbState 
{
	WAITING("Waiting"),
	INITIALIZED("Initialized"),
	INPROGRESS("Inprogress"),
	SKIPPED("Skipped"),
	FAILED("Failed"),
	COMPLETED("Completed");
	
	private String lbState;
	
	private ELbState(String lbState) {
		this.lbState = lbState;
	}
	
	public String getlbState() {
		return this.lbState;
	}
}
