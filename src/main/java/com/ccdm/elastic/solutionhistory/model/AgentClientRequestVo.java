package com.ccdm.elastic.solutionhistory.model;

import com.ccdm.solution.request.ServerCredentialVo;

public class AgentClientRequestVo 
{
	private ClientInstanceVo clientInstanceVo;
	private ServerCredentialVo serverCredentialVo;
	private String uploadFilePath;
	private String ticketSalt;
	private boolean deleteFileServer = false;
	
	public ClientInstanceVo getClientInstanceVo() {
		return clientInstanceVo;
	}
	public void setClientInstanceVo(ClientInstanceVo clientInstanceVo) {
		this.clientInstanceVo = clientInstanceVo;
	}
	public ServerCredentialVo getServerCredentialVo() {
		return serverCredentialVo;
	}
	public void setServerCredentialVo(ServerCredentialVo serverCredentialVo) {
		this.serverCredentialVo = serverCredentialVo;
	}
	public String getUploadFilePath() {
		return uploadFilePath;
	}
	public void setUploadFilePath(String uploadFilePath) {
		this.uploadFilePath = uploadFilePath;
	}
	public String getTicketSalt() {
		return ticketSalt;
	}
	public void setTicketSalt(String ticketSalt) {
		this.ticketSalt = ticketSalt;
	}
	public boolean isDeleteFileServer() {
		return deleteFileServer;
	}
	public void setDeleteFileServer(boolean deleteFileServer) {
		this.deleteFileServer = deleteFileServer;
	}
}
