package com.ccdm.elastic.solutionhistory.model;

public enum ESolutionState 
{
	WAITING("Waiting"),
	INITIALIZED("Initialized"),
	INPROGRESS("Inprogress"),
	SKIPPED("Skipped"),
	FAILED("Failed"),
	COMPLETED("Completed");
	
	private String solutionState;
	
	private ESolutionState(String solutionState) {
		this.solutionState = solutionState;
	}
	
	public String getSolutionState() {
		return this.solutionState;
	}
}
