package com.ccdm.elastic.solutionhistory.model;

public class InstanceTaskStatus {

	private String solutionHistoryId;
	private String instanceTaskName;
	private String status;
	
	public InstanceTaskStatus() {}
	
	public InstanceTaskStatus(String instanceTaskName, String status, String solutionHistoryId) {
		this.instanceTaskName = instanceTaskName;
		this.status = status;
		this.solutionHistoryId = solutionHistoryId;
	}
	
	public String getInstanceTaskName() {
		return instanceTaskName;
	}
	public void setInstanceTaskName(String instanceTaskName) {
		this.instanceTaskName = instanceTaskName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getSolutionHistoryId() {
		return solutionHistoryId;
	}

	public void setSolutionHistoryId(String solutionHistoryId) {
		this.solutionHistoryId = solutionHistoryId;
	}
	
}
