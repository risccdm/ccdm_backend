package com.ccdm.elastic.solutionhistory.model;

import com.ccdm.solution.request.ServerCredentialVo;

public class ScriptRequestVo {

	private String scriptId;
	private String scriptName;
	private String scriptCommand;
	private ClientInstanceVo clientInstanceVo;
	private ServerCredentialVo serverCredentialVo;
	
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	public String getScriptCommand() {
		return scriptCommand;
	}
	public void setScriptCommand(String scriptCommand) {
		this.scriptCommand = scriptCommand;
	}
	public ClientInstanceVo getClientInstanceVo() {
		return clientInstanceVo;
	}
	public void setClientInstanceVo(ClientInstanceVo clientInstanceVo) {
		this.clientInstanceVo = clientInstanceVo;
	}
	public String getScriptId() {
		return scriptId;
	}
	public void setScriptId(String scriptId) {
		this.scriptId = scriptId;
	}
	public ServerCredentialVo getServerCredentialVo() {
		return serverCredentialVo;
	}
	public void setServerCredentialVo(ServerCredentialVo serverCredentialVo) {
		this.serverCredentialVo = serverCredentialVo;
	}
	
}
