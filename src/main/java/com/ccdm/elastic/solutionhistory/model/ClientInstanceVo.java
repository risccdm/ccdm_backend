package com.ccdm.elastic.solutionhistory.model;

import java.util.Date;

public class ClientInstanceVo {

	private String instanceName;
	private String instanceId;
	private String instanceState;
	private Date launchTime;
	private String publicDns;
	private Integer port;
	private String keyName;
	private String publicIpv4;
	
	public ClientInstanceVo() {}
	
	public ClientInstanceVo(String publicDns, String keyName, Integer port, String instanceId, String publicIpv4, String instanceName) {
		this.publicDns = publicDns;
		this.keyName = keyName;
		this.port = port;
		this.instanceId = instanceId;
		this.publicIpv4 = publicIpv4;
		this.instanceName = instanceName;
	}
	
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getInstanceState() {
		return instanceState;
	}
	public void setInstanceState(String instanceState) {
		this.instanceState = instanceState;
	}
	public Date getLaunchTime() {
		return launchTime;
	}
	public void setLaunchTime(Date launchTime) {
		this.launchTime = launchTime;
	}
	public String getPublicDns() {
		return publicDns;
	}
	public void setPublicDns(String publicDns) {
		this.publicDns = publicDns;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getPublicIpv4() {
		return publicIpv4;
	}
	public void setPublicIpv4(String publicIpv4) {
		this.publicIpv4 = publicIpv4;
	}
	public String getKeyName() {
		return keyName;
	}
	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	
}
