package com.ccdm.elastic.solutionhistory.model;

public class AgentMasterConfigResponseVo {
	
	private ClientInstanceVo clientInstanceVo;
	private String ticketSalt;
	
	public ClientInstanceVo getClientInstanceVo() {
		return clientInstanceVo;
	}
	public void setClientInstanceVo(ClientInstanceVo clientInstanceVo) {
		this.clientInstanceVo = clientInstanceVo;
	}
	public String getTicketSalt() {
		return ticketSalt;
	}
	public void setTicketSalt(String ticketSalt) {
		this.ticketSalt = ticketSalt;
	}
	
}
