package com.ccdm.elastic.solutionhistory.model;

public class AgentClientResponseVo 
{
	private String output;

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}
}
