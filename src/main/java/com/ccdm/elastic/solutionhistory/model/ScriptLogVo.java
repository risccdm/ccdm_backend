package com.ccdm.elastic.solutionhistory.model;

import java.util.Date;

public class ScriptLogVo {

	private String scriptName;
	private String logMessage;
	private String status;
	private Date executedDate;
	
	public ScriptLogVo() {}
	
	public ScriptLogVo(String scriptName, String logMessage, String status, Date executedDate) {
		this.scriptName = scriptName;
		this.logMessage = logMessage;
		this.status = status;
		this.executedDate = executedDate;
	}
	
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	public String getLogMessage() {
		return logMessage;
	}
	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getExecutedDate() {
		return executedDate;
	}

	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}
	
}
