package com.ccdm.utils;

import java.io.IOException;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.errors.ErrorId;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.security.constants.SecurityConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class CCDMUtils 
{
	public static String BCryptPassword(String originalPassword) throws CCDMException 
	{
		if(originalPassword == null || originalPassword.isEmpty()) {
			throw new CCDMException(new ErrorId(ErrorCodes.INVALID_PASSWORD.getErrorCode(), 
									ErrorCodes.INVALID_PASSWORD.getErrorMessage()));
		}
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(originalPassword);
	}
	
	public static String getIpAddressFromRequest(HttpServletRequest request) 
	{
		String ipAddress = request.getHeader("X-FORWARDED-FOR");
		if (ipAddress == null) {
			ipAddress = request.getRemoteAddr();
		}
		String ips[] = ipAddress.split(",");
		ipAddress = ips[0];
		return ipAddress;
	}
	
	public static String convertToJsonString(Object obj) 
	{
		 ObjectMapper mapper = new ObjectMapper();
        String jsonString = null;
		try {
			jsonString = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
        return jsonString;
	}
	
	@SuppressWarnings("rawtypes")
	public static LinkedHashMap convertObjectToLinkedHashMap(Object obj) 
	{
		String jsonString = convertToJsonString(obj);
		ObjectMapper mapper = new ObjectMapper();
		LinkedHashMap linkedHashMap = new LinkedHashMap<>();
		try {
			linkedHashMap = mapper.readValue(jsonString, new TypeReference<LinkedHashMap>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return linkedHashMap;
	}
	
	public static boolean isBlank(String input) 
	{
		if(input == null || input.trim().length() == 0) {
			return true;
		}
		return false;
	}
	
	public static boolean isNotBlank(String input) {
		return !isBlank(input);
	}
	
	public static boolean isAPIAuthTokenPresent(HttpServletRequest request) {
		if(request.getHeader(SecurityConstants.AUTH_HEADER_NAME) != null) {
			return true;
		}
		return false;
	}
	
	public static boolean isValidId(Integer id) {
		if(id == null || id < 1) {
			return false;
		}
		return true;
	}
	
	public static boolean isNotValidId(Integer id) {
		return !isValidId(id);
	}
}
