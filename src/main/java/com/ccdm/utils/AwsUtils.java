package com.ccdm.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class AwsUtils 
{
	public Collection<String> getInstanceIdsAsList(String instanceId) 
	{
		List<String> instanceIds = new ArrayList<>();
		instanceIds.add(instanceId);
		return instanceIds;
	}
}
