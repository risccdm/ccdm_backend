package com.ccdm.admin.dao;

import java.util.List;

import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.RolePageMap;
import com.ccdm.vo.UserRoleMap;

public interface UserDAO 
{
	String getLoggedInUserName()  throws CCDMUnauthorizedException;

	AppUser getUser(String userName);

	List<UserRoleMap> getUserRole(Integer userId);

	List<RolePageMap> getRolePageMap(Integer roleId);
}
