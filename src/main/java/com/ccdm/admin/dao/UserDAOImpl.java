package com.ccdm.admin.dao;

import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.common.constants.UserConstants;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.RolePageMap;
import com.ccdm.vo.UserRoleMap;

@Repository
public class UserDAOImpl extends BaseDAOImpl implements UserDAO 
{
	@Override
	public String getLoggedInUserName() throws CCDMUnauthorizedException {
		return super.getLoggedInUserName();
	}
	
	public AppUser getUser(String userName) {
		AppUser user = getByField(AppUser.class, UserConstants.USER_NAME, userName);
		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserRoleMap> getUserRole(Integer userId) 
	{
		List<UserRoleMap> userRoleMaps = getSession().createCriteria(UserRoleMap.class).setFetchMode(UserConstants.USER_ROLE, FetchMode.JOIN)
				.setFetchMode(UserConstants.APP_USER, FetchMode.JOIN).add(Restrictions.eq(UserConstants.IS_ENABLED, true))
				.add(Restrictions.eq(UserConstants.APPUSER_DOT_USER_ID, userId)).list();
		return userRoleMaps;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RolePageMap> getRolePageMap(Integer roleId) 
	{
		List<RolePageMap> rolePageMap = getSession().createCriteria(RolePageMap.class).setFetchMode(UserConstants.ACCESSABLE_PAGES, FetchMode.JOIN)
				.add(Restrictions.eq(UserConstants.USER_ROLE_DOT_ROLE_ID, roleId)).list();
		return rolePageMap;
	}
}
