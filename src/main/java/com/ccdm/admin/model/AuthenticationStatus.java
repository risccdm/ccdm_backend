package com.ccdm.admin.model;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationStatus 
{
	boolean isAuthenticated;
	List<String> accessablePageNameList = new ArrayList<String>();
	private String userType;
    private String userName;
    private String appOwner;
    
	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	public void setAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	public List<String> getAccessablePageNameList() {
		return accessablePageNameList;
	}

	public void setAccessablePageNameList(List<String> accessablePageNameList) {
		this.accessablePageNameList = accessablePageNameList;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getAppOwner()
	{
		return appOwner;
	}

	public void setAppOwner(String appOwner)
	{
		this.appOwner = appOwner;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
}
