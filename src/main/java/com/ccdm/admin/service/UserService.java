package com.ccdm.admin.service;

import java.util.List;

import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.RolePageMap;
import com.ccdm.vo.UserRoleMap;

public interface UserService 
{
	AppUser getLoggedInUser() throws CCDMUnauthorizedException;

	List<UserRoleMap> getUserRole(Integer userId);

	List<RolePageMap> getRolePageMap(Integer roleId);
}
