package com.ccdm.admin.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.admin.dao.UserDAO;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.errors.ErrorId;
import com.ccdm.errors.ErrorId.Severity;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.RolePageMap;
import com.ccdm.vo.UserRoleMap;

@Service
@Transactional(readOnly = false)
public class UserServiceImpl implements UserService 
{
	@Autowired
	private UserDAO userDao;
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	public AppUser getLoggedInUser() throws CCDMUnauthorizedException 
	{   
		String userName = userDao.getLoggedInUserName();
		AppUser appUser = getUser(userName);
		if (appUser == null) {
			logger.error("App user not found for given user name. Given user name is :: " + userName);
			throw new CCDMUnauthorizedException(new ErrorId(ErrorCodes.INVALID_USER.getErrorCode(), 
					ErrorCodes.INTERNAL_ERROR.getErrorMessage(), Severity.FATAL), HttpStatus.UNAUTHORIZED);
		}
		return appUser;
	}
	
	public AppUser getUser(String userName) 
	{
		AppUser appUser = userDao.getUser(userName);
		return appUser;
	}

	@Override
	public List<UserRoleMap> getUserRole(Integer userId) 
	{
		return userDao.getUserRole(userId);
	}

	@Override
	public List<RolePageMap> getRolePageMap(Integer roleId) 
	{
		return userDao.getRolePageMap(roleId);
	}
}
