package com.ccdm.admin.controller;

import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;

@RestController
public class AdminController extends BaseController 
{
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
}
