package com.ccdm.admin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.admin.model.AuthenticationStatus;
import com.ccdm.admin.service.UserService;
import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.property.EnvironmentProperties;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.RolePageMap;
import com.ccdm.vo.UserRoleMap;

@RestController
@RequestMapping(value = "/user")
public class UserController extends BaseController
{
	@Autowired
	private UserService userService;
	
	@Autowired
	private EnvironmentProperties environmentProperties;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	@RequestMapping(value = "/getAuthenticationStatus", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getUserWithAccessPageName()
	{
		boolean isAuthenticated = false;
		
			AppUser appUser;
			try 
			{
				appUser = userService.getLoggedInUser();
			}
			catch (CCDMUnauthorizedException e) 
			{
				return getResponseEntity((Object) e.getLocalizedMessage(), "Error", e.getHttpStatus());
			}
			
			if (appUser.getUserId() != null){
				isAuthenticated = true;
			}
			if(isAuthenticated){
				List<String> accessablePageList = new ArrayList<>();
				List<UserRoleMap> userRoleMap = userService.getUserRole(appUser.getUserId());
				for (UserRoleMap userRoleMaps : userRoleMap) 
				{
					List<RolePageMap> rolePageMap = userService.getRolePageMap(userRoleMaps.getUserRole().getRoleId());
					for (RolePageMap rolePageMaps : rolePageMap) 
					{
						accessablePageList.add(rolePageMaps.getAccessablePages().getPageName());
					}
				}
				AuthenticationStatus authenticationStatus = new AuthenticationStatus();
				authenticationStatus.setAuthenticated(isAuthenticated);
				authenticationStatus.setAccessablePageNameList(accessablePageList);
				authenticationStatus.setUserName(appUser.getUserName());
				authenticationStatus.setUserType(appUser.getUserType());
				authenticationStatus.setAppOwner(environmentProperties.getEazycomAppOwner());
				return getResponseEntity(authenticationStatus);
			}
		
		return getResponseEntity(HttpStatus.UNAUTHORIZED);
	}

}
