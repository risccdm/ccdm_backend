package com.ccdm.solution.history.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.elastic.repository.solutionhistory.ISolutionHistoryRepository;
import com.ccdm.elastic.solutionhistory.model.InstanceTaskStatus;
import com.ccdm.elastic.solutionhistory.model.ScriptLogVo;
import com.ccdm.elastic.solutionhistory.model.ScriptRequestVo;
import com.ccdm.elastic.solutionhistory.model.ScriptResponseVo;
import com.ccdm.elastic.solutionhistory.model.SolutionClientMapModel;
import com.ccdm.elastic.solutionhistory.model.SolutionHistoryStatus;
import com.ccdm.elastic.vo.ExecutionScript;
import com.ccdm.elastic.vo.ScriptOutputLog;
import com.ccdm.elastic.vo.SolutionHistory;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.controller.InstanceController;
import com.ccdm.script.controller.ScriptController;
import com.ccdm.solution.history.dao.ISolutionHistoryDao;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.InstanceInfo;

@Service
@Transactional(readOnly = false)
public class SolutionHistoryServiceImpl implements ISolutionHistoryService {

	@Autowired
	private ISolutionHistoryRepository solutionHistoryRepository;

	@Autowired
	private ISolutionHistoryDao solutionHistoryDao;
	
	@Autowired
	private InstanceController instanceController;
	
	@Autowired
	private ScriptController scriptController;
	
	@Override
	public List<SolutionHistoryStatus> getSolutionStatus(Integer solutionId, String deployFor) throws CCDMException {
		CommonValidator.validateString(deployFor, ErrorCodes.INVALID_DEPLOY_NAME);
		List<SolutionHistory> solutionHistoryList = getSolutionHistoryStatus(solutionId, deployFor);
		List<SolutionHistoryStatus> historyStatusList = adaptToHistoryStatus(solutionHistoryList);
		return historyStatusList;
	}

	private List<SolutionHistory> getSolutionHistoryStatus(Integer solutionId, String deployFor) {
		List<SolutionHistory> solutionHistoryList = null;
		if(deployFor.equals("All") && solutionId < 1) {
			solutionHistoryList = geAllSolutionHistory();
		} else if(solutionId >= 1 && deployFor.equals("All")) {
			solutionHistoryList = solutionHistoryRepository.findAllBySolutionId(solutionId);
		} else if (solutionId < 1 && !deployFor.equals("All")) {
			solutionHistoryList = solutionHistoryRepository.findAllByCreatedFor(deployFor);
		} else if (solutionId >=1 && !deployFor.equals("All")) {
			solutionHistoryList = solutionHistoryRepository.findAllBySolutionIdAndCreatedForOrderBySeqNo(solutionId, deployFor);
		}
		return solutionHistoryList;
	}

	private List<SolutionHistoryStatus> adaptToHistoryStatus(List<SolutionHistory> solutionHistoryList) {
		Map<String, SolutionHistoryStatus> processingMap = new HashMap<String, SolutionHistoryStatus>();
		
		for(SolutionHistory history : solutionHistoryList) {
			if(processingMap.get(history.getInstanceUniqueId()) == null) {
				processingMap.put(history.getInstanceUniqueId(),  buildSoutionHistoryStatus(history));
			} else {
				List<InstanceTaskStatus> instanceTaskStatus = processingMap.get(history.getInstanceUniqueId()).getInstanceTaskStatus();
				instanceTaskStatus.add(new InstanceTaskStatus(history.getInstanceTaskName(), history.getStatus(), history.getId()));
			}
		}
		return convertSolutionHistoryMapToList(processingMap);
	}

	private List<SolutionHistoryStatus> convertSolutionHistoryMapToList( Map<String, SolutionHistoryStatus> processingMap) {
		List<SolutionHistoryStatus> solutionHistoryStatusList = new ArrayList<SolutionHistoryStatus>();
		for(SolutionHistoryStatus historyStatus : processingMap.values()) {
			solutionHistoryStatusList.add(historyStatus);
		}
		return solutionHistoryStatusList;
	}

	private SolutionHistoryStatus buildSoutionHistoryStatus(SolutionHistory history) {
		List<InstanceTaskStatus> instanceTaskStatus = new ArrayList<>();
		instanceTaskStatus.add(new InstanceTaskStatus(history.getInstanceTaskName(), history.getStatus(), history.getId()));
		return new SolutionHistoryStatus(history.getSolutionId(), history.getSolutionName(), history.getInstanceName(), history.getCreatedFor(), history.getServerProvider(), instanceTaskStatus);
	}

	@Override
	public List<SolutionClientMapModel> getSolutionClientMap(AppUser appUser) throws CCDMException {
		List<SolutionClientMapModel> clientMapModels = solutionHistoryDao.getSolutionClientMap(appUser);
		return clientMapModels;
	}
	
	private List<SolutionHistory> geAllSolutionHistory() {
		Iterator<SolutionHistory> solutionHistoryLIst = solutionHistoryRepository.findAll().iterator();
		List<SolutionHistory> historiesList =  new ArrayList<SolutionHistory>();
		while(solutionHistoryLIst.hasNext()) {
			historiesList.add(solutionHistoryLIst.next());
		}
		return historiesList;
	}

	@Override
	public ScriptLogVo getSolutionScriptLogVo(String historyId) throws CCDMException {
		CommonValidator.validateString(historyId, ErrorCodes.INVALID_SOLUTION_HISTORY_ID);
		SolutionHistory history = solutionHistoryRepository.findOne(historyId);
		if(history== null) {
			throw new CCDMException(ErrorCodes.SOLUTION_HISTORY_OBJECT_NULL);
		}
		ScriptRequestVo requestVo = new ObjectMapper().convertValue(history.getRequestObject(), ScriptRequestVo.class);
		ScriptResponseVo responseVo = new ObjectMapper().convertValue(history.getResponseObject(), ScriptResponseVo.class);
		if(requestVo == null || responseVo == null) {
			return new ScriptLogVo();
		}
		return new ScriptLogVo(requestVo.getScriptName(), responseVo.getLogMessage(), responseVo.getStatus(), responseVo.getExecutionDate());
	}

	@Override
	public List<ScriptLogVo> getSolutionScriptLogVoByInstanceInfoId(Integer instanceInfoId) throws CCDMException {
		List<ScriptLogVo> scriptLogVoList = new ArrayList<ScriptLogVo>();
		InstanceInfo instanceInfo = instanceController.getInstanceInfoById(instanceInfoId);
		List<ScriptOutputLog> outputLogList = scriptController.getScriptOutputLogByInstanceId(instanceInfo.getInstanceId());
		
		for(ScriptOutputLog  outputLog : outputLogList) {
			ExecutionScript executionScript =  scriptController.getScriptCommandByScriptId(outputLog.getScriptId()).getContent();
			scriptLogVoList.add(new ScriptLogVo(executionScript.getScriptName(), outputLog.getScriptOutput(), outputLog.getStatus(), outputLog.getExecutionDate()));
		}
		return scriptLogVoList;
	}
}
