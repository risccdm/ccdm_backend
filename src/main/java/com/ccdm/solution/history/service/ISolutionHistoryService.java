package com.ccdm.solution.history.service;

import java.util.List;

import com.ccdm.elastic.solutionhistory.model.ScriptLogVo;
import com.ccdm.elastic.solutionhistory.model.SolutionClientMapModel;
import com.ccdm.elastic.solutionhistory.model.SolutionHistoryStatus;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.vo.AppUser;

public interface ISolutionHistoryService {

	List<SolutionHistoryStatus> getSolutionStatus(Integer solutionId, String deployFor) throws CCDMException;

	List<SolutionClientMapModel> getSolutionClientMap(AppUser appUser)  throws CCDMException;

	ScriptLogVo getSolutionScriptLogVo(String historyId) throws CCDMException;

	List<ScriptLogVo> getSolutionScriptLogVoByInstanceInfoId(Integer instanceInfoId) throws CCDMException;

}
