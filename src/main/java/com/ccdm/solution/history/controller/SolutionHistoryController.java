package com.ccdm.solution.history.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.elastic.solutionhistory.model.ScriptLogVo;
import com.ccdm.elastic.solutionhistory.model.SolutionClientMapModel;
import com.ccdm.elastic.solutionhistory.model.SolutionHistoryStatus;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.solution.history.service.ISolutionHistoryService;
import com.ccdm.vo.AppUser;

@RestController
@RequestMapping(value = "/solutionhistory")
public class SolutionHistoryController extends BaseController {

	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	@Autowired
	private ISolutionHistoryService solutionHistoryService;
	
	@RequestMapping(value = "/status/{solutionId}", method = RequestMethod.GET)
	public CCDMResponse getSolutionStatus(@PathVariable(value = "solutionId") Integer solutionId, @RequestParam("deployFor") String deployFor) throws CCDMException {
		getLoggedInUser();
		List<SolutionHistoryStatus> historyStatus = solutionHistoryService.getSolutionStatus(solutionId, deployFor);
		return getCCDMResponse(historyStatus);
	}
	
	@RequestMapping(value = "/clientmap", method = RequestMethod.GET)
	public  CCDMResponse getSolutionClientMap() throws CCDMException {
		AppUser appUser =  getLoggedInUser();
		List<SolutionClientMapModel> clientMapModel = solutionHistoryService.getSolutionClientMap(appUser);
		return getCCDMResponse(clientMapModel);
	}
	
	@RequestMapping(value = "/script/log/{historyId}", method = RequestMethod.GET)
	public CCDMResponse getSolutionScriptLogVo(@PathVariable("historyId") String historyId) throws CCDMException {
		getLoggedInUser();
		ScriptLogVo logVo = solutionHistoryService.getSolutionScriptLogVo(historyId);
		return getCCDMResponse(logVo);
	}
	
	@RequestMapping(value = "/script/logs/{instanceInfoId}")
	public CCDMResponse getSolutionScriptLogVoByInstanceInfoId(@PathVariable("instanceInfoId") Integer instanceInfoId) throws CCDMException {
		getLoggedInUser();
		List<ScriptLogVo> logVoList = solutionHistoryService.getSolutionScriptLogVoByInstanceInfoId(instanceInfoId);
		return getCCDMResponse(logVoList);
	}
	
}
