package com.ccdm.solution.history.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.elastic.solutionhistory.model.SolutionClientMapModel;
import com.ccdm.vo.AppUser;
import com.ccdm.vo.SolutionClientMap;

@Repository
public class SolutionHistoryDaoImpl extends BaseDAOImpl implements ISolutionHistoryDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<SolutionClientMapModel> getSolutionClientMap(AppUser appUser) 
	{
		List<SolutionClientMapModel> clientMapModels = new ArrayList<SolutionClientMapModel>();
		Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(SolutionClientMap.class)
							.setFetchMode("solution", FetchMode.JOIN)
							.createAlias("solution", "sol")
							.createAlias("solution.appUser", "sol_user")
							.createAlias("customerInfo", "customer")
							.add(Restrictions.eq("sol_user.clientId", appUser.getClientId()))
							.setProjection(Projections.projectionList()
									.add(Projections.property("sol.solutionId"), "solutionId")
									.add(Projections.property("sol.solutionName"), "solutionName")
									.add(Projections.property("customer.customerName"), "deployForName"))
									.setResultTransformer(Transformers.aliasToBean(SolutionClientMapModel.class));
		clientMapModels = criteria.list();
		return clientMapModels;
	}

}
