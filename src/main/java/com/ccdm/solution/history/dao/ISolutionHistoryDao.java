package com.ccdm.solution.history.dao;

import java.util.List;

import com.ccdm.elastic.solutionhistory.model.SolutionClientMapModel;
import com.ccdm.vo.AppUser;

public interface ISolutionHistoryDao {

	List<SolutionClientMapModel> getSolutionClientMap(AppUser appUser);

}
