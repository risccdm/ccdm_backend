package com.ccdm.solution.request;

public class VirtualMachineVo {

	private String model;
	private String vCpu;
	private String memory;
	private String storage;
	
	public VirtualMachineVo() {}
	
	public VirtualMachineVo(String model, String vCpu, String memory, String storage) {
		this.model = model;
		this.vCpu = vCpu;
		this.memory = memory;
		this.storage = storage;
	}
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getvCpu() {
		return vCpu;
	}
	public void setvCpu(String vCpu) {
		this.vCpu = vCpu;
	}
	public String getMemory() {
		return memory;
	}
	public void setMemory(String memory) {
		this.memory = memory;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
}
