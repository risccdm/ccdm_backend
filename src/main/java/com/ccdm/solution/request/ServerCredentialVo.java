package com.ccdm.solution.request;

public class ServerCredentialVo {
	
	private String user;
	private String password;
	private String pemKey;
	private String accessId;
	private String accessKey;
	private String providerName;
	
	public ServerCredentialVo() {}
	
	public ServerCredentialVo(String user, String privateKey, String accessId, String accessKey, String providerName) {
		
		this.user = user;
		this.pemKey = privateKey;
		this.accessId = accessId;
		this.accessKey = accessKey;
		this.providerName = providerName;
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPemKey() {
		return pemKey;
	}
	public void setPemKey(String privateKey) {
		this.pemKey = privateKey;
	}
	public String getAccessId() {
		return accessId;
	}
	public void setAccessId(String accessId) {
		this.accessId = accessId;
	}
	public String getAccessKey() {
		return accessKey;
	}
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	
}
