package com.ccdm.solution.request;

public class ViewModelVo {

	private String name;
	private OsVo osVo;
	private VirtualMachineVo virtualMachineVo;
	
	public ViewModelVo() {}
	
	public ViewModelVo(String name, OsVo osVo, VirtualMachineVo virtualMachineVo) {
		this.name = name;
		this.osVo = osVo;
		this.virtualMachineVo = virtualMachineVo;
	}
	
	public OsVo getOsVo() {
		return osVo;
	}
	public void setOsVo(OsVo osVo) {
		this.osVo = osVo;
	}
	public VirtualMachineVo getVirtualMachineVo() {
		return virtualMachineVo;
	}
	public void setVirtualMachineVo(VirtualMachineVo virtualMachineVo) {
		this.virtualMachineVo = virtualMachineVo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
