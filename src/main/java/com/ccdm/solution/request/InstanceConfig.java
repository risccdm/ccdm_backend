package com.ccdm.solution.request;

public class InstanceConfig {

	private String instanceType;
	private String imageId;
	private String pemKey;
	private String imageProject;
	private String securityGroup;
	private Integer platformRefId;

	public InstanceConfig(){}
	
	public InstanceConfig(String instanceType, String imageId, String pemKey, String securityGroup, String imageProject, Integer platformRefId) {
		this.instanceType = instanceType;
		this.imageId = imageId;
		this.pemKey = pemKey;
		this.securityGroup = securityGroup;
		this.imageProject = imageProject;
		this.platformRefId = platformRefId;
	}
	
	public String getInstanceType() {
		return instanceType;
	}
	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getPemKey() {
		return pemKey;
	}
	public void setPemKey(String pemKey) {
		this.pemKey = pemKey;
	}

	public String getSecurityGroup() {
		return securityGroup;
	}

	public void setSecurityGroup(String securityGroup) {
		this.securityGroup = securityGroup;
	}

	public String getImageProject() {
		return imageProject;
	}

	public void setImageProject(String imageProject) {
		this.imageProject = imageProject;
	}

	public Integer getPlatformRefId() {
		return platformRefId;
	}

	public void setPlatformRefId(Integer platformRefId) {
		this.platformRefId = platformRefId;
	}
	
}
