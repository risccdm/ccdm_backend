package com.ccdm.solution.model;

public class VmScriptModel {

	
	private VmModel vmModel;
	private LoadBal loadBal;
	private ScriptModel scriptModel;
	
	public VmModel getVmModel() {
		return vmModel;
	}

	public void setVmModel(VmModel vmModel) {
		this.vmModel = vmModel;
	}
	
	public LoadBal getLoadBal() {
		return loadBal;
	}

	public void setLoadBal(LoadBal loadBal) {
		this.loadBal = loadBal;
	}

	public ScriptModel getScriptModel() {
		return scriptModel;
	}

	public void setScriptModel(ScriptModel scriptModel) {
		this.scriptModel = scriptModel;
	}

	
}
