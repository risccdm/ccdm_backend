package com.ccdm.solution.model;

public class ScriptModel {
	
	private String scriptId;
	private String scriptName;
	
	public ScriptModel() {}
	
	public ScriptModel(String scriptId, String scriptName) {
		this.scriptId = scriptId;
		this.scriptName = scriptName;
	}
	
	public String getScriptId() {
		return scriptId;
	}
	public void setScriptId(String scriptId) {
		this.scriptId = scriptId;
	}
	public String getScriptName() {
		return scriptName;
	}
	public void setScriptName(String scriptName) {
		this.scriptName = scriptName;
	}
	
}
