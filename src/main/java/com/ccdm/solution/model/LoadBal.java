package com.ccdm.solution.model;

public class LoadBal {
	
	private Integer lbId;
	private String lbTemplateName;
	
	public LoadBal() {}
	
	public LoadBal(Integer lbId, String lbTemplateName) {
		this.lbId = lbId;
		this.lbTemplateName = lbTemplateName;
	}
	
	public Integer getLbId() {
		return lbId;
	}
	public void setLbId(Integer lbId) {
		this.lbId = lbId;
	}
	public String getLbTemplateName() {
		return lbTemplateName;
	}
	public void setLbTemplateName(String lbTemplateName) {
		this.lbTemplateName = lbTemplateName;
	}
	
}
