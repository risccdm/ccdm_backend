package com.ccdm.solution.model;

public class NotifyModel {
	
	private Integer notifyId;
	private String notifyName;
	
	public NotifyModel(){}
	
	public NotifyModel(Integer notifyId,String notifyName){
		this.notifyId = notifyId;
		this.notifyName = notifyName;
	}
	
	public Integer getNotifyId() {
		return notifyId;
	}
	public void setNotifyId(Integer notifyId) {
		this.notifyId = notifyId;
	}
	public String getNotifyName() {
		return notifyName;
	}
	public void setNotifyName(String notifyName) {
		this.notifyName = notifyName;
	}
	
	
}
