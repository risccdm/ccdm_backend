package com.ccdm.solution.model;

public class SlaModel {
	
	private Integer slaTypeId;
	private String slaTypeName;
	
	public SlaModel(){}
	
	public SlaModel(Integer slaTypeId,String slaTypeName){
		this.slaTypeId = slaTypeId;
		this.slaTypeName = slaTypeName;
	}
	
	public Integer getSlaTypeId() {
		return slaTypeId;
	}
	public void setSlaTypeId(Integer slaTypeId) {
		this.slaTypeId = slaTypeId;
	}
	public String getSlaTypeName() {
		return slaTypeName;
	}
	public void setSlaTypeName(String slaTypeName) {
		this.slaTypeName = slaTypeName;
	}
	
}
