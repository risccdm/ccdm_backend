package com.ccdm.solution.model;

public class VmDetails {
	
	private VmModel vmModel;
	private ScriptModel scriptModel;
	
	public VmDetails() {}
	
	public VmDetails(VmModel vmModel, ScriptModel scriptModel) {
		this.vmModel = vmModel;
		this.scriptModel = scriptModel;
	}
	
	public VmModel getVmModel() {
		return vmModel;
	}
	public void setVmModel(VmModel vmModel) {
		this.vmModel = vmModel;
	}
	public ScriptModel getScriptModel() {
		return scriptModel;
	}
	public void setScriptModel(ScriptModel scriptModel) {
		this.scriptModel = scriptModel;
	}
	
}
