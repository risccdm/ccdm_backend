package com.ccdm.solution.model;

public class VmModel {
	
	private Integer vmId;
	private String vmTemplateName;
	
	public VmModel() {}
	
	public VmModel(Integer vmId, String vmTemplateName) {
		this.vmId = vmId;
		this.vmTemplateName = vmTemplateName;
	}
	
	public Integer getVmId() {
		return vmId;
	}
	public void setVmId(Integer vmId) {
		this.vmId = vmId;
	}
	public String getVmTemplateName() {
		return vmTemplateName;
	}
	public void setVmTemplateName(String vmTemplateName) {
		this.vmTemplateName = vmTemplateName;
	}
	
}
