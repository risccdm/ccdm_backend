package com.ccdm.solution.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SolutionDeployState {

	public static SolutionDeployState INSTANCE_CREATED =  new SolutionDeployState(100, "INSTANCE_CREATED", "Instance Created");
	public static SolutionDeployState INSTANCE_INFO_SAVED =  new SolutionDeployState(200, "INSTANCE_INFO_SAVED", "Instance Info Saved");
	public static SolutionDeployState ICINGA_MASTER_CONFIGURED =  new SolutionDeployState(300, "ICINGA_MASTER_CONFIGURED", "Icinga Master Configured");
	public static SolutionDeployState ICINGA_CLIENT_INSTALLED =  new SolutionDeployState(400, "ICINGA_CLIENT_INSTALLED", "Icinga Client Installed");
	public static SolutionDeployState SCRIPT_EXECUTED =  new SolutionDeployState(600, "SCRIPT_EXECUTED", "Script Executed");
	public static SolutionDeployState LB_EXECUTED = new SolutionDeployState(800,"LOAD BALANCER CREATED","LoadBalancer Created");
	public static SolutionDeployState DEPLOYED =  new SolutionDeployState(1000, "DEPLOYED", "Solution Deployed");
	
	private static Map<Integer, SolutionDeployState> mapOfDeploySolution = new HashMap<Integer, SolutionDeployState>();
	
	static {
		
		mapOfDeploySolution.put(INSTANCE_CREATED.stateId, INSTANCE_CREATED);
		mapOfDeploySolution.put(INSTANCE_INFO_SAVED.stateId, INSTANCE_INFO_SAVED);
		mapOfDeploySolution.put(ICINGA_MASTER_CONFIGURED.stateId, ICINGA_MASTER_CONFIGURED);
		mapOfDeploySolution.put(ICINGA_CLIENT_INSTALLED.stateId, ICINGA_CLIENT_INSTALLED);
		mapOfDeploySolution.put(SCRIPT_EXECUTED.stateId, SCRIPT_EXECUTED);
		mapOfDeploySolution.put(LB_EXECUTED.stateId, LB_EXECUTED);
		mapOfDeploySolution.put(DEPLOYED.stateId, DEPLOYED);
		
	}
	
	public static SolutionDeployState getById (Integer id) {
		return mapOfDeploySolution.get(id);
	}
	
	public static List<SolutionDeployState> getAllStates() {
		List<SolutionDeployState>  deployStatesList = new ArrayList<SolutionDeployState>();
		for(SolutionDeployState deployState : mapOfDeploySolution.values()) {
			deployStatesList.add(deployState);
		}
		return deployStatesList;
	}
		private Integer stateId;
		private String stateName;
		private String actualName;
		
		public SolutionDeployState(Integer stateId, String stateName, String actualName) {
			this.stateId = stateId;
			this.stateName = stateName;
			this.actualName = actualName;
		}
		
		public Integer getStateId() {
			return stateId;
		}
		public String getStateName() {
			return stateName;
		}
		public String getActualName() {
			return actualName;
		}
		
		@Override
		public String toString() {
			return "DeployState [stateId=" + stateId + ", stateName=" + stateName + ", actualName=" + actualName + "]";
		}
		
}
