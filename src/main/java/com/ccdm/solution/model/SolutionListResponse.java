package com.ccdm.solution.model;

import java.util.List;

public class SolutionListResponse {

	private SolutionModel solutionModel;
	private List<VmDetails> vmDetails;
	private SlaModel slaModel;
	private NotifyModel notifyModel;
	
	public SolutionListResponse() {}
	
	public SolutionListResponse(SolutionModel solutionModel, List<VmDetails> vmDetails, SlaModel slaModel, NotifyModel notifyModel) {
		this.solutionModel = solutionModel;
		this.vmDetails = vmDetails;
		this.slaModel = slaModel;
		this.notifyModel = notifyModel;
	}
	
	public SolutionModel getSolutionModel() {
		return solutionModel;
	}
	public void setSolutionModel(SolutionModel solutionModel) {
		this.solutionModel = solutionModel;
	}
	public List<VmDetails> getVmDetails() {
		return vmDetails;
	}
	public void setVmDetails(List<VmDetails> vmDetails) {
		this.vmDetails = vmDetails;
	}
	public SlaModel getSlaModel() {
		return slaModel;
	}
	public void setSlaModel(SlaModel slaModel) {
		this.slaModel = slaModel;
	}
	public NotifyModel getNotifyModel() {
		return notifyModel;
	}
	public void setNotifyModel(NotifyModel notifyModel) {
		this.notifyModel = notifyModel;
	}
	
}
