package com.ccdm.solution.model;

import java.util.Date;

public class SolutionModel {
	
	private Integer solutionId;
	private String solutionName;
	private Date creationDate;
	
	public SolutionModel() {
		
	}
	
	public SolutionModel(Integer solutionId, String solutionName) {
		this.solutionId = solutionId;
		this.solutionName = solutionName;
	}
	
	public SolutionModel(Integer solutionId, String solutionName, Date creationDate) {
		this.solutionId = solutionId;
		this.solutionName = solutionName;
		this.creationDate = creationDate;
	}
	
	public Integer getSolutionId() {
		return solutionId;
	}
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	public String getSolutionName() {
		return solutionName;
	}
	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
}
