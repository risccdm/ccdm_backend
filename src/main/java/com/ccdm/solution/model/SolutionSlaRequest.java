package com.ccdm.solution.model;

public class SolutionSlaRequest 
{
	private Integer solutionId;
	private Integer slaTypeId;
	public Integer getSolutionId() {
		return solutionId;
	}
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	public Integer getSlaTypeId() {
		return slaTypeId;
	}
	public void setSlaTypeId(Integer slaTypeId) {
		this.slaTypeId = slaTypeId;
	}
}
