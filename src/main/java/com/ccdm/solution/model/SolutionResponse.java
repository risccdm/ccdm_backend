package com.ccdm.solution.model;

import java.util.List;

public class SolutionResponse {

	private Integer solutionId;
	private String solutionName;
	private String description;
	private List<VmScriptModel> vmScriptModel;
	
	public List<VmScriptModel> getVmScriptModel() {
		return vmScriptModel;
	}
	public void setVmScriptModel(List<VmScriptModel> vmScriptModel) {
		this.vmScriptModel = vmScriptModel;
	}
	public String getSolutionName() {
		return solutionName;
	}
	public void setSolutionName(String solutionName) {
		this.solutionName = solutionName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getSolutionId() {
		return solutionId;
	}
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
}
