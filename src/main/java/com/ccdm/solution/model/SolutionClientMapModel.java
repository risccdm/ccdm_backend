package com.ccdm.solution.model;

import java.util.List;

public class SolutionClientMapModel 
{
	private List<SolutionModel> solutionModelList;
	private List<CustomerInfoModel> customerInfoModelList;
	
	public SolutionClientMapModel() {}
	
	public SolutionClientMapModel(List<SolutionModel> solutionModelList, List<CustomerInfoModel> customerInfoModelList)
	{
		this.solutionModelList = solutionModelList;
		this.customerInfoModelList = customerInfoModelList;
	}
	
	public List<SolutionModel> getSolutionModelList() {
		return solutionModelList;
	}

	public void setSolutionModelList(List<SolutionModel> solutionModelList) {
		this.solutionModelList = solutionModelList;
	}

	public List<CustomerInfoModel> getCustomerInfoModelList() {
		return customerInfoModelList;
	}

	public void setCustomerInfoModelList(List<CustomerInfoModel> customerInfoModelList) {
		this.customerInfoModelList = customerInfoModelList;
	}
}
