package com.ccdm.notification.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.notification.dao.INotificationDao;
import com.ccdm.notification.model.NotificationModel;
import com.ccdm.notification.model.NotificationRequest;
import com.ccdm.notification.setting.ENotificationAuthorization;
import com.ccdm.notification.setting.ENotificationType;
import com.ccdm.notification.setting.NotificationProvider;
import com.ccdm.solutions.controller.SolutionController;
import com.ccdm.vo.Notification;
import com.ccdm.vo.NotificationAspectReference;
import com.ccdm.vo.Solution;

@Service
@Transactional(readOnly = false)
public class NotificationServiceImpl implements NotificationService {

	private static final int ZERO = 0;

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);
	
	@Autowired
	private INotificationDao notificationDAO;
	
	@Autowired
	private NotificationProvider notificationProvider;
	
	@Autowired
	private SolutionController solutionController;
	
	@Override
	public NotificationRequest getNotificationDetails() {
		NotificationRequest notificationRequest = new NotificationRequest();
		notificationRequest.setNotificationAuthorization(buildNotificationAuthorization(ZERO));
		notificationRequest.setNotificationType(buildNotificationType(ZERO));
		return notificationRequest;
	}

	private List<NotificationModel> buildNotificationType(Integer inputValue) {
		List<NotificationModel> notificationModels = new ArrayList<NotificationModel>();
		ENotificationType[] notificationTypeList = ENotificationType.values();
		for(ENotificationType  notificationType : notificationTypeList) {
			notificationModels.add(new NotificationModel(notificationType.getNotificationType(), notificationProvider.isCommunicationTypeEnabled(inputValue, notificationType)));
		}
 		return notificationModels;
	}

	private List<NotificationModel> buildNotificationAuthorization(Integer inputValue) {
		List<NotificationModel> notificationModels = new ArrayList<NotificationModel>();
		ENotificationAuthorization[] authorizationList = ENotificationAuthorization.values();
		for(ENotificationAuthorization  authorization : authorizationList) {
			notificationModels.add(new NotificationModel(authorization.getNotificationAuthName(), notificationProvider.isAuthorizationEnabled(inputValue, authorization)));
		}
 		return notificationModels;
	}

	@Override
	public NotificationRequest getnotificationDetailsbyId(Integer notificationId) throws CCDMException {
		if(notificationId == null | notificationId < 1){
			LOGGER.error("Invalid Notification Id");
			throw new CCDMException(ErrorCodes.INVALID_NOTIFICATION_ID);
		}
		Notification notification = notificationDAO.getNotificationById(notificationId);
		NotificationRequest notificationRequest = new NotificationRequest();
		notificationRequest.setNotificationAuthorization(buildNotificationAuthorization(Integer.valueOf(notification.getNotificationAuthorization())));
		notificationRequest.setNotificationType(buildNotificationType(Integer.valueOf(notification.getNotificationType())));
		return notificationRequest;
	}

	@Override
	public void saveNotification(NotificationRequest notificationRequest) throws CCDMException {
		validateNotificationRequest(notificationRequest);
		
		Integer authorizationValue = getAuthorizationValue(notificationRequest.getNotificationAuthorization());
		Integer notificationTypeValue = getNotificationTypeValue(notificationRequest.getNotificationType());
		Notification notification = getNotification(notificationRequest.getSolutionId());
		notification.setNotificationAuthorization(String.valueOf((int)authorizationValue));
		notification.setNotificationType(String.valueOf((int)notificationTypeValue));
		saveOrUpdateNotification(notification);
		saveOrUpdateSolution(notificationRequest.getSolutionId(), notification);
	}

	private void saveOrUpdateSolution(Integer solutionId, Notification notification) {
		if(solutionId != null) {
			Solution solution = solutionController.getSolutionById(solutionId);
			solution.setNotification(notification);
			solutionController.saveSolution(solution);
		}
	}

	private Notification getNotification(Integer solutionId) {
		if(solutionId == null || solutionId < 1) {
			return new Notification();
		}
		Notification notification = solutionController.getSolutionById(solutionId).getNotification();
		if(notification == null) {
			return new Notification();
		}
		notification.setModificationDate(new Date());
		return notification;
	}

	private void saveOrUpdateNotification(Notification notification) {
		notificationDAO.saveNotification(notification);
	}

	private Integer getNotificationTypeValue(List<NotificationModel> notificationTypeList) throws CCDMException {
		Integer value = 0;
		for(NotificationModel model : notificationTypeList) {
			if(model.getValue()) {
				value = value + notificationProvider.getNotificationTypeValue(ENotificationType.getNotificationTypeByName(model.getName()));
			}
		}
		return value;
	}

	private Integer getAuthorizationValue(List<NotificationModel> notificationAuthorizationList) throws CCDMException {
		Integer value = 0;
		for(NotificationModel model : notificationAuthorizationList) {
			if(model.getValue()) {
				value = value + notificationProvider.getAuthorizationValue(ENotificationAuthorization.getNotificationAuthorizationByName(model.getName()));
			}
		}
		return value;
	}

	private void validateNotificationRequest(NotificationRequest notificationRequest) throws CCDMException {
		if(notificationRequest == null) {
			LOGGER.error("Notification Request Is Empty");
			throw new CCDMException(ErrorCodes.INVALID_NOTIFICATION_REQUEST);
		}
		if(notificationRequest.getNotificationAuthorization() == null  || notificationRequest.getNotificationAuthorization().isEmpty()) {
			LOGGER.error("Notification Request Is Empty");
			throw new CCDMException(ErrorCodes.INVALID_NOTIFICATION_AUTHORIZATION);
		}
		
		if(notificationRequest.getNotificationType() == null  || notificationRequest.getNotificationType().isEmpty()) {
			LOGGER.error("Notification Request Is Empty");
			throw new CCDMException(ErrorCodes.INVALID_NOTIFICATION_TYPE);
		}
		
		boolean isAtleastOneAuthTypeEnabled = isAtleastOneNotificationEnabled(notificationRequest.getNotificationAuthorization());
		boolean isAtleastOneNotifyCommunicationEnabled = isAtleastOneNotificationEnabled(notificationRequest.getNotificationType());
		
		if(!isAtleastOneAuthTypeEnabled && !isAtleastOneNotifyCommunicationEnabled) {
			LOGGER.error("Notification Request Is Empty");
			throw new CCDMException(ErrorCodes.ENABLE_ATLEAST_ONE_NOTIFICATION);
		}
		
	}

	private boolean isAtleastOneNotificationEnabled(List<NotificationModel> notificationAuthorization) {
		for(NotificationModel  model : notificationAuthorization) {
			if(model.getValue() != null || model.getValue()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public NotificationRequest getNotificationDetailsbySolutionId(Integer solutionId) throws CCDMException {
		if(solutionId == null || solutionId < 1) {
			throw new CCDMException(ErrorCodes.INVALID_SOLUTION_ID);
		}
		Solution solution = solutionController.getSolutionById(solutionId);
		if(solution == null || solution.getNotification() == null) {
			return getNotificationDetails();
		}
		NotificationRequest notificationRequest = getnotificationDetailsbyId(solution.getNotification().getId());
		notificationRequest.setSolutionId(solutionId);
		return notificationRequest;
	}
	
	@Override
	public List<NotificationAspectReference> getNotificationAspectReferences(){
		return notificationDAO.getNotificationAspectReferences();
	}

}
