package com.ccdm.notification.service;

import java.util.List;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.notification.model.NotificationRequest;
import com.ccdm.vo.NotificationAspectReference;

public interface NotificationService {

	NotificationRequest getNotificationDetails();

	NotificationRequest getnotificationDetailsbyId(Integer notificationId) throws CCDMException;

	void saveNotification(NotificationRequest notificationRequest) throws CCDMException;

	NotificationRequest getNotificationDetailsbySolutionId(Integer solutionId) throws CCDMException;

	List<NotificationAspectReference> getNotificationAspectReferences();


}
