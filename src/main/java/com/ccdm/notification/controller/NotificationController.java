package com.ccdm.notification.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.infrastructure.model.CCDMResponse;
import com.ccdm.notification.model.NotificationRequest;
import com.ccdm.notification.service.NotificationService;
import com.ccdm.vo.NotificationAspectReference;

@RestController
@RequestMapping(value = "/notification")
public class NotificationController extends BaseController {

	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}

	@Autowired
	private NotificationService notificationService;
	
	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public CCDMResponse getNotificationDetails() throws CCDMUnauthorizedException {
		getLoggedInUser();
		NotificationRequest notificationRequest = notificationService.getNotificationDetails();
		return getCCDMResponse(notificationRequest);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public CCDMResponse getNotificationDetailsbyId(@PathVariable(value = "id") Integer notificationId) throws CCDMException {
		getLoggedInUser();
		NotificationRequest notificationRequest = notificationService.getnotificationDetailsbyId(notificationId);
		return getCCDMResponse(notificationRequest);
	}
	
	@RequestMapping(value = "/save" , method = RequestMethod.POST)
	public CCDMResponse saveNotification(@RequestBody NotificationRequest notificationRequest) throws CCDMException {
		getLoggedInUser();
		notificationService.saveNotification(notificationRequest);
		return getCCDMResponse();
		
	}
	
	@RequestMapping(value = "/solution/{solutionId}", method = RequestMethod.GET)
	public CCDMResponse getNotificationDetailsbySolutionId(@PathVariable(value = "solutionId") Integer solutionId) throws CCDMException {
		getLoggedInUser();
		NotificationRequest notificationRequest = notificationService.getNotificationDetailsbySolutionId(solutionId);
		return getCCDMResponse(notificationRequest);
	}
	
	@RequestMapping(value = "/aspectReferences", method = RequestMethod.GET)
	public CCDMResponse getNotificationAspectReferences() throws CCDMUnauthorizedException{
		List<NotificationAspectReference> aspectReferences = notificationService.getNotificationAspectReferences();
		return getCCDMResponse(aspectReferences);
	}
}
