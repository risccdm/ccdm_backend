package com.ccdm.notification.model;

public class NotificationModel {
	private String name;
	private Boolean value;
	
	public NotificationModel(){}
	
	public NotificationModel(String name, boolean value)
	{
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}
	
}
