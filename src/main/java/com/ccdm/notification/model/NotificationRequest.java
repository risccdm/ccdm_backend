package com.ccdm.notification.model;

import java.util.ArrayList;
import java.util.List;

public class NotificationRequest {
	
	private Integer solutionId;
	private List<NotificationModel> notificationAuthorization = new ArrayList<NotificationModel>();
	private List<NotificationModel> notificationType = new ArrayList<NotificationModel>();
	
	public Integer getSolutionId() {
		return solutionId;
	}
	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	public List<NotificationModel> getNotificationAuthorization() {
		return notificationAuthorization;
	}
	public void setNotificationAuthorization(List<NotificationModel> notificationAuthorization) {
		this.notificationAuthorization = notificationAuthorization;
	}
	public List<NotificationModel> getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(List<NotificationModel> notificationType) {
		this.notificationType = notificationType;
	}
	
}
