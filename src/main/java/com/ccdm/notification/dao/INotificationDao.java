package com.ccdm.notification.dao;

import java.util.List;

import com.ccdm.vo.Notification;
import com.ccdm.vo.NotificationAspectReference;

public interface INotificationDao {

	Notification getNotificationById(Integer notificationId);

	Notification saveNotification(Notification notification);

	List<NotificationAspectReference> getNotificationAspectReferences();

}
