package com.ccdm.notification.dao;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.Notification;
import com.ccdm.vo.NotificationAspectReference;

@Repository
public class NotificationDaoImpl extends BaseDAOImpl implements INotificationDao {

	@Override
	public Notification getNotificationById(Integer notificationId) {
		Notification notification = (Notification) getSession().createCriteria(Notification.class)
									.add(Restrictions.idEq(notificationId)).uniqueResult();
		return notification;
	}

	@Override
	public Notification saveNotification(Notification notification) {
		super.saveOrUpdate(notification);
		return notification;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<NotificationAspectReference> getNotificationAspectReferences(){
		List<NotificationAspectReference> references = getSession().createCriteria(NotificationAspectReference.class).list();
		return references;
	}

}
