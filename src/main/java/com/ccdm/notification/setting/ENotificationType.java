package com.ccdm.notification.setting;

public enum ENotificationType {
	
	EMAIL("Email"),SMS("Sms");
	
	private String notificationType;
	
	private ENotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	
	public String getNotificationType() {
		return this.notificationType;
	}
	
	public static ENotificationType getNotificationTypeByName(String notifyType) {
		ENotificationType[] notificationTypes = ENotificationType.values();
		for(ENotificationType type : notificationTypes) {
			if(type.getNotificationType().equals(notifyType)) {
				return type;
			}
		}
		return null;
 	}
}
