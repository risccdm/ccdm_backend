package com.ccdm.notification.setting;

public enum ENotificationAuthorization {

	SOLUTION_DEPLOYMENT("Solution Deployment"),TEMPLATE_CHANGE("Template Change"),CUSTOMER_CONFIG_CHANGES("Customer ConfigurationChanges"),
	VM_DEPLOYMENT("VM Deployment"),CLOUD_CHANGE("Cloud Change");
	
	private String authorizationName;
	
	private ENotificationAuthorization(String authName) {
		this.authorizationName = authName;
	}
	
	public String getNotificationAuthName() {
		return this.authorizationName;
	}
	
	public static ENotificationAuthorization getNotificationAuthorizationByName(String authName) {
		ENotificationAuthorization[] authorizations = ENotificationAuthorization.values();
		for(ENotificationAuthorization auth : authorizations) {
			if(auth.getNotificationAuthName().equals(authName)) {
				return auth;
			}
		}
		return null;
	}
	
}
