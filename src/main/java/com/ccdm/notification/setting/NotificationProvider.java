package com.ccdm.notification.setting;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;

@Component
public class NotificationProvider {

	private static Map<ENotificationAuthorization, Integer> notificationAuth_map = new HashMap<ENotificationAuthorization, Integer>();
	private static Map<ENotificationType, Integer> notificationType_map = new HashMap<ENotificationType, Integer>();
	
	static {
		
		//For Notification Authorization Type
		notificationAuth_map.put(ENotificationAuthorization.SOLUTION_DEPLOYMENT, 1);
		notificationAuth_map.put(ENotificationAuthorization.TEMPLATE_CHANGE, 2);
		notificationAuth_map.put(ENotificationAuthorization.CUSTOMER_CONFIG_CHANGES, 4);
		notificationAuth_map.put(ENotificationAuthorization.VM_DEPLOYMENT, 8);
		notificationAuth_map.put(ENotificationAuthorization.CLOUD_CHANGE, 16);
		
		//For Notification Type
		notificationType_map.put(ENotificationType.EMAIL, 1);
		notificationType_map.put(ENotificationType.SMS, 2);
		
	}
	
	public boolean isAuthorizationEnabled(Integer input, ENotificationAuthorization authorization){
		return (input & notificationAuth_map.get(authorization)) == (notificationAuth_map.get(authorization));
	}
	
	public boolean isCommunicationTypeEnabled(Integer input, ENotificationType notificationType){
		return (input & notificationType_map.get(notificationType) ) == (notificationType_map.get(notificationType));
	}
	
	public Integer getTotalCountOfAuthorization() {
		Integer totalValue = 0;
		for(Entry<ENotificationAuthorization, Integer> entry : notificationAuth_map.entrySet()) {
			totalValue = totalValue + entry.getValue();
		}
		return totalValue;
	}
	
	public Integer getTotalCountOfCommunicationType() {
		Integer totalValue = 0;
		for(Entry<ENotificationType, Integer> entry : notificationType_map.entrySet()) {
			totalValue = totalValue + entry.getValue();
		}
		return totalValue;
	}
	
	public Integer getAuthorizationValue(ENotificationAuthorization authorization) throws CCDMException {
		if(authorization == null) {
			throw new CCDMException(ErrorCodes.INVALID_NOTIFICATION_AUTHORIZATION);
		}
		return notificationAuth_map.get(authorization);
	}
	
	public Integer getNotificationTypeValue(ENotificationType notificationType) throws CCDMException {
		if(notificationType == null) {
			throw new CCDMException(ErrorCodes.INVALID_NOTIFICATION_TYPE);
		}
		return notificationType_map.get(notificationType);
	}
}
