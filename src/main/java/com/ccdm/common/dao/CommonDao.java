package com.ccdm.common.dao;

import com.ccdm.vo.WebServerProvider;

public interface CommonDao {

	WebServerProvider getwebServerProviderByName(String providerName);

}
