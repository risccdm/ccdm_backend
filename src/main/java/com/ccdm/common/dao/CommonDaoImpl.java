package com.ccdm.common.dao;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ccdm.base.dao.BaseDAOImpl;
import com.ccdm.vo.WebServerProvider;

@Repository
public class CommonDaoImpl extends BaseDAOImpl implements CommonDao {

	@Override
	public WebServerProvider getwebServerProviderByName(String providerName) {
		WebServerProvider provider = (WebServerProvider) getSession().createCriteria(WebServerProvider.class)
									.add(Restrictions.eq("aliasName", providerName))
									.uniqueResult();
		return provider;
	}

}
