package com.ccdm.common.constants;

public class ElasticConstants {

	public static final String ELASTIC_DB_INDEX = "ccdm";
	public static final String EXECUTIONSCRIPT = "executionscript";
	public static final String SCRIPT_OUTPUT_LOG = "scriptoutputlog";
	public static final String SOLUTION_HISTORY = "solutionhistory";
	public static final String SEQUENCE_COUNTER	= "sequencecounter";
	public static final String LB_HISTORY	= "lbhistory";
}
