package com.ccdm.common.constants;

public class UserConstants 
{
	public static final String USER_NAME = "userName";
	public static final String USER_ROLE = "userRole";
	public static final String APP_USER = "appUser";
	public static final String ACCESSABLE_PAGES= "accessablePages";
	
	public static final String APPUSER_DOT_USER_ID = "appUser.userId";
	public static final String USER_ROLE_DOT_ROLE_ID = "userRole.roleId";
	
	public static final String PERCENTAGE = "%";
	
	public static final String IS_ENABLED = "isEnabled";
}
