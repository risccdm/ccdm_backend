package com.ccdm.common.constants;

public class SqlConstants 
{
	public static final Integer PAGE_INDEX_STARTS_FROM = 0;
	public static final Integer DEFAULT_PAGE_LIMIT = 10;
	
	public static final String SELECT_ID_FROM_INSTANCE_INFO_USING_INSTANCEID = "SELECT ID FROM instance_info WHERE INSTANCE_ID=";
	public static final String SELECT_ID_FROM_COSTDETAIL_USING_PROVIDERID_INSTANCETYPE_COSTTYPE = "SELECT ID FROM ccdmdb.cost_details WHERE PROVIDER_ID=? and INSTANCE_TYPE=? AND COST_TYPE=?";
	public static final String SAVE_COST_DETAIL_REFERENCE = "INSERT INTO instance_cost_reference(COST_DETAIL_ID , INSTANCE_INFO_ID, CREATION_DATE) VALUES (?,?,?)";
}
