package com.ccdm.common.validator;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import com.ccdm.errors.ErrorId;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.utils.CCDMUtils;

@Component
public class CommonValidator 
{
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static final String PHONE_NUMBER_PATTERN = "\\+?\\d[\\d -]{8,12}\\d";
	
	public static void validateId(Integer id, ErrorId errorId) throws CCDMException {
		if(id == null || id < 1) {
			throw new CCDMException(errorId);
		}
	}
	public static void validateString(String value, ErrorId errorId) throws CCDMException {
		if(CCDMUtils.isBlank(value)) {
			throw new CCDMException(errorId);
		}
	}
	public static boolean isValidId(Integer id) {
		if(id == null || id < 1) {
			return false;
		}
		return true;
	}
	public static void validateObject(Object obj, ErrorId errorId) throws CCDMException {
		if(obj == null) {
			throw new CCDMException(errorId);
		}
	}
	public static void validateEmail(String emailAddress, ErrorId errorId) throws CCDMException{
		if(!isEmailAddressValid(emailAddress)){
			throw new CCDMException(errorId);
		}
	}
	public static void validatePhoneNum(String phNumber, ErrorId errorId) throws CCDMException{
		if(!isPhoneNumberValid(phNumber)){
			throw new CCDMException(errorId);
		}
	}
	
	private static Boolean isPhoneNumberValid(String phNumber){
		if(phNumber != null && !phNumber.isEmpty()){
			Pattern pattern = Pattern.compile(PHONE_NUMBER_PATTERN);
			Matcher matcher = pattern.matcher(phNumber);
			Boolean isPhNumberValid = matcher.matches();
			return isPhNumberValid;
		}
		return false;
	}
	
	private static Boolean isEmailAddressValid(String emailAddress){
		if(emailAddress != null && !emailAddress.isEmpty()){
			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(emailAddress);
			Boolean isEmailValid = matcher.matches();
			return isEmailValid;
		}
		return false;
	}
	@SuppressWarnings("rawtypes")
	public static void validateList(List list , ErrorId errorId) throws CCDMException {
		if(list == null || list.size() <= 0){
			throw new CCDMException(errorId);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static void validateList(Map map , ErrorId errorId) throws CCDMException {
		if(map == null || map.size() <= 0){
			throw new CCDMException(errorId);
		}
	}
}
