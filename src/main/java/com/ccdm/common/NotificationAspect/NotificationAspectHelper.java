package com.ccdm.common.NotificationAspect;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.notification.controller.NotificationController;
import com.ccdm.vo.NotificationAspectReference;

@Component
public class NotificationAspectHelper {
	
	@Autowired
	private NotificationController notificationController;
	
	public static List<NotificationAspectReference> aspectReferences = new ArrayList<NotificationAspectReference>();
	
	public static List<NotificationAspectReference> getAspectReferences() {
		return aspectReferences;
	}

	public static void setAspectReferences(List<NotificationAspectReference> aspectReferences) {
		NotificationAspectHelper.aspectReferences = aspectReferences;
	}

	@PostConstruct
	public List<NotificationAspectReference> getNotificationAspectReferences() throws CCDMUnauthorizedException{
		if(aspectReferences.size() < 1){
			aspectReferences = notificationController.getNotificationAspectReferences().getContent();
		}
		return aspectReferences;
	}
}
