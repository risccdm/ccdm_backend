package com.ccdm.common.NotificationAspect;

public class NotificationArgumentProvider {

	private Integer solutionId;
	private String deployForName;

	public String getDeployForName() {
		return deployForName;
	}

	public void setDeployForName(String deployForName) {
		this.deployForName = deployForName;
	}

	public Integer getSolutionId() {
		return solutionId;
	}

	public void setSolutionId(Integer solutionId) {
		this.solutionId = solutionId;
	}
	
}
