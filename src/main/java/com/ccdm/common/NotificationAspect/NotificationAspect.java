package com.ccdm.common.NotificationAspect;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.common.validator.CommonValidator;
import com.ccdm.communication.controller.CommunicationController;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.exceptions.CCDMUnauthorizedException;
import com.ccdm.notification.controller.NotificationController;
import com.ccdm.notification.model.NotificationModel;
import com.ccdm.notification.model.NotificationRequest;
import com.ccdm.solution.model.DeployModel;
import com.ccdm.vo.NotificationAspectReference;
import com.twilio.sdk.TwilioRestException;

@Aspect
@Component
public class NotificationAspect {
	
	@Autowired
	private NotificationAspectHelper notificationAspectHelper;
	@Autowired
	private NotificationController notificationController;
	@Autowired
	private CommunicationController communicationController;
	
	private static String SMS = "Sms";
	private static String EMAIL = "Email";
	
	
	/*@Pointcut("execution(* ViewModelService.*(..))")
	public void test(){}
	
	@Before("execution(* com.ccdm.viewModel.service.ViewModelService.*(..))")
	public void beforeMethod()
	{
		System.out.println("before method");
	}
	
	@After("execution(* com.ccdm.viewModel.service.ViewModelService.*(..))")
	public void afterMethod(){
		System.out.println("after method");
	}
	
	@Around("execution(* com.ccdm.viewModel.service.ViewModelService.*(..))")
	public void aroundMethod(){
		System.out.println("around method");
	}
	
	@AfterReturning("execution(* com.ccdm.viewModel.*.*.*(..))")
	public void afterSuccessfulExec(JoinPoint joinPoint){
		Object[] methodArgs = joinPoint.getArgs();
		System.out.println("after successful view model method"+" "+joinPoint.getSignature().toString());
	}*/
	
	@AfterThrowing("execution(* com.ccdm.viewModel.service.ViewModelService.*(..))")
	public void afterException(){
		System.out.println("after exception method");
	}
	
	@AfterReturning("execution(* com.ccdm.solutions.*.*.*(..))")
	public void afterSuccessfulExec1(JoinPoint joinPoint) throws CCDMException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, AddressException, MessagingException, TwilioRestException{
		
		List<NotificationAspectReference> aspectReferences = notificationAspectHelper.getNotificationAspectReferences();
		String methodName = joinPoint.getSignature().toString();
		
		for (NotificationAspectReference aspectRef : aspectReferences) {
			
			String aspectAuthName = "";
			
			if (aspectRef.getMethodName().equals(methodName)) {
				
				aspectAuthName = aspectRef.getNotificationAuthorization();
				
				System.out.println(aspectAuthName);
				
				CommonValidator.validateString(aspectAuthName, ErrorCodes.INVALID_NOTIFICATION_AUTH_TYPE);
				
				Object[] methodArgs = joinPoint.getArgs();
				Method getMethod = methodArgs[0].getClass().getMethod("getSolutionId");
				Object solutionId = getMethod.invoke(methodArgs[0]);
				
				//NotificationArgumentProvider notificationArgumentProvider = (NotificationArgumentProvider) methodArgs[0];
				/*DeployModel arg = (DeployModel) methodArgs[0];*/
				
				CommonValidator.validateObject(solutionId, ErrorCodes.INVALID_SOLUTION_ID);
				
				NotificationRequest notificationRequest = notificationController
						.getNotificationDetailsbySolutionId((Integer) solutionId).getContent();
				
				List<NotificationModel> notificationAuthorizations = notificationRequest.getNotificationAuthorization();
			//	System.out.println(notificationRequest.getSolutionId());
				for (NotificationModel notificationAutorization : notificationAuthorizations) {
					
					if (notificationAutorization.getName().equals(aspectAuthName) && notificationAutorization.getValue()) {
						
						List<NotificationModel> notificationTypes = notificationRequest.getNotificationType();
						
						for (NotificationModel notificationType : notificationTypes) {
							
							if (notificationType.getName().equals(SMS) && notificationType.getValue()) {
								sendSMS(aspectAuthName, (Integer) solutionId);
							}
							
							if (notificationType.getName().equals(EMAIL) && notificationType.getValue()) {
								sendMail(aspectAuthName, (Integer) solutionId);
							}
						}
					}
				}
			}
			
		}
		
		/*System.out.println("after successful solutions method");*/
	}
	
	private void sendMail(String aspectAuthName, Integer solutionId) throws AddressException, MessagingException, CCDMException{
		String toAddress = "vinoth.p@codeboardtech.com";
		String mailContent = buildMailContent(aspectAuthName,solutionId);
		String mailSubject = buildMailSubject(aspectAuthName);
		communicationController.sendMail(toAddress, mailContent, mailSubject);
		System.out.println("mail sent to: "+toAddress);
	}
	private String buildMailSubject(String aspectAuthName){
		String mailSubject = aspectAuthName+" notification";
		
		return mailSubject;
	}
	private String buildMailContent(String aspectAuthName, Integer solutionId){
		String mailContent = "this mail is regarding "+aspectAuthName+" of solutionId: "+solutionId;
		
		return mailContent;
	}
	private void sendSMS(String aspectAuthName, Integer solutionId) throws CCDMException, TwilioRestException{
		String toPhNumber = "+919159530340";
		String smsBodyContent = BuildSmsBodyContent(aspectAuthName, solutionId);
		communicationController.sendMessage(toPhNumber, smsBodyContent);
		System.out.println("sms sent");
	}
	private String BuildSmsBodyContent(String aspectAuthName, Integer solutionId){
		String mailContent = "this sms is regarding "+aspectAuthName+" of solutionId: "+solutionId;
		
		return mailContent;
	}
	
	
}
