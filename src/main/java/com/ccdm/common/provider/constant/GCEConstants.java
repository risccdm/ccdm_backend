package com.ccdm.common.provider.constant;

public class GCEConstants {

	public static final String GCE_US_CENTRAL1_A = "us-central1-a";
	
	public static final String GCE_DEFAULT_NERWORK = "default";

	public static final String GCE_REGION_ADDRESS = "region-";
	
	public static final String GCE_DISK = "disk-";

}
