package com.ccdm.common.service;

import com.ccdm.exceptions.CCDMException;
import com.ccdm.vo.WebServerProvider;

public interface CommonService {

	Integer getCostDetail(Integer providerId, String instanceType, String costType) throws CCDMException;

	void saveCostReference(Integer instanceInfoId, Integer costDetailsId) throws CCDMException;

	WebServerProvider getwebServerProviderByName(String providerName);

}
