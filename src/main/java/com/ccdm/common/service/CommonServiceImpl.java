package com.ccdm.common.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ccdm.common.constants.SqlConstants;
import com.ccdm.common.dao.CommonDao;
import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.jdbc.connection.JdbcConnnectionProvider;
import com.ccdm.vo.WebServerProvider;


@Service
@Transactional
public class CommonServiceImpl implements CommonService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonServiceImpl.class);
	
	@Autowired
	private JdbcConnnectionProvider jdbcConnectionProvider;
	
	@Autowired
	private CommonDao commonDao;
	
	@Override
	public Integer getCostDetail(Integer providerId, String instanceType, String costType) throws CCDMException {
		Connection connection = jdbcConnectionProvider.getJdbcConnection();
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement(SqlConstants.SELECT_ID_FROM_COSTDETAIL_USING_PROVIDERID_INSTANCETYPE_COSTTYPE);
			statement.setInt(1, providerId);
			statement.setString(2, instanceType);
			statement.setString(3, costType);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				return resultSet.getInt("ID");
			}
		} catch (SQLException e) {
			LOGGER.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ",e.getMessage());
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		} finally{
		      //finally block used to close resources
		      try{
		         if(statement!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  LOGGER.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ", se.getMessage());
					throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		      }// do nothing
		      try{
		         if(connection!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  	LOGGER.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ",se.getMessage());
					throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		      }//end finally try
		   }
		return null;
	}

	@Override
	public void saveCostReference(Integer instanceInfoId, Integer costDetailsId) throws CCDMException {
		CommonValidator.validateId(costDetailsId, ErrorCodes.INVALID_COST_DETIALS_ID);
		CommonValidator.validateId(instanceInfoId, ErrorCodes.INVALID_INSTANCE_ID);
		Connection connection = jdbcConnectionProvider.getJdbcConnection();
		PreparedStatement preparedStatement = null;
		try {
			preparedStatement = (PreparedStatement) connection.prepareStatement(SqlConstants.SAVE_COST_DETAIL_REFERENCE);
			preparedStatement.setInt(1, costDetailsId);
			preparedStatement.setInt(2, instanceInfoId);
			preparedStatement.setDate(3, new java.sql.Date(new Date().getTime()));
		} catch (SQLException e) {
			LOGGER.error("Sql Exception While Getting CostDetail Id Using JDBC Connection ====>>> ", e.getMessage());
			throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		} finally{
		      //finally block used to close resources
		      try{
		         if(preparedStatement!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  LOGGER.error("Sql Exception While Saving CostReference Using JDBC Connection ====>>> ", se.getMessage());
					throw new CCDMException(ErrorCodes.INTERNAL_ERROR); 
		      }// do nothing
		      try{
		         if(connection!=null)
		        	 connection.close();
		      }catch(SQLException se){
		    	  	LOGGER.error("Sql Exception While Saving CostReference Using JDBC Connection ====>>> ", se.getMessage());
					throw new CCDMException(ErrorCodes.INTERNAL_ERROR);
		      }//end finally try
		   }
	}

	@Override
	public WebServerProvider getwebServerProviderByName(String providerName) {
		return commonDao.getwebServerProviderByName(providerName);
	}
	
}
