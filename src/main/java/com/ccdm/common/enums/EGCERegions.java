package com.ccdm.common.enums;

public enum EGCERegions {
	
	ASIAEAST1("asia-east1"), EUROPEWEST1("europe-west1"), USCENTRAL1("us-central1");
	
	private String regionName;
	
	private EGCERegions(String regionName) {
		this.regionName = regionName;
	}
	
	public String getGCERegionName() {
		return this.regionName;
	}
}
