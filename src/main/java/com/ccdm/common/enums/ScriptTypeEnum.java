package com.ccdm.common.enums;

public enum ScriptTypeEnum {

	COMMAND(1),FILE(2);
	
	private Integer scriptType;
	
	private ScriptTypeEnum(Integer scriptType) {
		this.scriptType = scriptType;
	}
	
	public Integer getType() {
		return this.scriptType;
	}
}
