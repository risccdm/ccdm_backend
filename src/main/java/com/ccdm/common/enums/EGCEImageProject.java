package com.ccdm.common.enums;

public enum EGCEImageProject {

	CENTOS_CLOUD("centos-cloud"), COREOS_CLOUD("coreos-cloud"), DEBIAN_CLOUD("debian-cloud")
	, RHEL_CLOUD("rhel-cloud"), SUSE_CLOUD("suse-cloud"), UBUNTU_OS_CLOUD("ubuntu-os-cloud")
	, WINDOWS_CLOUD("windows-cloud"), WINDOWS_SQL_CLOUD("windows-sql-cloud");
	
	private String imageProject;
	
	private EGCEImageProject(String imageProject) {
		this.imageProject = imageProject;
	}
	
	public String getImageProjectName() {
		return this.imageProject;
	}
}
