package com.ccdm.common.enums;

public enum ECostType {

	BASIC("BASIC"),ACCESSORIES("ACCESSORIES");
	
	private String costType;
	
	private ECostType(String costType) {
		this.costType = costType;
	}
	
	public String getCostType() {
		return this.costType;
	}
}
