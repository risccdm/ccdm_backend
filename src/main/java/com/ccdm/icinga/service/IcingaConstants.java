package com.ccdm.icinga.service;

public class IcingaConstants 
{
	// icinga master details
	public static final String MASTER_TAGNAME 		= "icinga-master";
	public static final String MASTER_INSTANCE_ID 	= "i-0389295d385f168b7";
	public static final String MASTER_PUBLIC_DNS 	= "35.163.246.210";
	public static final String MASTER_USER_NAME 	= "ubuntu";
	public static final String MASTER_IPV4 			= "35.163.246.210";
	public static final String MASTER_KEYNAME 		= "csdm";
	
	// icinga command config
	public static final String USER 				= "USER";
	public static final String CLIENT_INSTANCE_NAME = "CLIENT_INSTANCE_NAME";
	public static final String CLIENT_IP_ADDRESS 	= "CLIENT_IP_ADDRESS";	
	public static final String TICKET_SALT 			= "TICKET_SALT";
	public static final String FILE_PATH 			= "FILE_PATH";
	
	// Upload Script File Path
	public static final String ICINGA_CLIENT_CONFIG_FILE_PATH_IN_LOCAL = "./src/scripts/client-config.sh";
	public static final String ICINGA_CLIENT_CONFIG_FILE_PATH_IN_SERVER = "/home/USER/client-config.sh";
	
	// Upload Script File Command
	public static final String MASTER_COMMAND = "/home/"+USER+"/master-script/master-script.sh "+CLIENT_INSTANCE_NAME+" "+CLIENT_IP_ADDRESS;
	public static final String CLIENT_COMMAND = ICINGA_CLIENT_CONFIG_FILE_PATH_IN_SERVER +" "+CLIENT_INSTANCE_NAME +" "+TICKET_SALT;
	
	// Script Commands
	public static final String EXEC_PERMISSION_COMMAND 		= "sudo chmod 755 " + ICINGA_CLIENT_CONFIG_FILE_PATH_IN_SERVER+";";
	public static final String DELETE_FILE_COMMAND 			= "rm -rf "+ICINGA_CLIENT_CONFIG_FILE_PATH_IN_SERVER;
	public static final String REMOVE_TRAILING_CHAR_COMMAND = "sed -i 's/\\r$//' " + ICINGA_CLIENT_CONFIG_FILE_PATH_IN_SERVER+";";
}
