package com.ccdm.icinga.service;

import java.util.List;

import com.ccdm.report.host.model.HostReportVo;
import com.ccdm.report.service.model.ServiceGridModel;
import com.ccdm.report.service.model.ServiceReportVo;

public interface IcingaService {

	List<HostReportVo> getAllHosts();

	List<ServiceReportVo> getAllCriticalServices();

	List<ServiceReportVo> getAllServicesWithWarning();

	List<ServiceReportVo> getAllSucessfullServices();

	List<ServiceReportVo> getAllPendingServices();
	
	List<ServiceGridModel> getServiceGrid(Integer limit);

}
