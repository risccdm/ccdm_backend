package com.ccdm.icinga.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.ccdm.report.host.model.HostReportVo;
import com.ccdm.report.service.model.ServiceGridModel;
import com.ccdm.report.service.model.ServiceReportVo;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional(readOnly = false)
public class IcingaServiceImpl implements IcingaService {

	String username = "icingaadmin";
	String password = "root";
	
	@Override
	public List<HostReportVo> getAllHosts(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange
		 ("http://35.163.246.210/icingaweb2/monitoring/list/hosts?format=json", HttpMethod.GET, new HttpEntity(createHeaders(username, password)), String.class);
		List<HostReportVo> hosts = new ArrayList<>();
		try {
			List<HashMap<String, String>> responseList = new ObjectMapper().readValue(response.getBody(), ArrayList.class);
			for (HashMap<String, String> res : responseList) {
				HostReportVo host = new HostReportVo();
				host.setHostName(res.get("host_name"));
				host.setHostOutput(res.get("host_output"));
				host.setHostState(res.get("host_state"));
				Long longDate = Long.parseLong(res.get("host_last_state_change"));
				longDate = longDate * 1000L;
				Date date = new Date(longDate);
				host.setLastStateChange(date);
				
				hosts.add(host);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return hosts;
	}
	
	@Override
	public List<ServiceReportVo> getAllCriticalServices(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange
		 ("http://35.163.246.210/icingaweb2/monitoring/list/services?service_state=2&service_handled=0&format=json", HttpMethod.GET, 
				 new HttpEntity<String>(createHeaders(username, password)), String.class);
		List<ServiceReportVo> services = new ArrayList<>();
		try {
			List<HashMap<String, String>> responseList = new ObjectMapper().readValue(response.getBody(), ArrayList.class);
			for (HashMap<String, String> res : responseList) {
				
				ServiceReportVo service = new ServiceReportVo();
				service.setHostName(res.get("host_name"));
				service.setServiceName(res.get("service_description"));
				service.setServiceOutput(res.get("service_output"));
				service.setServiceState(res.get("service_state"));
				Long longDate = Long.parseLong(res.get("service_last_state_change"));
				longDate = longDate * 1000L;
				Date date = new Date(longDate);
				service.setLastStateChange(date);
				
				services.add(service);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return services;
	}
	
	@Override
	public List<ServiceReportVo> getAllServicesWithWarning(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange
		 ("http://35.163.246.210/icingaweb2/monitoring/list/services?service_state=1&service_handled=0&format=json", HttpMethod.GET, 
				 new HttpEntity<String>(createHeaders(username, password)), String.class);
		List<ServiceReportVo> services = new ArrayList<>();
		try {
			List<HashMap<String, String>> responseList = new ObjectMapper().readValue(response.getBody(), ArrayList.class);
			for (HashMap<String, String> res : responseList) {
				
				ServiceReportVo service = new ServiceReportVo();
				service.setHostName(res.get("host_name"));
				service.setServiceName(res.get("service_description"));
				service.setServiceOutput(res.get("service_output"));
				service.setServiceState(res.get("service_state"));
				Long longDate = Long.parseLong(res.get("service_last_state_change"));
				longDate = longDate * 1000L;
				Date date = new Date(longDate);
				service.setLastStateChange(date);
				
				services.add(service);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return services;
	}
	
	@Override
	public List<ServiceReportVo> getAllSucessfullServices(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange
		 ("http://35.163.246.210/icingaweb2/monitoring/list/services?service_state=0&format=json", HttpMethod.GET, 
				 new HttpEntity<String>(createHeaders(username, password)), String.class);
		List<ServiceReportVo> services = new ArrayList<>();
		try {
			List<HashMap<String, String>> responseList = new ObjectMapper().readValue(response.getBody(), ArrayList.class);
			for (HashMap<String, String> res : responseList) {
				
				ServiceReportVo service = new ServiceReportVo();
				service.setHostName(res.get("host_name"));
				service.setServiceName(res.get("service_description"));
				service.setServiceOutput(res.get("service_output"));
				service.setServiceState(res.get("service_state"));
				Long longDate = Long.parseLong(res.get("service_last_state_change"));
				longDate = longDate * 1000L;
				Date date = new Date(longDate);
				service.setLastStateChange(date);
				
				services.add(service);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return services;
	}
	
	@Override
	public List<ServiceReportVo> getAllPendingServices(){
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange
		 ("http://35.163.246.210/icingaweb2/monitoring/list/services?service_state=99", HttpMethod.GET, 
				 new HttpEntity<String>(createHeaders(username, password)), String.class);
		List<ServiceReportVo> services = new ArrayList<>();
		try {
			List<HashMap<String, String>> responseList = new ObjectMapper().readValue(response.getBody(), ArrayList.class);
			for (HashMap<String, String> res : responseList) {
				
				ServiceReportVo service = new ServiceReportVo();
				service.setHostName(res.get("host_name"));
				service.setServiceName(res.get("service_description"));
				service.setServiceOutput(res.get("service_output"));
				service.setServiceState(res.get("service_state"));
				Long longDate = Long.parseLong(res.get("service_last_state_change"));
				longDate = longDate * 1000L;
				Date date = new Date(longDate);
				service.setLastStateChange(date);
				
				services.add(service);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return services;
	}
	
	@Override
	public List<ServiceGridModel> getServiceGrid(Integer limit){
		Integer contentLimit = limit;
		if(contentLimit==null || contentLimit <= 0){
			contentLimit = 20;
		}
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange
		 ("http://35.163.246.210/icingaweb2/monitoring/list/servicegrid?problems&limit=20%2C"+contentLimit+"&format=json", HttpMethod.GET, 
				 new HttpEntity<String>(createHeaders(username, password)), String.class);
		List<ServiceGridModel> serviceGridModels = new ArrayList<>();
		try {
			List<HashMap<String, String>> responseList = new ObjectMapper().readValue(response.getBody(), ArrayList.class);
			for (HashMap<String, String> res : responseList) {
				
				ServiceGridModel serviceGridModel = new ServiceGridModel();
				serviceGridModel.setHostName(res.get("host_name"));
				serviceGridModel.setServiceHandled(res.get("service_handled"));
				serviceGridModel.setServiceName(res.get("service_description"));
				serviceGridModel.setServiceOutput(res.get("service_output"));
				serviceGridModel.setServiceState(res.get("service_state"));
				
				serviceGridModels.add(serviceGridModel);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return serviceGridModels;
	}
	
	private HttpHeaders createHeaders( String username, String password ){
		   return new HttpHeaders(){
			      {
			         String auth = username + ":" + password;
			         byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
			         String authHeader = "Basic " + new String( encodedAuth );
			         set( "Authorization", authHeader );
			         set("Accept", "application/json");
			      }
			   };
			}
	
	
}
