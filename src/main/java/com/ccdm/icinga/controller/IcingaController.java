package com.ccdm.icinga.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ccdm.base.controller.BaseController;
import com.ccdm.icinga.service.IcingaService;
import com.ccdm.infrastructure.model.CCDMResponse;

@RestController
@RequestMapping(value = "/icinga")
public class IcingaController extends BaseController {
	
	@Autowired
	private IcingaService icingaService;
	
	@Override
	protected Class<?> getClassName() {
		return this.getClass();
	}
	
	@RequestMapping(value = "/hosts", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getAllHosts() {
		return getResponseEntity(icingaService.getAllHosts());
	}
	
	@RequestMapping(value = "/criticalServices", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getAllCriticalServices() {
		return getResponseEntity(icingaService.getAllCriticalServices());
	}
	
	@RequestMapping(value = "/servicesWithWarning", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getAllServicesWithWarning() {
		return getResponseEntity(icingaService.getAllServicesWithWarning());
	}
	
	@RequestMapping(value = "/sucessfullServices", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getAllSucessfullServices() {
		return getResponseEntity(icingaService.getAllSucessfullServices());
	}
	
	@RequestMapping(value = "/pendingServices", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse>  getAllPendingServices() {
		return getResponseEntity(icingaService.getAllPendingServices());
	}
	@RequestMapping(value = "/serviceGrid/{limit}", method = RequestMethod.GET)
	public ResponseEntity<CCDMResponse> getServiceGrid(@PathVariable(value = "limit")Integer limit){
		return getResponseEntity(icingaService.getServiceGrid(limit));
	}

}
