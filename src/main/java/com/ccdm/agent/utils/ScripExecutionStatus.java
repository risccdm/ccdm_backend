package com.ccdm.agent.utils;

public enum ScripExecutionStatus 
{
	SUCCESS("Success"), FAILURE("Failure");
	
	private String scriptLogStatus;
	
	private ScripExecutionStatus(String scriptLogStatus) {
		this.scriptLogStatus = scriptLogStatus;
	}
	
	public String getScriptLogStatus() {
		return this.scriptLogStatus;
	}
}
