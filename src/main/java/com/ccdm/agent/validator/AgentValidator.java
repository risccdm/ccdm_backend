package com.ccdm.agent.validator;

import com.ccdm.agent.model.ExecutionScriptRequest;
import com.ccdm.common.validator.CommonValidator;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;

public class AgentValidator 
{
	public static void validateExecutionScript(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		CommonValidator.validateObject(executionScriptRequest, ErrorCodes.EXECUTION_SCRIPT_EMPTY);
		CommonValidator.validateString(executionScriptRequest.getScript(), ErrorCodes.INVALID_SCRIPT);
	}
}
