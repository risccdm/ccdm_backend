package com.ccdm.agent.jsch;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.ccdm.elastic.solutionhistory.model.ClientInstanceVo;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.instance.constants.InstanceConstant;
import com.ccdm.solution.request.ServerCredentialVo;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Component
public class JschSessionFactory 
{
	private static final Logger logger = LoggerFactory.getLogger(JschSessionFactory.class);
	private static String PEM_KEY_FILE_PATH = InstanceConstant.PEM_FILE_PATH;
	
	public Session getJschSession(ServerCredentialVo credentialVo, ClientInstanceVo instanceVo) throws CCDMException, IOException, InterruptedException
	{
		JSch jsch = new JSch();
		Session session = null;
		try
		{
			if(credentialVo.getPemKey() != null)
			{
				jsch.setKnownHosts(PEM_KEY_FILE_PATH + instanceVo.getKeyName()+".pem");
				jsch.addIdentity(PEM_KEY_FILE_PATH + instanceVo.getKeyName()+".pem");
				logger.info("Identity Added with pem file : "+instanceVo.getKeyName()+".pem");
			}
			
			if(instanceVo.getPort()!= null && instanceVo.getPort() > 0)
			{
				session = jsch.getSession(credentialVo.getUser(), instanceVo.getPublicDns(), instanceVo.getPort());
			}
			else
			{
				session = jsch.getSession(credentialVo.getUser(), instanceVo.getPublicDns());	// By Default TCP port 22 will be used in making the connection.
			}

			// disabling StrictHostKeyChecking may help to make connection but makes it insecure
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			
			logger.info("connecting to session...");
			session  = connectSession(session, 1);
			return session;
		}
		catch(JSchException e)
		{
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_CONNECT_TO_SERVER);
		}
	}
	
	private Session connectSession(Session session, Integer noOfTry) throws CCDMException, InterruptedException 
	{
		try
		{
			if(noOfTry <= 5)
			{
				if(!session.isConnected())
				{
					session.connect();
					logger.info("Session Connected..");
				}
			}
		}
		catch(Exception e)
		{
			logger.info("Connecting to Session.. try : "+noOfTry);
			noOfTry++;
			if(noOfTry <= 5)
			{
				Thread.sleep(1000 * noOfTry);
				connectSession(session, noOfTry);
			}
			else
			{
				throw new CCDMException(ErrorCodes.CANNOT_CONNECT_TO_SERVER);
			}
		}
		return session;
	}

}
