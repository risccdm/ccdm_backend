package com.ccdm.agent.jsch;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class JConnectEC2shell 
{
	public static void main(String[] args)
	{
		/*//Amazon server
		String user = "ubuntu";
		String host = "ec2-35-165-99-212.us-west-2.compute.amazonaws.com";
		int port = 22;
		String privateKey = "D:\\workspace2\\test.pem";*/
		
		// GCE Server
		String user = "riscsdm";
		String host = "146.148.56.23";
		int port = 22;
		String privateKey = "D:\\PemFile\\Gcepem.pem";
		
		try
		{
			JSch jsch = new JSch();
			
			jsch.setKnownHosts(privateKey);
			
			jsch.addIdentity(privateKey);
			
			jsch.setKnownHosts(privateKey);
			
			System.out.println("Identity Added..!");
			
			Session jschSession = jsch.getSession(user, host, port);
			
			//jschSession.setPassword("risccdm123");
			
			 // disabling StrictHostKeyChecking may help to make connection but makes it insecure
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			jschSession.setConfig(config);
			
			jschSession.connect();
			
			System.out.println("Session Connected..!");
			
			
//			 execCommand1(jschSession);
//			 execShFile(jschSession);
			 uploadShFile(jschSession);
			// downloadShFile(jschSession);
			
			
			// try 1
			/*Channel channel = jschSession.openChannel("shell");
			
			channel.setInputStream(new FileInputStream(new File("/home/ubuntu/testFolder/myScript.txt")));
			
			channel.setOutputStream(System.out);
			
			channel.connect(3*1000);
			channel.disconnect();
			System.out.println("disconnected.!");
			System.out.println("System Exit");
			System.exit(0);*/
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			System.out.println(e);
		}
	}

	private static void downloadShFile(Session jschSession) throws JSchException, SftpException, IOException 
	{
		Channel channel = getSftpChannel(jschSession);
		channel.connect();
		System.out.println("channel connected..");
		
		ChannelSftp channelSftp = (ChannelSftp) channel;
		channelSftp.cd("testFolder");
		
		byte[] buffer = new byte[1024];
		BufferedInputStream bis = new BufferedInputStream(channelSftp.get("myScript.sh"));
		
		File downloadFile = new File("D:\\ccdm_script_files\\downloadFile.sh");
		OutputStream os = new FileOutputStream(downloadFile);
		BufferedOutputStream bos = new BufferedOutputStream(os);
		
		int readCount;
		while((readCount = bis.read(buffer)) > 0)
		{
			System.out.print("writing file....");
			bos.write(buffer, 0, readCount);
		}
		
		bis.close();
		bos.close();
		
		Cleanup(jschSession, channelSftp);
	}

	private static void uploadShFile(Session jschSession) throws JSchException, SftpException, FileNotFoundException 
	{
		Channel channel = getSftpChannel(jschSession);
		channel.connect();
		System.out.println("channel connected");
		
		ChannelSftp channelSftp = (ChannelSftp) channel;
		channelSftp.cd("boomi");
		
		File f1 = new File("D:\\fileupload.txt");
		
		channelSftp.put(new FileInputStream(f1), f1.getName());
		
		System.out.println("file uploaded..!");
		
		channelSftp.exit();
		System.out.println("ftp channel exited");
		
		Cleanup(jschSession, channel);
		
	}

	private static void execShFile(Session jschSession) throws JSchException, IOException 
	{
		//String command = "chmod 500 myScript.sh; ubuntu/testFolder/myScript.sh";
		String command = "cd testFolder; sh myScript.sh";
		
		Channel channel = getExecChannel(jschSession);
		((ChannelExec) channel).setCommand(command);
		
		printChannelLog(jschSession, channel);
		
	}

	private static void execCommand1(Session jschSession) throws JSchException, IOException 
	{
		String command1 = "mkdir boomi;ls -ltr;";
		// String command1 = "mkdir commandDir";
		
		Channel channel = getExecChannel(jschSession);
		((ChannelExec) channel).setCommand(command1);
		
		printChannelLog(jschSession, channel);
	}

	private static void printChannelLog(Session jschSession, Channel channel) throws IOException, JSchException 
	{
		channel.setInputStream(null);
		((ChannelExec) channel).setErrStream(System.err);
		
		InputStream in  = channel.getInputStream();
		channel.connect();
		System.out.println("Channel connected..!");
		
		byte[] tmp = new byte[1024];
		while(true)
		{
			while(in.available() > 0)
			{
				int i = in.read(tmp, 0, 1024);
				if(i < 0) break;
				System.out.println(new String(tmp, 0, i));
				break;
			}
			
			if(channel.isClosed())
			{
				System.out.println("exit-status : "+channel.getExitStatus());
				break;
			}
			
			try
			{
				Thread.sleep(1000);
			}
			catch(Exception e) { }
		}
		
		Cleanup(jschSession, channel);
	}
	
	private static Channel getShellChannel(Session jschSession) throws JSchException 
	{
		Channel channel = jschSession.openChannel("shell");
		return channel;
	}
	
	private static Channel getSftpChannel(Session jschSession) throws JSchException 
	{
		Channel channel = jschSession.openChannel("sftp");
		return channel;
	}
	
	private static Channel getExecChannel(Session jschSession) throws JSchException 
	{
		Channel channel = jschSession.openChannel("exec");
		return channel;
	}
	
	private static void Cleanup(Session jschSession, Channel channel) 
	{
		channel.disconnect();
		jschSession.disconnect();
		System.out.println("DONE - Disconnected..!");
	}
}
