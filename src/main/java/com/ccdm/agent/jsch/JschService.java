package com.ccdm.agent.jsch;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.agent.AgentService;
import com.ccdm.agent.model.ExecutionScriptRequest;
import com.ccdm.agent.model.ExecutionScriptResponse;
import com.ccdm.agent.utils.ScripExecutionStatus;
import com.ccdm.agent.validator.AgentValidator;
import com.ccdm.exceptions.CCDMException;

@Component
public class JschService implements AgentService
{
	private static final Logger logger = LoggerFactory.getLogger(JschService.class);
	
	@Autowired
	private JschExec jschExec;
	
	@Override
	public ExecutionScriptResponse executeScript(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		AgentValidator.validateExecutionScript(executionScriptRequest);
		ExecutionScriptResponse response = new ExecutionScriptResponse();
		
		// TODO Validate executionScriptRequest
		logger.info("executing script in an instance : "+executionScriptRequest.getInstanceVo().getInstanceName());
		response.setExecutedDate(new Date());
		String output = jschExec.execute(executionScriptRequest);
		logger.info("script executed in an instance : "+executionScriptRequest.getInstanceVo().getInstanceName()+" on "+response.getExecutedDate());
		
		// Saving output
		response.setInstanceId(executionScriptRequest.getInstanceVo().getInstanceId());
		response.setScriptOutput(output);
		response.setStatus(ScripExecutionStatus.SUCCESS);
		return response;
	}
}
