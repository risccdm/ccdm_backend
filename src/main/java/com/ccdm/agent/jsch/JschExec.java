package com.ccdm.agent.jsch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ccdm.agent.model.ExecutionScriptRequest;
import com.ccdm.elastic.solutionhistory.model.AgentClientRequestVo;
import com.ccdm.errors.ErrorCodes;
import com.ccdm.exceptions.CCDMException;
import com.ccdm.solution.request.ServerCredentialVo;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Component
public class JschExec 
{
	private static final Logger logger = LoggerFactory.getLogger(JschExec.class);
	
	@Autowired
	private JschSessionFactory jschSessionFactory;
	@Autowired
	private JschChannelFactory jschChannelFactory;
	
	private Session session = null;
	private Channel channel = null;

	public String execute(ExecutionScriptRequest executionScriptRequest) throws CCDMException 
	{
		String scriptOuput = "";
		try
		{
			session = jschSessionFactory.getJschSession(executionScriptRequest.getServerCredentialVo(), executionScriptRequest.getInstanceVo());
			channel = jschChannelFactory.getExecChannel(session);
			
			((ChannelExec) channel).setCommand(executionScriptRequest.getScript());
			
			scriptOuput = logChannelOutput(session, channel);
		}
		catch(JSchException ex)
		{
			closingConnection(session, channel);
			logger.error(ex.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_OPEN_CHANNEL);
		}
		catch(Exception e)
		{
			closingConnection(session, channel);
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.JSCH_INTERNAL_ERROR);
		}
		return scriptOuput;
	}
	
	public void uploadFile(AgentClientRequestVo clientAgentRequest, ServerCredentialVo serverCredentialVo) throws CCDMException 
	{
		try
		{
			session = jschSessionFactory.getJschSession(serverCredentialVo, clientAgentRequest.getClientInstanceVo());
			channel = jschChannelFactory.getSftpChannel(session);
			
			logger.info("Connecting to Channel.. try : "+1);
			connectToInstance(channel, 1);
			
			ChannelSftp channelSftp = (ChannelSftp) channel;
			File f1 = new File(clientAgentRequest.getUploadFilePath());
			channelSftp.put(new FileInputStream(f1), f1.getName());
			logger.info("File Uploaded Successfully..");
			
			channelSftp.exit();
			logger.info("sftp channel Closed...");
			closingConnection(session, channel);
		}
		catch(JSchException ex)
		{
			closingConnection(session, channel);
			logger.error(ex.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_UPLOAD_FILE);
		}
		catch(Exception e)
		{
			closingConnection(session, channel);
			logger.error(e.getMessage());
			throw new CCDMException(ErrorCodes.JSCH_INTERNAL_ERROR);
		}
	}
	
	private void connectToInstance(Channel channel, Integer noOfTry) throws CCDMException 
	{
		try
		{
			if(!channel.isConnected())
			{
				if(noOfTry <= 5)
				{
					channel.connect(10000);
					logger.info("Channel Connected..");
				}
				else
				{
					throw new CCDMException(ErrorCodes.CANNOT_CONNECT_TO_SERVER);
				}
			}
		}
		catch(Exception e)
		{
			noOfTry++;
			logger.info("Connecting to Channel.. try : "+noOfTry);
			connectToInstance(channel, noOfTry);
		}
	}

	private String logChannelOutput(Session session, Channel channel) throws CCDMException, IOException, JSchException 
	{
		String scriptOutput = "";
		try
		{
			channel.setInputStream(null);
			((ChannelExec) channel).setErrStream(System.err);
		
			InputStream in  = channel.getInputStream();
			logger.info("Connecting Log Channel...");
			connectToInstance(channel, 1);
			//channel.connect(30000);
			
			StringBuffer strBuff = new StringBuffer();
			byte[] tmp = new byte[1024];
			while(true)
			{
				while(in.available() > 0)
				{
					int i = in.read(tmp, 0, 1024);
					if(i < 0) break;
					//System.out.println(new String(tmp, 0, i));
					strBuff.append(new String(tmp, 0, i)+System.lineSeparator());
					break;
				}
				
				if(channel.isClosed())
				{
					logger.info("exit-status : "+channel.getExitStatus());
					break;
				}
				
				try
				{
					// to get all logs - commend below thread sleep
					//Thread.sleep(10000);
				}
				catch(Exception e) { }
			}
			scriptOutput = strBuff.toString();
			closingConnection(session, channel);
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage());
			throw new CCDMException(ErrorCodes.CANNOT_LOG_CHANNEL_OUTPUT); 
		}
		return scriptOutput;
	}
	
	private void closingConnection(Session session, Channel channel) 
	{
		logger.info("DONE -- Disconnecting...");
		if(!channel.isClosed())
		{
			channel.disconnect();
			logger.info("channel disconnected...");
		}
		if(session.isConnected())
		{
			session.disconnect();
			logger.info("session disconnection...");
		}
	}
}
