package com.ccdm.agent.jsch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

@Component
public class JschChannelFactory 
{
	private static final Logger logger = LoggerFactory.getLogger(JschChannelFactory.class);
	
	public Channel getShellChannel(Session session) throws JSchException 
	{
		Channel channel = session.openChannel("shell");
		logger.info("'shell' channel opened...");
		return channel;
	}
	
	public Channel getSftpChannel(Session session) throws JSchException 
	{
		Channel channel = session.openChannel("sftp");
		logger.info("'sftp' channel opened...");
		return channel;
	}
	
	public Channel getExecChannel(Session session) throws JSchException 
	{
		Channel channel = session.openChannel("exec");
		logger.info("'exec' channel opened...");
		return channel;
	}
}
