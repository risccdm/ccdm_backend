package com.ccdm.agent.model;

import java.util.Date;

import com.ccdm.agent.utils.ScripExecutionStatus;

public class ExecutionScriptResponse 
{
	private ScripExecutionStatus status;
	private String scriptOutput;
	private String instanceId;
	private Date executedDate;
	
	public ScripExecutionStatus getStatus() {
		return status;
	}
	public void setStatus(ScripExecutionStatus status) {
		this.status = status;
	}
	public String getInstanceId() {
		return instanceId;
	}
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	public String getScriptOutput() {
		return scriptOutput;
	}
	public void setScriptOutput(String scriptOutput) {
		this.scriptOutput = scriptOutput;
	}
	public Date getExecutedDate() {
		return executedDate;
	}
	public void setExecutedDate(Date executedDate) {
		this.executedDate = executedDate;
	}
}
