package com.ccdm.agent;

import com.ccdm.agent.model.ExecutionScriptRequest;
import com.ccdm.agent.model.ExecutionScriptResponse;
import com.ccdm.exceptions.CCDMException;

public interface AgentService 
{
	ExecutionScriptResponse executeScript(ExecutionScriptRequest executionScriptRequest) throws CCDMException;
}
