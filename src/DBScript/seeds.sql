-- ------------- ACCESSABLE_PAGES ------------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (1,'DASHBOARD_MENU', 'dashboard');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (2,'SOLUTION_MANAGEMENT_MENU', '/solution/management');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (3,'VM_REPOSITORY_MANAGEMENT_MENU', '/vmrepository/management');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (4,'SLA_MANAGEMENT_MENU', '/sla/management');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (5,'GET_OPERATING_SYSTEMS', '/template/operatingSystems');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (6,'GET_VIRTUAL_MACHINES', '/template/vmSizes');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (7,'GET_WEB_SERVER_PROVIDERS', '/template/webServerProviders');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (8,'POST_SAVE_TEMPLATE', '/template/save');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (9,'GET_CHECK_TEMPLATE_PRESENT', '/template/isPresent');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (10,'VIEW_CREATE_TEMPLATE', '/createSolution.html');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (11,'VIEW_MONITOR', '/monitor.html');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (12,'VIEW_CREATE_SCRIPT', '/createScript.html');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (13,'GET_TEMPLATE_NAMES', '/template/names');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (14,'CREATE_INSTANCE', '/instance/create');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (15,'START_INSTANCE', '/instance/start/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (16,'STOP_INSTANCE', '/instance/stop/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (17,'DESCRIBE_INSTANCES', '/instance/describe');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (18,'SCRIPT_COMMAND_SAVE','/script/command/save');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (19,'SCRIPT_COMMAND_FILE_SAVE','/script/file/upload');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (20, 'INITIATE_INSTANCE_CREATION', '/template/init/instance');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (21, 'GET_ALL_SCRIPT_COMMANDS', '/script/commands');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (22, 'EXECUT_SCRIPT_COMMANDS', '/script/execute');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (23, 'GET_ICINGA_HOSTS', '/icinga/hosts');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (24, 'GET_CRITICAL_SERVICES_ICINGA', '/icinga/criticalServices');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (25, 'GET_SERVICES_WITH_WARNING_ICINGA', '/icinga/servicesWithWarning');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (26, 'GET_SUCCESSFUL_SERVICES_ICINGA', '/icinga/sucessfullServices');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (27, 'GET_PENDING_SERVICES_ICINGA', '/icinga/pendingServices');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (29, 'GET_ALL_INSTANCE_INFO', '/instance/list');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (30, 'INSTANCE_INFO_LIST', '/instanceInfo/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (31, 'VIEW_INSTANCE_INFO', '/instanceInfo.html');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (32,'EXECUTE_SCRIPT_GET_AND_DELETE_BY_ID', '/script/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (33, 'VM_TEMPLATE_MANAGEMENT_MENU', '/vmTemplate');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (34, 'GET_ALL_SOLUTION', '/solution/all');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (35, 'GET_ALL_VM', '/solution/vmTemplates');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (36, 'GET_ALL_SCRIPTS', '/solution/scripts');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (37, 'GET_SLA_NAMES', '/sla/types');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (38, 'DEPLOY_SOLUTION', '/solution/deploy');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (39, 'POST_SAVE_SOLUTION', '/solution/save');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (40, 'GET_NOTIFICATION_DETAILS', '/notification/details');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (41, 'GET_SLA_DETAIL', '/sla/detail/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (42, 'GET_NOTIFICATION_BY_ID', '/notification/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (43, 'GET_SLA_TYPE_BY_SOLUTION_ID', '/sla/type/bySolution/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (44, 'SAVE_NOTIFICATION_DETAILS', '/notification/save');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (45, 'UPDATE_SLA_IN-SOLUTION', '/solution/sla');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (46, 'DEPLOY_SOLUTION_MENU', '/deploy/solution');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (47, 'COST_VISUALIZATION_MENU', '/reports/provider/cost');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (48, 'MONITOR_MENU', '/monitor');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (49, 'GET_SOLUTION_HISTORY_STATUS', '/solutionhistory/status/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (50, 'GET_CLIENT_MAP', '/solutionhistory/clientmap');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (51, 'GET_SOLUTION_CLIENT_MAP', '/solution/solutionClientMap');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (52, 'UPDATE_INSTANCE_STATE', '/instance/describe/update');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (53, 'REBOOT_INSTANCE', '/instance/reboot/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (54, 'TERMINATE_INSTANCE', '/instance/terminate/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (55, 'GET_SOLUTION_BY_SOLUTION_ID', '/solution/**');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (56, 'UPDATE_SOLUTION', '/solution/update');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (57, 'OVER_ALL_COST_CALCULATION', '/report/instance/cost');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (58, 'COST_CALCULATION_PER_INSTANCE', '/report/instance/cost/detail');


-- ------------- APP_USER -------------------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`app_user` (`USER_NAME`, `PASSWORD`,`USER_EMAIL`,`CREATION_DATE`,`CLIENT_ID`,`USER_TYPE`) 
VALUES ('admin', '$2a$10$Nl7pCI.shQf0GpfNX2q2dO1djT6gRiizW.Rli42E/HUx0k9YiKfQC','admin@gmail.com','2015-05-05 07:10:00',1,'ADMIN');

INSERT INTO `ccdmdb`.`app_user` (`USER_NAME`, `PASSWORD`,`USER_EMAIL`,`CREATION_DATE`,`CLIENT_ID`,`USER_TYPE`) 
VALUES ('user', '$2a$10$Nl7pCI.shQf0GpfNX2q2dO1djT6gRiizW.Rli42E/HUx0k9YiKfQC','admin@gmail.com','2015-05-05 07:10:00',1,'USER');

-- ------------- USER_ROLE -------------------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`user_role` (`ROLE_ID`, `ROLE_NAME`) VALUES (1, 'ROLE_ADMIN');
INSERT INTO `ccdmdb`.`user_role` (`ROLE_ID`, `ROLE_NAME`) VALUES (2, 'ROLE_USER');

-- ------------- USER_ROLE_MAP ---------------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`user_role_map` (`USER_ID`, `ROLE_ID`, `IS_ENABLED`) VALUES (1, 1, 1);
INSERT INTO `ccdmdb`.`user_role_map` (`USER_ID`, `ROLE_ID`, `IS_ENABLED`) VALUES (2, 2, 1);

-- ------------- ROLE_PAGE_MAP ----------------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 1);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 2);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 3);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 4);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 1);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 5);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 6);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 7);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 8);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 9);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 10);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 11);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 12);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 13);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 5);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 6);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 7);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 8);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 9);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 10);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 11);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 13);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 13);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 14);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 14);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 15);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 15);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 16);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 16);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 17);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 17);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 18);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 18);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 19);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 19);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 20);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 20);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 21);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 21);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 22);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 22);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 21);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 21);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 22);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 22);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 23);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 24);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 25);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 26);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 27);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 23);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 24);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 25);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 26);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 27);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 29);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 29);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 30);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 31);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 30);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 31);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 32);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 32);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 33);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 34);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 34);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 35);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 35);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 36);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 36);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 37);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 37);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 38);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 38);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 39);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 39);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 40);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 40);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 41);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 41);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 42);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 42);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 43);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 43);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 44);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 44);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 45);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 45);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 46);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 47);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 48);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 48);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 49);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 49);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 50);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 50);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 51);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 51);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 52);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 52);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 53);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 53);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 54);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 54);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 55);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 55);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 56);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 56);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 57);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 57);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (1, 58);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES (2, 58);


-- ----------------------------------OPERATING_SYSTEMS--------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`operating_system` (`NAME`, `TYPE`, `OS_VERSION`) VALUES ('ubuntu', '64-bit', '14.04');
INSERT INTO `ccdmdb`.`operating_system` (`NAME`, `TYPE`, `OS_VERSION`) VALUES ('ubuntu', '64-bit', '16.04');
INSERT INTO `ccdmdb`.`operating_system` (`NAME`, `TYPE`, `OS_VERSION`) VALUES ('Centos', '64-bit', '6.0');
INSERT INTO `ccdmdb`.`operating_system` (`NAME`, `TYPE`, `OS_VERSION`) VALUES ('Centos', '64-bit', '7.0');

-- -----------------------------------------------VIRTUAL_MACHINES-----------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`virtual_machine` (`MODEL`, `V_CPU`, `MEMORY`, `STORAGE`) VALUES ('Small', '1', '2', '8GB');
INSERT INTO `ccdmdb`.`virtual_machine` (`MODEL`, `V_CPU`, `MEMORY`, `STORAGE`) VALUES ('Medium', '2', '4', '16GB');
INSERT INTO `ccdmdb`.`virtual_machine` (`MODEL`, `V_CPU`, `MEMORY`, `STORAGE`) VALUES ('Large', '2', '8', '32GB');

-- ------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `ccdmdb`.`web_server_provider` (`ID`, `NAME`,`ALIAS_NAME`) VALUES ('1', 'Amazon Web Server', 'amazon');
INSERT INTO `ccdmdb`.`web_server_provider` (`ID`, `NAME`,`ALIAS_NAME`) VALUES ('2', 'Google Web Server', 'google');

INSERT INTO `ccdmdb`.`server_credential` (`USER_ID`, `USER`, `PEM_KEY`, `ACCESS_ID`, `ACCESS_KEY`, `PROVIDER_ID`) VALUES ('1', 'ubuntu', 'aws', 'AKIAINUEC2UTC4EHAI5A', 'xmoEWz6r4DGUFQ/oo1Kv+srrdYutbz2TiXeAi9wC', '1');
INSERT INTO `ccdmdb`.`server_credential` (`USER_ID`, `USER`, `PEM_KEY`, `ACCESS_ID`, `ACCESS_KEY`, `PROVIDER_ID`) VALUES ('1', 'centos', 'aws', 'AKIAINUEC2UTC4EHAI5A', 'xmoEWz6r4DGUFQ/oo1Kv+srrdYutbz2TiXeAi9wC', '1');
INSERT INTO `ccdmdb`.`server_credential` (`USER_ID`, `USER`, `PEM_KEY`, `ACCESS_ID`, `ACCESS_KEY`, `PROVIDER_ID`) VALUES ('1', 'riscsdm', 'gce', '', '', '2');

INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (1,1,1,1,'t2.small','ami-30e65350','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (1,2,1,1,'t2.medium','ami-30e65350','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (1,3,1,1,'t2.large','ami-30e65350','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (2,1,1,1,'t2.small','ami-b7a114d7','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (2,2,1,1,'t2.medium','ami-b7a114d7','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (2,3,1,1,'t2.large','ami-b7a114d7','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (3,1,1,2,'t2.small','ami-05cf2265','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (3,2,1,2,'t2.medium','ami-05cf2265','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (3,3,1,2,'t2.large','ami-05cf2265','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (4,1,1,2,'t2.small','ami-05d57565','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (4,2,1,2,'t2.medium','ami-05d57565','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`MACHINE_TYPE`,`IMAGE_ID`,`IMAGE_PROJECT`) VALUES (4,3,1,2,'t2.large','ami-05d57565','');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('1', '1', '2', '3', 'n1-standard-1','ubuntu-1404-trusty-v20161213','ubuntu-os-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('1', '2', '2', '3', 'n1-standard-2','ubuntu-1404-trusty-v20161213','ubuntu-os-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('1', '3', '2', '3', 'n1-standard-3','ubuntu-1404-trusty-v20161213','ubuntu-os-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('2', '1', '2', '3', 'n1-standard-1','ubuntu-1604-xenial-v20161221','ubuntu-os-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('2', '2', '2', '3', 'n1-standard-2','ubuntu-1604-xenial-v20161221','ubuntu-os-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('2', '3', '2', '3', 'n1-standard-3','ubuntu-1604-xenial-v20161221','ubuntu-os-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('3', '1', '2', '3', 'n1-standard-1','centos-6-v20161212','centos-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('3', '2', '2', '3', 'n1-standard-2','centos-6-v20161212','centos-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('3', '3', '2', '3', 'n1-standard-3','centos-6-v20161212','centos-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('4', '1', '2', '3', 'n1-standard-1','centos-7-v20161212','centos-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('4', '2', '2', '3', 'n1-standard-2','centos-7-v20161212','centos-cloud');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `SERVER_CREDENTIAL_ID`, `MACHINE_TYPE`, `IMAGE_ID`,`IMAGE_PROJECT`) VALUES ('4', '3', '2', '3', 'n1-standard-3','centos-7-v20161212','centos-cloud');


-- notification_aspect_reference --
INSERT INTO `ccdmdb`.`notification_aspect_reference` (`METHOD_NAME`, `NOTIFICATION_AUTHORIZATION`) VALUES ('CCDMResponse com.ccdm.solutions.controller.SolutionController.deploySolution(DeployModel)', 'Solution Deployment');
INSERT INTO `ccdmdb`.`notification_aspect_reference` (`METHOD_NAME`, `NOTIFICATION_AUTHORIZATION`) VALUES ('CCDMResponse com.ccdm.solutions.controller.SolutionController.updateSolution(SolutionRequest)', 'Template Change');

INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('1', 't2.small', 'BASIC', '80');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('1', 't2.medium', 'BASIC', '90');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('1', 't2.large', 'BASIC', '100');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('2', 'n1-standard-1', 'BASIC', '80');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('2', 'n1-standard-2', 'BASIC', '90');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('2', 'n1-standard-3', 'BASIC', '100');

INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('anand');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('bala');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('sathish');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('boomi');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('arivoli');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('jegan');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('vinoth');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('dhanabal');
INSERT INTO `ccdmdb`.`customer_info` (`CUSTOMER_NAME`) VALUES ('kirpa');