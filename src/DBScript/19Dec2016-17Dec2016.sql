UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/list' WHERE `PAGE_ID`='29';

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('GET_SOLUTION_CLIENT_MAP', '/solution/solutionClientMap');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'GET_SOLUTION_CLIENT_MAP');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/start/**' WHERE `PAGE_ID`='15';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/stop/**' WHERE `PAGE_ID`='16';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instanceInfo/**' WHERE `PAGE_ID`='30';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/describe' WHERE `PAGE_ID`='17';

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('UPDATE_INSTANCE_STATE', '/instance/describe/update');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'UPDATE_INSTANCE_STATE');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('REBOOT_INSTANCE', '/instance/reboot/**');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'REBOOT_INSTANCE');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('TERMINATE_INSTANCE', '/instance/terminate/**');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'TERMINATE_INSTANCE');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('GET_SOLUTION_BY_SOLUTION_ID', '/solution/**');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'GET_SOLUTION_BY_SOLUTION_ID');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('UPDATE_SOLUTION', '/solution/update');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'UPDATE_SOLUTION');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

UPDATE `ccdmdb`.`app_user` SET `USER_TYPE`='ADMIN' WHERE `USER_ID`='1';
UPDATE `ccdmdb`.`app_user` SET `USER_TYPE`='USER' WHERE `USER_ID`='2';

-- S1_CostCalculation
ALTER TABLE `ccdmdb`.`provider_platform_reference` 
DROP COLUMN `COST_PER_HOUR`;

UPDATE `ccdmdb`.`provider_platform_reference` SET `PROVIDER_REFERENCE_1`='t2.micro' WHERE `ID`='1';
UPDATE `ccdmdb`.`provider_platform_reference` SET `PROVIDER_REFERENCE_1`='t2.micro' WHERE `ID`='2';
UPDATE `ccdmdb`.`provider_platform_reference` SET `PROVIDER_REFERENCE_1`='t2.micro' WHERE `ID`='3';

CREATE TABLE `ccdmdb`.`cost_details` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `PROVIDER_ID` INT NOT NULL,
  `INSTANCE_TYPE` VARCHAR(45) NOT NULL,
  `COST_TYPE` VARCHAR(45) NOT NULL,
  `ACCESSORIES_REF_ID` INT NULL DEFAULT NULL,
  `COST_VALUE` DOUBLE NULL,
  PRIMARY KEY (`ID`));
  
ALTER TABLE `ccdmdb`.`cost_details` 
ADD INDEX `FK_COST_DETAIL_PROVIDER_ID_idx` (`PROVIDER_ID` ASC) ;
ALTER TABLE `ccdmdb`.`cost_details` 
ADD CONSTRAINT `FK_COST_DETAIL_PROVIDER_ID`
  FOREIGN KEY (`PROVIDER_ID`)
  REFERENCES `ccdmdb`.`web_server_provider` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  CREATE TABLE `ccdmdb`.`instance_cost_reference` (
  `ID` INT NOT NULL AUTO_INCREMENT ,
  `COST_DETAIL_ID` INT NOT NULL ,
  `INSTANCE_INFO_ID` INT NOT NULL ,
  `CREATION_DATE` DATETIME NOT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `FK_INSTANCE_COST_REF_COST_DETAILS_ID_idx` (`COST_DETAIL_ID` ASC)  ,
  INDEX `FK_INSTANCE_REF_INFO_ID_idx` (`INSTANCE_INFO_ID` ASC)  ,
  CONSTRAINT `FK_INSTANCE_COST_REF_COST_DETAILS_ID`
    FOREIGN KEY (`COST_DETAIL_ID`)
    REFERENCES `ccdmdb`.`cost_details` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_INSTANCE_REF_INFO_ID`
    FOREIGN KEY (`INSTANCE_INFO_ID`)
    REFERENCES `ccdmdb`.`instance_info` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);