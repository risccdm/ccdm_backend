INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('CREATE_AWS_INSTANCE', '/awsInstance/create');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('START_AWS_INSTANCE', '/awsInstance/start');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('STOP_AWS_INSTANCE', '/awsInstance/stop');
INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('STATUS_AWS_INSTANCE', '/awsInstance/status');






ALTER TABLE `ccdmdb`.`solutions` 
DROP INDEX `fk_userid_app_user_userid_idx` ,
ADD INDEX `fk_userid_app_user_userid_idx` (`USER_ID` ASC, `NAME` ASC);


ALTER TABLE `ccdmdb`.`server_credentials` 
ADD COLUMN `ACCESS_ID` VARCHAR(245) NULL DEFAULT NULL AFTER `PORT`,
ADD COLUMN `ACCESS_KEY` VARCHAR(245) NULL DEFAULT NULL AFTER `ACCESS_ID`;

INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', '13');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', '13');

INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', '14');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', '14');

INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', '15');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', '15');

INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', '16');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', '16');



-- ------------- Instance Service Call Query Updated ----------------------------
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/create' WHERE `PAGE_ID`='13';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/start' WHERE `PAGE_ID`='14';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/stop' WHERE `PAGE_ID`='15';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/instance/status' WHERE `PAGE_ID`='16';

UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='CREATE_INSTANCE' WHERE `PAGE_ID`='13';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='START_INSTANCE' WHERE `PAGE_ID`='14';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='STOP_INSTANCE' WHERE `PAGE_ID`='15';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='STATUS_INSTANCE' WHERE `PAGE_ID`='16';
-- ------------- Instance Service Call Query Updated ----------------------------

ALTER TABLE `ccdmdb`.`server_instanace_info` 
CHANGE COLUMN `ID` `ID` INT(11) NOT NULL AUTO_INCREMENT;

-- ------------ Alter solution to template --------------------------------------
-- ------------------------------------------------------------------------------

ALTER TABLE `ccdmdb`.`solutions` 
RENAME TO  `ccdmdb`.`templates` ;

-- ---------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------
ALTER TABLE `ccdmdb`.`server_instanace_info` 
DROP FOREIGN KEY `solution_id`;
ALTER TABLE `ccdmdb`.`server_instanace_info` 
CHANGE COLUMN `SOLUTION_ID` `TEMPLATE_ID` INT(11) NULL ;
ALTER TABLE `ccdmdb`.`server_instanace_info` 
ADD CONSTRAINT `template_id`
  FOREIGN KEY (`TEMPLATE_ID`)
  REFERENCES `ccdmdb`.`templates` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

-- ---------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------

UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='CREATE_TEMPLATE' WHERE `PAGE_ID`='1';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='POST_SAVE_TEMPLATE' WHERE `PAGE_ID`='8';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='GET_CHECK_TEMPLATE_PRESENT' WHERE `PAGE_ID`='9';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='VIEW_CREATE_TEMPLATE' WHERE `PAGE_ID`='10';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_NAME`='GET_TEMPLATE_NAMES' WHERE `PAGE_ID`='13';

UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/operatingSystems' WHERE `PAGE_ID`='5';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/vmSizes' WHERE `PAGE_ID`='6';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/webServerProviders' WHERE `PAGE_ID`='7';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/saveSolution' WHERE `PAGE_ID`='8';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/isPresent' WHERE `PAGE_ID`='9';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/names' WHERE `PAGE_ID`='13';
UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='/template/save' WHERE `PAGE_ID`='8';

UPDATE `ccdmdb`.`accessable_pages` SET `PAGE_VALUE`='templates/new' WHERE `PAGE_ID`='1';

 -- Sample Server Credentials value for Create Instance
ALTER TABLE `ccdmdb`.`server_credentials` 
CHANGE COLUMN `HOST` `HOST` VARCHAR(100) NOT NULL;

INSERT INTO `ccdmdb`.`server_credentials` (`USER_ID`, `USER`, `HOST`, `PORT`, `ACCESS_ID`, `ACCESS_KEY`) 
VALUES ('1', 'ubuntu', 'ec2-35-163-10-157.us-west-2.compute.amazonaws.com', '22', 'AKIAINUEC2UTC4EHAI5A', 'xmoEWz6r4DGUFQ/oo1Kv+srrdYutbz2TiXeAi9wC');

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_ID`, `PAGE_NAME`, `PAGE_VALUE`) VALUES (20, 'INITIATE_INSTANCE_CREATION', '/template/init/instance');

INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', '20');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', '20');

ALTER TABLE `ccdmdb`.`server_instanace_info` 
CHANGE COLUMN `TAG_NAME` `TAG_NAME` VARCHAR(50) NULL DEFAULT NULL,
CHANGE COLUMN `INSTANCE_ID` `INSTANCE_ID` VARCHAR(100) NULL DEFAULT NULL,
CHANGE COLUMN `INSTANCE_TYPE` `INSTANCE_TYPE` VARCHAR(100) NULL DEFAULT NULL,
CHANGE COLUMN `AVAILABILITY_ZONE` `AVAILABILITY_ZONE` VARCHAR(50) NULL DEFAULT NULL,
CHANGE COLUMN `KEY_NAME` `KEY_NAME` VARCHAR(100) NULL DEFAULT NULL,
CHANGE COLUMN `SECURITY_GROUP` `SECURITY_GROUP` VARCHAR(100) NULL DEFAULT NULL;



