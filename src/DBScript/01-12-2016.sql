-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema ccdmdb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ccdmdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ccdmdb` DEFAULT CHARACTER SET latin1 ;
USE `ccdmdb` ;

-- -----------------------------------------------------
-- Table `ccdmdb`.`accessable_pages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`accessable_pages` (
  `PAGE_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `PAGE_NAME` VARCHAR(45) NOT NULL,
  `PAGE_VALUE` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`PAGE_ID`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`app_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`app_user` (
  `USER_ID` INT(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` VARCHAR(45) NOT NULL,
  `PASSWORD` VARCHAR(100) NOT NULL,
  `USER_EMAIL` VARCHAR(45) NOT NULL ,
  `USER_PHONE` VARCHAR(45) NULL DEFAULT NULL ,
  `USER_TYPE` VARCHAR(45) NULL DEFAULT NULL ,
  `CREATION_DATE` DATETIME NULL DEFAULT NULL ,
  `MODIFIED_DATE` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`USER_ID`)  ,
  UNIQUE INDEX `USER_NAME_UNIQUE` (`USER_NAME` ASC)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`sla_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`sla_type` (
  `SLA_TYPE_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PRIORITY_TYPE` VARCHAR(45) NULL DEFAULT NULL ,
  `RESPONSE_TIME` DATETIME NULL DEFAULT NULL ,
  `UPTIME` DATETIME NULL DEFAULT NULL ,
  `WORKING HOURS` DOUBLE NULL DEFAULT '0' ,
  `CREDITS` VARCHAR(50) NULL DEFAULT NULL ,
  PRIMARY KEY (`SLA_TYPE_ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`solution`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`solution` (
  `SOLUTION_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SOLUTION_NAME` VARCHAR(45) NOT NULL ,
  `DESCRIPTION` VARCHAR(500) NULL DEFAULT NULL ,
  `CREATION_DATE` DATETIME NOT NULL ,
  `MODIFICATION_DATE` DATETIME NULL DEFAULT NULL ,
  `SLA TYPE ID` INT(11) NULL DEFAULT NULL ,
  `NOTIFICATION_ID` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`SOLUTION_ID`)  ,
  INDEX `FK_SOLUTON_SLATYPE_SLATYPEID_idx` (`SLA TYPE ID` ASC)  ,
  INDEX `FK_SOLUTION_NOTIFICATION_ID_idx` (`NOTIFICATION_ID` ASC)  ,
  CONSTRAINT `FK_SOLUTION_NOTIFICATION_ID`
    FOREIGN KEY (`NOTIFICATION_ID`)
    REFERENCES `ccdmdb`.`notification` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOLUTON_SLATYPE_SLATYPEID`
    FOREIGN KEY (`SLA TYPE ID`)
    REFERENCES `ccdmdb`.`sla_type` (`SLA_TYPE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`notification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`notification` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SOLUTION_ID` INT(11) NOT NULL ,
  `NOTIFICATION_AUTHORIZATION` VARCHAR(45) NOT NULL ,
  `NOTIFICATION_TYPE` VARCHAR(45) NOT NULL ,
  `CREATION_DATE` DATETIME NULL DEFAULT NULL ,
  `MODIFICATION_DATE` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `FK_NOTIFICATION_SOLUTION_ID_idx` (`SOLUTION_ID` ASC)  ,
  CONSTRAINT `FK_NOTIFICATION_SOLUTION_ID`
    FOREIGN KEY (`SOLUTION_ID`)
    REFERENCES `ccdmdb`.`solution` (`SOLUTION_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`operating_systems`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`operating_systems` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NAME` VARCHAR(100) NOT NULL ,
  `TYPE` VARCHAR(100) NOT NULL ,
  `OS_VERSION` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`web_server_providers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`web_server_providers` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NAME` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`virtual_machines`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`virtual_machines` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `MODEL` VARCHAR(100) NOT NULL ,
  `V_CPU` INT(11) NOT NULL ,
  `MEMORY` INT(11) NOT NULL ,
  `STORAGE` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`provider_platform_reference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`provider_platform_reference` (
  `ID` INT(11) NOT NULL ,
  `OS` INT(11) NULL DEFAULT NULL ,
  `VM_SIZE` INT(11) NULL DEFAULT NULL ,
  `PROVIDER_ID` INT(11) NULL DEFAULT NULL ,
  `COST_PER_HOUR` DOUBLE NULL DEFAULT NULL ,
  `PROVIDER_REFERENCE_1` VARCHAR(100) NULL DEFAULT NULL ,
  `PROVIDER_REFERENCE_2` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `OS_ID_idx` (`OS` ASC)  ,
  INDEX `VM_SIZE_idx` (`VM_SIZE` ASC)  ,
  INDEX `PROVIDER_ID_idx` (`PROVIDER_ID` ASC)  ,
  CONSTRAINT `OS_ID`
    FOREIGN KEY (`OS`)
    REFERENCES `ccdmdb`.`operating_systems` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PROVIDER_ID`
    FOREIGN KEY (`PROVIDER_ID`)
    REFERENCES `ccdmdb`.`web_server_providers` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `VM_SIZE`
    FOREIGN KEY (`VM_SIZE`)
    REFERENCES `ccdmdb`.`virtual_machines` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`user_role` (
  `ROLE_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ROLE_NAME` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ROLE_ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`role_page_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`role_page_map` (
  `ROLE_PAGE_MAP_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ROLE_ID` INT(11) NOT NULL ,
  `PAGE_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`ROLE_PAGE_MAP_ID`)  ,
  INDEX `fk_ROLE_PAGE_MAP_PAGEID_idx` (`PAGE_ID` ASC)  ,
  INDEX `fk_ROLE_PAGE_MAP_ROLEID_idx` (`ROLE_ID` ASC)  ,
  CONSTRAINT `fk_ROLE_PAGE_MAP_PAGEID`
    FOREIGN KEY (`PAGE_ID`)
    REFERENCES `ccdmdb`.`accessable_pages` (`PAGE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ROLE_PAGE_MAP_ROLEID`
    FOREIGN KEY (`ROLE_ID`)
    REFERENCES `ccdmdb`.`user_role` (`ROLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`server_credentials`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`server_credentials` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `USER_ID` INT(11) NOT NULL ,
  `USER` VARCHAR(100) NOT NULL ,
  `PASSWORD` VARCHAR(100) NULL DEFAULT NULL ,
  `PRIVATE_KEY` VARCHAR(45) NULL DEFAULT NULL ,
  `ACCESS_ID` VARCHAR(245) NULL DEFAULT NULL ,
  `ACCESS_KEY` VARCHAR(245) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `fk_credential_userid_appuser_userid_idx` (`USER_ID` ASC)  ,
  CONSTRAINT `fk_credential_userid_appuser_userid`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`view_model`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`view_model` (
  `VM_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NAME` VARCHAR(100) NOT NULL ,
  `OS_ID` INT(11) NOT NULL ,
  `VIRTUAL_MACHINE_ID` INT(11) NOT NULL ,
  `PROVIDER_ID` INT(11) NOT NULL ,
  `USER_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`VM_ID`)  ,
  INDEX `fk_osid_operating_systems_id_idx` (`OS_ID` ASC)  ,
  INDEX `fk_vmid_virtual_machines_id_idx` (`VIRTUAL_MACHINE_ID` ASC)  ,
  INDEX `fk_providerid_web_server_providers_id_idx` (`PROVIDER_ID` ASC)  ,
  INDEX `fk_userid_app_user_userid_idx` (`USER_ID` ASC, `NAME` ASC)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`server_instanace_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`server_instanace_info` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `TAG_NAME` VARCHAR(50) NULL DEFAULT NULL ,
  `INSTANCE_ID` VARCHAR(100) NULL DEFAULT NULL ,
  `INSTANCE_TYPE` VARCHAR(100) NULL DEFAULT NULL ,
  `AVAILABILITY_ZONE` VARCHAR(50) NULL DEFAULT NULL ,
  `INSTANCE_STATE` VARCHAR(45) NULL DEFAULT NULL ,
  `KEY_NAME` VARCHAR(100) NULL DEFAULT NULL ,
  `LAUNCH_TIME` DATETIME NULL DEFAULT NULL ,
  `SECURITY_GROUP` VARCHAR(100) NULL DEFAULT NULL ,
  `TEMPLATE_ID` INT(11) NULL DEFAULT NULL ,
  `PUBLIC_DNS` VARCHAR(100) NOT NULL ,
  `PORT` INT(11) NULL DEFAULT '0' ,
  PRIMARY KEY (`ID`)  ,
  INDEX `solution_id_idx` (`TEMPLATE_ID` ASC)  ,
  CONSTRAINT `template_id`
    FOREIGN KEY (`ID`)
    REFERENCES `ccdmdb`.`view_model` (`VM_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`solution_vm_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`solution_vm_map` (
  `SOLUTION_VM_MAP_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SOLUTION_ID` INT(11) NOT NULL ,
  `VM_ID` INT(11) NOT NULL ,
  `SCRIPT_ID` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`SOLUTION_VM_MAP_ID`)  ,
  INDEX `FK_SOL_VM_MAP_SOL_ID_idx` (`SOLUTION_ID` ASC)  ,
  INDEX `FK_VM_MAP_VM_MODEL_idx` (`VM_ID` ASC)  ,
  CONSTRAINT `FK_VM_MAP_VM_MODEL`
    FOREIGN KEY (`VM_ID`)
    REFERENCES `ccdmdb`.`view_model` (`VM_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOL_VM_MAP_SOL_ID`
    FOREIGN KEY (`SOLUTION_ID`)
    REFERENCES `ccdmdb`.`solution` (`SOLUTION_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`user_role_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`user_role_map` (
  `USER_ROLE_MAP_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `USER_ID` INT(11) NOT NULL ,
  `ROLE_ID` INT(11) NOT NULL ,
  `IS_ENABLED` TINYINT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`USER_ROLE_MAP_ID`)  ,
  INDEX `fk_USER_ROLE_MAP_ROLEID_idx` (`ROLE_ID` ASC)  ,
  INDEX `fk_USER_ROLE_MAP_USER_ID_idx` (`USER_ID` ASC)  ,
  CONSTRAINT `fk_USER_ROLE_MAP_ROLEID`
    FOREIGN KEY (`ROLE_ID`)
    REFERENCES `ccdmdb`.`user_role` (`ROLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USER_ROLE_MAP_USER_ID`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
