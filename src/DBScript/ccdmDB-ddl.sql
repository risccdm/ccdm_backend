-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ccdmdb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ccdmdb` DEFAULT CHARACTER SET latin1 ;
USE `ccdmdb` ;

-- -----------------------------------------------------
-- Table `ccdmdb`.`accessable_pages`
-- -----------------------------------------------------


-- -----------------------------------------------------
-- Table `ccdmdb`.`app_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`app_user` (
  `USER_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `USER_NAME` VARCHAR(45) NOT NULL ,
  `PASSWORD` VARCHAR(100) NOT NULL ,
  `USER_EMAIL` VARCHAR(45) NOT NULL ,
  `USER_PHONE` VARCHAR(45) NULL DEFAULT NULL ,
  `USER_TYPE` VARCHAR(45) NULL DEFAULT NULL ,
  `CREATION_DATE` DATETIME NULL DEFAULT NULL ,
  `MODIFIED_DATE` DATETIME NULL DEFAULT NULL ,
  `CLIENT_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`USER_ID`)  ,
  UNIQUE INDEX `USER_NAME_UNIQUE` (`USER_NAME` ASC)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`web_server_provider`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`web_server_provider` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NAME` VARCHAR(100) NOT NULL ,
  `ALIAS_NAME` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`cost_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`cost_details` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PROVIDER_ID` INT(11) NOT NULL ,
  `INSTANCE_TYPE` VARCHAR(45) NOT NULL ,
  `COST_TYPE` VARCHAR(45) NOT NULL ,
  `ACCESSORIES_REF_ID` INT(11) NULL DEFAULT NULL ,
  `COST_VALUE` DOUBLE NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `FK_COST_DETAIL_PROVIDER_ID_idx` (`PROVIDER_ID` ASC)  ,
  CONSTRAINT `FK_COST_DETAIL_PROVIDER_ID`
    FOREIGN KEY (`PROVIDER_ID`)
    REFERENCES `ccdmdb`.`web_server_provider` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`customer_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`customer_info` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `CUSTOMER_NAME` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`server_credential`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`server_credential` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `USER_ID` INT(11) NOT NULL ,
  `USER` VARCHAR(100) NOT NULL ,
  `PASSWORD` VARCHAR(100) NULL DEFAULT NULL ,
  `PEM_KEY` VARCHAR(45) NULL DEFAULT NULL ,
  `ACCESS_ID` VARCHAR(245) NULL DEFAULT NULL ,
  `ACCESS_KEY` VARCHAR(245) NULL DEFAULT NULL ,
  `PROVIDER_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `fk_credential_userid_appuser_userid_idx` (`USER_ID` ASC)  ,
  INDEX `fk_providerId_Provider_idx` (`PROVIDER_ID` ASC)  ,
  CONSTRAINT `fk_server_credential_provider`
    FOREIGN KEY (`PROVIDER_ID`)
    REFERENCES `ccdmdb`.`web_server_provider` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_credential_userid_appuser_userid`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`operating_system`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`operating_system` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NAME` VARCHAR(100) NOT NULL ,
  `TYPE` VARCHAR(100) NOT NULL ,
  `OS_VERSION` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`virtual_machine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`virtual_machine` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `MODEL` VARCHAR(100) NOT NULL ,
  `V_CPU` INT(11) NOT NULL ,
  `MEMORY` INT(11) NOT NULL ,
  `STORAGE` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`provider_platform_reference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`provider_platform_reference` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `OS` INT(11) NULL DEFAULT NULL ,
  `VM_SIZE` INT(11) NULL DEFAULT NULL ,
  `PROVIDER_ID` INT(11) NULL DEFAULT NULL ,
  `SERVER_CREDENTIAL_ID` INT(11) NOT NULL ,
  `MACHINE_TYPE` VARCHAR(45) NULL DEFAULT NULL ,
  `IMAGE_ID` VARCHAR(45) NULL DEFAULT NULL ,
  `IMAGE_PROJECT` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `OS_ID_idx` (`OS` ASC)  ,
  INDEX `VM_SIZE_idx` (`VM_SIZE` ASC)  ,
  INDEX `PROVIDER_ID_idx` (`PROVIDER_ID` ASC)  ,
  INDEX `FK_SER_CRED_ID_idx` (`SERVER_CREDENTIAL_ID` ASC)  ,
  CONSTRAINT `FK_SERVER_CREDENTIAL_ID`
    FOREIGN KEY (`SERVER_CREDENTIAL_ID`)
    REFERENCES `ccdmdb`.`server_credential` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `OS_ID`
    FOREIGN KEY (`OS`)
    REFERENCES `ccdmdb`.`operating_system` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PROVIDER_ID`
    FOREIGN KEY (`PROVIDER_ID`)
    REFERENCES `ccdmdb`.`web_server_provider` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `VM_SIZE`
    FOREIGN KEY (`VM_SIZE`)
    REFERENCES `ccdmdb`.`virtual_machine` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`notification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`notification` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NOTIFICATION_AUTHORIZATION` VARCHAR(45) NOT NULL ,
  `NOTIFICATION_TYPE` VARCHAR(45) NOT NULL ,
  `CREATION_DATE` DATETIME NOT NULL ,
  `MODIFICATION_DATE` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`sla_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`sla_type` (
  `SLA_TYPE_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SLA_TYPE_NAME` VARCHAR(45) NOT NULL ,
  `USER_ID` INT(11) NOT NULL ,
  `CLIENT_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`SLA_TYPE_ID`)  ,
  INDEX `FK_SLA_USERID_idx` (`USER_ID` ASC)  ,
  CONSTRAINT `FK_SLA_USERID`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`solution`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`solution` (
  `SOLUTION_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SOLUTION_NAME` VARCHAR(45) NOT NULL ,
  `DESCRIPTION` VARCHAR(500) NULL DEFAULT NULL ,
  `CREATION_DATE` DATETIME NOT NULL ,
  `MODIFICATION_DATE` DATETIME NULL DEFAULT NULL ,
  `SLA_TYPE_ID` INT(11) NULL DEFAULT NULL ,
  `NOTIFICATION_ID` INT(11) NULL DEFAULT NULL ,
  `USER_ID` INT(11) NOT NULL ,
  `CLIENT_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`SOLUTION_ID`)  ,
  INDEX `FK_SOLUTON_SLATYPE_SLATYPEID_idx` (`SLA_TYPE_ID` ASC)  ,
  INDEX `FK_SOLUTION_NOTIFICATION_ID_idx` (`NOTIFICATION_ID` ASC)  ,
  INDEX `FK_SOLUTION_USER_ID_idx` (`USER_ID` ASC)  ,
  CONSTRAINT `FK_SOLUTION_NOTIFICATION_ID`
    FOREIGN KEY (`NOTIFICATION_ID`)
    REFERENCES `ccdmdb`.`notification` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOLUTION_USER_ID`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOLUTON_SLATYPE_SLATYPEID`
    FOREIGN KEY (`SLA_TYPE_ID`)
    REFERENCES `ccdmdb`.`sla_type` (`SLA_TYPE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`view_model`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`view_model` (
  `VM_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `NAME` VARCHAR(100) NOT NULL ,
  `OS_ID` INT(11) NOT NULL ,
  `VIRTUAL_MACHINE_ID` INT(11) NOT NULL ,
  `USER_ID` INT(11) NOT NULL ,
  `CLIENT_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`VM_ID`)  ,
  INDEX `fk_osid_operating_systems_id_idx` (`OS_ID` ASC)  ,
  INDEX `fk_vmid_virtual_machines_id_idx` (`VIRTUAL_MACHINE_ID` ASC)  ,
  INDEX `fk_userid_app_user_userid_idx` (`USER_ID` ASC, `NAME` ASC)  ,
  CONSTRAINT `FK_USERID_USER`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_VMID_OSID`
    FOREIGN KEY (`OS_ID`)
    REFERENCES `ccdmdb`.`operating_system` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_VMID_VIRMACHID`
    FOREIGN KEY (`VIRTUAL_MACHINE_ID`)
    REFERENCES `ccdmdb`.`virtual_machine` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`solution_vm_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`solution_vm_map` (
  `SOLUTION_VM_MAP_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SOLUTION_ID` INT(11) NOT NULL ,
  `VM_ID` INT(11) NOT NULL ,
  `SCRIPT_ID` VARCHAR(100) NULL DEFAULT NULL ,
  `IS_DELETED` TINYINT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`SOLUTION_VM_MAP_ID`)  ,
  INDEX `FK_SOL_VM_MAP_SOL_ID_idx` (`SOLUTION_ID` ASC)  ,
  INDEX `FK_VM_MAP_VM_MODEL_idx` (`VM_ID` ASC)  ,
  CONSTRAINT `FK_SOL_VM_MAP_SOL_ID`
    FOREIGN KEY (`SOLUTION_ID`)
    REFERENCES `ccdmdb`.`solution` (`SOLUTION_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_VM_MAP_VM_MODEL`
    FOREIGN KEY (`VM_ID`)
    REFERENCES `ccdmdb`.`view_model` (`VM_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`solution_client_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`solution_client_map` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `SOLUTION_ID` INT(11) NULL DEFAULT NULL ,
  `CUSTOMER_ID` INT(11) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `FK_SOLUTIN_ID_MAP_idx` (`SOLUTION_ID` ASC)  ,
  INDEX `FK_CUSTINFO_ID_idx` (`CUSTOMER_ID` ASC)  ,
  CONSTRAINT `FK_CUSTINFO_ID`
    FOREIGN KEY (`CUSTOMER_ID`)
    REFERENCES `ccdmdb`.`customer_info` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOLUTIN_ID_MAP`
    FOREIGN KEY (`SOLUTION_ID`)
    REFERENCES `ccdmdb`.`solution` (`SOLUTION_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`instance_info`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`instance_info` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `TAG_NAME` VARCHAR(50) NULL DEFAULT NULL ,
  `INSTANCE_ID` VARCHAR(100) NULL DEFAULT NULL ,
  `INSTANCE_TYPE` VARCHAR(100) NULL DEFAULT NULL ,
  `AVAILABILITY_ZONE` VARCHAR(50) NULL DEFAULT NULL ,
  `INSTANCE_STATE` VARCHAR(45) NULL DEFAULT NULL ,
  `KEY_NAME` VARCHAR(100) NULL DEFAULT NULL ,
  `LAUNCH_TIME` DATETIME NULL DEFAULT NULL ,
  `SECURITY_GROUP` VARCHAR(100) NULL DEFAULT NULL ,
  `PUBLIC_DNS` VARCHAR(100) NOT NULL ,
  `PORT` INT(11) NULL DEFAULT '0' ,
  `PUBLIC_IPv4` VARCHAR(100) NULL DEFAULT NULL ,
  `SOLUTION_CIENT_MAP_ID` INT(11) NULL DEFAULT NULL ,
  `SOLUTION_VM_MAP_ID` INT(11) NULL DEFAULT NULL ,
  `PROVIDER_PLATFORM_REF_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `FK_SOLUTION_CLIENT_MAP_idx` (`SOLUTION_CIENT_MAP_ID` ASC)  ,
  INDEX `FK_SOLUION_VM_MAP_idx` (`SOLUTION_VM_MAP_ID` ASC)  ,
  INDEX `FK_PLATFORM_PROV_REF_ID_MAP_idx` (`PROVIDER_PLATFORM_REF_ID` ASC)  ,
  CONSTRAINT `FK_PLATFORM_PROV_REF_ID_MAP`
    FOREIGN KEY (`PROVIDER_PLATFORM_REF_ID`)
    REFERENCES `ccdmdb`.`provider_platform_reference` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOLUION_VM_MAP`
    FOREIGN KEY (`SOLUTION_VM_MAP_ID`)
    REFERENCES `ccdmdb`.`solution_vm_map` (`SOLUTION_VM_MAP_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_SOLUTION_CLIENT_MAP`
    FOREIGN KEY (`SOLUTION_CIENT_MAP_ID`)
    REFERENCES `ccdmdb`.`solution_client_map` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`instance_cost_reference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`instance_cost_reference` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `COST_DETAIL_ID` INT(11) NOT NULL ,
  `INSTANCE_INFO_ID` INT(11) NOT NULL ,
  `CREATION_DATE` DATETIME NOT NULL ,
  PRIMARY KEY (`ID`)  ,
  INDEX `FK_INSTANCE_COST_REF_COST_DETAILS_ID_idx` (`COST_DETAIL_ID` ASC)  ,
  INDEX `FK_INSTANCE_REF_INFO_ID_idx` (`INSTANCE_INFO_ID` ASC)  ,
  CONSTRAINT `FK_INSTANCE_COST_REF_COST_DETAILS_ID`
    FOREIGN KEY (`COST_DETAIL_ID`)
    REFERENCES `ccdmdb`.`cost_details` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_INSTANCE_REF_INFO_ID`
    FOREIGN KEY (`INSTANCE_INFO_ID`)
    REFERENCES `ccdmdb`.`instance_info` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`notification_aspect_reference`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`notification_aspect_reference` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `METHOD_NAME` VARCHAR(500) NOT NULL ,
  `NOTIFICATION_AUTHORIZATION` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`user_role` (
  `ROLE_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ROLE_NAME` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`ROLE_ID`)  )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`role_page_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`role_page_map` (
  `ROLE_PAGE_MAP_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `ROLE_ID` INT(11) NOT NULL ,
  `PAGE_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`ROLE_PAGE_MAP_ID`)  ,
  INDEX `fk_ROLE_PAGE_MAP_PAGEID_idx` (`PAGE_ID` ASC)  ,
  INDEX `fk_ROLE_PAGE_MAP_ROLEID_idx` (`ROLE_ID` ASC)  ,
  CONSTRAINT `fk_ROLE_PAGE_MAP_PAGEID`
    FOREIGN KEY (`PAGE_ID`)
    REFERENCES `ccdmdb`.`accessable_pages` (`PAGE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ROLE_PAGE_MAP_ROLEID`
    FOREIGN KEY (`ROLE_ID`)
    REFERENCES `ccdmdb`.`user_role` (`ROLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`sla_detail`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`sla_detail` (
  `SLA_DETAIL_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `PRIORITY_TYPE` VARCHAR(45) NULL DEFAULT NULL ,
  `RESPONSE_TIME` INT(11) NULL DEFAULT NULL ,
  `UPTIME` INT(11) NULL DEFAULT NULL ,
  `WORKING_HOURS` INT(11) NULL DEFAULT NULL ,
  `CREDITS` INT(11) NULL DEFAULT NULL ,
  `SLA_TYPE` INT(11) NOT NULL ,
  PRIMARY KEY (`SLA_DETAIL_ID`)  ,
  INDEX `FK_SLA_DETAIL_SLA_TYPE_idx` (`SLA_TYPE` ASC)  ,
  CONSTRAINT `FK_SLA_DETAIL_SLA_TYPE`
    FOREIGN KEY (`SLA_TYPE`)
    REFERENCES `ccdmdb`.`sla_type` (`SLA_TYPE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `ccdmdb`.`user_role_map`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `ccdmdb`.`user_role_map` (
  `USER_ROLE_MAP_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `USER_ID` INT(11) NOT NULL ,
  `ROLE_ID` INT(11) NOT NULL ,
  `IS_ENABLED` TINYINT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`USER_ROLE_MAP_ID`)  ,
  INDEX `fk_USER_ROLE_MAP_ROLEID_idx` (`ROLE_ID` ASC)  ,
  INDEX `fk_USER_ROLE_MAP_USER_ID_idx` (`USER_ID` ASC)  ,
  CONSTRAINT `fk_USER_ROLE_MAP_ROLEID`
    FOREIGN KEY (`ROLE_ID`)
    REFERENCES `ccdmdb`.`user_role` (`ROLE_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_USER_ROLE_MAP_USER_ID`
    FOREIGN KEY (`USER_ID`)
    REFERENCES `ccdmdb`.`app_user` (`USER_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
