INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('OVER_ALL_COST_CALCULATION', '/report/instance/cost');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'OVER_ALL_COST_CALCULATION');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

INSERT INTO `ccdmdb`.`accessable_pages` (`PAGE_NAME`, `PAGE_VALUE`) VALUES ('COST_CALCULATION_PER_INSTANCE', '/report/instance/cost/detail');
SET @PAGE_ID = (SELECT PAGE_ID FROM ccdmdb.accessable_pages where PAGE_NAME = 'COST_CALCULATION_PER_INSTANCE');
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('1', @PAGE_ID);
INSERT INTO `ccdmdb`.`role_page_map` (`ROLE_ID`, `PAGE_ID`) VALUES ('2', @PAGE_ID);

INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('1', 't2.nano', 'BASIC', '100');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('1', 't2.micro', 'BASIC', '200');

UPDATE `ccdmdb`.`provider_platform_reference` SET `PROVIDER_REFERENCE_1`='t2.nano', `PROVIDER_REFERENCE_2`='ami-1e299d7e' WHERE `ID`='1';
UPDATE `ccdmdb`.`provider_platform_reference` SET `PROVIDER_REFERENCE_2`='ami-b7a114d7' WHERE `ID`='2';
UPDATE `ccdmdb`.`provider_platform_reference` SET `PROVIDER_REFERENCE_2`='ami-b7a114d7' WHERE `ID`='3';

UPDATE `ccdmdb`.`provider_platform_reference` SET `OS`='6', `VM_SIZE`='3', `PROVIDER_REFERENCE_1`='t2.micro' WHERE `ID`='3';
UPDATE `ccdmdb`.`provider_platform_reference` SET `OS`='2' WHERE `ID`='1';
UPDATE `ccdmdb`.`provider_platform_reference` SET `OS`='4' WHERE `ID`='2';

ALTER TABLE `ccdmdb`.`solution` DROP COLUMN `SOLUTION_STATE`;

-- -------------- Insert Query for Google Provider platform VM Creation
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `PROVIDER_REFERENCE_1`, `PROVIDER_REFERENCE_2`) VALUES ('1', '1', '2', 'n1-standard-1', 'ubuntu-1204-lts');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `PROVIDER_REFERENCE_1`, `PROVIDER_REFERENCE_2`) VALUES ('3', '2', '2', 'n1-standard-2', 'ubuntu-1404-lts');
INSERT INTO `ccdmdb`.`provider_platform_reference` (`OS`, `VM_SIZE`, `PROVIDER_ID`, `PROVIDER_REFERENCE_1`, `PROVIDER_REFERENCE_2`) VALUES ('5', '3', '2', 'n1-standard-4', 'ubuntu-1604-lts');

-- -------------- Insert Query for Google Provider Cost details
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('2', 'n1-standard-1', 'BASIC', '100');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('2', 'n1-standard-2', 'BASIC', '200');
INSERT INTO `ccdmdb`.`cost_details` (`PROVIDER_ID`, `INSTANCE_TYPE`, `COST_TYPE`, `COST_VALUE`) VALUES ('2', 'n1-standard-4', 'BASIC', '300');


UPDATE `ccdmdb`.`web_server_provider` SET `NAME`='Google Web Server' WHERE `ID`='2';

ALTER TABLE `ccdmdb`.`instance_info` 
CHANGE COLUMN `PUBLIC_IPv4` `PUBLIC_IPv4` VARCHAR(45) NULL DEFAULT NULL COMMENT '' ;

ALTER TABLE `ccdmdb`.`instance_info` 
CHANGE COLUMN `PUBLIC_IPv4` `PUBLIC_IPv4` VARCHAR(100) NULL DEFAULT NULL COMMENT '' ;

ALTER TABLE `ccdmdb`.`provider_platform_reference` 
ADD COLUMN `PROVIDER_REFERENCE_3` VARCHAR(100) NULL DEFAULT NULL COMMENT '' AFTER `PROVIDER_REFERENCE_2`;

ALTER TABLE `ccdmdb`.`server_credential` 
ADD COLUMN `PROVIDER_ID` INT(11) NULL AFTER `ACCESS_KEY`;

ALTER TABLE `ccdmdb`.`server_credential` 
ADD CONSTRAINT `fk_server_credential_provider`
  FOREIGN KEY (`PROVIDER_ID`)
  REFERENCES `ccdmdb`.`web_server_provider` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
  
  
  ALTER TABLE `ccdmdb`.`web_server_provider` 
ADD COLUMN `ALIAS_NAME` VARCHAR(45) NOT NULL AFTER `NAME`;

UPDATE `ccdmdb`.`web_server_provider` SET `ALIAS_NAME`='amazon' WHERE `ID`='1';
UPDATE `ccdmdb`.`web_server_provider` SET `ALIAS_NAME`='google' WHERE `ID`='2';

  
ALTER TABLE `ccdmdb`.`instance_info` 
ADD COLUMN `PROVIDER_ID` INT(11) NOT NULL AFTER `SOLUTION_VM_MAP_ID`;

ALTER TABLE `ccdmdb`.`instance_info` 
ADD CONSTRAINT `FK_INSTANCE_PROVIDER_ID`
  FOREIGN KEY (`PROVIDER_ID`)
  REFERENCES `ccdmdb`.`web_server_provider` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;


UPDATE `ccdmdb`.`operating_system` SET `NAME`='Ubuntu', `TYPE`='64-bit', `OS_VERSION`='14.04' WHERE `ID`='1';
UPDATE `ccdmdb`.`operating_system` SET `NAME`='Ubuntu', `OS_VERSION`='16.04' WHERE `ID`='2';
UPDATE `ccdmdb`.`operating_system` SET `NAME`='Centos', `TYPE`='64-bit', `OS_VERSION`='6.0' WHERE `ID`='3';
UPDATE `ccdmdb`.`operating_system` SET `NAME`='Centos', `OS_VERSION`='7.0' WHERE `ID`='4';
DELETE FROM `ccdmdb`.`operating_system` WHERE `ID`='5';
DELETE FROM `ccdmdb`.`operating_system` WHERE `ID`='6';
DELETE FROM `ccdmdb`.`operating_system` WHERE `ID`='7';
DELETE FROM `ccdmdb`.`operating_system` WHERE `ID`='8';

UPDATE `ccdmdb`.`virtual_machine` SET `MEMORY`='2', `STORAGE`='8GB' WHERE `ID`='1';
UPDATE `ccdmdb`.`virtual_machine` SET `V_CPU`='2', `MEMORY`='4', `STORAGE`='16GB' WHERE `ID`='2';
UPDATE `ccdmdb`.`virtual_machine` SET `V_CPU`='2', `STORAGE`='32GB' WHERE `ID`='3';

ALTER TABLE `ccdmdb`.`solution` 
DROP FOREIGN KEY `FK_PROVIDER_ID_MAP`;
ALTER TABLE `ccdmdb`.`solution` 
DROP COLUMN `PROVIDER_ID`,
DROP INDEX `FK_PROVIDER_ID_MAP_idx` ;

INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (1,1,1,1,'t2.small','ami-30e65350'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (1,2,1,1,'t2.medium','ami-30e65350'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (1,3,1,1,'t2.large','ami-30e65350'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (2,1,1,1,'t2.small','ami-b7a114d7'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (2,2,1,1,'t2.medium','ami-b7a114d7'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (2,3,1,1,'t2.large','ami-b7a114d7'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (3,1,1,2,'t2.small','ami-05cf2265'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (3,2,1,2,'t2.medium','ami-05cf2265'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (3,3,1,2,'t2.large','ami-05cf2265'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (4,1,1,2,'t2.small','ami-147ad974'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (4,2,1,2,'t2.medium','ami-147ad974'),
INSERT INTO `provider_platform_reference` (`OS`,`VM_SIZE`,`PROVIDER_ID`,`SERVER_CREDENTIAL_ID`,`PROVIDER_REFERENCE_1`,`PROVIDER_REFERENCE_2`) VALUES (4,3,1,2,'t2.large','ami-147ad974');

INSERT INTO `ccdmdb`.`server_credential` (`USER_ID`, `USER`, `PEM_KEY`, `ACCESS_ID`, `ACCESS_KEY`, `PROVIDER_ID`) VALUES ('1', 'ubuntu', 'aws.pem', 'AKIAINUEC2UTC4EHAI5A', 'xmoEWz6r4DGUFQ/oo1Kv+srrdYutbz2TiXeAi9wC', '1');
INSERT INTO `ccdmdb`.`server_credential` (`USER_ID`, `USER`, `PEM_KEY`, `ACCESS_ID`, `ACCESS_KEY`, `PROVIDER_ID`) VALUES ('1', 'root', 'aws.pem', 'AKIAINUEC2UTC4EHAI5A', 'xmoEWz6r4DGUFQ/oo1Kv+srrdYutbz2TiXeAi9wC', '1');
INSERT INTO `ccdmdb`.`server_credential` (`USER_ID`, `USER`, `PEM_KEY`, `ACCESS_ID`, `ACCESS_KEY`, `PROVIDER_ID`) VALUES ('1', 'riscsdm', 'gce.pem', '', '', '2');
UPDATE `ccdmdb`.`server_credential` SET `PRIVATE_KEY`='aws.pem' WHERE `ID`='1';

ALTER TABLE `ccdmdb`.`provider_platform_reference` 
DROP COLUMN `PROVIDER_REFERENCE_3`;

ALTER TABLE `ccdmdb`.`provider_platform_reference` 
ADD COLUMN `SERVER_CREDENTIAL_ID` INT(11) NOT NULL  AFTER `PROVIDER_ID`,

UPDATE `ccdmdb`.`provider_platform_reference` SET `SERVER_CREDENTIAL_ID`='1' WHERE `ID`>='1';

ALTER TABLE `ccdmdb`.`provider_platform_reference` 
ADD CONSTRAINT `FK_SERVER_CREDENTIAL_ID`
  FOREIGN KEY (`SERVER_CREDENTIAL_ID`)
  REFERENCES `ccdmdb`.`server_credential` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  ALTER TABLE `ccdmdb`.`instance_info` 
DROP FOREIGN KEY `FK_INSTANCE_PROVIDER_ID`;
ALTER TABLE `ccdmdb`.`instance_info` 
DROP COLUMN `PROVIDER_ID`,
DROP INDEX `FK_INSTANCE_PROVIDER_ID_idx` ;

ALTER TABLE `ccdmdb`.`instance_info` 
ADD COLUMN `PROVIDER_PLATFORM_REF_ID` INT(11) NOT NULL AFTER `SOLUTION_VM_MAP_ID`;

ALTER TABLE `ccdmdb`.`instance_info` 
ADD INDEX `FK_PLATFORM_PROV_REF_ID_MAP_idx` (`PROVIDER_PLATFORM_REF_ID` ASC) ;
ALTER TABLE `ccdmdb`.`instance_info` 
ADD CONSTRAINT `FK_PLATFORM_PROV_REF_ID_MAP`
  FOREIGN KEY (`PROVIDER_PLATFORM_REF_ID`)
  REFERENCES `ccdmdb`.`provider_platform_reference` (`ID`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

  
